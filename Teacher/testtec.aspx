﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="testtec.aspx.cs" Inherits="Teacher_testtec" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%" style="text-align: left; font-weight: bold;" >
    <tr>
        <td colspan="3">
            <h1>
                Tests</h1>
        </td>
    </tr>
    <tr>
        <td style="width: 165px">
            Test Name</td>
        <td style="width: 211px">
            <asp:TextBox ID="TextBox1" runat="server" Width="308px" CssClass="field" 
                ValidationGroup="a"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="TextBox1" ErrorMessage="*" ValidationGroup="a"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td style="width: 165px">
            &nbsp;</td>
        <td colspan="2">
            <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                Text="Register Test" ValidationGroup="a" />
&nbsp;&nbsp;
            </td>
    </tr>
    <tr>
        <td colspan="3">
            <br />
            <asp:Panel ID="p1" ScrollBars="Auto" Height="200px" width="100%"  runat="server">
            <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                DataKeyNames="teccod" DataSourceID="SqlDataSource1" 
                EmptyDataText="There are no data records to display." CellPadding="4" 
                ForeColor="#333333" GridLines="None" Width="100%">
                <AlternatingRowStyle BackColor="White" />
                <Columns>                   
                    <asp:BoundField DataField="tecnam" HeaderText="Test" 
                        SortExpression="tecnam" />                   
                    <asp:CheckBoxField DataField="pubsts" HeaderText="Publish Status" 
                        SortExpression="pubsts" />
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:cn %>" 
                DeleteCommand="DELETE FROM [tbtec] WHERE [teccod] = @teccod" 
                InsertCommand="INSERT INTO [tbtec] ([tecnam], [pubsts]) VALUES (@tecnam, @pubsts)" 
                ProviderName="<%$ ConnectionStrings:cn.ProviderName %>" 
                SelectCommand="SELECT [teccod], [tecnam], [pubsts] FROM [tbtec] WHERE ([TecteacherID] = @TecteacherID)" 
                
                
                UpdateCommand="UPDATE [tbtec] SET [tecnam] = @tecnam, [pubsts] = @pubsts WHERE [teccod] = @teccod">
                <DeleteParameters>
                    <asp:Parameter Name="teccod" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="tecnam" Type="String" />
                    <asp:Parameter Name="pubsts" Type="Boolean" />
                </InsertParameters>
                <SelectParameters>
                    <asp:SessionParameter Name="TecteacherID" SessionField="stid" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="tecnam" Type="String" />
                    <asp:Parameter Name="pubsts" Type="Boolean" />
                    <asp:Parameter Name="teccod" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            </asp:Panel>
            <br />
        </td>
    </tr>
</table>
</asp:Content>
