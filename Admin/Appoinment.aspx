﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/InnerAdmin.master" AutoEventWireup="true" CodeFile="Appoinment.aspx.cs" Inherits="Admin_Appoinment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%" style="text-align: left; font-weight: bold;">
    <tr>
    <td></td>
    <td ><asp:Label ID="ll" runat="server" Font-Bold="True" 
            Font-Size="X-Large" ForeColor="#003399">Appointment Schedule</asp:Label></td>
    <td></td>
    </tr>
    <tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr><tr><td></td><td></td><td></td></tr>
    <tr>
    <td style="text-align: left">
     Appointment Title : 
    </td>
    <td align=left>
        <asp:TextBox ID="txtTitle" runat="server"  size="30" Width="300px"></asp:TextBox>
    </td>
    </tr>
    
    <tr>
    <td align=left>
        Appointment Description : 
    </td>
    <td align=left>
        <asp:TextBox ID="txtText" runat="server"  TextMode="MultiLine" Rows="5" 
            Columns ="30" Width="300px"></asp:TextBox>
    </td>
    </tr>
    
    <tr>
    <td align=left>
        Designation</td>
    <td align=left>
        <asp:DropDownList ID="DropDownList1" runat="server" Width="200px">
            <asp:ListItem Value="1">Faculty</asp:ListItem>
            <asp:ListItem Value="2">Student</asp:ListItem>
            <asp:ListItem Value="3">Anonymous</asp:ListItem>
        </asp:DropDownList>
    </td>
    </tr>
    
    <tr>
    <td align=left>
        Name &amp; Department</td>
    <td align=left>
        <asp:TextBox ID="txtname" runat="server" Width="200px"></asp:TextBox>
    </td>
    </tr>
    
    <tr>
    <td align=left>
     Appointment Date :  
    </td>
    <td align=left>
        <asp:TextBox ID="txtDate" runat="server"  size="10" Width="200px"></asp:TextBox> 
       <asp:MaskedEditExtender ID="txtdate_MaskedEditExtender" runat="server"  Enabled="True" 
                    MaskType="DateTime" TargetControlID="txtDate" AcceptAMPM="True" 
                    Mask="99/99/9999 99:99"></asp:MaskedEditExtender>
                <asp:MaskedEditValidator ID="MaskedEditValidator1" runat="server" 
                    ControlExtender="txtdate_MaskedEditExtender" ControlToValidate="txtDate" 
                    Display="Dynamic" ForeColor="Red" InvalidValueBlurredMessage="*" 
                    MaximumValueBlurredMessage="*" MinimumValueBlurredText="*"></asp:MaskedEditValidator>
        (mm-dd-yy hh:mm)
    </td>
    </tr>
    
    <tr>
    <td align=left>
        &nbsp;</td>
    <td align=left>
        <asp:Label ID="lbldt" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
    </td>
    </tr>
    
    <tr>
    <td align=left>
        Rough Estimation&nbsp; :  
    </td>
    <td align=left>
        <asp:TextBox ID="txtNHB" runat="server"  size="10" Width="200px"></asp:TextBox> 
    </td>
    </tr>
    <tr>
    <td></td>
    <td><p />
    <asp:Button ID="btnAdd" runat="server" Text="Add Appointment" 
            onclick="btnAdd_Click" />
    <p /></td>
    <td></td>
    </tr>
    </table>
    
    
    <asp:Panel ID="p1" runat="server" ScrollBars="Auto " >
        <asp:GridView ID="GridShowSummary" runat="server" 
                    AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" 
                    GridLines="None" DataKeyNames="appid" 
            onrowediting="GridShowSummary_RowEditing" Width="100%">
                
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                    <asp:TemplateField HeaderText="Date &amp; Time">
                            <ItemTemplate>
                                <asp:Label ID="LblDate" runat="server" Text='<%# Eval("appdate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Title">
                            <ItemTemplate>
                                <asp:Label ID="Lbltitle" runat="server" Text='<%# Eval("apptitle") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- <asp:TemplateField HeaderText="Time">
                             <ItemTemplate>
                                 <asp:Label ID="Lbltime" runat="server"></asp:Label>
                             </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:CommandField ShowEditButton="True" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView></asp:Panel>
</asp:Content>

