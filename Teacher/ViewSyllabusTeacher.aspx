﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="ViewSyllabusTeacher.aspx.cs" Inherits="Teacher_ViewSyllabusTeacher" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table class="style1">
        <tr>
            <td class="style2">
                    &nbsp;</td>
            <td>
                    &nbsp;</td>
        </tr>
        <tr>
            <td class="style2">
                    <strong>Select Course</strong></td>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="CourseSyl" 
                        DataTextField="coursename" DataValueField="courseid" 
                    AutoPostBack="True" 
                    onselectedindexchanged="DropDownList1_SelectedIndexChanged1">
                </asp:DropDownList>
                <asp:SqlDataSource ID="CourseSyl" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:cn %>" 
                        SelectCommand="SELECT [courseid], [coursename] FROM [tbcourse] WHERE ([coursedelsts] = @coursedelsts)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="false" Name="coursedelsts" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style4" style="height: 18px">
            </td>
            <td class="style3" style="height: 18px">
            </td>
        </tr>
        <tr>
            <td class="style2" style="height: 18px">
            </td>
            <td style="height: 18px">
            </td>
        </tr>
        <tr>
            <td class="style2">
                    &nbsp;</td>
            <td>
                <asp:DataList ID="DataList1" runat="server" Width="128px">
                    <HeaderTemplate>
                        <table>
                            <tr  >
                                <th >
                                    Department</th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr style="background-color:transparent">
                            <td>
                                <asp:LinkButton ID="LinkButton1" runat="server" Text='<%#Eval("depsyl") %>'></asp:LinkButton>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <SeparatorTemplate><hr></SeparatorTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>
</asp:Content>

