﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="StudentSessionalrpt.aspx.cs" Inherits="Teacher_StudentSessional" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      <table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td>
                Course</td>
            <td>
                <asp:DropDownList ID="DDLCourse" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLCourse_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Department</td>
            <td>
                <asp:DropDownList ID="DDLDept" runat="server">
                    <asp:ListItem>-- Select Department --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Session</td>
            <td>
                <asp:DropDownList ID="DDLSession" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLSession_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Class</td>
            <td>
                <asp:DropDownList ID="DDLClass" runat="server" AutoPostBack="True" 
                    Enabled="False" onselectedindexchanged="DDLClass_SelectedIndexChanged">
                    <asp:ListItem>-- Select Class --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Subject</td>
            <td>
                <asp:DropDownList ID="DDLSubject" runat="server" Enabled="False">
                    <asp:ListItem>-- Choose Subject --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
          <tr>
              <td>
                  &nbsp;</td>
              <td>
                  <asp:Button ID="Button1" runat="server" style="font-weight: bold" Text="View" />
              </td>
              <td>
                  &nbsp;</td>
          </tr>
        <tr>
            <td colspan="2">
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
                    Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
                    WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%">
                    <LocalReport ReportPath="Teacher\st_sessional.rdlc">
                        <DataSources>
                            <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="DataSet1" />
                        </DataSources>
                    </LocalReport>
                </rsweb:ReportViewer>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:cn %>" 
                    SelectCommand="DispSessionalStdnt" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLCourse" Name="stid" 
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLSession" Name="sid" 
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td> 
                &nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td>
            </td>
        </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

