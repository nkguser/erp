﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace kitmerp
{
    public abstract class sqlconnect
    {       
           protected static SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);       
    }
    /*---------------------------------------------------------------------*/
    public interface intCategory
    {
        Int32 p_catid
        {
            get;
            set;
        }

        String p_catname
        {
            get;
            set;
        }

        Boolean p_catdelsts
        {
            get;
            set;
        }
    }
    public class clsCatprp : intCategory
    {
        private Int32 catid;
        private String catname;
        private Boolean catdelsts;
        public int p_catid
        {
            get
            {
                return catid;
            }
            set
            {
                catid = value;
            }
        }

        public string p_catname
        {
            get
            {
                return catname;
            }
            set
            {
                catname = value;
            }
        }

        public bool p_catdelsts
        {
            get
            {
                return catdelsts;
            }
            set
            {
                catdelsts = value;
            }
        }
    }
    public class clsCat : sqlconnect
    {
        public void Save_Rec(clsCatprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("inscat", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@catname", SqlDbType.VarChar, 20).Value = p.p_catname;
            cmd.Parameters.Add("@catdelsts", SqlDbType.Bit).Value = p.p_catdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsCatprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updcat", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = p.p_catid;
            cmd.Parameters.Add("@cname", SqlDbType.VarChar, 20).Value = p.p_catname;
            cmd.Parameters.Add("@cdelsts", SqlDbType.Bit).Value = p.p_catdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsCatprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delcat", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = p.p_catid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsCatprp> Display_rec(Int32 cid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspcat", con);
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsCatprp> obj = new List<clsCatprp>();
            while (dr.Read())
            {
                clsCatprp k = new clsCatprp();
                k.p_catid = Convert.ToInt32(dr[0]);
                k.p_catname = dr[1].ToString();
                k.p_catdelsts = Convert.ToBoolean(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
       
        public List<clsCatprp> Find_rec(Int32 cid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndcat", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsCatprp> obj = new List<clsCatprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsCatprp k = new clsCatprp();
                k.p_catid = Convert.ToInt32(dr[0]);
                k.p_catname = dr[1].ToString();
                k.p_catdelsts = Convert.ToBoolean(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intClass
    {
        Int32 p_classid
        {
            get;
            set;
        }
        Int32 p_depid
        {
            get;
            set;
        }
        String p_classname
        {
            get;
            set;
        }
        Int32 p_classSessionid
        {
            get;
            set;
        }
        Boolean p_classdelsts
        {
            get;
            set;
        }
    }
    public class clsClassprp : intClass
    {

        private Int32 classid, depid, classsessionid;
        private String classname;
        private Boolean classdelsts;
        public int p_classid
        {
            get
            {
                return classid;
            }
            set
            {
                classid= value;
            }
        }

        public int p_depid
        {
            get
            {
                return depid;
            }
            set
            {
                depid= value;
            }
        }

        public string p_classname
        {
            get
            {
                return classname;
            }
            set
            {
                classname = value;
            }
        }

        public int p_classSessionid
        {
            get
            {
                return classsessionid;
            }
            set
            {
                classsessionid = value;
            }
        }


        public bool p_classdelsts
        {
            get
            {
                return classdelsts;
            }
            set
            {
                classdelsts = value;
            }
        }
    }
    public class clsClass : sqlconnect
    {
        public void Save_Rec(clsClassprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insclass", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@cname", SqlDbType.VarChar, 50).Value = p.p_classname;
            cmd.Parameters.Add("@did", SqlDbType.Int).Value = p.p_depid;
            cmd.Parameters.Add("@csid", SqlDbType.Int).Value = p.p_classSessionid;
            cmd.Parameters.Add("@cdelsts", SqlDbType.Bit).Value = p.p_classdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsClassprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updclass", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = p.p_classid;
            cmd.Parameters.Add("@cname", SqlDbType.VarChar, 50).Value = p.p_classname;
            cmd.Parameters.Add("@did", SqlDbType.Int).Value = p.p_depid;
            cmd.Parameters.Add("@csid", SqlDbType.Int).Value = p.p_classSessionid;
            cmd.Parameters.Add("@cdelsts", SqlDbType.Bit).Value = p.p_classdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsClassprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delclass", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = p.p_classid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsClassprp> Display_rec(Int32 cid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspclass", con);
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsClassprp> obj = new List<clsClassprp>();
            while (dr.Read())
            {
                clsClassprp k = new clsClassprp();
                k.p_classid = Convert.ToInt32(dr[0]);
                k.p_classname = dr[1].ToString();
                k.p_depid = Convert.ToInt32(dr[2]);
                k.p_classSessionid = Convert.ToInt32(dr[3]);
                k.p_classdelsts = Convert.ToBoolean(dr[4]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsClassprp> Find_rec(Int32 cid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndclass", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsClassprp> obj = new List<clsClassprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsClassprp k = new clsClassprp();
                k.p_classid = Convert.ToInt32(dr[0]);
                k.p_classname = dr[1].ToString();
                k.p_depid = Convert.ToInt32(dr[2]);
                k.p_classSessionid = Convert.ToInt32(dr[3]);
                k.p_classdelsts = Convert.ToBoolean(dr[4]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intCourse
    {
        Int32 p_courseid
        {
            get;
            set;
        }
        String p_coursename
        {
            get;
            set;
        }
        Int32 p_coursesem
        {
            get;
            set;
        }
        Boolean p_coursedelsts
        {
            get;
            set;
        }
    }
    public class clsCourseprp : intCourse
    {
        private Int32 courseid, coursesem;
        private String coursename;
        private Boolean coursedelsts;
        public int p_courseid
        {
            get
            {
                return courseid;
            }
            set
            {
                courseid = value;
            }
        }

        public string p_coursename
        {
            get
            {
                return coursename;
            }
            set
            {
                coursename = value;
            }
        }

        public int p_coursesem
        {
            get
            {
                return coursesem;
            }
            set
            {
                coursesem = value;
            }
        }

        public bool p_coursedelsts
        {
            get
            {
                return coursedelsts;
            }
            set
            {
                coursedelsts = value;
            }
        }
    }
    public class clsCourse : sqlconnect
    {
        public void Save_Rec(clsCourseprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("inscourse", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@cname", SqlDbType.VarChar, 50).Value = p.p_coursename;
            cmd.Parameters.Add("@csem", SqlDbType.Int).Value = p.p_coursesem;
            cmd.Parameters.Add("@cdelsts", SqlDbType.Bit).Value = p.p_coursedelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsCourseprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updcourse", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = p.p_courseid;
            cmd.Parameters.Add("@cname", SqlDbType.VarChar, 50).Value = p.p_coursename;
            cmd.Parameters.Add("@csem", SqlDbType.Int).Value = p.p_coursesem;
            cmd.Parameters.Add("@cdelsts", SqlDbType.Bit).Value = p.p_coursedelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsCourseprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delcourse", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = p.p_courseid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsCourseprp> Display_rec(Int32 cid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspcourse", con);
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsCourseprp> obj = new List<clsCourseprp>();
            while (dr.Read())
            {
                clsCourseprp k = new clsCourseprp();
                k.p_courseid = Convert.ToInt32(dr[0]);
                k.p_coursename = dr[1].ToString();
                k.p_coursesem = Convert.ToInt32(dr[2]);
                k.p_coursedelsts = Convert.ToBoolean(dr[3]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsCourseprp> Find_rec(Int32 cid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndcourse", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsCourseprp> obj = new List<clsCourseprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsCourseprp k = new clsCourseprp();
                k.p_courseid = Convert.ToInt32(dr[0]);
                k.p_coursename = dr[1].ToString();
                k.p_coursesem = Convert.ToInt32(dr[2]);
                k.p_coursedelsts = Convert.ToBoolean(dr[3]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intDepartment
    {
        Int32 p_depid
        {
            get;
            set;
        }
        String p_depname
        {
            get;
            set;
        }
        Int32 p_depcourseid
        {
            get;
            set;
        }
        String p_depsyllabus
        {
            get;
            set;
        }
        Boolean p_depdelsts
        {
            get;
            set;
        }
    }
    public class clsDepartmentprp : intDepartment
    {
        private Int32 depid, depcourseid;
        private String depname, depsyllabus;
        private Boolean depdelsts;
        public int p_depid
        {
            get
            {
                return depid;
            }
            set
            {
                depid = value;
            }
        }

        public string p_depname
        {
            get
            {
                return depname;
            }
            set
            {
                depname = value;
            }
        }

        public int p_depcourseid
        {
            get
            {
                return depcourseid;
            }
            set
            {
                depcourseid = value;
            }
        }

        public string p_depsyllabus
        {
           get
            {
                return depsyllabus;
            }
            set
            {
                depsyllabus = value;
            }
        }

        public bool p_depdelsts
        {
            get
            {
                return depdelsts;
            }
            set
            {
                depdelsts = value;
            }
        }
    }
    public class clsDepartment : sqlconnect
    {
        public void Save_Rec(clsDepartmentprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insdepartment", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@dname", SqlDbType.VarChar, 50).Value = p.p_depname;
            cmd.Parameters.Add("@dcid", SqlDbType.Int).Value = p.p_depcourseid;
            cmd.Parameters.Add("@dsyl", SqlDbType.VarChar, 50).Value = p.p_depsyllabus;
            cmd.Parameters.Add("@cdelsts", SqlDbType.Bit).Value = p.p_depdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsDepartmentprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("upddepartment", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@did", SqlDbType.Int).Value = p.p_depid;
            cmd.Parameters.Add("@dname", SqlDbType.VarChar, 50).Value = p.p_depname;
            cmd.Parameters.Add("@dcid", SqlDbType.Int).Value = p.p_depcourseid;
            cmd.Parameters.Add("@dsyl", SqlDbType.VarChar, 50).Value = p.p_depsyllabus;
            cmd.Parameters.Add("@cdelsts", SqlDbType.Bit).Value = p.p_depdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsDepartmentprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("deldepartment", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@did", SqlDbType.Int).Value = p.p_depid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsDepartmentprp> Display_rec(Int32 did)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspdepartment", con);
            cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsDepartmentprp> obj = new List<clsDepartmentprp>();
            while (dr.Read())
            {
                clsDepartmentprp k = new clsDepartmentprp();
                k.p_depid = Convert.ToInt32(dr[0]);
                k.p_depname = dr[1].ToString();
                k.p_depcourseid = Convert.ToInt32(dr[2]);
                k.p_depsyllabus = dr[3].ToString();
                k.p_depdelsts = Convert.ToBoolean(dr[4]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsDepartmentprp> Find_rec(Int32 did)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fnddepartment", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsDepartmentprp> obj = new List<clsDepartmentprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsDepartmentprp k = new clsDepartmentprp();
                k.p_depid = Convert.ToInt32(dr[0]);
                k.p_depname = dr[1].ToString();
                k.p_depcourseid = Convert.ToInt32(dr[2]);
                k.p_depsyllabus = dr[3].ToString();
                k.p_depdelsts = Convert.ToBoolean(dr[4]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intFeedback
    {
        Int32 p_feedbackid
        {
            get;
            set;
        }

        String p_feedbackheader
        {
            get;
            set;
        }

        String p_feedbackcontent
        {
            get;
            set;
        }
        DateTime p_feedbackdate
        {
            get;
            set;
        }
        Int32 p_feedbacklevelid
        {
            get;
            set;
        }
        Int32 p_feedbacktolevelid
        {
            get;
            set;
        }
        String p_feedbackfromid
        {
            get;
            set;
        }
        Boolean p_feedbackdelsts
        {
            get;
            set;
        }
    }
    public class clsFeedbackprp : intFeedback
    {
        private Int32 feedbackid,feedbacklevelid,feedbacktolevelid;
        private string feedbackheader, feedbackcontent,feedbackfromid;
        private DateTime feedbackdate;
        private Boolean feedbacldelsts;

        public int p_feedbackid
        {
            get
            {
                return feedbackid;
            }
            set
            {
                feedbackid = value;
            }
        }

        public string p_feedbackheader
        {
            get
            {
                return feedbackheader;
            }
            set
            {
                feedbackheader = value;
            }
        }

        public string p_feedbackcontent
        {
            get
            {
                return feedbackcontent;
            }
            set
            {
               feedbackcontent = value;
            }
        }

        public DateTime p_feedbackdate
        {
            get
            {
                return feedbackdate;
            }
            set
            {
                feedbackdate = value;
            }
        }

        public int p_feedbacklevelid
        {
            get
            {
                return feedbacklevelid;
            }
            set
            {
                feedbacklevelid = value;
            }
        }

        public int p_feedbacktolevelid
        {
            get
            {
                return feedbacktolevelid;
            }
            set
            {
                feedbacktolevelid = value;
            }
        }

        public string p_feedbackfromid
        {
            get
            {
                return feedbackfromid;
            }
            set
            {
                feedbackfromid = value;
            }
        }

        public bool p_feedbackdelsts
        {
            get
            {
                return feedbacldelsts;
            }
            set
            {
                feedbacldelsts = value;
            }
        }
    }
    public class clsFeedback : sqlconnect
    {
        public void Save_Rec(clsFeedbackprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insfeedback", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@fheader", SqlDbType.VarChar, 100).Value = p.p_feedbackheader;
            cmd.Parameters.Add("@fcontent", SqlDbType.NText).Value = p.p_feedbackcontent;
            cmd.Parameters.Add("@fdate", SqlDbType.Date).Value = p.p_feedbackdate;
            cmd.Parameters.Add("@flid", SqlDbType.Int).Value = p.p_feedbacklevelid;
            cmd.Parameters.Add("@ftolid", SqlDbType.Int).Value = p.p_feedbacktolevelid;
            cmd.Parameters.Add("@ffromid", SqlDbType.VarChar, 50).Value = p.p_feedbackfromid;
            cmd.Parameters.Add("@fdelsts", SqlDbType.Bit).Value = p.p_feedbackdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsFeedbackprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updfeedback", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@fid", SqlDbType.Int).Value = p.p_feedbackid;
            cmd.Parameters.Add("@fheader", SqlDbType.VarChar, 100).Value = p.p_feedbackheader;
            cmd.Parameters.Add("@fcontent", SqlDbType.NText).Value = p.p_feedbackcontent;
            cmd.Parameters.Add("@fdate", SqlDbType.Date).Value = p.p_feedbackdate;
            cmd.Parameters.Add("@flid", SqlDbType.Int).Value = p.p_feedbacklevelid;
            cmd.Parameters.Add("@ftolid", SqlDbType.Int).Value = p.p_feedbacktolevelid;
            cmd.Parameters.Add("@ffromid", SqlDbType.VarChar, 50).Value = p.p_feedbackfromid;
            cmd.Parameters.Add("@fdelsts", SqlDbType.Bit).Value = p.p_feedbackdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsFeedbackprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delfeedback", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@fid", SqlDbType.Int).Value = p.p_feedbackid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsFeedbackprp> Display_rec(Int32 fid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspfeedback", con);
            cmd.Parameters.Add("@fid", SqlDbType.Int).Value = fid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsFeedbackprp> obj = new List<clsFeedbackprp>();
            while (dr.Read())
            {
                clsFeedbackprp k = new clsFeedbackprp();
                k.p_feedbackid = Convert.ToInt32(dr[0]);
                k.p_feedbackheader = dr[1].ToString();
                k.p_feedbackcontent = dr[2].ToString();
                k.p_feedbackdate = Convert.ToDateTime(dr[3]);
                k.p_feedbacklevelid = Convert.ToInt32(dr[4]);
                k.p_feedbacktolevelid = Convert.ToInt32(dr[5]);
                k.p_feedbackfromid = dr[6].ToString();
                k.p_feedbackdelsts = Convert.ToBoolean(dr[7]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsFeedbackprp> Find_rec(Int32 fid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndfeedback", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@fid", SqlDbType.Int).Value = fid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsFeedbackprp> obj = new List<clsFeedbackprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsFeedbackprp k = new clsFeedbackprp();
                k.p_feedbackid = Convert.ToInt32(dr[0]);
                k.p_feedbackheader = dr[1].ToString();
                k.p_feedbackcontent = dr[2].ToString();
                k.p_feedbackdate = Convert.ToDateTime(dr[3]);
                k.p_feedbacklevelid = Convert.ToInt32(dr[4]);
                k.p_feedbacktolevelid = Convert.ToInt32(dr[5]);
                k.p_feedbackfromid = dr[6].ToString();
                k.p_feedbackdelsts = Convert.ToBoolean(dr[7]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intGroup
    {
        Int32 p_groupid
        {
            get;
            set;
        }
        String p_groupname
        {
            get;
            set;
        }
        Int32 p_groupclassid
        {
            get;
            set;
        }
        Boolean p_groupdelsts
        {
            get;
            set;
        }
    }
    public class clsGroupprp : intGroup
    {
        private Int32 groupid, groupclassid;
        private String groupname;
        private Boolean groupdelsts;

        public Int32 p_groupid
        {
            get
            {
                return groupid;
            }
            set
            {
                groupid = value;
            }
        }
        public String p_groupname
        {
            get
            {
                return groupname;
            }
            set
            {
                groupname = value;
            }
        }
        public Int32 p_groupclassid
        {
            get
            {
                return groupclassid;
            }
            set
            {
                groupclassid = value;
            }
        }
        public bool p_groupdelsts
        {
            get
            {
                return groupdelsts;
            }
            set
            {
                groupdelsts= value;
            }
        }
    }
    public class clsGroup : sqlconnect
    {
        public void Save_Rec(clsGroupprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insgroup", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@gname", SqlDbType.VarChar, 50).Value = p.p_groupname;
            cmd.Parameters.Add("@gcid", SqlDbType.Int).Value = p.p_groupclassid;
            cmd.Parameters.Add("@cdelsts", SqlDbType.Bit).Value = p.p_groupdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsGroupprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updgroup", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = p.p_groupid;
            cmd.Parameters.Add("@gname", SqlDbType.VarChar, 50).Value = p.p_groupname;
            cmd.Parameters.Add("@gcid", SqlDbType.Int).Value = p.p_groupclassid;
            cmd.Parameters.Add("@cdelsts", SqlDbType.Bit).Value = p.p_groupdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsGroupprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delgroup", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = p.p_groupid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsGroupprp> Display_rec(Int32 gid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspgroup", con);
            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsGroupprp> obj = new List<clsGroupprp>();
            while (dr.Read())
            {
                clsGroupprp k = new clsGroupprp();
                k.p_groupid = Convert.ToInt32(dr[0]);
                k.p_groupname = dr[1].ToString();
                k.p_groupclassid = Convert.ToInt32(dr[2]);
                k.p_groupdelsts = Convert.ToBoolean(dr[3]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsGroupprp> Find_rec(Int32 gid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndgroup", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsGroupprp> obj = new List<clsGroupprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsGroupprp k = new clsGroupprp();
                k.p_groupid = Convert.ToInt32(dr[0]);
                k.p_groupname = dr[1].ToString();
                k.p_groupclassid = Convert.ToInt32(dr[2]);
                k.p_groupdelsts = Convert.ToBoolean(dr[3]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intLevel
    {
        Int32 p_levelid
        {
            get;
            set;
        }
        String p_leveldesc
        {
            get;
            set;
        }
        Boolean p_leveldelsts
        {
            get;
            set;
        }
    }
    public class clsLevelprp : intLevel
    {
        private Int32 levelid;
        private String leveldesc;
        private Boolean leveldelsts;
        public int p_levelid
        {
            get
            {
                return levelid;
            }
            set
            {
                levelid = value;
            }
        }

        public string p_leveldesc
        {
            get
            {
                return leveldesc;
            }
            set
            {
                leveldesc = value;
            }
        }

        public bool p_leveldelsts
        {
            get
            {
                return leveldelsts;
            }
            set
            {
                leveldelsts = value;
            }
        }
    }
    public class clsLevel : sqlconnect
    {
        public void Save_Rec(clsLevelprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("inslevel", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ldesc", SqlDbType.VarChar, 50).Value = p.p_leveldesc;
            cmd.Parameters.Add("@ldelsts", SqlDbType.Bit).Value = p.p_leveldelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsLevelprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updlevel", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@lid", SqlDbType.Int).Value = p.p_levelid;
            cmd.Parameters.Add("@ldesc", SqlDbType.VarChar, 50).Value = p.p_leveldesc;
            cmd.Parameters.Add("@ldelsts", SqlDbType.Bit).Value = p.p_leveldelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsLevelprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dellevel", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@lid", SqlDbType.Int).Value = p.p_levelid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsLevelprp> Display_rec(Int32 lid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dsplevel", con);
            cmd.Parameters.Add("@lid", SqlDbType.Int).Value = lid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsLevelprp> obj = new List<clsLevelprp>();
            while (dr.Read())
            {
                clsLevelprp k = new clsLevelprp();
                k.p_levelid = Convert.ToInt32(dr[0]);
                k.p_leveldesc = dr[1].ToString();
                k.p_leveldelsts = Convert.ToBoolean(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsLevelprp> Find_rec(Int32 lid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndlevel", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@lid", SqlDbType.Int).Value = lid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsLevelprp> obj = new List<clsLevelprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsLevelprp k = new clsLevelprp();
                k.p_levelid = Convert.ToInt32(dr[0]);
                k.p_leveldesc = dr[1].ToString();
                k.p_leveldelsts = Convert.ToBoolean(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intNotice
    {
        Int32 p_noticeid
        {
            get;
            set;
        }

        String p_noticesubject
        {
            get;
            set;
        }

        String p_noticecontent
        {
            get;
            set;
        }
        DateTime p_noticedate
        {
            get;
            set;
        }
        Int32 p_noticelevelid
        {
            get;
            set;
        }
        Int32 p_noticetolevelid
        {
            get;
            set;
        }
        Int32 p_noticefromid
        {
            get;
            set;
        }
        Boolean p_noticedelsts
        {
            get;
            set;
        }
    }
    public class clsNoticeprp : intNotice
    {
        private Int32 noticeid, noticelevelid, noticetolevelid,noticefromid;
        private string noticesubject, noticecontent ;
        private DateTime noticedate;
        private Boolean noticedelsts;
        public int p_noticeid
        {
            get
            {
                return noticeid;
            }
            set
            {
                noticeid = value;
            }
        }

        public string p_noticesubject
        {
            get
            {
                return noticesubject;
            }
            set
            {
                noticesubject = value;
            }
        }

        public string p_noticecontent
        {
            get
            {
                return noticecontent;
            }
            set
            {
                noticecontent = value;
            }
        }

        public DateTime p_noticedate
        {
            get
            {
                return noticedate;
            }
            set
            {
                noticedate = value;
            }
        }

        public int p_noticelevelid
        {
            get
            {
                return noticelevelid;
            }
            set
            {
                noticelevelid = value;
            }
        }

        public int p_noticetolevelid
        {
            get
            {
                return noticetolevelid;
            }
            set
            {
                noticetolevelid = value;
            }
        }

        public int p_noticefromid
        {
            get
            {
                return noticefromid;
            }
            set
            {
                noticefromid = value;
            }
        }

        public bool p_noticedelsts
        {
            get
            {
                return noticedelsts;
            }
            set
            {
                noticedelsts = value;
            }
        }
    }
    public class clsNotice : sqlconnect
    {
        public void Save_Rec(clsNoticeprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insnotice", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ndate", SqlDbType.Date).Value = p.p_noticedate;
            cmd.Parameters.Add("@nsubject", SqlDbType.VarChar, 100).Value = p.p_noticesubject;
            cmd.Parameters.Add("@ncontent", SqlDbType.NText).Value = p.p_noticecontent;
            cmd.Parameters.Add("@nlid", SqlDbType.Int).Value = p.p_noticelevelid;
            cmd.Parameters.Add("@ntolid", SqlDbType.Int).Value = p.p_noticetolevelid;
            cmd.Parameters.Add("@nfromid", SqlDbType.Int).Value = p.p_noticefromid;
            cmd.Parameters.Add("@ndelsts", SqlDbType.Bit).Value = p.p_noticedelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsNoticeprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updnotice", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@nid", SqlDbType.Int).Value = p.p_noticeid;
            cmd.Parameters.Add("@ndate", SqlDbType.Date).Value = p.p_noticedate;
            cmd.Parameters.Add("@nsubject", SqlDbType.VarChar, 100).Value = p.p_noticesubject;
            cmd.Parameters.Add("@ncontent", SqlDbType.NText).Value = p.p_noticecontent;
            cmd.Parameters.Add("@nlid", SqlDbType.Int).Value = p.p_noticelevelid;
            cmd.Parameters.Add("@ntolid", SqlDbType.Int).Value = p.p_noticetolevelid;
            cmd.Parameters.Add("@nfromid", SqlDbType.Int).Value = p.p_noticefromid;
            cmd.Parameters.Add("@ndelsts", SqlDbType.Bit).Value = p.p_noticedelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsNoticeprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delnotice", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@nid", SqlDbType.Int).Value = p.p_noticeid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsNoticeprp> Display_rec(Int32 nid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspnotice", con);
            cmd.Parameters.Add("@nid", SqlDbType.Int).Value = nid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsNoticeprp> obj = new List<clsNoticeprp>();
            while (dr.Read())
            {
                clsNoticeprp k = new clsNoticeprp();
                k.p_noticeid = Convert.ToInt32(dr[0]);
                k.p_noticedate = Convert.ToDateTime(dr[1]);
                k.p_noticesubject = dr[2].ToString();
                k.p_noticecontent = dr[3].ToString();
                k.p_noticelevelid = Convert.ToInt32(dr[4]);
                k.p_noticetolevelid = Convert.ToInt32(dr[5]);
                k.p_noticefromid = Convert.ToInt32(dr[6]);
                k.p_noticedelsts = Convert.ToBoolean(dr[7]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsNoticeprp> Find_rec(Int32 nid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndnotice", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@nid", SqlDbType.Int).Value = nid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsNoticeprp> obj = new List<clsNoticeprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsNoticeprp k = new clsNoticeprp();
                k.p_noticeid = Convert.ToInt32(dr[0]);
                k.p_noticedate = Convert.ToDateTime(dr[1]);
                k.p_noticesubject = dr[2].ToString();
                k.p_noticecontent = dr[3].ToString();
                k.p_noticelevelid = Convert.ToInt32(dr[4]);
                k.p_noticetolevelid = Convert.ToInt32(dr[5]);
                k.p_noticefromid = Convert.ToInt32(dr[6]);
                k.p_noticedelsts = Convert.ToBoolean(dr[7]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intPost
    {
        Int32 p_postid
        {
            get;
            set;
        }
        String p_postdesc
        {
            get;
            set;
        }
        Boolean p_postdelsts
        {
            get;
            set;
        }
    }
    public class clsPostprp : intPost
    {
        private Int32 postid;
        private String postdesc;
        private Boolean postdelsts;
        public int p_postid
        {
            get
            {
                return postid;
            }
            set
            {
                postid = value;
            }
        }

        public string p_postdesc
        {
            get
            {
                return postdesc;
            }
            set
            {
                postdesc = value;
            }
        }

        public bool p_postdelsts
        {
            get
            {
                return postdelsts;
            }
            set
            {
                postdelsts = value;
            }
        }
    }
    public class clsPost : sqlconnect
    {
        public void Save_Rec(clsPostprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("inspost", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@pname", SqlDbType.VarChar, 50).Value = p.p_postdesc;
            cmd.Parameters.Add("@pdelsts", SqlDbType.Bit).Value = p.p_postdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsPostprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updpost", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@pid", SqlDbType.Int).Value = p.p_postid;
            cmd.Parameters.Add("@pname", SqlDbType.VarChar, 50).Value = p.p_postdesc;
            cmd.Parameters.Add("@pdelsts", SqlDbType.Bit).Value = p.p_postdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsPostprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delpost", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@pid", SqlDbType.Int).Value = p.p_postid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsPostprp> Display_rec(Int32 pid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dsppost", con);
            cmd.Parameters.Add("@pid", SqlDbType.Int).Value = pid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsPostprp> obj = new List<clsPostprp>();
            while (dr.Read())
            {
                clsPostprp k = new clsPostprp();
                k.p_postid = Convert.ToInt32(dr[0]);
                k.p_postdesc = dr[1].ToString();
                k.p_postdelsts = Convert.ToBoolean(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsPostprp> Find_rec(Int32 pid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndpost", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@pid", SqlDbType.Int).Value = pid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsPostprp> obj = new List<clsPostprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsPostprp k = new clsPostprp();
                k.p_postid = Convert.ToInt32(dr[0]);
                k.p_postdesc = dr[1].ToString();
                k.p_postdelsts = Convert.ToBoolean(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intReapproval
    {
        Int32 p_reapprovalid
        {
            get;
            set;
        }
        String p_reapprovaldesc
        {
            get;
            set;
        }
        Boolean p_reapprovaldelsts
        {
            get;
            set;
        }
    }
    public class clsReapprovalprp : intReapproval
    {
        private Int32 reapprovalid;
        private String reapprovaldesc;
        private Boolean reapprovaldelsts;
        public int p_reapprovalid
        {
            get
            {
                return reapprovalid;
            }
            set
            {
                reapprovalid = value;
            }
        }

        public string p_reapprovaldesc
        {
            get
            {
                return reapprovaldesc;
            }
            set
            {
                reapprovaldesc = value;
            }
        }

        public bool p_reapprovaldelsts
        {
            get
            {
                return reapprovaldelsts;
            }
            set
            {
                reapprovaldelsts = value;
            }
        }
    }
    public class clsReapproval : sqlconnect
    {
        public void Save_Rec(clsReapprovalprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insreapproval", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@raname", SqlDbType.VarChar, 50).Value = p.p_reapprovaldesc;
            cmd.Parameters.Add("@radelsts", SqlDbType.Bit).Value = p.p_reapprovaldelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsReapprovalprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updreapproval", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@raid", SqlDbType.Int).Value = p.p_reapprovalid;
            cmd.Parameters.Add("@raname", SqlDbType.VarChar, 50).Value = p.p_reapprovaldesc;
            cmd.Parameters.Add("@radelsts", SqlDbType.Bit).Value = p.p_reapprovaldelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsReapprovalprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delreapproval", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@raid", SqlDbType.Int).Value = p.p_reapprovalid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsReapprovalprp> Display_rec(Int32 raid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspreapproval", con);
            cmd.Parameters.Add("@raid", SqlDbType.Int).Value = raid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsReapprovalprp> obj = new List<clsReapprovalprp>();
            while (dr.Read())
            {
                clsReapprovalprp k = new clsReapprovalprp();
                k.p_reapprovalid = Convert.ToInt32(dr[0]);
                k.p_reapprovaldesc = dr[1].ToString();
                k.p_reapprovaldelsts = Convert.ToBoolean(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsReapprovalprp> Find_rec(Int32 raid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndreapproval", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@raid", SqlDbType.Int).Value = raid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsReapprovalprp> obj = new List<clsReapprovalprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsReapprovalprp k = new clsReapprovalprp();
                k.p_reapprovalid = Convert.ToInt32(dr[0]);
                k.p_reapprovaldesc = dr[1].ToString();
                k.p_reapprovaldelsts = Convert.ToBoolean(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intSession
    {
        Int32 p_sessionid
        {
            get;
            set;
        }
        Int32 p_sessionstart
        {
            get;
            set;
        }
        Int32 p_sessionend
        {
            get;
            set;
        }
        Boolean p_sessiondelsts
        {
            get;
            set;
        }
    }
    public class clsSessionprp : intSession
    {
        private Int32 sessionid, sessionstart, sessionend;
        private Boolean sessiondelsts;
        public Int32 p_sessionid
        {
            get
            {
                return sessionid;
            }
            set
            {
                sessionid = value;
            }
        }
        public Int32 p_sessionstart
        {
            get
            {
                return sessionstart;
            }
            set
            {
                sessionstart = value;
            }
        }
        public Int32 p_sessionend
        {
            get
            {
                return sessionend;
            }
            set
            {
                sessionend = value;
            }
        }
        public bool p_sessiondelsts
        {
            get
            {
                return sessiondelsts;
            }
            set
            {
                sessiondelsts = value;
            }
        }
    }
    public class clsSession : sqlconnect
    {
        public void Save_Rec(clsSessionprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("inssession", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sstart", SqlDbType.Int).Value = p.p_sessionstart;
            cmd.Parameters.Add("@send", SqlDbType.Int).Value = p.p_sessionend;
            cmd.Parameters.Add("@sdelsts", SqlDbType.Bit).Value = p.p_sessiondelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsSessionprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updsession", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = p.p_sessionid;
            cmd.Parameters.Add("@sstart", SqlDbType.Int).Value = p.p_sessionstart;
            cmd.Parameters.Add("@send", SqlDbType.Int).Value = p.p_sessionend;
            cmd.Parameters.Add("@sdelsts", SqlDbType.Bit).Value = p.p_sessiondelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsSessionprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delsession", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = p.p_sessionid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsSessionprp> Display_rec(Int32 sid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspsession", con);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsSessionprp> obj = new List<clsSessionprp>();
            while (dr.Read())
            {
                clsSessionprp k = new clsSessionprp();
                k.p_sessionid = Convert.ToInt32(dr[0]);
                k.p_sessionstart = Convert.ToInt32(dr[1]);
                k.p_sessionend = Convert.ToInt32(dr[2]);
                k.p_sessiondelsts = Convert.ToBoolean(dr[3]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsSessionprp> Find_rec(Int32 sid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndsession", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsSessionprp> obj = new List<clsSessionprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsSessionprp k = new clsSessionprp();
                k.p_sessionid = Convert.ToInt32(dr[0]);
                k.p_sessionstart = Convert.ToInt32(dr[1]);
                k.p_sessionend = Convert.ToInt32(dr[2]);
                k.p_sessiondelsts = Convert.ToBoolean(dr[3]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intStudent
    {
        Int32 p_studentid
        {
            get;
            set;
        }
        Int32 p_studentrollno
        {
            get;
            set;
        }
        Int32 p_studentuserid
        {
            get;
            set;
        }
        Int32 p_studentgroupid
        {
            get;
            set;
        }
        String p_studentregno
        {
            get;
            set;
        }
        String p_studentfname
        {
            get;
            set;
        }
        String p_studentmname
        {
            get;
            set;
        }
        String p_studentlname
        {
            get;
            set;
        }
        String p_studentmothername
        {
            get;
            set;
        }
        String p_studentfathername
        {
            get;
            set;
        }
        DateTime p_studentdob
        {
            get;
            set;
        }
        Int32 p_studentcategoryid
        {
            get;
            set;
        }
        String p_studentcorresadd 
        {
            get;
            set;
        }
        String p_studentpermasadd
        {
            get;
            set;
        }
        Int64 p_studentphone1
        {
            get;
            set;
        }
        Int64 p_studentphone2
        {
            get;
            set;
        }
        String p_studentemailid
        {
            get;
            set;
        }
        String p_studentphoto
        {
            get;
            set;
        }
        Int32 p_student10marks
        {
            get;
            set;
        }
        Int32 p_student12marks
        {
            get;
            set;
        }
        String p_studentbloodgroup
        {
            get;
            set;
        }
        String p_studentachievement
        {
            get;
            set;
        }
        String p_studentskill
        {
            get;
            set;
        }
        String p_studenttraining
        {
            get;
            set;
        }
        String p_studenthobbies
        {
            get;
            set;
        }
        String p_studentresumefile
        {
            get;
            set;
        }
        Boolean p_studentishostler
        {
            get;
            set;
        }
        Boolean p_studentdelsts
        {
            get;
            set;
        }
    }
    public class clsStudentprp : intStudent
    {
        private Int32 studentid,studentrollno,studentuserid, studentgroupid,studentcategoryid,  student10marks, student12marks;
        private String studentregno, studentfname, studentmname, studentlname, studentfathername, studentmothername, studentcorresadd, studentpermaadd, studentemailid, studentbloodgroup, studentphoto, studentachievement, studentskill, studenttraining, studenthobbies,studentresumefile;
        private DateTime  studentdob;
        private Int64 studentphone1, studentphone2;
        private Boolean studentishostler,studentdelsts;
        public int p_studentid
        {
            get
            {
                return studentid;
            }
            set
            {
                studentid = value;
            }
        }

        public int p_studentrollno
        {
            get
            {
                return studentrollno;
            }
            set
            {
                studentrollno = value;
            }
        }

        public int p_studentuserid
        {
            get
            {
                return studentuserid;
            }
            set
            {
                studentuserid = value;
            }
        }

        public int p_studentgroupid
        {
            get
            {
                return studentgroupid;
            }
            set
            {
                studentgroupid = value;
            }
        }

        public string p_studentregno
        {
            get
            {
                return studentregno;
            }
            set
            {
                studentregno = value;
            }
        }

        public string p_studentfname
        {
            get
            {
                return studentfname;
            }
            set
            {
                studentfname = value;
            }
        }

        public string p_studentmname
        {
            get
            {
                return studentmname;
            }
            set
            {
                studentmname = value;
            }
        }

        public string p_studentlname
        {
            get
            {
                return studentlname;
            }
            set
            {
                studentlname = value;
            }
        }

        public string p_studentmothername
        {
            get
            {
                return studentmothername;
            }
            set
            {
                studentmothername = value;
            }
        }

        public string p_studentfathername
        {
            get
            {
                return studentfathername;
            }
            set
            {
                studentfathername = value;
            }
        }

        public DateTime p_studentdob
        {
            get
            {
                return studentdob;
            }
            set
            {
                studentdob = value;
            }
        }

        public int p_studentcategoryid
        {
            get
            {
                return studentcategoryid;
            }
            set
            {
                studentcategoryid = value;
            }
        }

        public string p_studentcorresadd
        {
            get
            {
                return studentcorresadd;
            }
            set
            {
                studentcorresadd = value;
            }
        }

        public string p_studentpermasadd
        {
            get
            {
                return studentpermaadd;
            }
            set
            {
                studentpermaadd = value;
            }
        }

        public long p_studentphone1
        {
            get
            {
                return studentphone1;
            }
            set
            {
                studentphone1 = value;
            }
        }

        public long p_studentphone2
        {
            get
            {
                return studentphone2;
            }
            set
            {
                studentphone2 = value;
            }
        }

        public string p_studentemailid
        {
            get
            {
                return studentemailid;
            }
            set
            {
                studentemailid = value;
            }
        }

        public string p_studentphoto
        {
            get
            {
                return studentphoto;
            }
            set
            {
                studentphoto = value;
            }
        }

        public int p_student10marks
        {
            get
            {
                return student10marks;
            }
            set
            {
                student10marks = value;
            }
        }

        public int p_student12marks
        {
            get
            {
                return student12marks;
            }
            set
            {
               student12marks= value;
            }
        }

        public string p_studentbloodgroup
        {
            get
            {
                return studentbloodgroup;
            }
            set
            {
                studentbloodgroup = value;
            }
        }

        public string p_studentachievement
        {
            get
            {
                return studentachievement;
            }
            set
            {
                studentachievement = value;
            }
        }

        public string p_studentskill
        {
            get
            {
                return studentskill;
            }
            set
            {
                studentskill = value;
            }
        }

        public string p_studenttraining
        {
            get
            {
                return studenttraining;
            }
            set
            {
                studenttraining = value;
            }
        }

        public string p_studenthobbies
        {
            get
            {
                return studenthobbies;
            }
            set
            {
                studenthobbies = value;
            }
        }

        public string p_studentresumefile
        {
            get
            {
                return studentresumefile;
            }
            set
            {
                studentresumefile = value;
            }
        }

        public bool p_studentishostler
        {
            get
            {
                return studentishostler;
            }
            set
            {
                studentishostler = value;
            }
        }

        public bool p_studentdelsts
        {
            get
            {
                return studentdelsts;
            }
            set
            {
                studentdelsts = value;
            }
        }
    }
    public class clsStudent : sqlconnect
    {
        public void Save_Rec(clsStudentprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insstudent", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@suid", SqlDbType.Int).Value = p.p_studentuserid;
            cmd.Parameters.Add("@srno", SqlDbType.Int).Value = p.p_studentrollno;
            cmd.Parameters.Add("@sgid", SqlDbType.Int).Value = p.p_studentgroupid;
            cmd.Parameters.Add("@sregno", SqlDbType.VarChar, 50).Value = p.p_studentregno;
            cmd.Parameters.Add("@sfname", SqlDbType.VarChar, 50).Value = p.p_studentfname;
            cmd.Parameters.Add("@smname", SqlDbType.VarChar, 50).Value = p.p_studentmname;
            cmd.Parameters.Add("@slname", SqlDbType.VarChar, 50).Value = p.p_studentlname;
            cmd.Parameters.Add("@smtname", SqlDbType.VarChar, 50).Value = p.p_studentmothername;
            cmd.Parameters.Add("@sftname", SqlDbType.VarChar, 50).Value = p.p_studentfathername;
            cmd.Parameters.Add("@sd1", SqlDbType.DateTime).Value = p.p_studentdob;
            cmd.Parameters.Add("@scid", SqlDbType.Int).Value = p.p_studentcategoryid;
            cmd.Parameters.Add("@scorres", SqlDbType.VarChar, 300).Value = p.p_studentcorresadd;
            cmd.Parameters.Add("@sperma", SqlDbType.VarChar, 300).Value = p.p_studentpermasadd;
            cmd.Parameters.Add("@sp1", SqlDbType.BigInt).Value = p.p_studentphone1;
            cmd.Parameters.Add("@sp2", SqlDbType.BigInt).Value = p.p_studentphone2;
            cmd.Parameters.Add("@semail", SqlDbType.VarChar, 50).Value = p.p_studentemailid;
            cmd.Parameters.Add("@sphoto", SqlDbType.VarChar, 100).Value =p.p_studentphoto;
            cmd.Parameters.Add("@stenth", SqlDbType.Int).Value = p.p_student10marks;
            cmd.Parameters.Add("@stwelth", SqlDbType.Int).Value =p.p_student12marks;
            cmd.Parameters.Add("@sblood", SqlDbType.VarChar, 4).Value = p.p_studentbloodgroup;
            cmd.Parameters.Add("@sachieve", SqlDbType.VarChar, 200).Value = p.p_studentachievement;
            cmd.Parameters.Add("@sskill", SqlDbType.VarChar, 200).Value = p.p_studentskill;
            cmd.Parameters.Add("@straining", SqlDbType.VarChar, 200).Value = p.p_studenttraining;
            cmd.Parameters.Add("@shobbies", SqlDbType.VarChar, 200).Value = p.p_studenthobbies;
            cmd.Parameters.Add("@sresume", SqlDbType.VarChar, 100).Value = p.p_studentresumefile;
            cmd.Parameters.Add("@shostler", SqlDbType.Bit).Value = p.p_studentishostler;
            cmd.Parameters.Add("@sdelsts", SqlDbType.Bit).Value = p.p_studentdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsStudentprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updstudent", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = p.p_studentid;
            cmd.Parameters.Add("@suid", SqlDbType.Int).Value = p.p_studentuserid;
            cmd.Parameters.Add("@srno", SqlDbType.Int).Value = p.p_studentrollno;
            cmd.Parameters.Add("@sgid", SqlDbType.Int).Value = p.p_studentgroupid;
            cmd.Parameters.Add("@sregno", SqlDbType.VarChar, 50).Value = p.p_studentregno;
            cmd.Parameters.Add("@sfname", SqlDbType.VarChar, 50).Value = p.p_studentfname;
            cmd.Parameters.Add("@smname", SqlDbType.VarChar, 50).Value = p.p_studentmname;
            cmd.Parameters.Add("@slname", SqlDbType.VarChar, 50).Value = p.p_studentlname;
            cmd.Parameters.Add("@smtname", SqlDbType.VarChar, 50).Value = p.p_studentmothername;
            cmd.Parameters.Add("@sftname", SqlDbType.VarChar, 50).Value = p.p_studentfathername;
            cmd.Parameters.Add("@sd1", SqlDbType.DateTime).Value = p.p_studentdob;
            cmd.Parameters.Add("@scid", SqlDbType.Int).Value = p.p_studentcategoryid;
            cmd.Parameters.Add("@scorres", SqlDbType.VarChar, 300).Value = p.p_studentcorresadd;
            cmd.Parameters.Add("@sperma", SqlDbType.VarChar, 300).Value = p.p_studentpermasadd;
            cmd.Parameters.Add("@sp1", SqlDbType.BigInt).Value = p.p_studentphone1;
            cmd.Parameters.Add("@sp2", SqlDbType.BigInt).Value = p.p_studentphone2;
            cmd.Parameters.Add("@semail", SqlDbType.VarChar, 50).Value = p.p_studentemailid;
            cmd.Parameters.Add("@sphoto", SqlDbType.VarChar, 100).Value = p.p_studentphoto;
            cmd.Parameters.Add("@stenth", SqlDbType.Int).Value = p.p_student10marks;
            cmd.Parameters.Add("@stwelth", SqlDbType.Int).Value = p.p_student12marks;
            cmd.Parameters.Add("@sblood", SqlDbType.VarChar, 4).Value = p.p_studentbloodgroup;
            cmd.Parameters.Add("@sachieve", SqlDbType.VarChar, 200).Value = p.p_studentachievement;
            cmd.Parameters.Add("@sskill", SqlDbType.VarChar, 200).Value = p.p_studentskill;
            cmd.Parameters.Add("@straining", SqlDbType.VarChar, 200).Value = p.p_studenttraining;
            cmd.Parameters.Add("@shobbies", SqlDbType.VarChar, 200).Value = p.p_studenthobbies;
            cmd.Parameters.Add("@sresume", SqlDbType.VarChar, 100).Value = p.p_studentresumefile;
            cmd.Parameters.Add("@shostler", SqlDbType.Bit).Value = p.p_studentishostler;
            cmd.Parameters.Add("@sdelsts", SqlDbType.Bit).Value = p.p_studentdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsStudentprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delstudent", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = p.p_studentid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsStudentprp> Display_rec(Int32 sid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspstudent", con);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsStudentprp> obj = new List<clsStudentprp>();
            while (dr.Read())
            {
                clsStudentprp k = new clsStudentprp();
                k.p_studentid=Convert.ToInt32(dr[0]);
                k.p_studentuserid = Convert.ToInt32(dr[1]);
                k.p_studentrollno = Convert.ToInt32(dr[2]);
                k.p_studentgroupid = Convert.ToInt32(dr[3]);
                k.p_studentregno = dr[4].ToString();
                k.p_studentfname = dr[5].ToString();
                k.p_studentmname = dr[6].ToString();
                k.p_studentlname = dr[7].ToString();
                k.p_studentmothername = dr[8].ToString();
                k.p_studentfathername = dr[9].ToString();
                k.p_studentdob = Convert.ToDateTime(dr[10]);
                k.p_studentcategoryid = Convert.ToInt32(dr[11]);
                k.p_studentcorresadd = dr[12].ToString();
                k.p_studentpermasadd = dr[13].ToString();
                k.p_studentphone1 = Convert.ToInt64(dr[14]);
                k.p_studentphone2 = Convert.ToInt64(dr[15]);
                k.p_studentemailid = dr[16].ToString();
                k.p_studentphoto = dr[17].ToString();
                k.p_student10marks = Convert.ToInt32(dr[18]);
                k.p_student12marks=Convert.ToInt32(dr[19]);
                k.p_studentbloodgroup= dr[20].ToString();
                k.p_studentachievement= dr[21].ToString();
                k.p_studentskill= dr[22].ToString();
                k.p_studenttraining= dr[23].ToString();
                k.p_studenthobbies= dr[24].ToString();
                k.p_studentresumefile= dr[25].ToString();
                k.p_studentishostler= Convert.ToBoolean(dr[26]);
                k.p_studentdelsts=Convert.ToBoolean(dr[27]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsStudentprp> Find_rec(Int32 sid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndstudent", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsStudentprp> obj = new List<clsStudentprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsStudentprp k = new clsStudentprp();
                k.p_studentid = Convert.ToInt32(dr[0]);
                k.p_studentuserid = Convert.ToInt32(dr[1]);
                k.p_studentrollno = Convert.ToInt32(dr[2]);
                k.p_studentgroupid = Convert.ToInt32(dr[3]);
                k.p_studentregno = dr[4].ToString();
                k.p_studentfname = dr[5].ToString();
                k.p_studentmname = dr[6].ToString();
                k.p_studentlname = dr[7].ToString();
                k.p_studentmothername = dr[8].ToString();
                k.p_studentfathername = dr[9].ToString();
                k.p_studentdob = Convert.ToDateTime(dr[10]);
                k.p_studentcategoryid = Convert.ToInt32(dr[11]);
                k.p_studentcorresadd = dr[12].ToString();
                k.p_studentpermasadd = dr[13].ToString();
                k.p_studentphone1 = Convert.ToInt64(dr[14]);
                k.p_studentphone2 = Convert.ToInt64(dr[15]);
                k.p_studentemailid = dr[16].ToString();
                k.p_studentphoto = dr[17].ToString();
                k.p_student10marks = Convert.ToInt32(dr[18]);
                k.p_student12marks = Convert.ToInt32(dr[19]);
                k.p_studentbloodgroup = dr[20].ToString();
                k.p_studentachievement = dr[21].ToString();
                k.p_studentskill = dr[22].ToString();
                k.p_studenttraining = dr[23].ToString();
                k.p_studenthobbies = dr[24].ToString();
                k.p_studentresumefile = dr[25].ToString();
                k.p_studentishostler = Convert.ToBoolean(dr[26]);
                k.p_studentdelsts = Convert.ToBoolean(dr[27]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intStudentattnd
    {
        Int32 p_studentattndstudentid
        {
            get;
            set;
        }
        Int32 p_studentattndsubjectid
        {
            get;
            set;
        }
        DateTime p_studentattnddate
        {
            get;
            set;
        }
        Boolean p_studentattndattend
        {
            get;
            set;
        }
        Int32 p_studentattndapprovlvid
        {
            get;
            set;
        }
        Boolean p_studentattnddelsts
        {
            get;
            set;
        }
    }
    public class clsStudentattndprp : intStudentattnd
    {
        private Int32 studentattndstudentid,studentattndsubjectid,studentattndapprovlvid ;
        private DateTime studentattnddate;
        private Boolean studentattndattend, studentattnddelsts;
        public int p_studentattndstudentid
        {
            get
            {
                return studentattndstudentid;
            }
            set
            {
               studentattndstudentid = value;
            }
        }

        public int p_studentattndsubjectid
        {
            get
            {
                return studentattndsubjectid;
            }
            set
            {
                studentattndsubjectid = value;
            }
        }

        public DateTime p_studentattnddate
        {
            get
            {
                return studentattnddate;
            }
            set
            {
                 studentattnddate= value;
            }
        }

        public bool p_studentattndattend
        {
            get
            {
                return studentattndattend;
            }
            set
            {
                studentattndattend = value;
            }
        }

        public int p_studentattndapprovlvid
        {
            get
            {
                return studentattndapprovlvid;
            }
            set
            {
                studentattndapprovlvid = value;
            }
        }

        public bool p_studentattnddelsts
        {
            get
            {
                return studentattnddelsts;
            }
            set
            {
                studentattnddelsts = value;
            }
        }
    }
    public class clsStudentattnd : sqlconnect
    {
        public void Save_Rec(clsStudentattndprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insStudentattnd", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sastudentid", SqlDbType.Int).Value = p.p_studentattndstudentid;
            cmd.Parameters.Add("@sasubjectid", SqlDbType.Int).Value = p.p_studentattndsubjectid;
            cmd.Parameters.Add("@sadate", SqlDbType.Date).Value = p.p_studentattnddate;
            cmd.Parameters.Add("@saattend", SqlDbType.Bit).Value = p.p_studentattndattend;
            cmd.Parameters.Add("@saapprovlvid", SqlDbType.Int).Value = p.p_studentattndapprovlvid;
            cmd.Parameters.Add("@sadelsts", SqlDbType.Bit).Value = p.p_studentattnddelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsStudentattndprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updStudentattnd", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sastudentid", SqlDbType.Int).Value = p.p_studentattndstudentid;
            cmd.Parameters.Add("@sasubjectid", SqlDbType.Int).Value = p.p_studentattndsubjectid;
            cmd.Parameters.Add("@sadate", SqlDbType.Date).Value = p.p_studentattnddate;
            cmd.Parameters.Add("@saattend", SqlDbType.Bit).Value = p.p_studentattndattend;
            cmd.Parameters.Add("@saapprovlvid", SqlDbType.Int).Value = p.p_studentattndapprovlvid;
            cmd.Parameters.Add("@sadelsts", SqlDbType.Bit).Value = p.p_studentattnddelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsStudentattndprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delStudentattnd", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@said", SqlDbType.Int).Value = p.p_studentattndstudentid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsStudentattndprp> Display_rec(Int32 said)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspStudentattnd", con);
            cmd.Parameters.Add("@said", SqlDbType.Int).Value = said;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsStudentattndprp> obj = new List<clsStudentattndprp>();
            while (dr.Read())
            {
                clsStudentattndprp k = new clsStudentattndprp();
                k.p_studentattndstudentid = Convert.ToInt32(dr[0]);
                k.p_studentattndsubjectid = Convert.ToInt32(dr[1]);
                k.p_studentattnddate = Convert.ToDateTime(dr[2]);
                k.p_studentattndattend = Convert.ToBoolean(dr[3]);
                k.p_studentattndapprovlvid = Convert.ToInt32(dr[4]);
                k.p_studentattnddelsts = Convert.ToBoolean(dr[5]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsStudentattndprp> Find_rec(Int32 said)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndStudentattnd", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@said", SqlDbType.Int).Value = said;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsStudentattndprp> obj = new List<clsStudentattndprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsStudentattndprp k = new clsStudentattndprp();
                k.p_studentattndstudentid = Convert.ToInt32(dr[0]);
                k.p_studentattndsubjectid = Convert.ToInt32(dr[1]);
                k.p_studentattnddate = Convert.ToDateTime(dr[2]);
                k.p_studentattndattend = Convert.ToBoolean(dr[3]);
                k.p_studentattndapprovlvid = Convert.ToInt32(dr[4]);
                k.p_studentattnddelsts = Convert.ToBoolean(dr[5]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intStudentsessional
    {
        Int32 p_studentsessionalstudentid
        {
            get;
            set;
        }
        Int32 p_studentsessionalsubjectid
        {
            get;
            set;
        }
        Int32 p_studentsessionalno
        {
            get;
            set;
        }
        DateTime p_studentsessionaldate
        {
            get;
            set;
        }
        Boolean p_studentsessionalattnd
        {
            get;
            set;
        }
        Int32 p_studentsessionalmarks           /////////////
        {
            get;
            set;
        }
        Int32 p_studentsessionalmmarks        
        {
            get;
            set;
        }
        Int32 p_studentsessionalreqlvlid
        {
            get;
            set;
        }
        Boolean p_studentsessionaldelsts
        {
            get;
            set;
        }
    }
    public class clsStudentsessionalprp : intStudentsessional
    {
        private Int32 studentsessionalstudentid, studentsessionalsubjectid, studentsessionalreqlvlid,studentsessionalmarks,studentsessionalmmarks,studentsessionalno;
        private DateTime studentsessionaldate;
        private Boolean studentsessionalattnd, studentsessionaldelsts;
        public int p_studentsessionalstudentid
        {
            get
            {
                return studentsessionalstudentid;
            }
            set
            {
                studentsessionalstudentid = value;
            }
        }

        public int p_studentsessionalsubjectid
        {
            get
            {
                return studentsessionalsubjectid;
            }
            set
            {
                studentsessionalsubjectid = value;
            }
        }
        public int p_studentsessionalno
        {
            get
            {
                return studentsessionalno;
            }
            set
            {
                studentsessionalno = value;
            }
        }
        public DateTime p_studentsessionaldate
        {
            get
            {
                return studentsessionaldate;
            }
            set
            {
                studentsessionaldate = value;
            }
        }

        public bool p_studentsessionalattnd
        {
            get
            {
                return studentsessionalattnd;
            }
            set
            {
                studentsessionalattnd = value;
            }
        }
        public int p_studentsessionalmarks
        {
            get
            {
                return studentsessionalmarks;
            }
            set
            {
                studentsessionalmarks = value;
            }
        }

        public int p_studentsessionalmmarks
        {
            get
            {
                return studentsessionalmmarks;
            }
            set
            {
                studentsessionalmmarks = value;
            }
        }
        public int p_studentsessionalreqlvlid
        {
            get
            {
                return studentsessionalreqlvlid;
            }
            set
            {
                studentsessionalreqlvlid = value;
            }
        }

        public bool p_studentsessionaldelsts
        {
            get
            {
                return studentsessionaldelsts;
            }
            set
            {
                studentsessionaldelsts = value;
            }
        }
    }
    public class clsStudentsessional : sqlconnect
    {
        public void Save_Rec(clsStudentsessionalprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insStudentsessional", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ssstudentid", SqlDbType.Int).Value = p.p_studentsessionalstudentid;
            cmd.Parameters.Add("@sssubjectid", SqlDbType.Int).Value = p.p_studentsessionalstudentid;
            cmd.Parameters.Add("@ssno", SqlDbType.Int).Value = p.p_studentsessionalno;
            cmd.Parameters.Add("@ssdate", SqlDbType.Date).Value = p.p_studentsessionaldate;
            cmd.Parameters.Add("@ssattend", SqlDbType.Bit).Value = p.p_studentsessionalattnd;
            cmd.Parameters.Add("@ssmarks", SqlDbType.Real).Value = p.p_studentsessionalmarks;
            cmd.Parameters.Add("@ssmmarks", SqlDbType.Int).Value = p.p_studentsessionalmmarks;
            cmd.Parameters.Add("@ssreqlvlid", SqlDbType.Int).Value = p.p_studentsessionalreqlvlid;
            cmd.Parameters.Add("@ssdelsts", SqlDbType.Bit).Value = p.p_studentsessionaldelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsStudentsessionalprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updStudentsessional", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ssstudentid", SqlDbType.Int).Value = p.p_studentsessionalstudentid;
            cmd.Parameters.Add("@sssubjectid", SqlDbType.Int).Value = p.p_studentsessionalstudentid;
            cmd.Parameters.Add("@ssno", SqlDbType.Int).Value = p.p_studentsessionalno;
            cmd.Parameters.Add("@ssdate", SqlDbType.Date).Value = p.p_studentsessionaldate;
            cmd.Parameters.Add("@ssattend", SqlDbType.Bit).Value = p.p_studentsessionalattnd;
            cmd.Parameters.Add("@ssmarks", SqlDbType.Real).Value = p.p_studentsessionalmarks;
            cmd.Parameters.Add("@ssmmarks", SqlDbType.Int).Value = p.p_studentsessionalmmarks;
            cmd.Parameters.Add("@ssreqlvlid", SqlDbType.Int).Value = p.p_studentsessionalreqlvlid;
            cmd.Parameters.Add("@ssdelsts", SqlDbType.Bit).Value = p.p_studentsessionaldelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsStudentsessionalprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delStudentsessional", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ssid", SqlDbType.Int).Value = p.p_studentsessionalstudentid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsStudentsessionalprp> Display_rec(Int32 ssid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspStudentsessional", con);
            cmd.Parameters.Add("@ssid", SqlDbType.Int).Value = ssid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsStudentsessionalprp> obj = new List<clsStudentsessionalprp>();
            while (dr.Read())
            {
                clsStudentsessionalprp k = new clsStudentsessionalprp();
                k.p_studentsessionalstudentid = Convert.ToInt32(dr[0]);
                k.p_studentsessionalstudentid = Convert.ToInt32(dr[1]);
                k.p_studentsessionalno = Convert.ToInt32(dr[2]);
                k.p_studentsessionaldate = Convert.ToDateTime(dr[3]);
                k.p_studentsessionalattnd = Convert.ToBoolean(dr[4]);
                k.p_studentsessionalmarks=Convert.ToInt32(dr[5]);
                k.p_studentsessionalmmarks=Convert.ToInt32(dr[6]);
                k.p_studentsessionalreqlvlid = Convert.ToInt32(dr[7]);
                k.p_studentsessionaldelsts = Convert.ToBoolean(dr[8]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsStudentsessionalprp> Find_rec(Int32 ssid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndStudentsessional", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ssid", SqlDbType.Int).Value = ssid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsStudentsessionalprp> obj = new List<clsStudentsessionalprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsStudentsessionalprp k = new clsStudentsessionalprp();
                k.p_studentsessionalstudentid = Convert.ToInt32(dr[0]);
                k.p_studentsessionalstudentid = Convert.ToInt32(dr[1]);
                k.p_studentsessionalno = Convert.ToInt32(dr[2]);
                k.p_studentsessionaldate = Convert.ToDateTime(dr[3]);
                k.p_studentsessionalattnd = Convert.ToBoolean(dr[4]);
                k.p_studentsessionalmarks = Convert.ToInt32(dr[5]);
                k.p_studentsessionalmmarks = Convert.ToInt32(dr[6]);
                k.p_studentsessionalreqlvlid = Convert.ToInt32(dr[7]);
                k.p_studentsessionaldelsts = Convert.ToBoolean(dr[8]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intSubject
    {
        Int32 p_subjectid
        {
            get;
            set;
        }
        String p_subjectunicode
        {
            get;
            set;
        }
        Int32 p_subjectsem
        {
            get;
            set;
        }
        String p_subjecttitle
        {
            get;
            set;
        }
        String p_subjectshorttitle
        {
            get;
            set;
        }
        Int32 p_subjectdepid
        {
            get;
            set;
        }
        Int32 p_subjecttypeid
        {
            get;
            set;
        }
        Boolean p_subjectdelsts
        {
            get;
            set;
        }
    }
    public class clsSubjectprp : intSubject
    {
        private Int32 subjectid, subjectsem, subjectdepid, subjecttypeid;
        private String subjectunicode, subjecttitle,subjectshorttitle;
        private Boolean subjectdelsts;
        public int p_subjectid
        {
            get
            {
                return subjectid;
            }
            set
            {
                subjectid = value;
            }
        }

        public string p_subjectunicode
        {
            get
            {
                return subjectunicode;
            }
            set
            {
                subjectunicode = value;
            }
        }

        public int p_subjectsem
        {
            get
            {
                return subjectsem;
            }
            set
            {
                subjectsem = value;
            }
        }

        public string p_subjecttitle
        {
            get
            {
                return subjecttitle;
            }
            set
            {
                subjecttitle = value;
            }
        }
        public string p_subjectshorttitle
        {
            get
            {
                return subjectshorttitle;
            }
            set
            {
                subjectshorttitle = value;
            }
        }
        public int p_subjectdepid
        {
            get
            {
                return subjectdepid;
            }
            set
            {
                subjectdepid = value;
            }
        }

        public int p_subjecttypeid
        {
            get
            {
                return subjecttypeid;
            }
            set
            {
                subjecttypeid = value;
            }
        }

        public bool p_subjectdelsts
        {
            get
            {
                return subjectdelsts;
            }
            set
            {
                subjectdelsts = value;
            }
        }
    }
    public class clsSubject : sqlconnect
    {
        public void Save_Rec(clsSubjectprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("inssubject", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sunicode", SqlDbType.VarChar, 50).Value = p.p_subjectunicode;
            cmd.Parameters.Add("@ssem", SqlDbType.Int).Value = p.p_subjectsem;
            cmd.Parameters.Add("@stitle", SqlDbType.VarChar, 100).Value = p.p_subjecttitle;
            cmd.Parameters.Add("@sstitle", SqlDbType.VarChar, 10).Value = p.p_subjectshorttitle;
            cmd.Parameters.Add("@sdid", SqlDbType.Int).Value = p.p_subjectdepid;
            cmd.Parameters.Add("@stid", SqlDbType.Int).Value = p.p_subjecttypeid;
            cmd.Parameters.Add("@sdelsts", SqlDbType.Bit).Value = p.p_subjectdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsSubjectprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updsubject", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = p.p_subjectid;
            cmd.Parameters.Add("@sunicode", SqlDbType.VarChar, 50).Value = p.p_subjectunicode;
            cmd.Parameters.Add("@ssem", SqlDbType.Int).Value = p.p_subjectsem;
            cmd.Parameters.Add("@stitle", SqlDbType.VarChar, 100).Value = p.p_subjecttitle;
            cmd.Parameters.Add("@sdid", SqlDbType.Int).Value = p.p_subjectdepid;
            cmd.Parameters.Add("@stid", SqlDbType.Int).Value = p.p_subjecttypeid;
            cmd.Parameters.Add("@sdelsts", SqlDbType.Bit).Value = p.p_subjectdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsSubjectprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delsubject", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = p.p_subjectid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsSubjectprp> Display_rec(Int32 sid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspsubject", con);
            cmd.Parameters.Add("@u", SqlDbType.Int).Value = sid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsSubjectprp> obj = new List<clsSubjectprp>();
            while (dr.Read())
            {
                clsSubjectprp k = new clsSubjectprp();
                k.p_subjectid = Convert.ToInt32(dr[0]);
                k.p_subjectunicode = dr[1].ToString().ToUpper();
                k.p_subjectsem = Convert.ToInt32(dr[2]);
                k.p_subjecttitle = dr[3].ToString();
                k.p_subjectdepid = Convert.ToInt32(dr[4]);
               // k.p_subjecttypeid = Convert.ToInt32(dr[5]);
                //k.p_subjectdelsts = Convert.ToBoolean(dr[6]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsSubjectprp> Find_rec(Int32 sid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndsubject", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsSubjectprp> obj = new List<clsSubjectprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsSubjectprp k = new clsSubjectprp();
                k.p_subjectid = Convert.ToInt32(dr[0]);
                k.p_subjectunicode = dr[1].ToString();
                k.p_subjectsem = Convert.ToInt32(dr[2]);
                k.p_subjecttitle = dr[3].ToString();
                k.p_subjectdepid = Convert.ToInt32(dr[4]);
                k.p_subjecttypeid = Convert.ToInt32(dr[5]);
                k.p_subjectdelsts = Convert.ToBoolean(dr[6]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intSubjectAllotment
    {
        Int32 p_subjectallotteacherid
        {
            get;
            set;
        }
        Int32 p_subjectallotsubjectid
        {
            get;
            set;
        }
        Int32 p_subjectallotgroupid
        {
            get;
            set;
        }
        Boolean p_subjectallotdelsts
        {
            get;
            set;
        }
    }
    public class clsSubjectAllotmentprp : intSubjectAllotment
    {
        private Int32 subjectallotteacherid, subjectallotsubjectid, subjectallotgroupid;
        private Boolean subjectallotdelsts;
        public int p_subjectallotteacherid
        {
            get
            {
                return subjectallotteacherid;
            }
            set
            {
                subjectallotteacherid = value;
            }
        }

        public int p_subjectallotsubjectid
        {
            get
            {
                return subjectallotsubjectid;
            }
            set
            {
                subjectallotsubjectid = value;
            }
        }

        public int p_subjectallotgroupid
        {
            get
            {
                return subjectallotgroupid;
            }
            set
            {
                subjectallotgroupid = value;
            }
        }

        public bool p_subjectallotdelsts
        {
            get
            {
                return subjectallotdelsts;
            }
            set
            {
                subjectallotdelsts = value;
            }
        }
    }
    public class clsSubjectAllotment : sqlconnect
    {
        public void Save_Rec(clsSubjectAllotmentprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("inssubjectallot", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@satid", SqlDbType.Int).Value = p.p_subjectallotteacherid;
            cmd.Parameters.Add("@sasid", SqlDbType.Int).Value = p.p_subjectallotsubjectid;
            cmd.Parameters.Add("@sagid", SqlDbType.Int).Value = p.p_subjectallotgroupid;
            cmd.Parameters.Add("@sadelsts", SqlDbType.Bit).Value = p.p_subjectallotdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsSubjectAllotmentprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updsubjectallot", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@satid", SqlDbType.Int).Value = p.p_subjectallotteacherid;
            cmd.Parameters.Add("@sasid", SqlDbType.Int).Value = p.p_subjectallotsubjectid;
            cmd.Parameters.Add("@sagid", SqlDbType.Int).Value = p.p_subjectallotgroupid;
            cmd.Parameters.Add("@sadelsts", SqlDbType.Bit).Value = p.p_subjectallotdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsSubjectAllotmentprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delsubjectallot", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@satid", SqlDbType.Int).Value = p.p_subjectallotteacherid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsSubjectAllotmentprp> Display_rec(Int32 satid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspzubjectallot", con);
            cmd.Parameters.Add("@satid", SqlDbType.Int).Value = satid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsSubjectAllotmentprp> obj = new List<clsSubjectAllotmentprp>();
            while (dr.Read())
            {
                clsSubjectAllotmentprp k = new clsSubjectAllotmentprp();
                k.p_subjectallotteacherid = Convert.ToInt32(dr[0]);
                k.p_subjectallotsubjectid = Convert.ToInt32(dr[1]);
                k.p_subjectallotgroupid = Convert.ToInt32(dr[2]);
                k.p_subjectallotdelsts = Convert.ToBoolean(dr[3]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsSubjectAllotmentprp> Find_rec(Int32 satid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndsession", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@satid", SqlDbType.Int).Value = satid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsSubjectAllotmentprp> obj = new List<clsSubjectAllotmentprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsSubjectAllotmentprp k = new clsSubjectAllotmentprp();
                k.p_subjectallotteacherid = Convert.ToInt32(dr[0]);
                k.p_subjectallotsubjectid = Convert.ToInt32(dr[1]);
                k.p_subjectallotgroupid = Convert.ToInt32(dr[2]);
                k.p_subjectallotdelsts = Convert.ToBoolean(dr[3]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intSubjecttype
    {
        Int32 p_subjecttypeid
        {
            get;
            set;
        }
        String p_subjecttypename
        {
            get;
            set;
        }
        Boolean p_subjectdelsts
        {
            get;
            set;
        }
    }
    public class clsSubjecttypeprp : intSubjecttype
    {
        private Int32 subjecttypeid;
        private String subjecttypename;
        private Boolean subjectdelsts;
        public int p_subjecttypeid
        {
            get
            {
                return subjecttypeid;
            }
            set
            {
                subjecttypeid = value;
            }
        }

        public string p_subjecttypename
        {
            get
            {
                return subjecttypename;
            }
            set
            {
                subjecttypename = value;
            }
        }

        public bool p_subjectdelsts
        {
            get
            {
                return subjectdelsts;
            }
            set
            {
                subjectdelsts = value;
            }
        }
    }
    public class clsSubjecttype : sqlconnect
    {
        public void Save_Rec(clsSubjecttypeprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("inssubjecttype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@stname", SqlDbType.VarChar, 50).Value = p.p_subjecttypename;
            cmd.Parameters.Add("@stdelsts", SqlDbType.Bit).Value = p.p_subjectdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsSubjecttypeprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updsubjecttype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@stid", SqlDbType.Int).Value = p.p_subjecttypeid;
            cmd.Parameters.Add("@stname", SqlDbType.VarChar, 50).Value = p.p_subjecttypename;
            cmd.Parameters.Add("@stdelsts", SqlDbType.Bit).Value = p.p_subjectdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsSubjecttypeprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delsubjecttype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@stid", SqlDbType.Int).Value = p.p_subjecttypeid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsSubjecttypeprp> Display_rec(Int32 stid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspsubjecttype", con);
            cmd.Parameters.Add("@stid", SqlDbType.Int).Value = stid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsSubjecttypeprp> obj = new List<clsSubjecttypeprp>();
            while (dr.Read())
            {
                clsSubjecttypeprp k = new clsSubjecttypeprp();
                k.p_subjecttypeid = Convert.ToInt32(dr[0]);
                k.p_subjecttypename = dr[1].ToString();
                k.p_subjectdelsts = Convert.ToBoolean(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsSubjecttypeprp> Find_rec(Int32 stid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndsybjecttype", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@stid", SqlDbType.Int).Value = stid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsSubjecttypeprp> obj = new List<clsSubjecttypeprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsSubjecttypeprp k = new clsSubjecttypeprp();
                k.p_subjecttypeid = Convert.ToInt32(dr[0]);
                k.p_subjecttypename = dr[1].ToString();
                k.p_subjectdelsts = Convert.ToBoolean(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intTeacher
    {
        Int32 p_teacherid
        {
            get;
            set;
        }
        Int32 p_teacheruserid
        {
            get;
            set;
        }
        String p_teacherclgid
        {
            get;
            set;
        }
        String p_teachersalutation
        {
            get;
            set;
        }
        Int32 p_teacherdepid
        {
            get;
            set;
        }
        String p_teacherfname
        {
            get;
            set;
        }
        String p_teachermname
        {
            get;
            set;
        }
        String p_teacherlname
        {
            get;
            set;
        }
        DateTime p_teacherdob
        {
            get;
            set;
        }
        DateTime p_teacherdoj
        {
            get;
            set;
        }
        String p_teacherfathername
        {
            get;
            set;
        }
        String p_teachermothername
        {
            get;
            set;
        }
        String p_teachercorresadd
        {
            get;
            set;
        }
        String p_teacherpermaadd
        {
            get;
            set;
        }
        Int32 p_teacherpostid
        {
            get;
            set;
        }
        String p_teacherqualification
        {
            get;
            set;
        }
        String p_teacherexp
        {
            get;
            set;
        }
        Int64 p_teacherphone1
        {
            get;
            set;
        }
        Int64 p_teacherphone2
        {
            get;
            set;
        }
        String p_teacheremailid
        {
            get;
            set;
        }
        String p_teacherphoto
        {
            get;
            set;
        }
        Boolean p_teachergender
        {
            get;
            set;
        }
        String p_teacherresrchjrnls
        {
            get;
            set;
        }
        String p_teacherresrchcnfrncs
        {
            get;
            set;
        }
        String p_teacherworkshopseminar
        {
            get;
            set;
        }
        String p_teacherotheracts
        {
            get;
            set;
        }
        String p_teacherspecialization
        {
            get;
            set;
        }
        String p_teacherprojectaward
        {
            get;
            set;
        }
        String p_teachersubjectundertaken
        {
            get;
            set;
        }
        String p_teachercnfrncconducted
        {
            get;
            set;
        }
        String p_teachercnfrncattend
        {
            get;
            set;
        }
        String p_teacherotherinfo
        {
            get;
            set;
        }
        Int32 p_teachercategoryid
        {
            get;
            set;
        }
        Boolean p_teachermaritalstatus
        {
            get;
            set;
        }
        String p_teachermembershipofprofessionalbodies
        {
            get;
            set;
        }
        String p_teacherpanno
        {
            get;
            set;
        }
        String p_teacherreligion
        {
            get;
            set;
        }
        String p_teacherpfno
        {
            get;
            set;
        }
        String p_teachersalarybankname
        {
            get;
            set;
        }
        String p_teachersalarybranchname
        {
            get;
            set;
        }
        String p_teacherbankaccno
        {
            get;
            set;
        }
        String p_teacherISFCcode
        {
            get;
            set;
        }
        Int64 p_teachergrosspay
        {
            get;
            set;
        }
        String p_teacherpayscale
        {
            get;
            set;
        }
        Boolean p_teacherTORNTsts
        {
            get;
            set;
        }
        String p_teacherappointtype
        {
            get;
            set;
        }
        String p_teacherunivaprroveletterno
        {
            get;
            set;
        }
        DateTime p_teacherunivapprovedate
        {
            get;
            set;
        }
        Boolean p_teacherdelsts
        {
            get;
            set;
        }
    }
    public class clsTeacherprp : intTeacher
    {
        private Int32 teacherid,teacheruserid,teacherdepid,teacherpostid,teachercategoryid;
        private Int64 teacherphone1,teacherphone2,teachergrosspay;
        private string teacherclgid, teachersalutation, teacherpayscale,teacheruniaprroveletterno, teacherappointtype,teacherworkshopseminar, teacherqualification, teacherotheracts, teacherspecialization, teacherprojectaward, teachersubjectundertaken, teachercnfrncconducted, teachercnfrncattend, teacherotherinfo, teacherresrchjrnls, teacherresrchcnfrncs, teacherexp, teacherphoto, teacherfname, teachermname, teacherlname, teacherfathername, teachermothername, teacherpermaadd, teachercorresadd, teacheremailid, teachermembershipofprofessionalbodies, teacherpanno, teacherreligion, teacherpfno, teachersalarybankname, teachersalarybranchname, teacherbankaccno, teacherISFCcode;
        private DateTime teacherdob, teacherdoj, teacheruniaprrovedate;
        private Boolean teachergender,teacherdelsts,teachermaritalstatus,teacherTORNTsts;
        public int p_teacherid
        {
            get
            {
                return teacherid;
            }
            set
            {
                teacherid = value;
            }
        }

        public int p_teacheruserid
        {
            get
            {
                return teacheruserid;
            }
            set
            {
                teacheruserid = value;
            }
        }

        public string p_teacherclgid
        {
            get
            {
                return teacherclgid;
            }
            set
            {
                teacherclgid = value;
            }
        }

        public string p_teachersalutation
        {
            get
            {
                return teachersalutation;
            }
            set
            {
                teachersalutation = value;
            }
        }

        public int p_teacherdepid
        {
            get
            {
                return teacherdepid;
            }
            set
            {
                teacherdepid = value;
            }
        }

        public string p_teacherfname
        {
            get
            {
                return teacherfname;
            }
            set
            {
                teacherfname = value;
            }
        }

        public string p_teachermname
        {
            get
            {
                return teachermname;
            }
            set
            {
                teachermname = value;
            }
        }

        public string p_teacherlname
        {
            get
            {
                return teacherlname;
            }
            set
            {
                teacherlname = value;
            }
        }

        public DateTime p_teacherdob
        {
            get
            {
                return teacherdob;
            }
            set
            {
                teacherdob = value;
            }
        }

        public DateTime p_teacherdoj
        {
            get
            {
                return teacherdoj;
            }
            set
            {
                teacherdoj = value;
            }
        }

        public string p_teacherfathername
        {
            get
            {
                return teacherfathername;
            }
            set
            {
                teacherfathername = value;
            }
        }

        public string p_teachermothername
        {
            get
            {
                return teachermothername;
            }
            set
            {
                teachermothername = value;
            }
        }

        public string p_teachercorresadd
        {
            get
            {
                return teachercorresadd;
            }
            set
            {
                teachercorresadd = value;
            }
        }

        public string p_teacherpermaadd
        {
            get
            {
                return teacherpermaadd;
            }
            set
            {
                teacherpermaadd = value;
            }
        }

        public int p_teacherpostid
        {
            get
            {
                return teacherpostid;
            }
            set
            {
                teacherpostid = value;
            }
        }

        public string p_teacherqualification
        {
            get
            {
                return teacherqualification;
            }
            set
            {
                teacherqualification = value;
            }
        }

        public string p_teacherexp
        {
            get
            {
                return teacherexp;
            }
            set
            {
                teacherexp = value;
            }
        }

        public long p_teacherphone1
        {
            get
            {
                return teacherphone1;
            }
            set
            {
                teacherphone1 = value;
            }
        }

        public long p_teacherphone2
        {
            get
            {
                return teacherphone2;
            }
            set
            {
                teacherphone2 = value;
            }
        }

        public string p_teacheremailid
        {
            get
            {
                return teacheremailid;
            }
            set
            {
                teacheremailid = value;
            }
        }

        public string p_teacherphoto
        {
            get
            {
                return teacherphoto;
            }
            set
            {
                teacherphoto = value;
            }
        }

        public bool p_teachergender
        {
            get
            {
                return teachergender;
            }
            set
            {
                teachergender = value;
            }
        }

        public string p_teacherresrchjrnls
        {
            get
            {
                return teacherresrchjrnls;
            }
            set
            {
                teacherresrchjrnls = value;
            }
        }

        public string p_teacherresrchcnfrncs
        {
            get
            {
                return teacherresrchcnfrncs;
            }
            set
            {
                teacherresrchcnfrncs = value;
            }
        }

        public string p_teacherworkshopseminar
        {
            get
            {
                return teacherworkshopseminar;
            }
            set
            {
                teacherworkshopseminar = value;
            }
        }

        public string p_teacherotheracts
        {
            get
            {
                return teacherotheracts;
            }
            set
            {
                teacherotheracts = value;
            }
        }

        public string p_teacherspecialization
        {
            get
            {
                return teacherspecialization;
            }
            set
            {
                teacherspecialization = value;
            }
        }

        public string p_teacherprojectaward
        {
            get
            {
                return teacherprojectaward;
            }
            set
            {
                teacherprojectaward = value;
            }
        }

        public string p_teachersubjectundertaken
        {
            get
            {
                return teachersubjectundertaken;
            }
            set
            {
                teachersubjectundertaken = value;
            }
        }

        public string p_teachercnfrncconducted
        {
            get
            {
                return teachercnfrncconducted;
            }
            set
            {
                teachercnfrncconducted = value;
            }
        }

        public string p_teachercnfrncattend
        {
            get
            {
                return teachercnfrncattend;
            }
            set
            {
                teachercnfrncattend = value;
            }
        }

        public string p_teacherotherinfo
        {
            get
            {
                return teacherotherinfo;
            }
            set
            {
                teacherotherinfo = value;
            }
        }

        public int p_teachercategoryid
        {
            get
            {
                return teachercategoryid;
            }
            set
            {
                teachercategoryid = value;
            }
        }

        public bool p_teachermaritalstatus
        {
            get
            {
                return teachermaritalstatus;
            }
            set
            {
                teachermaritalstatus = value;
            }
        }

        public string p_teachermembershipofprofessionalbodies
        {
            get
            {
                return teachermembershipofprofessionalbodies;
            }
            set
            {
                teachermembershipofprofessionalbodies = value;
            }
        }

        public string p_teacherpanno
        {
            get
            {
                return teacherpanno;
            }
            set
            {
                teacherpanno = value;
            }
        }

        public string p_teacherreligion
        {
            get
            {
                return teacherreligion;
            }
            set
            {
                teacherreligion = value;
            }
        }

        public string p_teacherpfno
        {
            get
            {
                return teacherpfno;
            }
            set
            {
                teacherpfno = value;
            }
        }

        public string p_teachersalarybankname
        {
            get
            {
                return teachersalarybankname;
            }
            set
            {
                teachersalarybankname = value;
            }
        }

        public string p_teachersalarybranchname
        {
            get
            {
                return teachersalarybranchname;
            }
            set
            {
                teachersalarybranchname = value;
            }
        }

        public string p_teacherbankaccno
        {
            get
            {
                return teacherbankaccno;
            }
            set
            {
                teacherbankaccno = value;
            }
        }

        public string p_teacherISFCcode
        {
            get
            {
                return teacherISFCcode;
            }
            set
            {
                teacherISFCcode = value;
            }
        }

        public long p_teachergrosspay
        {
            get
            {
                return teachergrosspay;
            }
            set
            {
                teachergrosspay = value;
            }
        }

        public string p_teacherpayscale
        {
            get
            {
                return teacherpayscale;
            }
            set
            {
                teacherpayscale = value;
            }
        }

        public bool p_teacherTORNTsts
        {
            get
            {
                return teacherTORNTsts;
            }
            set
            {
                teacherTORNTsts = value;
            }
        }

        public string p_teacherappointtype
        {
            get
            {
                return teacherappointtype;
            }
            set
            {
                teacherappointtype = value;
            }
        }

        public string p_teacherunivaprroveletterno
        {
            get
            {
                return teacheruniaprroveletterno;
            }
            set
            {
                teacheruniaprroveletterno = value;
            }
        }

        public DateTime p_teacherunivapprovedate
        {
            get
            {
                return teacheruniaprrovedate;
            }
            set
            {
                teacheruniaprrovedate = value;
            }
        }

        public bool p_teacherdelsts
        {
            get
            {
                return teacherdelsts;
            }
            set
            {
                teacherdelsts = value;
            }
        }
    }
    public class clsTeacher : sqlconnect
    {
        public void Save_Rec(clsTeacherprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insteacher", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@userlogid", SqlDbType.VarChar, 50).Value = p.p_teacheruserid;
            cmd.Parameters.Add("@tclgid", SqlDbType.VarChar, 50).Value = p.p_teacherclgid;
            cmd.Parameters.Add("@tsalutation", SqlDbType.VarChar, 8).Value = p.p_teachersalutation;
            cmd.Parameters.Add("@tdid", SqlDbType.Int).Value = p.p_teacherdepid;
            cmd.Parameters.Add("@tfname", SqlDbType.VarChar, 50).Value = p.p_teacherfname;
            cmd.Parameters.Add("@tmname", SqlDbType.VarChar, 50).Value = p.p_teachermname;
            cmd.Parameters.Add("@tlname", SqlDbType.VarChar, 50).Value = p.p_teacherlname;
            cmd.Parameters.Add("@tdob", SqlDbType.DateTime).Value = p.p_teacherdob;
            cmd.Parameters.Add("@tdoj", SqlDbType.DateTime).Value = p.p_teacherdoj;
            cmd.Parameters.Add("@tftname", SqlDbType.VarChar, 50).Value = p.p_teacherfathername;
            cmd.Parameters.Add("@tmtname", SqlDbType.VarChar, 50).Value = p.p_teachermothername;
            cmd.Parameters.Add("@tcorres", SqlDbType.VarChar, 500).Value = p.p_teachercorresadd;
            cmd.Parameters.Add("@tperma", SqlDbType.VarChar, 500).Value = p.p_teacherpermaadd;
            cmd.Parameters.Add("@tpid", SqlDbType.Int).Value = p.p_teacherpostid;
            cmd.Parameters.Add("@tqualification", SqlDbType.VarChar, 50).Value = p.p_teacherqualification;
            cmd.Parameters.Add("@texperience", SqlDbType.VarChar, 50).Value = p.p_teacherexp;
            cmd.Parameters.Add("@tp1", SqlDbType.BigInt).Value = p.p_teacherphone1;
            cmd.Parameters.Add("@tp2", SqlDbType.BigInt).Value = p.p_teacherphone2;
            cmd.Parameters.Add("@temail", SqlDbType.VarChar, 50).Value = p.p_teacheremailid;
            cmd.Parameters.Add("@tphoto", SqlDbType.VarChar, 100).Value =p.p_teacherphoto;
            cmd.Parameters.Add("@tg1", SqlDbType.Bit).Value = p.p_teachergender;
            cmd.Parameters.Add("@tresrchjour", SqlDbType.VarChar, 500).Value = p.p_teacherresrchjrnls;
            cmd.Parameters.Add("@tresrchcnfrncs", SqlDbType.VarChar, 500).Value =p.p_teacherresrchcnfrncs;
            cmd.Parameters.Add("@tworkshop", SqlDbType.VarChar, 500).Value = p.p_teacherworkshopseminar;
            cmd.Parameters.Add("@totheracts", SqlDbType.VarChar, 500).Value = p.p_teacherotheracts;
            cmd.Parameters.Add("@tspecialization", SqlDbType.VarChar, 500).Value = p.p_teacherspecialization;
            cmd.Parameters.Add("@tproject", SqlDbType.VarChar, 500).Value = p.p_teacherprojectaward;
            cmd.Parameters.Add("@tsubundertaken", SqlDbType.VarChar, 500).Value = p.p_teachersubjectundertaken;
            cmd.Parameters.Add("@tcnfrnconduct", SqlDbType.VarChar, 500).Value = p.p_teachercnfrncconducted;
            cmd.Parameters.Add("@tcnfrnattend", SqlDbType.VarChar, 500).Value = p.p_teachercnfrncattend;
            cmd.Parameters.Add("@totherinfo", SqlDbType.VarChar, 500).Value = p.p_teacherotherinfo;
            cmd.Parameters.Add("@tcid", SqlDbType.Int).Value = p.p_teachercategoryid;
            cmd.Parameters.Add("@tmsts", SqlDbType.Bit).Value = p.p_teachermaritalstatus;
            cmd.Parameters.Add("@tmemofprofbody ", SqlDbType.VarChar, 500).Value = p.p_teachermembershipofprofessionalbodies;
            cmd.Parameters.Add("@tpan ", SqlDbType.VarChar, 50).Value = p.p_teacherpanno;
            cmd.Parameters.Add("@treligion ", SqlDbType.VarChar, 50).Value = p.p_teacherreligion;
            cmd.Parameters.Add("@tpf ", SqlDbType.VarChar, 500).Value = p.p_teacherpfno;
            cmd.Parameters.Add("@tsbankname ", SqlDbType.VarChar, 100).Value = p.p_teachersalarybankname;
            cmd.Parameters.Add("@tsbranchname ", SqlDbType.VarChar, 100).Value = p.p_teachersalarybranchname;
            cmd.Parameters.Add("@tbaccnumber", SqlDbType.VarChar, 50).Value = p.p_teacherbankaccno;
            cmd.Parameters.Add("@tisfc ", SqlDbType.VarChar, 50).Value = p.p_teacherISFCcode;
            cmd.Parameters.Add("@tgrosspay", SqlDbType.BigInt).Value = p.p_teachergrosspay;
            cmd.Parameters.Add("@tpayscale ", SqlDbType.VarChar, 50).Value = p.p_teacherpayscale;
            cmd.Parameters.Add("@ttornt", SqlDbType.Bit).Value = p.p_teacherTORNTsts;
            cmd.Parameters.Add("@tapptype ", SqlDbType.VarChar, 50).Value = p.p_teacherappointtype;
            cmd.Parameters.Add("@tuniappletternumber ", SqlDbType.VarChar, 50).Value = p.p_teacherunivaprroveletterno;
            cmd.Parameters.Add("@ttuniappdate", SqlDbType.DateTime).Value = p.p_teacherunivapprovedate;
            cmd.Parameters.Add("@tdelsts", SqlDbType.Bit).Value = p.p_teacherdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsTeacherprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updteacher", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@tid", SqlDbType.Int).Value = p.p_teacherid;
            cmd.Parameters.Add("@userlogid", SqlDbType.VarChar, 50).Value = p.p_teacheruserid;
            cmd.Parameters.Add("@tclgid", SqlDbType.VarChar, 50).Value = p.p_teacherclgid;
            cmd.Parameters.Add("@tsalutation", SqlDbType.VarChar, 8).Value = p.p_teachersalutation;
            cmd.Parameters.Add("@tdid", SqlDbType.Int).Value = p.p_teacherdepid;
            cmd.Parameters.Add("@tfname", SqlDbType.VarChar, 50).Value = p.p_teacherfname;
            cmd.Parameters.Add("@tmname", SqlDbType.VarChar, 50).Value = p.p_teachermname;
            cmd.Parameters.Add("@tlname", SqlDbType.VarChar, 50).Value = p.p_teacherlname;
            cmd.Parameters.Add("@tdob", SqlDbType.DateTime).Value = p.p_teacherdob;
            cmd.Parameters.Add("@tdoj", SqlDbType.DateTime).Value = p.p_teacherdoj;
            cmd.Parameters.Add("@tftname", SqlDbType.VarChar, 50).Value = p.p_teacherfathername;
            cmd.Parameters.Add("@tmtname", SqlDbType.VarChar, 50).Value = p.p_teachermothername;
            cmd.Parameters.Add("@tcorres", SqlDbType.VarChar, 500).Value = p.p_teachercorresadd;
            cmd.Parameters.Add("@tperma", SqlDbType.VarChar, 500).Value = p.p_teacherpermaadd;
            cmd.Parameters.Add("@tpid", SqlDbType.Int).Value = p.p_teacherpostid;
            cmd.Parameters.Add("@tqualification", SqlDbType.VarChar, 50).Value = p.p_teacherqualification;
            cmd.Parameters.Add("@texperience", SqlDbType.VarChar, 50).Value = p.p_teacherexp;
            cmd.Parameters.Add("@tp1", SqlDbType.BigInt).Value = p.p_teacherphone1;
            cmd.Parameters.Add("@tp2", SqlDbType.BigInt).Value = p.p_teacherphone2;
            cmd.Parameters.Add("@temail", SqlDbType.VarChar, 50).Value = p.p_teacheremailid;
            cmd.Parameters.Add("@tphoto", SqlDbType.VarChar, 100).Value = p.p_teacherphoto;
            cmd.Parameters.Add("@tg1", SqlDbType.Bit).Value = p.p_teachergender;
            cmd.Parameters.Add("@tresrchjour", SqlDbType.VarChar, 500).Value = p.p_teacherresrchjrnls;
            cmd.Parameters.Add("@tresrchcnfrncs", SqlDbType.VarChar, 500).Value = p.p_teacherresrchcnfrncs;
            cmd.Parameters.Add("@tworkshop", SqlDbType.VarChar, 500).Value = p.p_teacherworkshopseminar;
            cmd.Parameters.Add("@totheracts", SqlDbType.VarChar, 500).Value = p.p_teacherotheracts;
            cmd.Parameters.Add("@tspecialization", SqlDbType.VarChar, 500).Value = p.p_teacherspecialization;
            cmd.Parameters.Add("@tproject", SqlDbType.VarChar, 500).Value = p.p_teacherprojectaward;
            cmd.Parameters.Add("@tsubundertaken", SqlDbType.VarChar, 500).Value = p.p_teachersubjectundertaken;
            cmd.Parameters.Add("@tcnfrnconduct", SqlDbType.VarChar, 500).Value = p.p_teachercnfrncconducted;
            cmd.Parameters.Add("@tcnfrnattend", SqlDbType.VarChar, 500).Value = p.p_teachercnfrncattend;
            cmd.Parameters.Add("@totherinfo", SqlDbType.VarChar, 500).Value = p.p_teacherotherinfo;
            cmd.Parameters.Add("@tcid", SqlDbType.Int).Value = p.p_teachercategoryid;
            cmd.Parameters.Add("@tmsts", SqlDbType.Bit).Value = p.p_teachermaritalstatus;
            cmd.Parameters.Add("@tmemofprofbody ", SqlDbType.VarChar, 500).Value = p.p_teachermembershipofprofessionalbodies;
            cmd.Parameters.Add("@tpan ", SqlDbType.VarChar, 50).Value = p.p_teacherpanno;
            cmd.Parameters.Add("@treligion ", SqlDbType.VarChar, 50).Value = p.p_teacherreligion;
            cmd.Parameters.Add("@tpf ", SqlDbType.VarChar, 500).Value = p.p_teacherpfno;
            cmd.Parameters.Add("@tsbankname ", SqlDbType.VarChar, 100).Value = p.p_teachersalarybankname;
            cmd.Parameters.Add("@tsbranchname ", SqlDbType.VarChar, 100).Value = p.p_teachersalarybranchname;
            cmd.Parameters.Add("@tbaccnumber", SqlDbType.VarChar, 50).Value = p.p_teacherbankaccno;
            cmd.Parameters.Add("@tisfc ", SqlDbType.VarChar, 50).Value = p.p_teacherISFCcode;
            cmd.Parameters.Add("@tgrosspay", SqlDbType.BigInt).Value = p.p_teachergrosspay;
            cmd.Parameters.Add("@tpayscale ", SqlDbType.VarChar, 50).Value = p.p_teacherpayscale;
            cmd.Parameters.Add("@ttornt", SqlDbType.Bit).Value = p.p_teacherTORNTsts;
            cmd.Parameters.Add("@tapptype ", SqlDbType.VarChar, 50).Value = p.p_teacherappointtype;
            cmd.Parameters.Add("@tuniappletternumber ", SqlDbType.VarChar, 50).Value = p.p_teacherunivaprroveletterno;
            cmd.Parameters.Add("@ttuniappdate", SqlDbType.DateTime).Value = p.p_teacherunivapprovedate;
            cmd.Parameters.Add("@tdelsts", SqlDbType.Bit).Value = p.p_teacherdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsTeacherprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delteacher", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@tid", SqlDbType.Int).Value = p.p_teacherid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsTeacherprp> Display_rec(Int32 tid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspteacher", con);
            cmd.Parameters.Add("@tid", SqlDbType.Int).Value = tid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsTeacherprp> obj = new List<clsTeacherprp>();
            while (dr.Read())
            {
                clsTeacherprp k = new clsTeacherprp();
                k.p_teacherid=Convert.ToInt32(dr[0]);
                k.p_teacheruserid=Convert.ToInt32(dr[1]);
                k.p_teacherclgid=dr[2].ToString();
                k.p_teachersalutation=dr[3].ToString();
                k.p_teacherdepid=Convert.ToInt32(dr[4]);
                k.p_teacherfname=dr[5].ToString();
                k.p_teachermname=dr[6].ToString();
                k.p_teacherlname=dr[7].ToString();
                k.p_teacherdob=Convert.ToDateTime(dr[8]);
                k.p_teacherdoj=Convert.ToDateTime(dr[9]);
                k.p_teacherfathername=dr[10].ToString();
                k.p_teachermothername=dr[11].ToString();
                k.p_teachercorresadd=dr[12].ToString();
                k.p_teacherpermaadd=dr[13].ToString();
                k.p_teacherpostid=Convert.ToInt32(dr[14]);
                k.p_teacherqualification=dr[15].ToString();
                k.p_teacherexp=dr[16].ToString();
                k.p_teacherphone1=Convert.ToInt64(dr[17]);
                k.p_teacherphone2=Convert.ToInt64(dr[18]);
                k.p_teacheremailid=dr[19].ToString();
                k.p_teacherphoto=dr[20].ToString();
                k.p_teachergender=Convert.ToBoolean(dr[21]);
                k.p_teacherresrchjrnls=dr[22].ToString();
                k.p_teacherresrchcnfrncs=dr[23].ToString();
                k.p_teacherworkshopseminar=dr[24].ToString();
                k.p_teacherotheracts=dr[25].ToString();
                k.p_teacherspecialization=dr[26].ToString();
                k.p_teacherprojectaward=dr[27].ToString();
                k.p_teachersubjectundertaken=dr[28].ToString();
                k.p_teachercnfrncconducted=dr[29].ToString();
                k.p_teachercnfrncattend=dr[30].ToString();
                k.p_teacherotherinfo=dr[31].ToString();
                k.p_teachercategoryid=Convert.ToInt32(dr[32]);
                k.p_teachermaritalstatus=Convert.ToBoolean(dr[33]);
                k.p_teachermembershipofprofessionalbodies=dr[34].ToString();
                k.p_teacherpanno=dr[35].ToString();
                k.p_teacherreligion=dr[36].ToString();
                k.p_teacherpfno=dr[37].ToString();
                k.p_teachersalarybankname=dr[38].ToString();
                k.p_teachersalarybranchname=dr[39].ToString();
                k.p_teacherbankaccno=dr[40].ToString();
                k.p_teacherISFCcode=dr[41].ToString();
                k.p_teachergrosspay=Convert.ToInt64(dr[42]);
                k.p_teacherpayscale=dr[43].ToString();
                k.p_teacherTORNTsts=Convert.ToBoolean(dr[44]);
                k.p_teacherappointtype=dr[45].ToString();
                k.p_teacherunivaprroveletterno=dr[46].ToString();
                k.p_teacherunivapprovedate=Convert.ToDateTime(dr[47]);
                k.p_teacherdelsts=Convert.ToBoolean(dr[48]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsTeacherprp> Find_rec(Int32 tid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndteacher", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@tid", SqlDbType.Int).Value = tid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsTeacherprp> obj = new List<clsTeacherprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsTeacherprp k = new clsTeacherprp();
                k.p_teacherid = Convert.ToInt32(dr[0]);
                k.p_teacheruserid = Convert.ToInt32(dr[1]);
                k.p_teacherclgid = dr[2].ToString();
                k.p_teachersalutation = dr[3].ToString();
                k.p_teacherdepid = Convert.ToInt32(dr[4]);
                k.p_teacherfname = dr[5].ToString();
                k.p_teachermname = dr[6].ToString();
                k.p_teacherlname = dr[7].ToString();
                k.p_teacherdob = Convert.ToDateTime(dr[8]);
                k.p_teacherdoj = Convert.ToDateTime(dr[9]);
                k.p_teacherfathername = dr[10].ToString();
                k.p_teachermothername = dr[11].ToString();
                k.p_teachercorresadd = dr[12].ToString();
                k.p_teacherpermaadd = dr[13].ToString();
                k.p_teacherpostid = Convert.ToInt32(dr[14]);
                k.p_teacherqualification = dr[15].ToString();
                k.p_teacherexp = dr[16].ToString();
                k.p_teacherphone1 = Convert.ToInt64(dr[17]);
                k.p_teacherphone2 = Convert.ToInt64(dr[18]);
                k.p_teacheremailid = dr[19].ToString();
                k.p_teacherphoto = dr[20].ToString();
                k.p_teachergender = Convert.ToBoolean(dr[21]);
                k.p_teacherresrchjrnls = dr[22].ToString();
                k.p_teacherresrchcnfrncs = dr[23].ToString();
                k.p_teacherworkshopseminar = dr[24].ToString();
                k.p_teacherotheracts = dr[25].ToString();
                k.p_teacherspecialization = dr[26].ToString();
                k.p_teacherprojectaward = dr[27].ToString();
                k.p_teachersubjectundertaken = dr[28].ToString();
                k.p_teachercnfrncconducted = dr[29].ToString();
                k.p_teachercnfrncattend = dr[30].ToString();
                k.p_teacherotherinfo = dr[31].ToString();
                k.p_teachercategoryid = Convert.ToInt32(dr[32]);
                k.p_teachermaritalstatus = Convert.ToBoolean(dr[33]);
                k.p_teachermembershipofprofessionalbodies = dr[34].ToString();
                k.p_teacherpanno = dr[35].ToString();
                k.p_teacherreligion = dr[36].ToString();
                k.p_teacherpfno = dr[37].ToString();
                k.p_teachersalarybankname = dr[38].ToString();
                k.p_teachersalarybranchname = dr[39].ToString();
                k.p_teacherbankaccno = dr[40].ToString();
                k.p_teacherISFCcode = dr[41].ToString();
                k.p_teachergrosspay = Convert.ToInt64(dr[42]);
                k.p_teacherpayscale = dr[43].ToString();
                k.p_teacherTORNTsts = Convert.ToBoolean(dr[44]);
                k.p_teacherappointtype = dr[45].ToString();
                k.p_teacherunivaprroveletterno = dr[46].ToString();
                k.p_teacherunivapprovedate = Convert.ToDateTime(dr[47]);
                k.p_teacherdelsts = Convert.ToBoolean(dr[48]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intTimetable
    {
        Int32 p_timetableid
        {
            get;
            set;
        }
        String p_timetablefile
        {
            get;
            set;
        }
        Int32 p_timetableclassid
        {
            get;
            set;
        }
        Boolean p_timetabledelsts
        {
            get;
            set;
        }
    }
    public class clsTimetableprp : intTimetable
    {
        private Int32 timetableid,timetableclassid;
        private String timetablefile;
        private Boolean timetabledelsts;
        public int p_timetableid
        {
            get
            {
                return timetableid;
            }
            set
            {
                timetableid = value;
            }
        }

        public string p_timetablefile
        {
            get
            {
                return timetablefile;
            }
            set
            {
                timetablefile = value;
            }
        }

        public int p_timetableclassid
        {
            get
            {
                return timetableclassid;
            }
            set
            {
                timetableclassid = value;
            }
        }

        public bool p_timetabledelsts
        {
            get
            {
                return timetabledelsts;
            }
            set
            {
                timetabledelsts = value;
            }
        }
    }
    public class clsTimetable : sqlconnect
    {
        public void Save_Rec(clsTimetableprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("instimetable", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ttfile", SqlDbType.VarChar, 50).Value = p.p_timetablefile;
            cmd.Parameters.Add("@ttcid", SqlDbType.Int).Value = p.p_timetableclassid;
            cmd.Parameters.Add("@ttdelsts", SqlDbType.Bit).Value = p.p_timetabledelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsTimetableprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updtimetable", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ttid", SqlDbType.Int).Value = p.p_timetableid;
            cmd.Parameters.Add("@ttfile", SqlDbType.VarChar, 50).Value = p.p_timetablefile;
            cmd.Parameters.Add("@ttcid", SqlDbType.Int).Value = p.p_timetableclassid;
            cmd.Parameters.Add("@ttdelsts", SqlDbType.Bit).Value = p.p_timetabledelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsTimetableprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("deltimetable", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ttid", SqlDbType.Int).Value = p.p_timetableid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsTimetableprp> Display_rec(Int32 ttid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dsptimetable", con);
            cmd.Parameters.Add("@ttid", SqlDbType.Int).Value = ttid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsTimetableprp> obj = new List<clsTimetableprp>();
            while (dr.Read())
            {
                clsTimetableprp k = new clsTimetableprp();
                k.p_timetableid = Convert.ToInt32(dr[0]);
                k.p_timetablefile = dr[1].ToString();
                k.p_timetableclassid = Convert.ToInt32(dr[2]);
                k.p_timetabledelsts = Convert.ToBoolean(dr[3]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsTimetableprp> Find_rec(Int32 ttid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndtimetable", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ttid", SqlDbType.Int).Value = ttid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsTimetableprp> obj = new List<clsTimetableprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsTimetableprp k = new clsTimetableprp();
                k.p_timetableid = Convert.ToInt32(dr[0]);
                k.p_timetablefile = dr[1].ToString();
                k.p_timetableclassid = Convert.ToInt32(dr[2]);
                k.p_timetabledelsts = Convert.ToBoolean(dr[3]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intUser
    {
        Int32 p_userid
        {
            get;
            set;
        }
        String p_userlogid
        {
            get;
            set;
        }
        String p_userpwd
        {
            get;
            set;
        }
        Int32 p_userroleid
        {
            get;
            set;
        }
        Boolean p_userlogsts
        {
            get;
            set;
        }
        Boolean p_userdelsts
        {
            get;
            set;
        }
    }
    public class clsUserprp : intUser
    {
        private Int32 userid, userroleid;
        private String userlogid, userpwd;
        private Boolean userlogsts, userdelsts;
        public int p_userid
        {
            get
            {
                return userid;
            }
            set
            {
                userid = value;
            }
        }

        public string p_userlogid
        {
            get
            {
                return userlogid;
            }
            set
            {
                userlogid = value;
            }
        }

        public string p_userpwd
        {
            get
            {
                return userpwd;
            }
            set
            {
                userpwd = value;
            }
        }

        public int p_userroleid
        {
            get
            {
                return userroleid;
            }
            set
            {
                userroleid = value;
            }
        }

        public bool p_userlogsts
        {
            get
            {
                return userlogsts;
            }
            set
            {
                userlogsts = value;
            }
        }

        public bool p_userdelsts
        {
            get
            {
                return userdelsts;
            }
            set
            {
                userdelsts = value;
            }
        }
    }
    public class clsUser : sqlconnect
    {
        public void Save_Rec(clsUserprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insuser", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@ulogid", SqlDbType.VarChar, 50).Value = p.p_userlogid;
            cmd.Parameters.Add("@upwd", SqlDbType.VarChar,50).Value = p.p_userpwd;
            cmd.Parameters.Add("@uroleid", SqlDbType.Int).Value = p.p_userroleid;
            cmd.Parameters.Add("@ulogsts", SqlDbType.Bit).Value = p.p_userlogsts;
            cmd.Parameters.Add("@udelsts", SqlDbType.Bit).Value = p.p_userdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsUserprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("upduser", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@uid", SqlDbType.Int).Value = p.p_userid;
            cmd.Parameters.Add("@ulogid", SqlDbType.VarChar, 50).Value = p.p_userlogid;
            cmd.Parameters.Add("@upwd", SqlDbType.VarChar, 50).Value = p.p_userpwd;
            cmd.Parameters.Add("@uroleid", SqlDbType.Int).Value = p.p_userroleid;
            cmd.Parameters.Add("@ulogsts", SqlDbType.Bit).Value = p.p_userlogsts;
            cmd.Parameters.Add("@udelsts", SqlDbType.Bit).Value = p.p_userdelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsUserprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("deluser", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@uid", SqlDbType.Int).Value = p.p_userid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsUserprp> Display_rec(Int32 uid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspuser", con);
            cmd.Parameters.Add("@uid", SqlDbType.Int).Value = uid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsUserprp> obj = new List<clsUserprp>();
            while (dr.Read())
            {
                clsUserprp k = new clsUserprp();
                k.p_userid = Convert.ToInt32(dr[0]);
                k.p_userlogid = dr[1].ToString();
                k.p_userpwd = dr[2].ToString();
                k.p_userroleid = Convert.ToInt32(dr[3]);
                k.p_userlogsts = Convert.ToBoolean(dr[4]);
                k.p_userdelsts = Convert.ToBoolean(dr[5]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsUserprp> Find_rec(Int32 uid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fnduser", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@uid", SqlDbType.Int).Value = uid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsUserprp> obj = new List<clsUserprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsUserprp k = new clsUserprp();
                k.p_userid = Convert.ToInt32(dr[0]);
                k.p_userlogid = dr[1].ToString();
                k.p_userpwd = dr[2].ToString();
                k.p_userroleid = Convert.ToInt32(dr[3]);
                k.p_userlogsts = Convert.ToBoolean(dr[4]);
                k.p_userdelsts = Convert.ToBoolean(dr[5]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public interface intUserrole
    {
        Int32 p_userroleid
        {
            get;
            set;
        }
        String p_userroledesc
        {
            get;
            set;
        }
        Boolean p_userroledelsts
        {
            get;
            set;
        }
    }
    public class clsUserroleprp : intUserrole
    {
        Int32 userroleid;
        String userroledesc;
        Boolean userroledelsts;
        public int p_userroleid
        {
            get
            {
                return userroleid;
            }
            set
            {
                userroleid = value;
            }
        }

        public string p_userroledesc
        {
            get
            {
                return userroledesc;
            }
            set
            {
                userroledesc = value;
            }
        }

        public bool p_userroledelsts
        {
            get
            {
                return userroledelsts;
            }
            set
            {
                userroledelsts = value;
            }
        }
    }
    public class clsUserrole : sqlconnect
    {
        public void Save_Rec(clsUserroleprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insuserrole", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@urdesc", SqlDbType.VarChar, 50).Value = p.p_userroledesc;
            cmd.Parameters.Add("@urdelsts", SqlDbType.Bit).Value = p.p_userroledelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsUserroleprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("upduserrole", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@urid", SqlDbType.Int).Value = p.p_userroleid;
            cmd.Parameters.Add("@urdesc", SqlDbType.VarChar, 50).Value = p.p_userroledesc;
            cmd.Parameters.Add("@urdelsts", SqlDbType.Bit).Value = p.p_userroledelsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsUserroleprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("deluserrole", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@urid", SqlDbType.Int).Value = p.p_userroleid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsUserroleprp> Display_rec(Int32 urid)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspuserrole", con);
            cmd.Parameters.Add("@urid", SqlDbType.Int).Value = urid;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsUserroleprp> obj = new List<clsUserroleprp>();
            while (dr.Read())
            {
                clsUserroleprp k = new clsUserroleprp();
                k.p_userroleid = Convert.ToInt32(dr[0]);
                k.p_userroledesc = dr[1].ToString();
                k.p_userroledelsts = Convert.ToBoolean(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        public List<clsUserroleprp> Find_rec(Int32 urid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fnduserrole", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@urid", SqlDbType.Int).Value = urid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsUserroleprp> obj = new List<clsUserroleprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsUserroleprp k = new clsUserroleprp();
                k.p_userroleid = Convert.ToInt32(dr[0]);
                k.p_userroledesc = dr[1].ToString();
                k.p_userroledelsts = Convert.ToBoolean(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
    /*---------------------------------------------------------------------*/
    public class ERP:sqlconnect
    {      
       
        public static DataSet AllSession()
        {
            SqlDataAdapter adp = new SqlDataAdapter("dispsession", con);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds;
        }
        public static DataSet AllCourse()
        {
            SqlDataAdapter adp = new SqlDataAdapter("dispcourse", con);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds;
        }
        public static DataSet AllDepartment(int cid)
        {
            SqlCommand cmd = new SqlCommand("dispdept");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds;
        }
        public static DataSet AllDepartment1()
        {
            SqlCommand cmd = new SqlCommand("dispdept1");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds;
        }
        public  static DataSet AllClass(Int32 did, Int32 sid)
        {
            SqlCommand cmd = new SqlCommand("dispclass");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds;
        }
        public static DataSet AllClassHOD(Int32 did, Int32 sid,Int32 tid)
        {
            SqlCommand cmd = new SqlCommand("dispclasshod");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            cmd.Parameters.Add("@tid", SqlDbType.Int).Value = tid;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds;
        }
        public static DataSet AllClassDIR(Int32 sid)
        {
            SqlCommand cmd = new SqlCommand("dispclassdir");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            //cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            //cmd.Parameters.Add("@tid", SqlDbType.Int).Value = tid;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds;
        }
        public static DataSet AllClassAdmin(Int32 sid)
        {
            SqlCommand cmd = new SqlCommand("dispclassADmin");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds;
        }
        public static DataSet AllGroup(Int32 cid)
        {
            SqlCommand cmd = new SqlCommand("dispgroup");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds;
        }
        public static DataSet TeacherClass(Int32 tid, Int32 sid)
        {
            SqlCommand cmd = new SqlCommand("dispteacherclass");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = con;
            cmd.Parameters.Add("@tid", SqlDbType.Int).Value = tid;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
           // cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds;
        }
        public static DataSet TeacherSubject(Int32 tid, Int32 gid)
        {
            SqlCommand cmd = new SqlCommand("DispTeacherSubject", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@tid", SqlDbType.Int).Value = tid;
            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            cmd.Dispose();
            return ds;
        }
        public  static DataSet DispSessional(Int32 sno, Int32 sid, Int32 gid)
        {
            SqlCommand cmd = new SqlCommand("DispSessional", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sno", SqlDbType.Int).Value = sno;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            cmd.Dispose();
            return ds;
        }
        public static DataSet DispSessionalsubmit(Int32 sno, Int32 sid, Int32 gid)
        {
            SqlCommand cmd = new SqlCommand("DispSessionalsubmit", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sno", SqlDbType.Int).Value = sno;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            cmd.Dispose();
            return ds;
        }
        public static DataSet DispSessionalHOD(Int32 sno, Int32 sid, Int32 gid)
        {
            SqlCommand cmd = new SqlCommand("DispSessionalHOD", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sno", SqlDbType.Int).Value = sno;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            cmd.Dispose();
            return ds;
        }
        public static DataSet DispSessionalDIR(Int32 sno, Int32 sid, Int32 gid)
        {
            SqlCommand cmd = new SqlCommand("DispSessionalDIR", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sno", SqlDbType.Int).Value = sno;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            cmd.Dispose();
            return ds;
        }
        public static DataSet search(Int32 cid, Int32 rno, string name)
        {
            SqlCommand cmd = new SqlCommand("aaa", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value =name;
            cmd.Parameters.Add("@rno", SqlDbType.Int).Value = rno;
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds;
        }
        public static DataSet det(Int32 s,Int32 r)
        {
            SqlCommand cmd = new SqlCommand("dispsearch", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = s;
            cmd.Parameters.Add("@sts", SqlDbType.Int).Value = r;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds;
        }
        public Int32 ChangePWD(Int32 uid, String OldPwd, string NewPwd)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("ChangePwd", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@uid", SqlDbType.Int).Value = uid;
            cmd.Parameters.Add("@oldpwd", SqlDbType.VarChar, 50).Value = OldPwd;
            cmd.Parameters.Add("@Newpwd", SqlDbType.VarChar, 50).Value = NewPwd;
            cmd.Parameters.Add("@ret", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();
            Int32 k = Convert.ToInt32(cmd.Parameters["@ret"].Value);
            cmd.Dispose();
            con.Close();
            return k;
        }
    }
}