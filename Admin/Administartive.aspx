﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/InnerAdmin.master" AutoEventWireup="true" CodeFile="Administartive.aspx.cs" Inherits="Admin_Administartive" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="styledmenu">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 18px">
            </td>
            <td colspan="3" style="height: 18px" align="center">
                <h1>Appoint/Modify Management</h1></td>
            <td style="height: 18px">
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="3" align="center">
                <asp:RadioButtonList ID="RBLOpt" runat="server" AutoPostBack="True" 
                    Font-Bold="True" RepeatDirection="Horizontal" 
                    onselectedindexchanged="RBLOpt_SelectedIndexChanged">
                    <asp:ListItem Value="0">Director</asp:ListItem>
                    <asp:ListItem Value="1">Dean Acedamic Affairs</asp:ListItem>
                    <asp:ListItem Value="2">Head Of Departments</asp:ListItem>
                    <asp:ListItem Value="3">Training &amp; Placement Officer</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        </table>
        <table class="styledmenu">
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="3">
                
                    <table class="styledmenu">
                        <tr>
                            <td align="left" style="width: 300px">
                                Course</td>
                            <td style="width: 155px">
                                :</td>
                            <td align="left">
                                <asp:DropDownList ID="DDLCourse" runat="server" AutoPostBack="True" 
                                    Width="200px" onselectedindexchanged="DDLCourse_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align=left style="width: 300px">
                                Department</td>
                            <td style="width: 155px">
                                :</td>
                            <td align=left>
                                <asp:DropDownList ID="DDLDept" runat="server" Width="200px" AutoPostBack="True" 
                                    onselectedindexchanged="DDLDept_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 300px">
                                Current
                                <asp:Label ID="Label1" runat="server"></asp:Label>
                            </td>
                            <td style="width: 155px">
                                :</td>
                            <td align="left">
                                <asp:DropDownList ID="DDLSts" runat="server" Width="200px" Enabled="False">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left" style="width: 300px; height: 18px;">
                                </td>
                            <td style="width: 155px; height: 18px;">
                                </td>
                            <td align="left" style="height: 18px">
                                <asp:LinkButton ID=lbremove runat=server Text="Transfer/Terminate" 
                                    Enabled="False" onclick="lbremove_Click"></asp:LinkButton></td>
                            <td align="left" style="height: 18px">
                                </td>
                        </tr>
                        <tr>
                            <td align=left style="width: 300px">
                                Teachers</td>
                            <td style="width: 155px">
                                :</td>
                            <td align=left>
                                <asp:DropDownList ID="DDLTeachers" runat="server" Width="300px">
                                </asp:DropDownList>
                            </td>
                            <td align="left">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="height: 18px; width: 300px;">
                                </td>
                            <td style="width: 155px;" class="unwatermarked">
                                </td>
                            <td style="height: 18px">
                                </td>
                            <td style="height: 18px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 300px">
                                &nbsp;</td>
                            <td style="width: 155px">
                                &nbsp;</td>
                            <td align=left>
                                <asp:Button ID="Btnmodify" runat="server" Text="Allot/Modify" Height="30px" 
                                    Width="150px" onclick="Btnmodify_Click" Enabled="False" />
                            </td>
                            <td align="left">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 300px">
                                &nbsp;</td>
                            <td style="width: 155px">
                                &nbsp;</td>
                            <td align=left>
             <asp:Label ID="Lblmsg" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="#336699"></asp:Label>
                            </td>
                            <td align="left">
                                &nbsp;</td>
                        </tr>
                    </table>
               
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="3">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="3">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

