﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;

public partial class dir_Barcode : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        barimage(Request.QueryString["a"]);
    }
    private void barimage(string s)
    {
        if (Request.QueryString["a"] != null)
        {
            Int32 w = s.Length * 30;
            Int32 h = 100;
            Bitmap bmp = new Bitmap(w, h);
            Graphics gr = Graphics.FromImage(bmp);
            Font fnt = new Font("IDAutomationHC39M", 18);
            PointF pntf = new PointF(1, 1);
            SolidBrush brwr = new SolidBrush(Color.Black);
            SolidBrush br = new SolidBrush(Color.White);
            gr.FillRectangle(br, 0, 0, w, w);
            gr.DrawString(s, fnt, brwr, pntf);
            Response.ContentType = "image/jpeg";
            bmp.Save(Response.OutputStream, ImageFormat.Jpeg);
        }
    }
}