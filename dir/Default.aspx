﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dir/MasterPage.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="dir_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
        <style type="text/css">
            .abc
            {
                font-size: larger;
                font-weight: bold;
                cursor: pointer;
            }
            .xyz
            {
                display: none;
                visibility: hidden;
            }
            .black_overlay
            {
                display: none;
                position: absolute;
                top: 4%;
                left: 0%;
                width: 100%;
                height: 155%;
                background-color: black;
                z-index: auto;
                overflow: auto;
                -moz-opacity: 0.8;
                opacity: 100;
                filter: alpha(opacity=100);
            }
            .white_content
            {
                display: none;
                position: absolute;
                top: 10%;
                left: 19%;
                width: 880px;
                padding: 0px;
                border: 0px solid #a6c25c;
                background-color: white;
                z-index: 1002;
                overflow: auto;
            }
            .headertext
            {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
                color: #f19a19;
                font-weight: bold;
            }
            .newsReadmore a
            {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 12px;
                padding-right: 10px;
                color: #CCCCCC;
            }
            .newsReadmore a:hover
            {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 12px;
                padding-right: 10px;
                color: #ffffff;
            }
            .news
            {
                margin: 0px 0px 0px 9px;
                padding: 0px;
                width: 865px;
                float: left;
            }
            .news_bck
            {
                margin: 10px 0px 0px 8px;
                padding: 0px;
                width: 241px;
                height: 220px;
                float: left;
                background: url(news_bck.png) no-repeat;
            }
            .news_head
            {
                margin: 5px 0px 0px 7px;
                padding: 0px;
                width: 227px;
                height: 44px;
                float: left;
            }
            .message
            {
                margin: 0px 0px 0px 10px;
                padding: 0px;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 24px;
                color: #186fbe;
                float: left;
            }
            .message_img
            {
                margin: 0px;
                padding: 0px;
                float: right;
                width: 46px;
                height: 46px;
            }
            .line
            {
                margin: 0px 0px 0px 15px;
                padding: 0px;
                width: 209px;
                height: 1px;
                float: left;
                background: url(line.png) no-repeat;
            }
            .navi
            {
                margin: 15px 0px 0px 0px;
                padding: 0px;
                width: 240px;
                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
                color: #333333;
                float: left;
            }
            .navi ul
            {
                margin: 0px;
                padding: 0px 0px 0px 0px;
            }
            .navi ul li
            {
                margin: 0px;
                padding: 5px 0px 5px 10px;
                list-style: none;
                background: #333333;
                border-bottom: 1px solid #FFFFFF;
            }
            .navi ul li:hover
            {
                background: #0066FF;
                border-bottom: 1px solid #FFFFFF;
            }
            .navi ul li a
            {
                color: #fff;
                text-decoration: none;
            }
            
            
            .bodymain
            {
                background-image: url(RES/BG.jpg);
                background-repeat: repeat-x;
                background-color: #fff;
                font: Geneva, Arial, Helvetica, sans-serif;
                font-size: 12px;
                color: #224466;
                line-height: 20px;
                margin-left: 0px;
                margin-top: 0px;
            }
            .style1
            {
                color: #dd0000;
                font-family: Geneva, Arial, Helvetica, sans-serif;
                font-weight: bold;
                line-height: 12px;
            }
            .SmallGray SmallGray:link
            {
                color: #CCCCCC;
                font-size: 10px;
            }
            
            .SmallLinks
            {
                color: #ff0000;
            }
            
            .SmallLinks a
            {
                color: #224466;
                font-size: 12px;
                font-size: bold;
                font-family: Geneva, Arial, Helvetica, sans-serif;
                text-decoration: underline;
            }
            
            .FltrTxt
            {
                background-image: url('RES/imgTxtBox_BG.gif');
                -moz-border-radius: 5px;
                -khtml-border-radius: 5px;
                -webkit-border-radius: 5px;
            }
            
            .cmd
            {
                border-style: none;
                border-color: inherit;
                border-width: medium;
                background-image: url('RES/imgCmd_BG.gif');
                color: #fff;
                height: 26px;
                font-weight: bold;
                -moz-border-radius: 5px;
                -khtml-border-radius: 5px;
                -webkit-border-radius: 5px;
            }
            a
            {
                text-decoration: none;
            }
            .sDate
            {
                font-size: 10px;
                font-family: Geneva, Arial, Helvetica, sans-serif;
                color: #FF0000;
                font-weight: bold;
            }
            .accordian_bck
            {
                margin: 5px 0px 5px 0px;
                padding: 10px;
                width: 220px;
                height: 339px;
                float: left;
                background: #323232;
                -moz-border-radius: 10px;
                font-family: Arial, Helvetica, sans-serif;
            }
            
            
            .text
            {
                padding: 0px;
            }
            .text-left
            {
                width: 250px;
                float: right;
            }
            .text-center
            {
                width: 500px;
                float: left;
                padding: 40px 0px 0px 0px;
            }
            .style34
            {
                width: 865px;
                height: 504px;
            }
        </style>
        <script type="text/javascript" language="javascript">
            //        function GetData(id) {
            //            var e;
            //            e = document.getElementById('d' + id);
            //            if (e) {

            //                if (e.style.display != "block") {
            //                    e.style.display = "block";
            //                    e.style.visibility = "visible";
            //                }
            //                else {
            //                    e.style.display = "none";
            //                    e.style.visibility = "hidden";
            //                }
            //            }

            //        }
            //        function GetData1(id) {
            //            var e1;
            //            e1 = document.getElementById('g' + id);
            //            if (e1) {

            //                if (e1.style.display != "block") {
            //                    e1.style.display = "block";
            //                    e1.style.visibility = "visible";
            //                }
            //                else {
            //                    e1.style.display = "none";
            //                    e1.style.visibility = "hidden";
            //                }
            //            }

            //        }
            function ShowImages() {
                document.getElementById('light').style.display = 'block';
                document.getElementById('fade').style.display = 'block'
                return false;
            }
        </script>
    
   
    <table>
        <tr>
            <td style="border: 1px solid #2D4262; width: 865px; height: 125px">
                <asp:Panel ID="Panel1" runat="server">
                    <table class="simplemenu">
                        <tr>
                            <td align="left">
                                <asp:Image ID="Image2" runat="server" Width="100px" Height="100px" ImageUrl="~/dir/pj.jpg" />
                            </td>
                            <td>
                                <b>Remainder's</b><br />
                                <asp:FormView ID="__schedulefrm" runat="server" DataSourceID="SqlDataSource1" AllowPaging="true"
                                    DataKeyNames="entrydate">
                                    <EmptyDataTemplate>
                                        <asp:LinkButton ID="_newbtn" CommandName="New" runat="server" Text="Add Entry"></asp:LinkButton>
                                    </EmptyDataTemplate>
                                    <ItemTemplate>
                                        <h1>
                                            <%# Eval("entrydate", "{0:D}")%></h1>
                                        <%# Eval("entry") %>
                                        <br />
                                        <br />
                                        <asp:LinkButton ID="_editbtn" Text="Edit Entry" CommandName="Edit" runat="server"></asp:LinkButton>
                                        <asp:LinkButton ID="_deletebtn" Text="Delete Entry" CommandName="Delete" OnClientClick="return confirm('Delete Entry?');"
                                            runat="server"></asp:LinkButton>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="_entrylbl" Text="Entry" AssociatedControlID="_entrytb" runat="server"></asp:Label>
                                        <br />
                                        <asp:TextBox ID="_entrytb" Text='<%#Bind("entry") %>' TextMode="MultiLine" Columns="40"
                                            Rows="8" runat="server"></asp:TextBox>
                                        <br />
                                        <asp:LinkButton ID="_updatebtn" runat="server" CommandName="Update" Text="Update Entry"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                        <asp:Label ID="_entrylbl" Text="Entry" AssociatedControlID="_entrytb" runat="server"></asp:Label>
                                        <asp:TextBox ID="_entrytb" Text='<%# Bind("entry") %>' TextMode="MultiLine" Columns="40"
                                            Rows="8" runat="server"></asp:TextBox><br />
                                        <asp:Button ID="_insertbtn" Text="Insert" CommandName="Insert" runat="server" />
                                        <asp:Button ID="_cancelbtn" Text="Cancel" CommandName="Cancel" runat="server" />
                                    </InsertItemTemplate>
                                </asp:FormView>
                            </td>
                            <td style="height: 95px;" align="right">
                                <div>
                                    <asp:Calendar ID="_cal" runat="server" OnDayRender="calSchedule_DayRender"></asp:Calendar>
                                    <br />
                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:smsConnectionString %>"
                                        SelectCommand="SELECT [entrydate], [entry] FROM [calendar_tbl] WHERE ([entrydate] = @entrydate)"
                                        InsertCommand="Insert calendar_tbl(entrydate,entry) values(@entrydate,@entry)"
                                        UpdateCommand="Update calendar_tbl set entry=@entry where entrydate=@entrydate"
                                        DeleteCommand="Delete calendar_tbl where entrydate=@entrydate">
                                        <SelectParameters>
                                            <asp:ControlParameter Name="Entrydate" ControlID="_cal" PropertyName="SelectedDate" />
                                        </SelectParameters>
                                        <InsertParameters>
                                            <asp:ControlParameter Name="entrydate" ControlID="_cal" PropertyName="SelectedDate" />
                                        </InsertParameters>
                                    </asp:SqlDataSource>
                                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:smsConnectionString %>"
                                        SelectCommand="Select entrydate from calendar_tbl"></asp:SqlDataSource>
                               <asp:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender3" runat="server"
        TargetControlID="_cal" VerticalSide="Middle" HorizontalSide="left" Enabled="True">
    </asp:AlwaysVisibleControlExtender>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table class="styledmenu">
                                    <tr>
                                        <td align="left">
                                            Dr. P. J. George<br />
                                            Director-Principal
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>  
         
    
    
    <table >
        <tr>
        <td style="border:1px solid #2D4262; " class="style34">
        
            <table >
            <th align="center" style="border:1px solid #2D4262; width:280px"  bgcolor="#2D4262"><b style="color:White">NoticeBoard</b></th>
             <th align="center"  style="border:1px solid #2D4262; width:280px" bgcolor="#2D4262"><b style="color:White">Alerts</b></th>
             <th align="center"  style="border:1px solid #2D4262; width:280px" bgcolor="#2D4262"><b style="color:White">Events</b></th>
                <tr>
                    <td valign="top" rowspan="2"  align="left"  style="border:1px solid #2D4262;text-align:left;margin-top:10px; width:280px;height:500px;float:none ">
                         <center>
                         <asp:DataList ID="DataList1" runat="server"  DataKeyField="noticeid" 
                             onitemcommand="DataList1_ItemCommand" style="text-align: center" 
                             Width="100%"  >
                <ItemTemplate>
              <table width="100%" style="text-align: left; font-weight: bold;"><tr>
              
                 <td><asp:Label ID="l1" Text='<%# Eval("noticesubject") %>' runat="server" 
                        Font-Bold="True" Font-Size="Smaller" ForeColor="#000066"/>    </td>
               <td><asp:Label ID="l2" Text='<%# Eval("noticedate","{0:d}") %>' runat="server" 
                        Font-Bold="True" Font-Size="Smaller" ForeColor="#000066"/></td>
                <td><asp:ImageButton ID="imgbtn" ImageUrl="~/images/info.png" Height="10px" 
                        Width="10px" runat="server" CommandName="Show" /></td>
              </tr></table>
                </ItemTemplate>
                </asp:DataList></center>
                </td>
                    <td valign="top" style="border:1px solid #2D4262;text-align:left;margin-top:10px; width:280px;height:250px" align="left">
                        <center>
                         <asp:DataList ID="DataList2" runat="server" 
                             onitemcommand="DataList2_ItemCommand" DataKeyField="noticeid" 
                             style="text-align: center" Width="100%" >
                <ItemTemplate>
                <table width="100%" style="text-align: left; font-weight: bold;"><tr>
              
                 <td>
                 <asp:Label ID="l3" Text='<%# Eval("noticesubject") %>' runat=server 
                        Font-Bold="True" Font-Size="Smaller" ForeColor="#000066"></asp:Label></td>
                 <td><asp:Label ID="l4" Text='<%# Eval("noticedate","{0:d}") %>' runat=server 
                        Font-Bold="True" Font-Size="Smaller" ForeColor="#000066"></asp:Label></td>
                <td><asp:ImageButton ID="imgbtn1" ImageUrl="~/images/info.png" Height="10px" 
                        Width="10px" runat="server" CommandName="Show" /></td></tr></table>
                </ItemTemplate>
               
                </asp:DataList></center></td>
                    <td style="border:1px solid #2D4262; width:280px;height:250px">
                        <table border="0" cellpadding="2" cellspacing="2" style="margin-top: 0px; border: 1px solid #CCCCCC;
        padding: 1px;" width="350px">
                            <tr>
                                <td valign="top" style="height:250px">
                                    
                                        <asp:DataList ID="DataList3" runat="server" DataKeyField="Eventid" 
                                            onitemcommand="DataListE_ItemCommand" RepeatColumns="5" 
                                            RepeatDirection="Horizontal" ShowFooter="False" Width="100%">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="Image1" runat="server" CommandName="Select" Height="50px" 
                                                                ImageUrl='<%#"~/event/"+Eval("Eventlogo") %>' 
                                                                ToolTip='<%# Eval("EventNAme") %>' Width="50px" />
                                                        </td>
                                                        <td>
                                                            <h2>
                                                            </h2>
                                        <br />
                                        <br />
                                                        </td>
                                                    </tr>
                                                </table>
                            <br />
                            <br />
                                            </ItemTemplate>
                                        </asp:DataList>
                                   
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>                
                <td colspan='2' style="border:1px solid #2D4262; width:720px;height:250px">
                    <table>
                <th align="center" style="border:1px solid #2D4262; width:720px"  bgcolor="#2D4262">
                    <b style="color:White">Teacher&#39;s Attendance</b></th>
                <tr>
                <td align="center" valign=top style="margin: 10px 0px 10px; padding: 0px; width: 560px; height: 240px; 
                background: #FFFFFF; border: 1px solid #0066FF; border-top-left-radius: 41px; border-top-right-radius: 41px;">
                <%--<asp:Image ID="ads" runat="server" ImageUrl="~/dir/events_img.png"  height="240px" Width="55px"/>--%>
<center>
    
<asp:DataList ID="DataList4" runat="server" RepeatColumns="9" 
            RepeatDirection="Horizontal" onitemdatabound="DataList1_ItemDataBound" 
        onselectedindexchanged="DataList4_SelectedIndexChanged" Width="100%">
        <ItemTemplate>
                            <table>
                            <tr>
                            <td>
                            <asp:Label ID="lbl1" runat="server" Visible="false" Text='<%#Eval("teachattdtypeid") %>' />
                                <asp:Image ID="Image1" runat="server" ImageUrl='<%#"~/Teacherpic/" + Eval("teacherphoto") %>' ToolTip='<%#Eval("name")+ "-"+Eval("teachattdtypedesc") %>' Height="45px" Width="45px" BorderColor="gold" BorderWidth="2px"/>
                                <br />
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("name") %>' 
                                    Font-Bold="True" Font-Size="Smaller" ForeColor="#000066" />
                            </td>
                            <td>                                                          
                            
                                
                            
                            </td>
                            </tr>
                            </table>
                            </ItemTemplate>
                            
        </asp:DataList><br />
        <asp:Label ID="Label3" runat="server" Font-Bold="True" ForeColor="#000099" 
        Visible="False" Font-Size="Large"></asp:Label>
</center>
    
            </td>
                      
                      </tr></table>
                    
                </tr>
            </table>
        
        </td>
            
        </tr>
    </table>
    
<div id="light" class="white_content">
     <table cellpadding=0 cellspacing=0 border=0 style="background-color:#a6c25c;" width="100%">
        <tr>
            <td height="16px"><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"><img src="../close.gif" style="border :0px"  width="13px" align="right" height="13px"/></a></td>
        </tr>
        <tr>
            <td style="padding-left:16px;padding-right:16px;padding-bottom:16px"> 
                <table align="center"  border="0" cellpadding="0" cellspacing="0" style="background-color:#fff" width="100%">
                    <tr>
                        <td align="center" colspan="2" class="headertext" ></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Repeater ID="imglightbox" runat="server">
                                <ItemTemplate>
                
                                    <p align="left" ><%#Eval("noticesubject") %><br />       
                                    <i><%#Eval("noticedate","{0:d}") %></i></p><br /><br />
                                    <%#Eval("noticecontent") %><br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td height="10px"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div align="center" class=" headertext">
    </div>
</div>
<div id="fade" class="black_overlay">
</div>   
    
    

   <asp:Panel ID="g1" runat="server" ScrollBars=Auto >

    <asp:GridView ID="GridTeacher" runat="server" AutoGenerateColumns="False"
        CellPadding="4" DataKeyNames="appid" ForeColor="#333333" GridLines="None" 
            EmptyDataText="No Appointments Pending!!" ShowHeaderWhenEmpty="True">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <EditRowStyle BackColor="#999999" />
        <EmptyDataRowStyle Font-Bold="True" ForeColor="White" />
        <EmptyDataTemplate>
        No Appointment Today till Now!!
        </EmptyDataTemplate>
        <Columns>
            <asp:TemplateField HeaderText="Today's" InsertVisible="False" 
                >
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("appdate") %>'></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Appoinment" >
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("appwithname") %>'></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
           
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    </asp:Panel>
    <asp:SqlDataSource ID="level" runat="server" ConnectionString="<%$ ConnectionStrings:cn %>"
        SelectCommand="SELECT [subjectid], [subjecttitle] FROM [tbsubject]"></asp:SqlDataSource>
    <asp:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
        TargetControlID="g1" VerticalSide="Bottom" HorizontalSide="Right">
    </asp:AlwaysVisibleControlExtender>
   </asp:Content>
