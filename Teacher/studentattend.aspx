﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="studentattend.aspx.cs" Inherits="Teacher_studentattend" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
          <table width="100%" style="text-align: left; font-weight: bold;">
       
        <tr>
            <td >
                Session</td>
            <td>
                <asp:DropDownList ID="DDLSession" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLSession_SelectedIndexChanged" Width="300px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Class</td>
            <td>
                <asp:DropDownList ID="DDLClass" runat="server" AutoPostBack="True" 
                    Enabled="False" onselectedindexchanged="DDLClass_SelectedIndexChanged" 
                    Width="300px">
                    <asp:ListItem Value="0">-- Select Class --</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td >
                Subject</td>
            <td>
                <asp:DropDownList ID="DDLSubject" runat="server" Enabled="False" Width="300px" 
                    onselectedindexchanged="DDLSubject_SelectedIndexChanged" 
                    AutoPostBack="True">
                    <asp:ListItem Value="0">-- Choose Subject --</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td >
                Total No. Of Periods Delivered</td>
            <td>
            <asp:Label ID="LBLTotalDelivered" runat="server"></asp:Label>
            </td>
        </tr>
         <tr>
            <td >
                Choose Option</td>
            <td align="left">
                <asp:RadioButtonList ID="RBLOpt" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="RBLOpt_SelectedIndexChanged" 
                    RepeatDirection="Horizontal" Enabled="False">
                    <asp:ListItem Value="0">Fill</asp:ListItem>
                    <asp:ListItem Value="1">View</asp:ListItem>
                </asp:RadioButtonList>
               
                </td>
        </tr>
              <tr>
                  <td colspan="2">
                      <asp:Label ID="LBLmsg" runat="server" Font-Bold="True" ForeColor="#003300"></asp:Label>
                  </td>
              </tr>
        </table>
              <asp:Panel ID="Panelfill" runat="server" Visible="False">
        <table width="100%" style="text-align: left; font-weight: bold;">
          <tr>
            <td style="width: 345px"> Date :
             </td>
            <td> 
                    <asp:TextBox ID="TxtDate" runat="server" Width="300px"></asp:TextBox>
                  <asp:CalendarExtender Format="dd/MM/yyyy "  ID="TxtDate_CalendarExtender" runat="server" 
                      Enabled="True" TargetControlID="TxtDate">
                  </asp:CalendarExtender>
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                      ControlToValidate="TxtDate" ErrorMessage="RequiredFieldValidator" 
                      Font-Bold="True" ForeColor="Red" ValidationGroup="abc">*</asp:RequiredFieldValidator>
                  <asp:CompareValidator ID="CompareValidator3" runat="server" 
                      ControlToValidate="TxtDate" ErrorMessage="CompareValidator" Font-Bold="True" 
                      ForeColor="Red" Operator="DataTypeCheck" Type="Date" ValidationGroup="abc">*</asp:CompareValidator>
            </td>   
        </tr>
                          <tr>
                          <td colspan="2">
                          <div  style="overflow:scroll;height:300px;visibility:inherit">
                              <asp:GridView ID="GridFill" runat="server" AutoGenerateColumns="False" 
                                  CellPadding="4" DataKeyNames="studentid" ForeColor="#333333" GridLines="None" 
                                  onrowdatabound="GridFill_RowDataBound" Width="100%">
                                  <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                  <Columns>
                                      <asp:TemplateField HeaderText="Roll No">
                                          <ItemTemplate>
                                              <asp:Label ID="LblRollNo" runat="server" Text='<%# Eval("studentRollno") %>'></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Attendance">
                                          <ItemTemplate>
                                              <asp:RadioButtonList ID="RBLAttendance" runat="server" 
                                                  RepeatDirection="Horizontal">
                                                  <asp:ListItem Selected="True" Value="True">Present</asp:ListItem>
                                                  <asp:ListItem Value="false">Absent</asp:ListItem>
                                              </asp:RadioButtonList>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Total Attended">
                                          <ItemTemplate>
                                              <asp:Label ID="LblAt" runat="server"></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="%age">
                                          <ItemTemplate>
                                              <asp:Label ID="Lblpercent" runat="server"></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                  </Columns>
                                  <EditRowStyle BackColor="#999999" />
                                  <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                  <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                  <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                  <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                  <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                  <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                  <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                  <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                  <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                              </asp:GridView></div>
                              <asp:LinkButton ID="LinkButton2" runat="server" onclick="LinkButton2_Click" 
                                  ValidationGroup="abc">Save</asp:LinkButton>
                              </td>
                              
                          </tr>
                      </table>
    </asp:Panel>
              <asp:Panel ID="Panelview" runat="server" Visible="False">     
          <table width="100%" style="text-align: left; font-weight: bold;">
          <tr>
          <td></td>
             <td align="left">
                      <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" 
                          onselectedindexchanged="RadioButtonList1_SelectedIndexChanged">
                          <asp:ListItem Value="0">Not Approved</asp:ListItem>
                          <asp:ListItem Value="1">Approved</asp:ListItem>
                      </asp:RadioButtonList>
                      </td></tr>
                      <tr>
                          <td style="width: 345px">
                              Date :</td>
                          <td align="left">
                              <asp:DropDownList ID="DDLDate" runat="server" AutoPostBack="True" 
                                  DataTextFormatString="{0:D}" 
                                  onselectedindexchanged="DDLDate_SelectedIndexChanged" Width="300px">
                                  <asp:ListItem>-- Choose Date --</asp:ListItem>
                              </asp:DropDownList>
                          </td>
                      </tr>
                      </table>
 </asp:Panel>
              <asp:Panel ID="Panel1" runat="server" Visible="False">
                        <table width="100%" style="text-align: left; font-weight: bold;">
                                      <tr>
                                          <td colspan="2" >
                                          <div  style="overflow:scroll;height:300px;visibility:inherit">
                                              <asp:GridView ID="GridShow" runat="server" AutoGenerateColumns="False" 
                                                  CellPadding="4" DataKeyNames="studentid" ForeColor="#333333" GridLines="None" 
                                                  onrowcancelingedit="GridShow_RowCancelingEdit" 
                                                  onrowdatabound="GridShow_RowDataBound" onrowediting="GridShow_RowEditing" 
                                                  onrowupdating="GridShow_RowUpdating" Width="100%" >
                                                  <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                                  <Columns>
                                                      <asp:TemplateField HeaderText="Roll No">
                                                          <ItemTemplate>
                                                              <asp:Label ID="LblRollNo" runat="server" Text='<%# Eval("studentRollno") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="Attendance">
                                                          <EditItemTemplate>
                                                              <asp:RadioButtonList ID="RBLAttendance" runat="server" 
                                                                  RepeatDirection="Horizontal">
                                                                  <asp:ListItem Selected="True" Value="True">Present</asp:ListItem>
                                                                  <asp:ListItem Value="false">Absent</asp:ListItem>
                                                              </asp:RadioButtonList>
                                                          </EditItemTemplate>
                                                          <ItemTemplate>
                                                              <asp:Label ID="LBLAttend" runat="server" 
                                                                  Text='<%# Eval("studentattndattend") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="Total Attended">
                                                          <ItemTemplate>
                                                              <asp:Label ID="LblAt" runat="server"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="%age">
                                                          <ItemTemplate>
                                                              <asp:Label ID="Lblpercent" runat="server"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateField>
                                                      <asp:TemplateField>
                                                          <EditItemTemplate>
                                                              <asp:LinkButton ID="LBUpdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                                              &nbsp;&nbsp;&nbsp;
                                                              <asp:LinkButton ID="LBCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                          </EditItemTemplate>
                                                          <ItemTemplate>
                                                              <asp:LinkButton ID="LBEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                          </ItemTemplate>
                                                      </asp:TemplateField>
                                                  </Columns>
                                                  <EditRowStyle BackColor="#999999" />
                                                  <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                  <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                  <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                                  <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                                  <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                  <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                                  <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                                  <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                                  <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                              </asp:GridView></div>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td style="width: 345px">
                                              Approve<strong> upto:</strong> 
                                              
                                              </td>
                                              <td><asp:DropDownList ID="DDLdatesubmithod" 
                                                  runat="server" 
                                                  onselectedindexchanged="DDLdatesubmithod_SelectedIndexChanged" 
                                                  Width="300px" AutoPostBack="True" DataTextFormatString="{0:D}">
                                                  <asp:ListItem Value="0">-- Choose Date --</asp:ListItem>
                                              </asp:DropDownList>

                                                  &nbsp;<asp:LinkButton ID="LnkBtmSubmitHod" runat="server" 
                                                  onclick="LnkBtmSubmitHod_Click" Visible="False">Approve</asp:LinkButton>
                                          </td>
                                      </tr>
                                  </table>
  </asp:Panel>
  <asp:Panel ID="Panel2" runat="server" Visible="False">
                         <table width="100%" style="text-align: left; font-weight: bold;" >
                         <tr>
                          <td >
                          <div  style="overflow:scroll;height:300px;visibility:inherit">
                              <asp:GridView ID="GridShowSubmit" runat="server" AutoGenerateColumns="False" 
                                  CellPadding="4" DataKeyNames="studentid" ForeColor="#333333" GridLines="None" 
                                  onrowdatabound="GridShowSubmit_RowDataBound" Width="100%">
                                  <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                  <Columns>
                                      <asp:TemplateField HeaderText="Roll No">
                                          <ItemTemplate>
                                              <asp:Label ID="LblRollNo" runat="server" Text='<%# Eval("studentRollno") %>'></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Attendance">
                                          <ItemTemplate>
                                              <asp:Label ID="LBLAttend" runat="server" 
                                                  Text='<%# Eval("studentattndattend") %>'></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Total Attended">
                                          <ItemTemplate>
                                              <asp:Label ID="LblAt" runat="server"></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="%age">
                                          <ItemTemplate>
                                              <asp:Label ID="Lblpercent" runat="server"></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                  </Columns>
                                  <EditRowStyle BackColor="#999999" />
                                  <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                  <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                  <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                  <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                  <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                  <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                  <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                  <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                  <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                              </asp:GridView></div>
                          </td>
                              
                        </tr>
                        </table>
  </asp:Panel>         
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

