﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;
using System.IO;
using System.Web.Services.Protocols;
using AjaxControlToolkit;


public partial class Admin_regstudent : System.Web.UI.Page
{
    
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        //CascadingDropDown cc1 = ((CascadingDropDown)(DetailsView1.Rows[2].FindControl("CascadingDropDown1")));
        //cc1.ContextKey = Convert.ToString(Session["other"]); 
        if (Page.IsPostBack == false)
        {
            seserp.DropdownWebService obj = new seserp.DropdownWebService();
            obj.sum(Convert.ToInt32(Session["other"]));
        }
        
    }
    protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
        // Int32 rno, gid, cid, p1, p2, tenth, twelth;
        // string regno, fname, mname, lname, mtname, ftname, corres, perma, email, photo, blood, achieve, skill, training, hobbies, resume;
        Int32 gid, cid, tenth;
        Int64 p1;
        string fname, mtname, ftname, corres, userlogid;
        DateTime d1;
        Boolean hostler;
        TextBox rno = ((TextBox)(DetailsView1.Rows[0].FindControl("TextBox2")));
        TextBox regno = ((TextBox)(DetailsView1.Rows[1].FindControl("TextBox4")));
        gid = Convert.ToInt32(((DropDownList)(DetailsView1.Rows[5].FindControl("ddlGroup"))).SelectedValue);
        fname = ((TextBox)(DetailsView1.Rows[6].FindControl("TextBox6"))).Text;
        TextBox mname = ((TextBox)(DetailsView1.Rows[7].FindControl("TextBox8")));
        TextBox lname = ((TextBox)(DetailsView1.Rows[8].FindControl("TextBox10")));
        mtname = ((TextBox)(DetailsView1.Rows[9].FindControl("TextBox12"))).Text;
        ftname = ((TextBox)(DetailsView1.Rows[10].FindControl("TextBox14"))).Text;
        d1 = Convert.ToDateTime(((TextBox)(DetailsView1.Rows[11].FindControl("TextBox16"))).Text);
        cid = Convert.ToInt32(((DropDownList)(DetailsView1.Rows[12].FindControl("dropdownlist10"))).SelectedValue);
        corres = ((TextBox)(DetailsView1.Rows[13].FindControl("TextBox18"))).Text;
        TextBox perma = ((TextBox)(DetailsView1.Rows[14].FindControl("TextBox20")));
        p1 = Convert.ToInt64(((TextBox)(DetailsView1.Rows[15].FindControl("TextBox22"))).Text);
        TextBox p2 = ((TextBox)(DetailsView1.Rows[16].FindControl("TextBox24")));
        TextBox email = ((TextBox)(DetailsView1.Rows[17].FindControl("TextBox26")));
        FileUpload photo = ((FileUpload)(DetailsView1.Rows[18].FindControl("FileUpload2")));
        tenth = Convert.ToInt32(((TextBox)(DetailsView1.Rows[19].FindControl("TextBox28"))).Text);
        TextBox twelth = ((TextBox)(DetailsView1.Rows[20].FindControl("TextBox30")));
        TextBox blood = ((TextBox)(DetailsView1.Rows[21].FindControl("TextBox32")));
        TextBox achieve = ((TextBox)(DetailsView1.Rows[22].FindControl("TextBox34")));
        TextBox skill = ((TextBox)(DetailsView1.Rows[23].FindControl("TextBox36")));
        TextBox training = ((TextBox)(DetailsView1.Rows[24].FindControl("TextBox38")));
        TextBox hobbies = ((TextBox)(DetailsView1.Rows[25].FindControl("TextBox40")));
        FileUpload resume = ((FileUpload)(DetailsView1.Rows[26].FindControl("FileUpload4")));
        //hostler = Convert.ToBoolean(((RadioButtonList)(DetailsView1.Rows[29].FindControl("RBLgender"))).SelectedValue);
        CheckBox hostl = ((CheckBox)(DetailsView1.Rows[27].FindControl("CheckBox2")));
        DropDownList ddldep = (DropDownList)(DetailsView1.Rows[3].FindControl("ddlDepartment"));
        if (hostl.Checked == true)
        {
            hostler = true;
        }
        else
        {
            hostler = false;
        }
        if (lname.Text.Length.Equals(3))
        {
            userlogid = fname.Substring(0, 2) + lname.Text.Substring(0, 3) + rno.Text.Substring(2, 5) + ddldep.SelectedItem.ToString();
        }
        else
        {
            userlogid = fname.Substring(0, 2) + fname.Substring(0, 2) + rno.Text.Substring(2, 5) + ddldep.SelectedItem.ToString();
        }
        SqlCommand cmd = new SqlCommand("insstudent", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@userlogid", SqlDbType.VarChar, 50).Value = userlogid.ToUpper();
        SqlCommand cmdcheck = new SqlCommand("select count(*) as cnt from tbstudent where studentrollno=@u", con);
        cmdcheck.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(rno.Text);
        Int32 check = Convert.ToInt32(cmdcheck.ExecuteScalar());
        if (check == 0)
        {

            cmd.Parameters.Add("@rno", SqlDbType.Int).Value = Convert.ToInt32(rno.Text);

            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
            cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
            cmd.Parameters.Add("@p1", SqlDbType.BigInt).Value = p1;
            if (p2.Text == null || p2.Text == "")
            {
                cmd.Parameters.Add("@p2", SqlDbType.BigInt).Value = Convert.DBNull;
            }
            else
            {
                cmd.Parameters.Add("@p2", SqlDbType.BigInt).Value = Convert.ToInt64(p2.Text);
            }
            cmd.Parameters.Add("@tenth", SqlDbType.Int).Value = tenth;
            if (twelth.Text == null || twelth.Text == "")
            {
                cmd.Parameters.Add("@twelth", SqlDbType.Int).Value = Convert.DBNull;
            }
            else
            {
                cmd.Parameters.Add("@twelth", SqlDbType.Int).Value = Convert.ToInt32(twelth.Text);
            }
            if (regno.Text == null)
            {
                cmd.Parameters.Add("@regno", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
            }
            else
            {
                cmd.Parameters.Add("@regno", SqlDbType.VarChar, 50).Value = regno.Text;
            }
            cmd.Parameters.Add("@fname", SqlDbType.VarChar, 50).Value = fname;
            if (mname.Text == null)
            {
                cmd.Parameters.Add("@mname", SqlDbType.VarChar, 50).Value = " ";
            }
            else
            {
                cmd.Parameters.Add("@mname", SqlDbType.VarChar, 50).Value = mname.Text;
            }
            if (lname.Text == null)
            {
                cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50).Value = " ";
            }
            else
            {
                cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50).Value = lname.Text;
            }
            cmd.Parameters.Add("@mtname", SqlDbType.VarChar, 50).Value = mtname;
            cmd.Parameters.Add("@ftname", SqlDbType.VarChar, 50).Value = ftname;
            cmd.Parameters.Add("@corres", SqlDbType.VarChar, 300).Value = corres;
            if (perma.Text == null)
            {
                cmd.Parameters.Add("@perma", SqlDbType.VarChar, 300).Value = Convert.DBNull.ToString();
            }
            else
            {
                cmd.Parameters.Add("@perma", SqlDbType.VarChar, 300).Value = perma.Text;
            }
            if (photo.FileName == null || photo.FileName == string.Empty || photo.FileName == "")
            {
                cmd.Parameters.Add("@photo", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
            }
            else
            {
                DirectoryInfo di = new DirectoryInfo(Server.MapPath("~\\Studentpic" + "\\"));
                if (di.Exists == false)
                    di.Create();
                string fn = rno.Text.Substring(2, 5) + Path.GetExtension(photo.PostedFile.FileName);
                string sp = Server.MapPath("~\\Studentpic" + "\\");
                if (sp.EndsWith("\\") == false)
                    sp += "\\";
                sp += fn;
                photo.PostedFile.SaveAs(sp);
                cmd.Parameters.Add("@photo", SqlDbType.VarChar, 100).Value = fn;
            }
            if (email.Text == null)
            {
                cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
            }
            else
            {
                cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = email.Text;
            }
            if (blood.Text == null)
            {
                cmd.Parameters.Add("@blood", SqlDbType.VarChar, 4).Value = Convert.DBNull.ToString();
            }
            else
            {
                cmd.Parameters.Add("@blood", SqlDbType.VarChar, 4).Value = blood.Text;
            }
            if (achieve.Text == null)
            {
                cmd.Parameters.Add("@achieve", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
            }
            else
            {
                cmd.Parameters.Add("@achieve", SqlDbType.VarChar, 200).Value = achieve.Text;
            }
            if (skill.Text == null)
            {
                cmd.Parameters.Add("@skill", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
            }
            else
            {
                cmd.Parameters.Add("@skill", SqlDbType.VarChar, 200).Value = skill.Text;
            }
            if (training.Text == null)
            {
                cmd.Parameters.Add("@training", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
            }
            else
            {
                cmd.Parameters.Add("@training", SqlDbType.VarChar, 200).Value = training.Text;
            }
            if (hobbies.Text == null)
            {
                cmd.Parameters.Add("@hobbies", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
            }
            else
            {
                cmd.Parameters.Add("@hobbies", SqlDbType.VarChar, 200).Value = hobbies.Text;
            }
            if (resume.FileName == null || resume.FileName == string.Empty || resume.FileName == "")
            {
                cmd.Parameters.Add("@resume", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
            }
            else
            {
                DirectoryInfo di1 = new DirectoryInfo(Server.MapPath("~\\Studentresume" + "\\"));
                if (di1.Exists == false)
                    di1.Create();
                string fn1 = rno.Text.Substring(2, 5) + Path.GetExtension(resume.PostedFile.FileName);
                string sp1 = Server.MapPath("~\\Studentresume" + "\\");
                if (sp1.EndsWith("\\") == false)
                    sp1 += "\\";
                sp1 += fn1;
                photo.PostedFile.SaveAs(sp1);
                cmd.Parameters.Add("@resume", SqlDbType.VarChar, 100).Value = fn1;
            }
            //cmd.Parameters.Add("@resume", SqlDbType.VarChar, 100).Value = resume;
            cmd.Parameters.Add("@d1", SqlDbType.DateTime).Value = d1;
            cmd.Parameters.Add("@hostler", SqlDbType.Bit).Value = hostler;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            DetailsView1.ChangeMode(DetailsViewMode.ReadOnly);
            DetailsView1.ChangeMode(DetailsViewMode.Insert);
            
            Response.Redirect("~/admin/regstudent.aspx");
        }
        else
        {
            rno.Text = string.Empty;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Roll Number Not Available !');", true);

        }
    }
    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        //   DetailsView1.ChangeMode(e.NewMode);
        Response.Redirect("~/admin/regstudent.aspx");
    }
    protected void DetailsView1_PageIndexChanging(object sender, DetailsViewPageEventArgs e)
    {
        DetailsView1.PageIndex = e.NewPageIndex;
    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox rno = ((TextBox)(DetailsView1.Rows[0].FindControl("TextBox2")));
            if (rno.Text == string.Empty || rno.Text == "")
            {
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Enter Roll Number in the given field !');", true);
            }
            else
            {
                SqlCommand cmdcheck = new SqlCommand("select count(*) as cnt from tbstudent where studentrollno=@u", con);
                cmdcheck.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(rno.Text);
                Int32 check = Convert.ToInt32(cmdcheck.ExecuteScalar());
                if (check == 0)
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Roll Number Available !');", true);
                }
                else
                {
                    rno.Text = string.Empty;
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Roll Number Not Available !');", true);

                }
            }
        }
        catch (Exception exp)
        {
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Enter numeric number only !');", true);
        }
    }
}