﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="Studentview.aspx.cs" Inherits="Teacher_Studentview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      <table class="style1">
            <tr>
                <td class="style2" colspan="2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td class="style2">
                    Roll-ID</td>
                <td>
                    <asp:TextBox ID="TextBox41" runat="server" 
                        ontextchanged="TextBox41_TextChanged" ValidationGroup="abc" Width="308px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2" colspan="2">
                    &nbsp;</td>
                <td>
                    <asp:CompareValidator ID="CompareValidator52" runat="server" 
                        ControlToValidate="TextBox41" Display="Dynamic" ErrorMessage="*" 
                        Font-Bold="True" ForeColor="Red" Operator="DataTypeCheck" Type="Integer" 
                        ValidationGroup="abc"></asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                        ControlToValidate="TextBox41" Display="Dynamic" ErrorMessage="**" 
                        Font-Bold="True" ForeColor="Red" ValidationGroup
                       ="abc"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style2" colspan="2">
                    &nbsp;</td>
                <td>
                    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" Height="16px" 
                        onitemdeleting="DetailsView1_ItemDeleting" 
                        onitemupdating="DetailsView1_ItemUpdating" 
                        onmodechanging="DetailsView1_ModeChanging" Visible="False" Width="380px">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
                        <EditRowStyle BackColor="#999999" />
                        <EmptyDataTemplate>
                            No Record&nbsp; Found<br />
                        </EmptyDataTemplate>
                        <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
                        <Fields>
                            <asp:TemplateField HeaderText="Roll Number" SortExpression="studentrollno">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("studentrollno") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                        ControlToValidate="TextBox1" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                        ControlToValidate="TextBox1" ErrorMessage="**" ForeColor="Red" 
                                        Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Eval("studentrollno") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                        ControlToValidate="TextBox2" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator2" runat="server" 
                                        ControlToValidate="TextBox2" ErrorMessage="**" ForeColor="Red" 
                                        Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("studentrollno") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Registration Number" 
                                SortExpression="studentregno">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Eval("studentregno") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Eval("studentregno") %>'></asp:TextBox>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("studentregno") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Course">
                                
                                <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("coursename") %>'></asp:Label>
                    </ItemTemplate></asp:TemplateField>
                            <asp:TemplateField HeaderText="Department">
                                <EditItemTemplate>
                                    <asp:DropDownList ID="DropDownList3" runat="server">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                                  <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("depname") %>'></asp:Label>
                    </ItemTemplate></asp:TemplateField>
                            <asp:TemplateField HeaderText="Session">
                                
                                <ItemTemplate>
                        <asp:Label ID="Label28" runat="server" Text='<%# Eval("sessionstart") %>'></asp:Label>
                    </ItemTemplate></asp:TemplateField>
                            <asp:TemplateField HeaderText="Class">
                               
                                <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Eval("classname") %>'></asp:Label>
                    </ItemTemplate></asp:TemplateField>
                            <asp:TemplateField HeaderText="Group">
                                <EditItemTemplate>
                                    <asp:DropDownList ID="DropDownList7" runat="server">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                                <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Eval("groupname") %>'></asp:Label>
                    </ItemTemplate></asp:TemplateField>
                            <asp:TemplateField HeaderText="First Name" SortExpression="studentfname">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Eval("studentfname") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                        ControlToValidate="TextBox5" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Eval("studentfname") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                        ControlToValidate="TextBox6" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("studentfname") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Middle Name" SortExpression="studentmname">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox7" runat="server" Text='<%# Eval("studentmname") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox8" runat="server" Text='<%# Eval("studentmname") %>'></asp:TextBox>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label8" runat="server" Text='<%# Eval("studentmname") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Name" SortExpression="studentlname">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox9" runat="server" Text='<%# Eval("studentlname") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                        ControlToValidate="TextBox9" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox10" runat="server" Text='<%# Eval("studentlname") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                        ControlToValidate="TextBox10" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label9" runat="server" Text='<%# Eval("studentlname") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mother Name" SortExpression="studentmothername">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox11" runat="server" 
                                        Text='<%# Eval("studentmothername") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                        ControlToValidate="TextBox11" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox12" runat="server" 
                                        Text='<%# Eval("studentmothername") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                                        ControlToValidate="TextBox12" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label10" runat="server" Text='<%# Eval("studentmothername") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Father Name" SortExpression="studentfathername">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox13" runat="server" 
                                        Text='<%# Eval("studentfathername") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                                        ControlToValidate="TextBox13" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox14" runat="server" 
                                        Text='<%# Eval("studentfathername") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                                        ControlToValidate="TextBox14" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label11" runat="server" Text='<%# Eval("studentfathername") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date Of Birth" SortExpression="studentdob">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox15" runat="server" 
                                        Text='<%# Eval("studentdob","{0:d}") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                                        ControlToValidate="TextBox15" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator3" runat="server" 
                                        ControlToValidate="TextBox15" ErrorMessage="**" ForeColor="Red" 
                                        Operator="DataTypeCheck" Type="Date" ValidationGroup="abc"></asp:CompareValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox16" runat="server" Text='<%# Eval("studentdob") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
                                        ControlToValidate="TextBox16" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator4" runat="server" 
                                        ControlToValidate="TextBox16" ErrorMessage="**" ForeColor="Red" 
                                        Operator="DataTypeCheck" Type="Date" ValidationGroup="abc"></asp:CompareValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label12" runat="server" Text='<%# Eval("studentdob","{0:d}") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category" SortExpression="studentcategoryid">
                                <EditItemTemplate>
                                    <asp:DropDownList ID="DropDownList9" runat="server" DataSourceID="Category" 
                                        DataTextField="categoryname" DataValueField="categoryid">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="Category" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:cn %>" 
                                        SelectCommand="SELECT [categoryid], [categoryname] FROM [tbcategory] WHERE ([categorydelsts] = @categorydelsts)">
                                        <SelectParameters>
                                            <asp:Parameter DefaultValue="false" Name="categorydelsts" Type="Boolean" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:DropDownList ID="DropDownList10" runat="server" DataSourceID="Category0" 
                                        DataTextField="categoryname" DataValueField="categoryid">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="Category0" runat="server" 
                                        ConnectionString="<%$ ConnectionStrings:cn %>" 
                                        SelectCommand="SELECT [categoryid], [categoryname] FROM [tbcategory] WHERE ([categorydelsts] = @categorydelsts)">
                                        <SelectParameters>
                                            <asp:Parameter DefaultValue="false" Name="categorydelsts" Type="Boolean" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Text='<%# Eval("studentcategoryid") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Correspondence Address" 
                                SortExpression="studentcorresadd">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox17" runat="server" Height="92px" 
                                        Text='<%# Eval("studentcorresadd") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorcorres" runat="server" 
                                        ControlToValidate="TextBox17" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox18" runat="server" Height="92px" 
                                        Text='<%# Eval("studentcorresadd") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorcorres1" runat="server" 
                                        ControlToValidate="TextBox18" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label14" runat="server" Text='<%# Eval("studentcorresadd") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Permanent Address" 
                                SortExpression="studentpermaadd">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox19" runat="server" Height="92px" 
                                        Text='<%# Eval("studentpermaadd") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox20" runat="server" Height="92px" 
                                        Text='<%# Eval("studentpermaadd") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label15" runat="server" Text='<%# Eval("studentpermaadd") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Phone Number" SortExpression="studentphone1">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox21" runat="server" Text='<%# Eval("studentphone1") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                                        ControlToValidate="TextBox21" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator5" runat="server" 
                                        ControlToValidate="TextBox21" ErrorMessage="**" ForeColor="Red" 
                                        Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox22" runat="server" Text='<%# Eval("studentphone1") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" 
                                        ControlToValidate="TextBox22" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator6" runat="server" 
                                        ControlToValidate="TextBox22" ErrorMessage="**" ForeColor="Red" 
                                        Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label16" runat="server" Text='<%# Eval("studentphone1") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Alternate Phone Number" 
                                SortExpression="studentphone2">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox23" runat="server" Text='<%# Eval("studentphone2") %>'></asp:TextBox>
                                    <asp:CompareValidator ID="CompareValidator50" runat="server" 
                                        ControlToValidate="TextBox23" ErrorMessage="**" ForeColor="Red" 
                                        Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox24" runat="server" Text='<%# Eval("studentphone2") %>'></asp:TextBox>
                                    <asp:CompareValidator ID="CompareValidator51" runat="server" 
                                        ControlToValidate="TextBox24" ErrorMessage="**" ForeColor="Red" 
                                        Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label17" runat="server" Text='<%# Eval("studentphone2") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Email_ID" SortExpression="studentemail_id">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox25" runat="server" 
                                        Text='<%# Eval("studentemail_id") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                        ControlToValidate="TextBox25" ErrorMessage="***" ForeColor="Red" 
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                        ValidationGroup="abc"></asp:RegularExpressionValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox26" runat="server" 
                                        Text='<%# Eval("studentemail_id") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                        ControlToValidate="TextBox26" ErrorMessage="***" ForeColor="Red" 
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                        ValidationGroup="abc"></asp:RegularExpressionValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label18" runat="server" Text='<%# Eval("studentemail_id") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Picture" SortExpression="studentphoto">
                                <EditItemTemplate>
                                    <asp:FileUpload ID="FileUpload1" runat="server" />
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:FileUpload ID="FileUpload2" runat="server" />
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Image ID="Image1" runat="server" Height="80px" 
                                        ImageUrl='<%# Eval("studentphoto") %>' Width="80px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="10 Marks" SortExpression="student10marks">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox27" runat="server" Text='<%# Eval("student10marks") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" 
                                        ControlToValidate="TextBox27" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator7" runat="server" 
                                        ControlToValidate="TextBox27" ErrorMessage="**" ForeColor="Red" 
                                        Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox28" runat="server" Text='<%# Eval("student10marks") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" 
                                        ControlToValidate="TextBox28" ErrorMessage="*" ForeColor="Red" 
                                        ValidationGroup="abc"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="CompareValidator8" runat="server" 
                                        ControlToValidate="TextBox28" ErrorMessage="**" ForeColor="Red" 
                                        Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label19" runat="server" Text='<%# Eval("student10marks") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="12 Marks" SortExpression="student12marks">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox29" runat="server" Text='<%# Eval("student12marks") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox30" runat="server" Text='<%# Eval("student12marks") %>'></asp:TextBox>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label20" runat="server" Text='<%# Eval("student12marks") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Blood Group" SortExpression="studentbloodgroup">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox31" runat="server" 
                                        Text='<%# Eval("studentbloodgroup") %>'></asp:TextBox>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox32" runat="server" 
                                        Text='<%# Eval("studentbloodgroup") %>'></asp:TextBox>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label21" runat="server" Text='<%# Eval("studentbloodgroup") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Achievement" SortExpression="studentachievement">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox33" runat="server" Height="92px" 
                                        Text='<%# Eval("studentachievement") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox34" runat="server" Height="92px" 
                                        Text='<%# Eval("studentachievement") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label22" runat="server" Text='<%# Eval("studentachievement") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Skill" SortExpression="studentskill">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox35" runat="server" Height="92px" 
                                        Text='<%# Eval("studentskill") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox36" runat="server" Height="92px" 
                                        Text='<%# Eval("studentskill") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label23" runat="server" Text='<%# Eval("studentskill") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Training" SortExpression="studenttraining">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox37" runat="server" Height="92px" 
                                        Text='<%# Eval("studenttraining") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox38" runat="server" Height="92px" 
                                        Text='<%# Eval("studenttraining") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label24" runat="server" Text='<%# Eval("studenttraining") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Hobbies" SortExpression="studenthobbies">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox39" runat="server" Height="92px" 
                                        Text='<%# Eval("studenthobbies") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:TextBox ID="TextBox40" runat="server" Height="92px" 
                                        Text='<%# Eval("studenthobbies") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label25" runat="server" Text='<%# Eval("studenthobbies") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Resume File" SortExpression="studentresumefile">
                                <EditItemTemplate>
                                    <asp:FileUpload ID="FileUpload3" runat="server" />
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:FileUpload ID="FileUpload4" runat="server" />
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lk" runat="server" Text='<%# Eval("studentresumefile") %>'></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IS Hostler" SortExpression="studentishostler">
                                <EditItemTemplate>
                                    <asp:CheckBox ID="CheckBox1" runat="server" 
                                        Text='<%# Eval("studentishostler") %>' />
                                </EditItemTemplate>
                                <InsertItemTemplate>
                                    <asp:CheckBox ID="CheckBox2" runat="server" 
                                        Text='<%# Eval("studentishostler") %>' />
                                </InsertItemTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label27" runat="server" Text='<%# Eval("studentishostler") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                            
                        </Fields>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    </asp:DetailsView>
                </td>
            </tr>
            <tr>
                <td class="style2" colspan="2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2" colspan="2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
    
                    &nbsp;</td>
            </tr>
        </table>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

