﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dir/MasterPage.master" AutoEventWireup="true" CodeFile="sattd.aspx.cs" Inherits="dir_sattd" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      <table width="100%" style="text-align: left; font-weight: bold;">
             <tr>
              <td>
                  Session</td>
              <td>
                  <asp:DropDownList ID="DDLSession" runat="server" AutoPostBack="True" 
                      onselectedindexchanged="DDLSession_SelectedIndexChanged" Width="200px">
                  </asp:DropDownList>
              </td>
              <td>
                  &nbsp;</td>
          </tr>
             <tr>
            <td>
                Class</td>
            <td>
                <asp:DropDownList ID="DDLClass" runat="server" AutoPostBack="True" 
                    Enabled="False" Width="200px" 
                    onselectedindexchanged="DDLClass_SelectedIndexChanged">
                    <asp:ListItem>-- Select Class --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
             <%--<tr>
            <td>
                Semester</td>
            <td>
                <asp:DropDownList ID="DDLSem" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLSem_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>--%>
             <%--     <tr>
              <td>
                  Group</td>
              <td>
                  <asp:DropDownList ID="DDLGrp" runat="server" 
                     style="height: 22px" onselectedindexchanged="DDLGrp_SelectedIndexChanged">
                      <asp:ListItem>-- Select Group --</asp:ListItem>
                  </asp:DropDownList>
              </td>
              <td>
                  &nbsp;</td>
          </tr>--%>
             <tr>
            <td>
                Subject</td>
            <td>
                <asp:DropDownList ID="DDLSubject" runat="server" 
                    AutoPostBack="True" 
                    onselectedindexchanged="DDLSubject_SelectedIndexChanged" Width="200px" Enabled="False" 
                    >
                    <asp:ListItem>-- Choose Subject --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
             <tr>
                    <td>
                        Total No. Of&nbsp; Periods Delivered</td>
                    <td style="margin-left: 40px">
                        <asp:Label ID="LBLTotalDelivered" runat="server"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
      <asp:Panel ID="datepanel" runat="server" Visible="False" >
                <table width="100%" style="text-align: left; font-weight: bold;">
        
        
       
                <tr>
                
                    <td style="width: 423px;" align="left">
                        Date </td>
                    <td align="left">
                        
                        <asp:DropDownList ID="DDLDate" runat="server" AutoPostBack="True" 
                            DataTextFormatString="{0:D}" 
                            onselectedindexchanged="DDLDate_SelectedIndexChanged" Width="200px">
                            <asp:ListItem>-- Choose Date --</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                   
                </tr> 
                </table>
                </asp:Panel>
      <asp:Panel ID="approvedpanel" runat="server" Visible="False">
                         <table width="100%" style="text-align: left; font-weight: bold;">
                         <tr>
                          <td align="center">
                                <div  style="overflow:scroll;height:300px;visibility:inherit">
                              <asp:GridView ID="GridShowSubmit" runat="server" AutoGenerateColumns="False" 
                                  CellPadding="4" DataKeyNames="studentid" ForeColor="#333333" GridLines="None" 
                                  onrowdatabound="GridShowSubmit_RowDataBound" Width="800px">
                                  <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                  <Columns>
                                      <asp:TemplateField HeaderText="Roll No">
                                          <ItemTemplate>
                                              <asp:Label ID="LblRollNo2" runat="server" Text='<%# Eval("studentRollno") %>'></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Attendance">
                                          <ItemTemplate>
                                              <asp:Label ID="LBLAttend2" runat="server" 
                                                  Text='<%# Eval("studentattndattend") %>'></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Total Attended">
                                          <ItemTemplate>
                                              <asp:Label ID="LblAt2" runat="server"></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="%age">
                                          <ItemTemplate>
                                              <asp:Label ID="Lblpercent2" runat="server"></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                  </Columns>
                                  <EditRowStyle BackColor="#999999" />
                                  <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                  <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                  <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                  <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                  <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                  <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                  <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                  <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                  <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                              </asp:GridView>
                              </div>
                              
                          </td>
                      </tr>
                <tr>
                    <td>
                        </td>
                    
                </tr>
                </table>
                </asp:Panel>       
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

