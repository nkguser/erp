﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_Administartive : System.Web.UI.Page
{
    static int role = 0;
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
    }
    protected void RBLOpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RBLOpt.SelectedIndex == 0)
        {
            DDLCourseNameBind(1);
            ddldepclear();
            Label1.Text = "Director";
            role = 5;
            Lblmsg.Text = string.Empty;
            Appoint(role);
        }
        else if (RBLOpt.SelectedIndex == 1)
        {
            Label1.Text = "Dean Of Acedamic Affairs";
            DDLCourseNameBind(1);
            ddldepclear();
            role = 8;
            Lblmsg.Text = string.Empty;
            Appoint(role);
        }
        else if (RBLOpt.SelectedIndex == 2)
        {
            Label1.Text = "Head Of Department";
            DDLCourseNameBind(2);
            ddldepclear();
            role = 2;
            Lblmsg.Text = string.Empty;
            //Appoint(role);
        }
        else
        {
            Label1.Text = "Training & Placement Officer";
            DDLCourseNameBind(2);
            ddldepclear();
            role = 6;
            Lblmsg.Text = string.Empty;
            //Appoint(role);
        }
    }

    private void Appoint(int role)
    {
        SqlCommand cmd;
        if (RBLOpt.SelectedIndex == 2 || RBLOpt.SelectedIndex == 3)
        {
            cmd = new SqlCommand("SELECT tbuser.userid as id, tbteacher.teacherfname + ' ' + tbteacher.teachermname + ' ' + tbteacher.teacherlname AS name FROM tbuser INNER JOIN tbteacher ON tbuser.userid = tbteacher.teacheruserid INNER JOIN tbdep ON tbteacher.teacherdepid = tbdep.depid INNER JOIN tbcourse ON tbdep.depcourseid = tbcourse.courseid WHERE (tbuser.userroleid = @roleid) AND (tbcourse.courseclgid = @clgid) AND (tbuser.userdelsts = 0) AND (tbdep.depid = @did)", con);
            cmd.Parameters.Add("roleid", SqlDbType.Int).Value = role;
            cmd.Parameters.Add("did", SqlDbType.Int).Value = Convert.ToInt32(DDLDept.SelectedValue);
            cmd.Parameters.Add("clgid", SqlDbType.Int).Value = Convert.ToInt32(Session["other"]);
        }
        else
        {
            cmd = new SqlCommand("SELECT tbuser.userid as id, tbteacher.teacherfname + ' ' + tbteacher.teachermname + ' ' + tbteacher.teacherlname AS name FROM tbuser INNER JOIN tbteacher ON tbuser.userid = tbteacher.teacheruserid INNER JOIN tbdep ON tbteacher.teacherdepid = tbdep.depid INNER JOIN tbcourse ON tbdep.depcourseid = tbcourse.courseid WHERE (tbuser.userroleid = @roleid) AND (tbcourse.courseclgid = @clgid) AND (tbuser.userdelsts = 0)", con);
            cmd.Parameters.Add("roleid", SqlDbType.Int).Value = role;
            cmd.Parameters.Add("clgid", SqlDbType.Int).Value = Convert.ToInt32(Session["other"]);
        }
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.HasRows)
        {
            DDLSts.DataTextField = "name";
            DDLSts.DataValueField = "id";
            DDLSts.DataSource = dr;
            DDLSts.DataBind();
            lbremove.Enabled = true;
            Btnmodify.Enabled = false;
        }
        else
        {
            DDLSts.Items.Clear();
            DDLSts.Items.Insert(0, "NA");
            lbremove.Enabled = false;
            Btnmodify.Enabled = true;
        }
        dr.Close();
        cmd.Dispose();
    }
    private DataSet AllCourse(int s)
    {
        SqlCommand cmd = new SqlCommand("dispadmincourse", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@s", SqlDbType.Int).Value = s;
        cmd.Parameters.Add("@clgid", SqlDbType.Int).Value = Convert.ToInt32(Session["other"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    private void DDLCourseNameBind(int s)
    {
        DataSet ds = AllCourse(s);
        DDLCourse.DataValueField = "courseid";
        DDLCourse.DataTextField = "coursename";
        DDLCourse.DataSource = ds;
        DDLCourse.DataBind();
        DDLCourse.Items.Insert(0, "--Select Course --");
    }
    private DataSet AllDepartment(int cid)
    {
        SqlCommand cmd = new SqlCommand("dispdept");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    protected void DDLCourse_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLCourse.SelectedIndex == 0 && RBLOpt.SelectedIndex == 2 || RBLOpt.SelectedIndex == 3)
        {
            ddldepclear();
            DDLSts.Items.Clear();
            lbremove.Enabled = false;
            Btnmodify.Enabled = false;
        }
        else if (DDLCourse.SelectedIndex==0)
        {
            ddldepclear();
        }
        else
        {
            DataSet ds = AllDepartment(Convert.ToInt32(DDLCourse.SelectedValue));
            DDLDept.DataValueField = "depid";
            DDLDept.DataTextField = "depname";
            DDLDept.DataSource = ds;
            DDLDept.DataBind();
            DDLDept.Items.Insert(0, "--Select Department --");
            DDLDept.Enabled = true;
        }
    }
    private void ddldepclear()
    {
        DDLDept.Items.Clear();
        DDLDept.Items.Insert(0, "-- Select Department --");
        DDLDept.Enabled = false;
        DDLTeachers.Items.Clear();
        DDLTeachers.Items.Insert(0, "-- Select Teacher --");
        DDLTeachers.Enabled = false;
    }
    private DataSet DDlTeacherBind(int did)
    {
        SqlCommand cmd = new SqlCommand("SELECT tbteacher.teacheruserid, tbteacher.teacherfname + ' ' + tbteacher.teachermname + ' ' + tbteacher.teacherlname AS Name FROM tbteacher INNER JOIN tbuser ON tbteacher.teacheruserid = tbuser.userid WHERE (tbteacher.teacherdepid = @did) AND (tbteacher.teacherdelsts = 0) AND (tbuser.userroleid = 3) ", con);
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    protected void DDLDept_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLDept.SelectedIndex == 0 && RBLOpt.SelectedIndex == 2 || RBLOpt.SelectedIndex == 3)
        {
            ddlteacherclear();
            DDLSts.Items.Clear();
            lbremove.Enabled = false;
            Btnmodify.Enabled = false;
        }
        else if (DDLDept.SelectedIndex == 0)
        {
            ddlteacherclear();
        }
        else if (DDLDept.SelectedIndex != 0 && Convert.ToString(DDLSts.SelectedItem) == "NA" && RBLOpt.SelectedIndex == 2 || RBLOpt.SelectedIndex == 3)
        {
            depteacher();
        }
        else if (DDLDept.SelectedIndex!=0 && RBLOpt.SelectedIndex==2 || RBLOpt.SelectedIndex==3)
        {
            Appoint(role);
            
        }
        else if (Convert.ToString(DDLSts.SelectedItem) == "NA")
        {
            depteacher();
        }
        else
        {
            return;
        }
    }

    private void depteacher()
    {
        DataSet ds = DDlTeacherBind(Convert.ToInt32(DDLDept.SelectedValue));
        DDLTeachers.DataValueField = "teacheruserid";
        DDLTeachers.DataTextField = "name";
        DDLTeachers.DataSource = ds;
        DDLTeachers.DataBind();
        DDLTeachers.Items.Insert(0, "-- Select Teacher --");
        DDLTeachers.Enabled = true;
    }
    private void ddlteacherclear()
    {
        DDLTeachers.Items.Clear();
        DDLTeachers.Items.Insert(0, "-- Select Teacher --");
        DDLTeachers.Enabled = false;
    }
    protected void Btnmodify_Click(object sender, EventArgs e)
    {
        if (role == 5)
        {
            SqlCommand cmd = new SqlCommand("update tbuser set userroleid=5 where userid=" + Convert.ToInt32(DDLTeachers.SelectedValue), con);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            Lblmsg.Text = DDLTeachers.SelectedItem + " Is Now Appointed For Post Of Director of the Institute";
        }
        else if (role == 6)
        {
            SqlCommand cmd = new SqlCommand("update tbuser set userroleid=6 where userid=" + Convert.ToInt32(DDLTeachers.SelectedValue), con);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            Lblmsg.Text = DDLTeachers.SelectedItem + " Is Now Appointed For the Post Of Trainig & Placement Officer";
        }
        else if (role == 8)
        {
            SqlCommand cmd = new SqlCommand("update tbuser set userroleid=8 where userid=" + Convert.ToInt32(DDLTeachers.SelectedValue), con);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            Lblmsg.Text = DDLTeachers.SelectedItem + " Is Now Appointed For the Post Of Dean Of Acedamic Affairs of the Institute";
        }
        else
        {
            SqlCommand cmd = new SqlCommand("update tbuser set userroleid=2 where userid=" + Convert.ToInt32(DDLTeachers.SelectedValue), con);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            Lblmsg.Text = DDLTeachers.SelectedItem + " Is Now Appointed For the Post Of Head Of Department";
            DDLSts.Items.Clear();
        } 
       
        Appoint(role);
        DDLDept.SelectedIndex = 0;
        DDLTeachers.SelectedIndex = 0;
        DDLTeachers.Enabled = false;
         
    }
    protected void lbremove_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand("update tbuser set userroleid=3 where userid="+Convert.ToInt32(DDLSts.SelectedValue),con);
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        if (role==5)
        {
            Lblmsg.Text = DDLSts.SelectedItem + " Is Transfer/Terminated from the Post Of Director";
           
        }
        else if (role==6)
        {
            Lblmsg.Text = DDLSts.SelectedItem + " Is Transfer/Terminated from the Post Of Trainig & Placement Officer";
           
        }
        else if (role == 8)
        {
            Lblmsg.Text = DDLSts.SelectedItem + " Is Transfer/Terminated from the Post Of Dean Of Acedamic Affairs";
        }
        else
        {
            Lblmsg.Text = DDLSts.SelectedItem + " Is Transfer/Terminated from the Post Of Head Of Department";
        }
        Appoint(role);
        DDLDept.SelectedIndex = 0;
    }
}