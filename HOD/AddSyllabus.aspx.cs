﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

public partial class HOD_AddSyllabus : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        DirectoryInfo di = new DirectoryInfo(Server.MapPath("~\\Syllabus" + "\\" + Convert.ToString(Session["depid"])));
        if (di.Exists == false)
            di.Create();
        string fn = FileUpload1.FileName;
        string sp = Server.MapPath("~\\Syllabus" + "\\" + Convert.ToString(Session["depid"]));
        if (sp.EndsWith("\\") == false)
            sp += "\\";
        sp += fn;
        FileUpload1.PostedFile.SaveAs(sp);
        SqlCommand cmd = new SqlCommand("update tbdep set depsyl=@u where depid=@d", con);
        cmd.Parameters.Add("@u", SqlDbType.VarChar,50).Value = fn;
        cmd.Parameters.Add("@d", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        Label1.Text = "Syllabus Uploaded Successfully";
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        DirectoryInfo di = new DirectoryInfo(Server.MapPath("~\\Timetable" + "\\" + Convert.ToString(Session["depid"])));
        if (di.Exists == false)
            di.Create();
        string fn = FileUpload2.FileName;
        string sp = Server.MapPath("~\\Timetable" + "\\" + Convert.ToString(Session["depid"]));
        if (sp.EndsWith("\\") == false)
            sp += "\\";
        sp += fn;
        FileUpload2.PostedFile.SaveAs(sp);
        SqlCommand cmd = new SqlCommand("update tbdep set deptimetable=@u where depid=@d", con);
        cmd.Parameters.Add("@u", SqlDbType.VarChar, 50).Value = fn;
        cmd.Parameters.Add("@d", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        Label2.Text = "Time Table Uploaded Successfully";
    }
}