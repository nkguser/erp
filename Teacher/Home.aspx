﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true"
    CodeFile="Home.aspx.cs" Inherits="Teacher_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <head>
        <style type="text/css">
            .abc
            {
                font-size: larger;
                font-weight: bold;
                cursor: pointer;
            }
            .xyz
            {
                display: none;
                visibility: hidden;
            }
            .black_overlay
            {
                display: none;
                position: absolute;
                top: 4%;
                left: 0%;
                width: 100%;
                height: 155%;
                background-color: black;
                z-index: auto;
                overflow: auto;
                -moz-opacity: 0.8;
                opacity: 100;
                filter: alpha(opacity=100);
            }
            .white_content
            {
                display: none;
                position: absolute;
                top: 10%;
                left: 19%;
                width: 880px;
                padding: 0px;
                border: 0px solid #a6c25c;
                background-color: white;
                z-index: 1002;
                overflow: auto;
            }
            .headertext
            {
                font-family: Arial, Helvetica, sans-serif;
                font-size: 14px;
                color: #f19a19;
                font-weight: bold;
            }
        </style>
        <script type="text/javascript" language="javascript">
            //        function GetData(id) {
            //            var e;
            //            e = document.getElementById('d' + id);
            //            if (e) {

            //                if (e.style.display != "block") {
            //                    e.style.display = "block";
            //                    e.style.visibility = "visible";
            //                }
            //                else {
            //                    e.style.display = "none";
            //                    e.style.visibility = "hidden";
            //                }
            //            }

            //        }
            //        function GetData1(id) {
            //            var e1;
            //            e1 = document.getElementById('g' + id);
            //            if (e1) {

            //                if (e1.style.display != "block") {
            //                    e1.style.display = "block";
            //                    e1.style.visibility = "visible";
            //                }
            //                else {
            //                    e1.style.display = "none";
            //                    e1.style.visibility = "hidden";
            //                }
            //            }

            //        }
            function ShowImages() {
                document.getElementById('light').style.display = 'block';
                document.getElementById('fade').style.display = 'block'
                return false;
            }
        </script>
    </head>
    <table>
        <tr>
            <td style="border: 1px solid #2D4262; width: 865px; height: 125px">
                <asp:Panel ID="Panel1" runat="server">
                    <table class="styledmenu">
                        <tr>
                            <td align="left">
                                <asp:Image ID="Image2" runat="server" Width="80px" Height="80px" />
                            </td>
                            <td style="height: 95px">
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table class="styledmenu">
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblname" runat="server"></asp:Label>
                                            <br />
                                            <asp:Label ID="lblroll" runat="server"></asp:Label>
                                            <br />
                                            <asp:Label ID="lbldep" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="border: 1px solid #2D4262; width: 865px; height: 500px">
                <table>
                    <th align="center" style="border: 1px solid #2D4262; width: 280px" bgcolor="#2D4262">
                        <b style="color: White">NoticeBoard</b>
                    </th>
                    <th align="center" style="border: 1px solid #2D4262; width: 280px" bgcolor="#2D4262">
                        <b style="color: White">Alerts</b>
                    </th>
                    <th align="center" style="border: 1px solid #2D4262; width: 280px" bgcolor="#2D4262">
                        <b style="color: White">Events</b>
                    </th>
                    <tr>
                        <td rowspan="2" align="left" style="border: 1px solid #2D4262; width: 280px; height: 500px;
                            float: none" valign="top" >
                           <center> 
                               <asp:DataList ID="DataList1" runat="server" DataKeyField="noticeid" OnItemCommand="DataList1_ItemCommand"
                                Style="text-align: center" Width="100%">
                                <ItemTemplate>
                                    <table width="100%" style="text-align: left; font-weight: bold;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="LBLS" runat="server" Text='<%#Eval("noticesubject") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="LBLD" runat="server" Text='<%#Eval("noticedate","{0:d}") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="imgbtn" ImageUrl="~/images/info.png" Height="15px" Width="15px"
                                                    runat="server" CommandName="Show" />
                                                <%-- onclick="imgbtn_Click" />--%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList></center>
                        </td>
                        <td style="border: 1px solid #2D4262; width: 280px; height: 250px" align="left" valign=top>
                          <center>
                          <asp:Label ID="LBLalert" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                              <asp:DataList ID="DataList2" runat="server" OnItemCommand="DataList2_ItemCommand"
                                DataKeyField="noticeid" Style="text-align: center" Width="100%">
                                <ItemTemplate>
                                    <table width="100%" style="text-align: left; font-weight: bold;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="LBLS1" runat="server" Text='<%#Eval("noticesubject") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="LBLD1" runat="server" Text='<%#Eval("noticedate","{0:d}") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:ImageButton ID="imgbtn1" ImageUrl="~/images/info.png" Height="15px" Width="15px"
                                                    runat="server" CommandName="Show" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList></center>
                        </td>
                        <td style="border: 1px solid #2D4262; width: 280px; height: 250px">
                            <table border="0" cellpadding="2" cellspacing="2" style="margin-top: 0px; border: 1px solid #CCCCCC;
                                padding: 1px;" width="350px">
                                <tr>
                                    <td valign="top" style="height: 250px" >
                                        
                                            <asp:DataList ID="DataList3" runat="server" DataKeyField="Eventid" OnItemCommand="DataListE_ItemCommand"
                                                RepeatColumns="3" RepeatDirection="Horizontal" ShowFooter="False" 
                                                Width="100%">
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:ImageButton ID="Image1" runat="server" CommandName="Select" Height="50px" ImageUrl='<%#"~/event/"+Eval("Eventlogo") %>'
                                                                    ToolTip='<%# Eval("EventNAme") %>' Width="50px" />
                                                            </td>
                                                            <td>
                                                                <h2>
                                                                </h2>
                                                                <br />
                                                                <br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <br />
                                                </ItemTemplate>
                                            </asp:DataList>
                                        
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #2D4262; width: 280px; height: 250px">
                            <table>
                                <th align="center" style="border: 1px solid #2D4262; width: 280px" bgcolor="#2D4262">
                                    <b style="color: White">Resources & Updates</b>
                                </th>
                                <tr>
                                    <td  style="margin: 10px 0px 10px; padding: 0px; width: 280px; height: 240px; float: left;
                                        background: #FFFFFF; border: 1px solid #0066FF; border-top-left-radius: 41px;border-top-right-radius: 41px;">
                                       <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No Record Found</b>
                                       
                                    </td>
                                </tr>
                            </table>
                            <td style="border: 1px solid #2D4262; width: 280px; height: 250px">
                                <table>
                                    <th align="center" style="border: 1px solid #2D4262; width: 280px" bgcolor="#2D4262">
                                        <b style="color: White">Attendance</b>
                                    </th>
                                    <tr>
                                        <td style="margin: 10px 0px 10px; padding: 0px; width: 350px; height: 240px; float: left;
                                            background: #FFFFFF; border: 1px solid #0066FF; border-top-left-radius: 41px;
                                            border-top-right-radius: 41px;" valign="top">
                                            <%--<asp:Image ID="ads" runat="server" ImageUrl="~/dir/events_img.png"  height="240px" Width="55px"/>--%>
                                            <center>
                                                <br />
                                                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False"
                                                    DataSourceID="SqlDataSource1" EmptyDataText="There are no data records to display."
                                                    GridLines="None" ShowHeader="False" CellPadding="4" ForeColor="#333333">
                                                    <AlternatingRowStyle BackColor="White" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="teachattddate" SortExpression="teachattddate">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("teachattddate", "{0:D}") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("teachattddate") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="teachattdtypedesc" SortExpression="teachattdtypedesc">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("teachattdtypedesc") %>'></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("teachattdtypedesc") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EditRowStyle BackColor="#2461BF" />
                                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                                    <RowStyle BackColor="#EFF3FB" />
                                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                                </asp:GridView>
                                            </center>
                                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:cn %>"
                                                DeleteCommand="DELETE FROM [tbteacherattd] WHERE [teachid] = @teachid AND [teachattddate] = @teachattddate"
                                                InsertCommand="INSERT INTO [tbteacherattd] ([teachid], [teachattddate], [teachattdtypeid]) VALUES (@teachid, @teachattddate, @teachattdtypeid)"
                                                ProviderName="<%$ ConnectionStrings:cn.ProviderName %>" SelectCommand="SELECT TOP (7) tbteacherattd.teachattddate, tbteachattdtype.teachattdtypedesc FROM tbteacherattd INNER JOIN tbteachattdtype ON tbteacherattd.teachattdtypeid = tbteachattdtype.teachtattdtypeid WHERE (tbteacherattd.teachid = @tid) ORDER BY tbteacherattd.teachattddate DESC"
                                                UpdateCommand="UPDATE [tbteacherattd] SET [teachattdtypeid] = @teachattdtypeid WHERE [teachid] = @teachid AND [teachattddate] = @teachattddate">
                                                <DeleteParameters>
                                                    <asp:Parameter Name="teachid" Type="Int32" />
                                                    <asp:Parameter DbType="Date" Name="teachattddate" />
                                                </DeleteParameters>
                                                <InsertParameters>
                                                    <asp:Parameter Name="teachid" Type="Int32" />
                                                    <asp:Parameter DbType="Date" Name="teachattddate" />
                                                    <asp:Parameter Name="teachattdtypeid" Type="Int32" />
                                                </InsertParameters>
                                                <SelectParameters>
                                                    <asp:SessionParameter Name="tid" SessionField="stid" />
                                                </SelectParameters>
                                                <UpdateParameters>
                                                    <asp:Parameter Name="teachattdtypeid" Type="Int32" />
                                                    <asp:Parameter Name="teachid" Type="Int32" />
                                                    <asp:Parameter DbType="Date" Name="teachattddate" />
                                                </UpdateParameters>
                                            </asp:SqlDataSource>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div id="light" class="white_content">
        <table cellpadding="0" cellspacing="0" border="0" style="background-color: #a6c25c;"
            width="100%">
            <tr>
                <td height="16px">
                    <a href="javascript:void(0)" onclick="document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'">
                        <img src="../close.gif" style="border: 0px" width="13px" align="right" height="13px" /></a>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 16px; padding-right: 16px; padding-bottom: 16px">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="background-color: #fff"
                        width="100%">
                        <tr>
                            <td align="center" colspan="2" class="headertext">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Repeater ID="imglightbox" runat="server">
                                    <ItemTemplate>
                                        <p align="left">
                                            <%#Eval("noticesubject") %><br />
                                            <i>
                                                <%#Eval("noticedate","{0:d}") %></i></p>
                                        <br />
                                        <br />
                                        <%#Eval("noticecontent") %><br />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr>
                            <td height="10px">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div align="center" class=" headertext">
        </div>
    </div>
    <div id="fade" class="black_overlay">
    </div>
</asp:Content>
