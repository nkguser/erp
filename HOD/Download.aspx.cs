﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class HOD_Download : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        Int32 a = Convert.ToInt32(Session["depid"]);
        SqlCommand cmd = new SqlCommand("select depsyl,deptimetable from tbdep where depid=@u", con);
        cmd.Parameters.Add("@u", SqlDbType.Int).Value = a;
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            string b = dr[0].ToString();
            string c = dr[1].ToString();
            HyperLink1.NavigateUrl = "~/Timetable/" + a + "/" + c + "";
            HyperLink2.NavigateUrl = "~/Syllabus/" + a + "/" + b + "";
        }
        dr.Close();
        cmd.Dispose();
    }
}