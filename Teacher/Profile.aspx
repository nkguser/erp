﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="Profile.aspx.cs" Inherits="Teacher_Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
         <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" 
            CellPadding="4" ForeColor="#333333" 
            GridLines="None" Height="16px" onitemupdating="DetailsView1_ItemUpdating" 
            onmodechanging="DetailsView1_ModeChanging" Width="100%" 
            DataKeyNames="teacherid" 
            style="margin-right: 0px; margin-top: 15px">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
            <EditRowStyle BackColor="#999999" />
            <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
            <Fields>
                <asp:TemplateField HeaderText="Teacher ID">
                    <ItemTemplate>
                        <asp:Label ID="Label47" runat="server" Text='<%# Eval("teacherclgid") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Course">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("Coursename") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Department" SortExpression="teacherdepid">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("Depname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Salutation" SortExpression="teachersalutation">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("teachersalutation") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="First Name" SortExpression="teacherfname">
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("teacherfname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Middle Name" SortExpression="teachermname">
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Eval("teachermname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Last Name" SortExpression="teacherlname">
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Eval("teacherlname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date Of Birth" SortExpression="teacherdob">
                    <ItemTemplate>
                        <asp:Label ID="Label7" runat="server" Text='<%# Eval("teacherdob" ,"{0:d}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date Of Joining" SortExpression="teacherdoj">
                    <ItemTemplate>
                        <asp:Label ID="Label8" runat="server" Text='<%# Eval("teacherdoj","{0:d}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Father's name" 
                    SortExpression="teacherfathername">
                    <ItemTemplate>
                        <asp:Label ID="Label9" runat="server" Text='<%# Eval("teacherfathername") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mother's Name" 
                    SortExpression="teachermothername">
                    <ItemTemplate>
                        <asp:Label ID="Label10" runat="server" Text='<%# Eval("teachermothername") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Correspondence Address" 
                    SortExpression="teachercorresadd">
                    <ItemTemplate>
                        <asp:Label ID="Label11" runat="server" Text='<%# Eval("teachercorresadd") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Permanent Address" 
                    SortExpression="teacherpermaadd">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox17" runat="server" Height="92px" 
                            Text='<%# Eval("teacherpermaadd") %>' 
                            TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label12" runat="server" Text='<%# Eval("teacherpermaadd") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Post" SortExpression="teacherpostid">
                    <ItemTemplate>
                        <asp:Label ID="Label13" runat="server" Text='<%# Eval("postdesc") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Qualification" 
                    SortExpression="teacherqualification">
                    <ItemTemplate>
                        <asp:Label ID="Label14" runat="server" 
                            Text='<%# Eval("teacherqualification") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Experience" SortExpression="teacherexp">
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("teacherexp") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Phone Number" SortExpression="teacherphone1">
                    <ItemTemplate>
                        <asp:Label ID="Label16" runat="server" Text='<%# Eval("teacherphone1") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Alternate Phone Number" 
                    SortExpression="teacherphone2">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox25" runat="server" Text='<%# Eval("teacherphone2") %>'></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator7" runat="server" ValidationGroup="abc" ErrorMessage="**" ForeColor="Red" Operator="DataTypeCheck"  ControlToValidate="TextBox25" Type="Integer"></asp:CompareValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label17" runat="server" Text='<%# Eval("teacherphone2") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Picture" SortExpression="teacherphoto">
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" Height="80px" 
                            ImageUrl='<%#"~/Teacherpic/"+ Eval("teacherphoto") %>' Width="80px" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Gender" SortExpression="teachergender">
                    <ItemTemplate>
                        <asp:Label ID="Label20" runat="server" Text='<%# Eval("teachergender") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="E_mail" SortExpression="teacheremailid">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox27" runat="server" Text='<%# Eval("teacheremailid") %>'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="abc" ErrorMessage="***" ForeColor="Red" ControlToValidate="TextBox27" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label18" runat="server" Text='<%# Eval("teacheremailid") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Research Journals" 
                    SortExpression="teacherresrchjrnls">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox31" runat="server" Height="92px" 
                            Text='<%# Eval("teacherresrchjrnls") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label21" runat="server" Text='<%# Eval("teacherresrchjrnls") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Research Conferences" 
                    SortExpression="teacherresrchcnfrncs">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox33" runat="server" Height="92px" 
                            Text='<%# Eval("teacherresrchcnfrncs") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label22" runat="server" 
                            Text='<%# Eval("teacherresrchcnfrncs") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Workshop/Seminar" 
                    SortExpression="teacherworkshopseminar">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox35" runat="server" Height="92px" 
                            Text='<%# Eval("teacherworkshopseminar") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label23" runat="server" 
                            Text='<%# Eval("teacherworkshopseminar") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Other Acts" SortExpression="teacherotheracts">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox37" runat="server" Height="92px" 
                            Text='<%# Eval("teacherotheracts") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label24" runat="server" Text='<%# Eval("teacherotheracts") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Specialization" 
                    SortExpression="teacherspecialization">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox39" runat="server" Height="92px" 
                            Text='<%# Eval("teacherspecialization") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label25" runat="server" 
                            Text='<%# Eval("teacherspecialization") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Project/Award" 
                    SortExpression="teacherprojectaward">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox41" runat="server" Height="92px" 
                            Text='<%# Eval("teacherprojectaward") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label26" runat="server" 
                            Text='<%# Eval("teacherprojectaward") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Subject Undertaken" 
                    SortExpression="teachersubjectundertaken">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox43" runat="server" Height="92px" 
                            Text='<%# Eval("teachersubjectundertaken") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label27" runat="server" 
                            Text='<%# Eval("teachersubjectundertaken") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Conferences Conducted" 
                    SortExpression="teachercnfrncconducted">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox45" runat="server" Height="92px" 
                            Text='<%# Eval("teachercnfrncconducted") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label28" runat="server" 
                            Text='<%# Eval("teachercnfrncconducted") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Conferences Attended" 
                    SortExpression="teachercnfrncattend">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox47" runat="server" Height="92px" 
                            Text='<%# Eval("teachercnfrncattend") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label29" runat="server" 
                            Text='<%# Eval("teachercnfrncattend") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Other Info" SortExpression="teacherotherinfo">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox49" runat="server" Height="92px" 
                            Text='<%# Eval("teacherotherinfo") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label30" runat="server" Text='<%# Eval("teacherotherinfo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Maritial Status" 
                    SortExpression="teachermaritalstatus">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox3" Text='<%# Eval("teachermaritalstatus") %>' runat="server" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label32" runat="server" 
                            Text='<%# Eval("teachermaritalstatus") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Membership Of Professional Bodies" 
                    SortExpression="teachermembershipofprofessionalbodies">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox51" runat="server" Height="92px" 
                            Text='<%# Eval("teachermembershipofprofessionalbodies") %>' 
                            TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label33" runat="server" 
                            Text='<%# Eval("teachermembershipofprofessionalbodies") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Category" SortExpression="teachercategoryid">
                    
                    <ItemTemplate>
                        <asp:Label ID="Label31" runat="server" Text='<%# Eval("teachercategoryid") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pan Number" SortExpression="teacherpanno">
                    <ItemTemplate>
                        <asp:Label ID="Label34" runat="server" Text='<%# Eval("teacherpanno") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PF Number" SortExpression="teacherpfno">
                    <ItemTemplate>
                        <asp:Label ID="Label35" runat="server" Text='<%# Eval("teacherpfno") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Salary Bank Name" 
                    SortExpression="teachersalaryBankname">
                    <ItemTemplate>
                        <asp:Label ID="Label36" runat="server" 
                            Text='<%# Eval("teachersalaryBankname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Salary Branch Name" 
                    SortExpression="teachersalarybranchname">
                    <ItemTemplate>
                        <asp:Label ID="Label37" runat="server" 
                            Text='<%# Eval("teachersalarybranchname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Bank Account Number" 
                    SortExpression="teacherbankaccno">
                    <ItemTemplate>
                        <asp:Label ID="Label38" runat="server" Text='<%# Eval("teacherbankaccno") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ISFC Code" SortExpression="teacherISFCcode">
                    
                    <ItemTemplate>
                        <asp:Label ID="Label39" runat="server" Text='<%# Eval("teacherISFCcode") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Gross Pay" SortExpression="teachergrosspay">
                    
                    <ItemTemplate>
                        <asp:Label ID="Label40" runat="server" Text='<%# Eval("teachergrosspay") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pay Scale" SortExpression="teacherpayscale">
                   
                    <ItemTemplate>
                        <asp:Label ID="Label41" runat="server" Text='<%# Eval("teacherpayscale") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Teacher Or Non Teaching" 
                    SortExpression="teacherTORNTsts">
                    
                    <ItemTemplate>
                        <asp:Label ID="Label42" runat="server" Text='<%# Eval("teacherTORNTsts") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Appointment Type" 
                    SortExpression="teacherappointtype">
                    
                    <ItemTemplate>
                        <asp:Label ID="Label43" runat="server" Text='<%# Eval("teacherappointtype") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="University Approvel Letter Number" 
                    SortExpression="teacherUnivaprroveletterno">
                    
                    <ItemTemplate>
                        <asp:Label ID="Label44" runat="server" 
                            Text='<%# Eval("teacherUnivaprroveletterno") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="University Approvel Date" 
                    SortExpression="teacherunivapproveDate">
                    <ItemTemplate>
                        <asp:Label ID="Label45" runat="server" 
                            Text='<%# Eval("teacherunivapproveDate","{0:d}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Religion">
                    <ItemTemplate>
                        <asp:Label ID="Label46" runat="server" Text='<%# Eval("teacherreligion") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <EditItemTemplate>
                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="update" ValidationGroup="abc">Update</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="LinkButton5" runat="server" CommandName="cancel" >Cancel</asp:LinkButton>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:LinkButton ID="LinkButton6" runat="server" CommandName="insert">Insert</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="LinkButton7" runat="server" CommandName="cancel" >Cancel</asp:LinkButton>
                    </InsertItemTemplate>
                    <ItemTemplate>
                         &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CommandName="edit">Edit</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Fields>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        </asp:DetailsView>
</asp:Content>

