﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Student_testskltst : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        if(con.State==ConnectionState.Closed)
            con.Open();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        ot.clsscr obj = new ot.clsscr();
        Int32 a = obj.chksts(Convert.ToInt32(Session["stid"]), Convert.ToInt32(DropDownList1.SelectedValue));
        if (a == -1 || a > 10)
        {
           string path= Server.MapPath("~/student/testtst.aspx");
            Session["tcod"] = DropDownList1.SelectedValue;
            Session["tout"] = 45;
            ot.clsscrprp objprp = new ot.clsscrprp();
            objprp.p_scrdat = DateTime.Now.ToShortDateString();
            objprp.p_scrregcod = Convert.ToInt32(Session["stid"]);
            objprp.p_scrteccod = Convert.ToInt32(Session["tcod"]);
            objprp.p_scrval = 0;
            Int32 b=obj.Save_Rec(objprp);
            Session["key"] = b;
            //Session["wait"]=2;
            //ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:window.open('~/student/testtst.aspx','win2',width=350','toolbar=0','location=0','directories=0','status=0','menubar=0','resizable=0','fullscreen=1','scrollbars=1' );", true); 
            //ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:window.open('" + path +"');", true); 
            string url = "testtst.aspx";
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.open('");
            sb.Append(url);
            sb.Append("');");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(),"script", sb.ToString());
            //Response.Redirect("~/student/testtst.aspx");
        }
        else
            Label1.Text = "You have attempted this test within 10 days.Try Later";
    }
}