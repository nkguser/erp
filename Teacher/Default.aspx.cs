﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class Teacher_Default : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {       
        if (Page.IsPostBack == false)
        {
            DDLCourseBind();
            DDLSessionBind();
        }
    }
    private void DDLCourseBind()
    {        
        DDLCourse.DataTextField = "Coursename";
        DDLCourse.DataValueField = "Courseid";
        DDLCourse.DataSource = ERP.AllCourse();
        DDLCourse.DataBind();
        DDLDept.Enabled = true;
        DDLCourse.Items.Insert(0, "-- Select Course --");
    }
    private void DDLSessionBind()
    {        
        DDLSession.DataTextField = "session";
        DDLSession.DataValueField = "sessionid";
        DDLSession.DataSource = ERP.AllSession();
        DDLSession.DataBind();
        DDLSession.Items.Insert(0, "-- Select session --");
    }
    protected void DDLCourse_SelectedIndexChanged(object sender, EventArgs e)
    {        
        DDLDept.DataTextField = "Depname";
        DDLDept.DataValueField = "Depid";
        DDLDept.DataSource = ERP.AllDepartment(Convert.ToInt32(DDLCourse.SelectedValue));
        DDLDept.DataBind();
        DDLDept.Items.Insert(0, "-- Select Department --");
    }
    protected void DDLSession_SelectedIndexChanged(object sender, EventArgs e)
    {        
        DDLClass.DataTextField = "Class";
        DDLClass.DataValueField = "subjectallotgroupid";
        DDLClass.DataSource = ERP.TeacherClass(Convert.ToInt32(Session["stid"]), Convert.ToInt32(DDLSession.SelectedValue));
        DDLClass.DataBind();
        DDLClass.Enabled = true;
        DDLSubject.Enabled = true;
        DDLClass.Items.Insert(0, "-- Select Class --");
    }
    protected void DDLClass_SelectedIndexChanged(object sender, EventArgs e)
    {        
        DDLSubject.DataTextField = "subject";
        DDLSubject.DataValueField = "subjectallotsubjectid";
        DDLSubject.DataSource = ERP.TeacherSubject(Convert.ToInt32(Session["stid"]), Convert.ToInt32(DDLClass.SelectedValue));
        DDLSubject.DataBind();
        DDLSubject.Items.Insert(0, "-- Select Subject --");
        GridFillBind();
    }   
    private void GridFillBind()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT studentid,studentrollno FROM tbStudent where studentgroupid=@gid", con);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridFill.DataSource = ds;
        GridFill.DataBind();
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        Application.Lock();
        Int32 c = GridFill.Rows.Count;
        for (Int32 i = 0; i < c; i++)
        {
            GridFill.SelectRow(i);            
            Int32 l = Convert.ToInt32(GridFill.SelectedDataKey.Value);
            Boolean b = Convert.ToBoolean(((RadioButtonList)(GridFill.SelectedRow.FindControl("RBLAttendance"))).SelectedValue);
            Decimal d = Convert.ToDecimal(((TextBox)(GridFill.SelectedRow.FindControl("TxtMarks"))).Text);
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("INSERT INTO tbstudentsessional (studentsessionalstudentid, studentsessionalsubjectid, studentsessionalno, studentsessionaldate, studentsessionalattnd, studentsessionalmarks, studentsessionalmmarks, studentsessionalreqlvlid) VALUES (@rid,@sid,@sesn_no,@date,@at,@m,@mm, 1)", con);
            cmd.Parameters.Add("@rid", SqlDbType.Int).Value = l;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = DDLSubject.SelectedValue;
            DateTime sdate = Convert.ToDateTime(TxtDate.Text);
            cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(sdate.ToShortDateString());
            cmd.Parameters.Add("@sesn_no", SqlDbType.Int).Value = Convert.ToInt32(DDLSessional.SelectedValue);
            cmd.Parameters.Add("@at", SqlDbType.Bit).Value = b;
            cmd.Parameters.Add("@m", SqlDbType.Decimal).Value = d;
            cmd.Parameters.Add("@mm", SqlDbType.Int).Value = Convert.ToInt32(TXTMaxMarks.Text);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        Application.UnLock();
        Server.Transfer("~/teacher/studentsessional.aspx");
    }
    private Int32 SessionalNo()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SessionalNot", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@s", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.ExecuteScalar();
        Int32 k = Convert.ToInt32(cmd.Parameters["@s"].Value);
        cmd.Dispose();
        con.Close();
        return k;
    }
    private Int32 SessionalNoHOD()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SessionalNoHodsubmit", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@s", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.ExecuteScalar();
        Int32 k = Convert.ToInt32(cmd.Parameters["@s"].Value);
        cmd.Dispose();
        con.Close();
        return k;
    }
}