﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HOD_frmhodgallerypage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            string QsId;
            dvlb.Visible = false;
            dvslide.Visible = false;
            if (Request.QueryString["id"].ToString() != string.Empty & Request.QueryString["id"].ToString() != null)
            {
                QsId = Request.QueryString["id"].ToString();

                if (QsId == "1")
                {
                    dvlb.Visible = true;
                    DllightBind();
                }
                else if (QsId == "2")
                {
                    dvslide.Visible = true;
                    DlSlideBind();
                }
            }

        }
    }
    public void DllightBind()
    {
        nsphotosnak.clsalbdet obj = new nsphotosnak.clsalbdet();
        Dllightbox.DataSource = obj.Display_Rec
            (Convert.ToInt32(Request.QueryString["acod"]));
        Dllightbox.DataBind();
    }
    public void DlSlideBind()
    {
        nsphotosnak.clsalbdet obj = new nsphotosnak.clsalbdet();
        Dlslide.DataSource = obj.Display_Rec
            (Convert.ToInt32(Request.QueryString["acod"]));
        Dlslide.DataBind();
    }
    protected void Dllightbox_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}