﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Teacher_Profile : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (Page.IsPostBack == false)
            det_bind();
    }
    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        Boolean msts;
        TextBox perma = ((TextBox)(DetailsView1.Rows[12].FindControl("TextBox17")));
        TextBox p2 = ((TextBox)(DetailsView1.Rows[17].FindControl("TextBox25")));
        TextBox email = ((TextBox)(DetailsView1.Rows[20].FindControl("TextBox27")));
        TextBox resrchjour = ((TextBox)(DetailsView1.Rows[21].FindControl("TextBox31")));
        TextBox resrchcnfrncs = ((TextBox)(DetailsView1.Rows[22].FindControl("TextBox33")));
        TextBox workshop = ((TextBox)(DetailsView1.Rows[23].FindControl("TextBox35")));
        TextBox otheracts = ((TextBox)(DetailsView1.Rows[24].FindControl("TextBox37")));
        TextBox specialization = ((TextBox)(DetailsView1.Rows[25].FindControl("TextBox39")));
        TextBox project = ((TextBox)(DetailsView1.Rows[26].FindControl("TextBox41")));
        TextBox subundertaken = ((TextBox)(DetailsView1.Rows[27].FindControl("TextBox43")));
        TextBox cnfrnconduct = ((TextBox)(DetailsView1.Rows[28].FindControl("TextBox45")));
        TextBox cnfrnattend = ((TextBox)(DetailsView1.Rows[29].FindControl("TextBox47")));
        TextBox otherinfo = ((TextBox)(DetailsView1.Rows[30].FindControl("TextBox49")));
        CheckBox mst = ((CheckBox)(DetailsView1.Rows[31].FindControl("CheckBox3")));
        if (mst.Checked == true)
        {
            msts = true;
        }
        else
        {
            msts = false;
        }
        TextBox memofprofbody = ((TextBox)(DetailsView1.Rows[32].FindControl("TextBox51")));
        SqlCommand cmd = new SqlCommand("updviewteacher", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        if (perma.Text == null)
        {
            cmd.Parameters.Add("@perma", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@perma", SqlDbType.VarChar, 500).Value = perma.Text;
        }
        if (p2.Text == null || p2.Text=="")
        {
            cmd.Parameters.Add("@p2", SqlDbType.BigInt).Value = Convert.DBNull;
        }
        else
        {
            cmd.Parameters.Add("@p2", SqlDbType.BigInt).Value = Convert.ToInt64(p2.Text);
        }
        if (email.Text == null)
        {
            cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = email.Text;
        }
        if (resrchjour.Text == null)
        {
            cmd.Parameters.Add("@resrchjour", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@resrchjour", SqlDbType.VarChar, 500).Value = resrchjour.Text;
        }
        if (resrchcnfrncs.Text == null)
        {
            cmd.Parameters.Add("@resrchcnfrncs", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@resrchcnfrncs", SqlDbType.VarChar, 500).Value = resrchcnfrncs.Text;
        }
        if (workshop.Text == null)
        {
            cmd.Parameters.Add("@workshop", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@workshop", SqlDbType.VarChar, 500).Value = workshop.Text;
        }
        if (otheracts.Text == null)
        {
            cmd.Parameters.Add("@otheracts", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@otheracts", SqlDbType.VarChar, 500).Value = otheracts.Text;
        }
        if (specialization.Text == null)
        {
            cmd.Parameters.Add("@specialization", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@specialization", SqlDbType.VarChar, 500).Value = specialization.Text;
        }
        if (project.Text == null)
        {
            cmd.Parameters.Add("@project", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@project", SqlDbType.VarChar, 500).Value = project.Text;
        }
        if (subundertaken.Text == null)
        {
            cmd.Parameters.Add("@subundertaken", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@subundertaken", SqlDbType.VarChar, 500).Value = subundertaken.Text;
        }
        if (cnfrnconduct.Text == null)
        {
            cmd.Parameters.Add("@cnfrnconduct", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@cnfrnconduct", SqlDbType.VarChar, 500).Value = cnfrnconduct.Text;
        }
        if (cnfrnattend.Text == null)
        {
            cmd.Parameters.Add("@cnfrnattend", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@cnfrnattend", SqlDbType.VarChar, 500).Value = cnfrnattend.Text;
        }
        if (otherinfo.Text == null)
        {
            cmd.Parameters.Add("@otherinfo", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@otherinfo", SqlDbType.VarChar, 500).Value = otherinfo.Text;
        }
        cmd.Parameters.Add("@msts", SqlDbType.Bit).Value = msts;
        if (memofprofbody.Text == null)
        {
            cmd.Parameters.Add("@memofprofbody ", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@memofprofbody ", SqlDbType.VarChar, 500).Value = memofprofbody.Text;
        }
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly);
        det_bind();
    }
    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        DetailsView1.ChangeMode(e.NewMode);
        det_bind();
    }
    private void det_bind()
    {
        SqlCommand cmd = new SqlCommand("dispteacher", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("tid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DetailsView1.DataSource = ds;
        DetailsView1.DataBind();
    }
}