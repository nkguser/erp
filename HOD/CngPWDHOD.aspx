﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="CngPWDHOD.aspx.cs" Inherits="HOD_CngPWDHOD" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <table class="styledmenu">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                    <table class="style56" __designer:mapid="2a7c">
                        <tr __designer:mapid="2a7d">
                            <td class="style57" __designer:mapid="2a7e">
                                &nbsp;</td>
                            <td __designer:mapid="2a7f">
                                &nbsp;</td>
                        </tr>
                        <tr __designer:mapid="2a80">
                            <td rowspan="4" __designer:mapid="2a81">
                                &nbsp;</td>
                            <td class="style58" __designer:mapid="2a83">
                                Current password :</td>
                            <td class="style59" __designer:mapid="2a84">
                                <asp:TextBox ID="TXTOldPwd" runat="server" Width="181px" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TXTOldPwd" Display="Dynamic" 
                                    ErrorMessage="*" ForeColor="Red" ValidationGroup="abc">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr __designer:mapid="2a87">
                            <td class="style60" __designer:mapid="2a88">
                                New Password :</td>
                            <td class="style61" __designer:mapid="2a89">
                                <asp:TextBox ID="TXTNewPwd" runat="server" Width="181px" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                    ControlToValidate="TXTNewPwd" Display="Dynamic" 
                                    ErrorMessage="**" ForeColor="Red" ValidationGroup="abc">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr __designer:mapid="2a8c">
                            <td class="style64" __designer:mapid="2a8d">
                                Confirm Password :</td>
                            <td class="style65" __designer:mapid="2a8e">
                                <asp:TextBox ID="TXTConfirmPwd" runat="server" Width="181px" 
                                    TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                    ControlToValidate="TXTConfirmPwd" Display="Dynamic" 
                                    ErrorMessage="**" ForeColor="Red" ValidationGroup="abc">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr __designer:mapid="2a91">
                            <td class="style64" __designer:mapid="2a92">
                                <asp:LinkButton ID="LBChangePwd" runat="server" ForeColor="#567098" 
                                    onclick="LBChangePwd_Click" ValidationGroup="abc">Change Password</asp:LinkButton>
                            </td>
                            <td class="style65" __designer:mapid="2a94">
                                <asp:Label ID="LblMessage" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr __designer:mapid="2a96">
                            <td align="center" class="style57" __designer:mapid="2a97">
                                &nbsp;</td>
                            <td __designer:mapid="2a98">
                                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                    ControlToCompare="TXTNewPwd" ControlToValidate="TXTConfirmPwd" 
                                    ErrorMessage="CompareValidator" ForeColor="Red">Password is Mismatched</asp:CompareValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

