﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class HOD_AddNotice : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (Page.IsPostBack == false)
            grd_bind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlCommand cmd1 = new SqlCommand("select count(*) as cnt from tbnotice where noticeid=@u",con);
        cmd1.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(ViewState["cod"]);
        Int32 a = Convert.ToInt32(cmd1.ExecuteScalar());
        if (a == 0)
        {
            if (Button1.Text == "Save")
            {
                SqlCommand cmd = new SqlCommand("insert into tbNotice(noticedate,Noticesubject,noticecontent,noticelevelid,noticetolevelid,noticefromid,noticedelsts) values(@nd,@ns,@nc,@nlid,Null,@nfid,0) ", con);
                cmd.Parameters.Add("@ns", SqlDbType.VarChar, 100).Value = txtheader.Text;
                cmd.Parameters.Add("@nc", SqlDbType.NText).Value = Editor1.Content;
                cmd.Parameters.Add("@nd", SqlDbType.DateTime).Value = System.DateTime.Now;
                cmd.Parameters.Add("@nlid", SqlDbType.Int).Value = Convert.ToInt32(DropDownList1.SelectedValue);
                cmd.Parameters.Add("nfid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
                txtheader.Text = String.Empty;
                Editor1.Content = String.Empty;
                lblmsg.Text = "Notice Submitted Successfully";
                grd_bind();
            }
        }
        else
        {
            SqlCommand cmd = new SqlCommand("update tbNotice set noticedate=@nd,Noticesubject=@ns,noticecontent=@nc,noticelevelid=@nlid,noticefromid=@nfid where noticeid=@d", con);
            cmd.Parameters.Add("@ns", SqlDbType.VarChar, 100).Value = txtheader.Text;
            cmd.Parameters.Add("@nc", SqlDbType.NText).Value = Editor1.Content;
            Int32 x = Convert.ToInt32(ViewState["cod"]);
            cmd.Parameters.Add("@d", SqlDbType.Int).Value = Convert.ToInt32(ViewState["cod"]);
            cmd.Parameters.Add("@nd", SqlDbType.DateTime).Value = System.DateTime.Now;
            cmd.Parameters.Add("@nlid", SqlDbType.Int).Value = Convert.ToInt32(DropDownList1.SelectedValue);
            cmd.Parameters.Add("nfid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
            txtheader.Text = String.Empty;
            Editor1.Content = String.Empty;
            lblmsg.Text = "Notice Updated Successfully";
            Button1.Text = "Save";
            grd_bind();
        }
    }
    private void grd_bind()
    {
        SqlCommand cmd2 = new SqlCommand("select noticeid,noticedate,noticesubject,noticecontent from tbnotice where noticefromid=@u ",con);
        cmd2.Parameters.Add("@u",SqlDbType.Int).Value=Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd2);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridView1.DataSource = ds;
        GridView1.DataBind();
        cmd2.Dispose();
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Int32 a = Convert.ToInt32(GridView1.DataKeys[e.NewEditIndex][0]);
        ViewState["cod"] = a;
        SqlCommand cmd3 = new SqlCommand("select noticesubject,noticecontent,noticelevelid from tbnotice where noticeid=@u  ", con);
        cmd3.Parameters.Add("@u",SqlDbType.Int).Value=a;
        SqlDataReader dr = cmd3.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            txtheader.Text = dr["noticesubject"].ToString();
            Editor1.Content = dr["noticecontent"].ToString();
            DropDownList1.SelectedIndex = -1;
            DropDownList1.Items.FindByValue(dr["noticelevelid"].ToString()).Selected = true;
        }
        dr.Close();
        cmd3.Dispose();
        Button1.Text = "update";
        e.Cancel = true;
    }
}