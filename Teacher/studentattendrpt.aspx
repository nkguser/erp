﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="studentattendrpt.aspx.cs" Inherits="Teacher_studentattend" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      <table width="100%" style="text-align: left; font-weight: bold;">
    <%--   <tr>
            <td>
                Course</td>
            <td>
                <asp:DropDownList ID="DDLCourse" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLCourse_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Department</td>
            <td>
                <asp:DropDownList ID="DDLDept" runat="server">
                    <asp:ListItem>-- Select Department --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>--%>
        <tr>
            <td>
                Session</td>
            <td>
                <asp:DropDownList ID="DDLSession" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLSession_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Class</td>
            <td>
                <asp:DropDownList ID="DDLClass" runat="server" AutoPostBack="True" 
                    Enabled="False" onselectedindexchanged="DDLClass_SelectedIndexChanged">
                    <asp:ListItem>-- Select Class --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Subject</td>
            <td>
                <asp:DropDownList ID="DDLSubject" runat="server" Enabled="False">
                    <asp:ListItem>-- Choose Subject --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style19" colspan="3">
                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
                    Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
                    WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
                    <LocalReport ReportPath="Teacher\st_attend.rdlc">
                        <DataSources>
                            <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="rpt_a" />
                        </DataSources>
                    </LocalReport>
                </rsweb:ReportViewer>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:cn %>" SelectCommand="rpt_t_a" 
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DDLClass" Name="gid" 
                            PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="DDLSubject" Name="sid" 
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style19">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        </table>        
        <table>
        <tr>
            <td colspan="2">
                &nbsp;</td>
            <td>              
                <br />
            </td>
        </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

