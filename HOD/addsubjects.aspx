﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="addsubjects.aspx.cs" Inherits="HOD_addsubjects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <center>  
    <table style="text-align: left; font-weight: bold;">
    <tr>
        <td>
            <strong style="font-size: large" xml:lang="aa">Add Subject:</strong></td>
        <td>
            <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Green"></asp:Label>
        </td>
    </tr>
    <tr>
        <td>
            Univertsity Code :</td>
        <td>
            <asp:TextBox ID="TxtUniCode" runat="server" Width="400px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ControlToValidate="TxtUniCode" ErrorMessage="RequiredFieldValidator" 
                ForeColor="Red" ValidationGroup="abc">*</asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            Subject Title</td>
        <td>
            <asp:TextBox ID="TxtSubTitle" runat="server" Width="400px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ControlToValidate="TxtSubTitle" ErrorMessage="RequiredFieldValidator" 
                ForeColor="Red" ValidationGroup="abc">*</asp:RequiredFieldValidator>
        </td>
    </tr>
        <tr>
            <td>
                Sub-Title</td>
            <td>
                <asp:TextBox ID="TxtSubshortTitle" runat="server" Width="400px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="TxtSubshortTitle" ErrorMessage="RequiredFieldValidator" 
                    ForeColor="Red" ValidationGroup="abc">*</asp:RequiredFieldValidator>
            </td>
        </tr>
    <tr>
        <td>
            Subject Semester</td>
        <td>
            <asp:DropDownList ID="DDLSem" runat="server" Width="400px">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            Subject Type</td>
        <td>
            <asp:DropDownList ID="DDLType" runat="server" Width="400px">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Save" 
                ValidationGroup="abc" Height="30px" Width="100px" />
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    </table></center>
    <table width="100%" style="text-align: left; font-weight: bold;">
    <tr>
        <td colspan="2">
         <div style="overflow:scroll;height:300px;width:auto">
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                CellPadding="4" DataSourceID="ObjectDataSource1" ForeColor="#333333" 
                GridLines="None" EmptyDataText="There are no data for display...." 
                 ShowHeaderWhenEmpty="True" Width="100%">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:BoundField DataField="p_subjectunicode" HeaderText="University Code" 
                        SortExpression="p_subjectunicode" />
                    <asp:BoundField DataField="p_subjectsem" HeaderText="Semester" 
                        SortExpression="p_subjectsem" />
                    <asp:BoundField DataField="p_subjecttitle" HeaderText="Title" 
                        SortExpression="p_subjecttitle" />
                    <%--<asp:TemplateField HeaderText="Subject Type">
                    <ItemTemplate>
                                <asp:Label ID="LblSubjecttype" runat="server" Text='<%# Eval("Subjecttypename") %>'></asp:Label>
                            </ItemTemplate>
                    </asp:TemplateField>--%>
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView></div>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                SelectMethod="Display_rec" TypeName="kitmerp.clsSubject" 
                OldValuesParameterFormatString="original_{0}">
                <SelectParameters>
                    <asp:SessionParameter Name="sid" SessionField="depid" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
        </td>
    </tr>
</table>
   

    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

