﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net;
using kitmerp;
using System.IO;

public partial class Admin_ViewSyllabusAdmin : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    SqlDataReader dr;
    Int32 id;
    string syl1, tt1, dns, dnt;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (Page.IsPostBack == false)
        {
            CourseBind();
        }
    }
    private void CourseBind()
    {
        DataSet ds = AllCourse();
        DropDownList1.DataValueField = "courseid";
        DropDownList1.DataTextField = "coursename";
        DropDownList1.DataSource = ds;
        DropDownList1.DataBind();
        DropDownList1.Items.Insert(0, "--Select Course --");
    }
    private DataSet AllCourse()
    {
        SqlDataAdapter adp = new SqlDataAdapter("select * from tbcourse", con);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    private void dlist_bind(Int32 a)
    {

        SqlCommand cmd1 = new SqlCommand("select depid, depname,depsyl,deptimetable from tbdep where depcourseid=@cid", con);
        cmd1.Parameters.Add("cid", SqlDbType.Int).Value = a;
        SqlDataAdapter adp = new SqlDataAdapter(cmd1);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DataList1.DataSource = ds;
        DataList1.DataBind();
        DataList1.Visible = true;
    }
    protected void DataList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 a = Convert.ToInt32(DropDownList1.SelectedValue);
        dlist_bind(a);
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {


        SqlCommand cmd = new SqlCommand("select depid, depname,depsyl,deptimetable from tbdep where depname=" + dns, con);

        dr = cmd.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            id = Convert.ToInt32(dr["depid"]);
            syl1 = dr["depsyl"].ToString();
            //tt1 = dr["deptimetable"].ToString();
        }
        WebClient req = new WebClient();
        HttpResponse response = HttpContext.Current.Response;
        response.Clear();
        response.ClearContent();
        response.ClearHeaders();
        response.Buffer = true;
        response.AddHeader("Content-Disposition", "attachment;filename=\"" + Server.MapPath("~/Syllabus" + "/" + id + "/" + syl1) + "\"");
        byte[] data = req.DownloadData(Server.MapPath("~/Syllabus" + "/" + id + "/" + syl1));
        response.BinaryWrite(data);
        response.End();
        dr.Close();
        cmd.Dispose();

    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand("select depid, depname,depsyl,deptimetable from tbdep where depname=" + Convert.ToInt32(DataList1.DataKeys[0]), con);
        cmd.Parameters.Add("cid", SqlDbType.Int).Value = Convert.ToInt32(DropDownList1.SelectedValue);
        dr = cmd.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            id = Convert.ToInt32(dr["depid"]);
            //syl1 = dr["depsyl"].ToString();
            tt1 = dr["deptimetable"].ToString();
        }
        WebClient req = new WebClient();
        HttpResponse response = HttpContext.Current.Response;
        response.Clear();
        response.ClearContent();
        response.ClearHeaders();
        response.Buffer = true;
        response.AddHeader("Content-Disposition", "attachment;filename=\"" + Server.MapPath("~/TimeTable" + "/" + id + "/" + tt1) + "\"");
        byte[] data = req.DownloadData(Server.MapPath("~/TimeTable" + "/" + id + "/" + tt1));
        response.BinaryWrite(data);
        response.End();
        dr.Close();
    }
}
//    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
//    {
//        if (e.CommandName == "Show")
//        {
//            //Button imgbtn = sender as Button;
//            ////Find Image button in gridview
//            //Button imgbtntxt = (Button)Repeater1.FindControl("imgbtn");
//            //Assign imagebutton url to image field in lightbox
//            Int32 a = Convert.ToInt32(DataList1.DataKeys[e.Item.ItemIndex]);
//        }
//    }
//}

//var fileInfo = new System.IO.FileInfo(filePath);
//        Response.ContentType = "application/octet-stream";
//        Response.AddHeader("Content-Disposition", String.Format("attachment;filename=\"{0}\"", filePath));
//        Response.AddHeader("Content-Length", fileInfo.Length.ToString());
//        Response.WriteFile(filePath);
//        Response.End();



//Response.ContentType = "image/jpeg";
//Response.AppendHeader("Content-Disposition","attachment; filename=SailBig.jpg");
//Response.TransmitFile( Server.MapPath("~/images/sailbig.jpg") );
//Response.End();

//Bitmap bmp = wwWebUtils.CornerImage(backcolor, color, c, Radius, Height, Width);
 
//Response.ContentType = "image/jpeg";
//Response.AppendHeader("Content-Disposition","attachment; filename=LeftCorner.jpg");
//bmp.Save(Response.OutputStream, ImageFormat.Jpeg);



//Response.Clear();
//Response.AppendHeader("Content-Disposition", "attachment; filename=" + fi.Name);
//Response.AppendHeader("Content-Length", fi.Length.ToString());
//Response.ContentType = "application//octet-stream";
//Response.TransmitFile(fi.FullName);
//Response.Flush();
//Response.End();



// Response.Clear()
//        Response.AddHeader("content-disposition", "attachment;filename=FileName.txt")
//        Response.Charset = ""
//        Response.Cache.SetCacheability(HttpCacheability.NoCache)
//        Response.ContentType = "application/vnd.text"

//        Dim stringWrite As System.IO.StringWriter = New System.IO.StringWriter()
//        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)

//        Response.Write(str.ToString())

//        Response.TransmitFile(Server.MapPath("~/FileName.txt"))

//        Response.End()


//<code lang="vb"
//Public Shared Sub DownloadFile(ByVal FilePath As String, Optional ByVal ContentType As String = "")

//If File.Exists(FilePath) Then
//Dim myFileInfo As FileInfo
//Dim StartPos As Long = 0, FileSize As Long, EndPos As Long

//myFileInfo = New FileInfo(FilePath)
//FileSize = myFileInfo.Length
//EndPos = FileSize

//HttpContext.Current.Response.Clear()
//HttpContext.Current.Response.ClearHeaders()
//HttpContext.Current.Response.ClearContent()

//Dim Range As String = HttpContext.Current.Request.Headers("Range")
//If Not ((Range Is Nothing) Or (Range = "")) Then
//Dim StartEnd As Array = Range.SubString(Range.LastIndexOf("=") + 1).Split("-")
//If Not StartEnd(0) = "" Then
//StartPos = CType(StartEnd(0), Long)
//End If
//If StartEnd.GetUpperBound(0) >= 1 And Not StartEnd(1) = "" Then
//EndPos = CType(StartEnd(1), Long)
//Else
//EndPos = FileSize - StartPos
//End If
//If EndPos > FileSize Then
//EndPos = FileSize - StartPos
//End If
//HttpContext.Current.Response.StatusCode = 206
//HttpContext.Current.Response.StatusDescription = "Partial Content"
//HttpContext.Current.Response.AppendHeader("Content-Range", "bytes " & StartPos & "-" & EndPos & "/" & FileSize)
//End If

//If Not (ContentType = "") And (StartPos = 0) Then
//HttpContext.Current.Response.ContentType = ContentType
//End If
//Try
//HttpContext.Current.Response.AppendHeader("Content-disposition", "attachment; filename=" & myFileInfo.Name)
//HttpContext.Current.Response.WriteFile(FilePath, StartPos, EndPos)
//HttpContext.Current.Response.End()
//Catch Ex As Exception

//End Try
//End If
//End Sub
//</code&gt



 //using System.IO;
 //   using System.Threading;

 //   protected void Page_Load(object sender, EventArgs e)
 //   {
 //       string filename = Request["file"].ToString();
 //       fileDownload(filename, Server.MapPath("~/downloadstuff/"+filename));
 //   }
 //   private void fileDownload(string fileName, string fileUrl)
 //   {
 //       Page.Response.Clear();
 //       bool success = ResponseFile(Page.Request, Page.Response, fileName, fileUrl, 1024000);
 //       if (!success)
 //           Response.Write("Downloading Error!");
 //       Page.Response.End();

 //   }
 //   public static bool ResponseFile(HttpRequest _Request, HttpResponse _Response, string _fileName, string _fullPath, long _speed)
 //   {
 //       try
 //       {
 //           FileStream myFile = new FileStream(_fullPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
 //           BinaryReader br = new BinaryReader(myFile);
 //           try
 //           {
 //               _Response.AddHeader("Accept-Ranges", "bytes");
 //               _Response.Buffer = false;
 //               long fileLength = myFile.Length;
 //               long startBytes = 0;

 //               int pack = 10240; //10K bytes
 //               int sleep = (int)Math.Floor((double)(1000 * pack / _speed)) + 1;
 //               if (_Request.Headers["Range"] != null)
 //               {
 //                   _Response.StatusCode = 206;
 //                   string[] range = _Request.Headers["Range"].Split(new char[] { '=', '-' });
 //                   startBytes = Convert.ToInt64(range[1]);
 //               }
 //               _Response.AddHeader("Content-Length", (fileLength - startBytes).ToString());
 //               if (startBytes != 0)
 //               {
 //                   _Response.AddHeader("Content-Range", string.Format(" bytes {0}-{1}/{2}", startBytes, fileLength - 1, fileLength));
 //               }
 //               _Response.AddHeader("Connection", "Keep-Alive");
 //               _Response.ContentType = "application/octet-stream";
 //               _Response.AddHeader("Content-Disposition", "attachment;filename=" + HttpUtility.UrlEncode(_fileName, System.Text.Encoding.UTF8));

 //               br.BaseStream.Seek(startBytes, SeekOrigin.Begin);
 //               int maxCount = (int)Math.Floor((double)((fileLength - startBytes) / pack)) + 1;

 //               for (int i = 0; i < maxCount; i++)
 //               {
 //                   if (_Response.IsClientConnected)
 //                   {
 //                       _Response.BinaryWrite(br.ReadBytes(pack));
 //                       Thread.Sleep(sleep);
 //                   }
 //                   else
 //                   {
 //                       i = maxCount;
 //                   }
 //               }
 //           }
 //           catch
 //           {
 //               return false;
 //           }
 //           finally
 //           {
 //               br.Close();
 //               myFile.Close();
 //           }
 //       }
 //       catch
 //       {
 //           return false;
 //       }
 //       return true;
 //   }protected void  DataList1_ItemCommand(object source, DataListCommandEventArgs e)

