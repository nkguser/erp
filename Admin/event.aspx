﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/InnerAdmin.master" AutoEventWireup="true" CodeFile="event.aspx.cs" Inherits="Admin_event" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%" style="text-align: left; font-weight: bold;">
      <tr>
      <td align="center" colspan="3">
          <asp:Label ID="ll" runat="server" Font-Bold="True" Font-Size="XX-Large" 
              ForeColor="#003399">Events Programme </asp:Label></td>
      </tr>
      <tr>
      <td></td>
      </tr>
      <tr>
      <td></td>
      </tr>
        <tr>
            <td style="width: 110px"  >
                Event Type</td>
            <td >
                <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" 
                    DataSourceID="SqlDataSource2" DataTextField="Eventcatdetail" 
                    DataValueField="Eventcatid">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 110px" >
                Event Title</td>
            <td >
                <asp:TextBox ID="TextBox1" runat="server" Width="400px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 110px" >
                Venue</td>
            <td >
                <asp:TextBox ID="TextBox2" runat="server" Width="400px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 110px" >
                From (Date)-DD/MM/YYYY </td>
            <td >
                <asp:TextBox ID="TextBox3" runat="server" Width="200px"></asp:TextBox>
                <asp:CalendarExtender Format="dd/MM/yyyy "  ID="TextBox3_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="TextBox3">
                </asp:CalendarExtender>
               <%-- <asp:TextBox ID="TextBox6" runat="server" Width="50px"></asp:TextBox>
                <asp:TextBoxWatermarkExtender ID="TextBox6_TextBoxWatermarkExtender" 
                    runat="server" Enabled="True" TargetControlID="TextBox6" 
                    WatermarkText="HH:MM AM/PM">
                </asp:TextBoxWatermarkExtender>--%>
            </td>
        </tr>
        <tr>
            <td style="height: 26px; width: 110px;" >
                To (Date)</td>
            <td style="height: 26px" >
                <asp:TextBox ID="TextBox5" runat="server" Width="200px"></asp:TextBox>
                <asp:CalendarExtender Format="dd/MM/yyyy "  ID="TextBox5_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="TextBox5">
                </asp:CalendarExtender>
               <%-- <asp:TextBox ID="TextBox7" runat="server" Width="50px"></asp:TextBox>
                <asp:TextBoxWatermarkExtender ID="TextBox7_TextBoxWatermarkExtender" 
                    runat="server" Enabled="True" TargetControlID="TextBox7" 
                    WatermarkText="HH:MM AM/PM">
                </asp:TextBoxWatermarkExtender>--%>
            </td>
        </tr>
        <tr>
            <td style="width: 110px" >
                Event
                Logo</td>
            <td >
                <asp:FileUpload ID="FileUpload1" runat="server" Width="300px" />
            </td>
        </tr>
        <tr>
            <td style="width: 110px" >
                Event Details</td>
            <td align="left">
                <cc1:Editor ID="Editor2" runat="server" Height="300px" Width="750px" />
            </td>
        </tr>
        <tr>
            <td style="width: 110px" >
                </td>
            <td >
                <asp:Button ID="Button1" runat="server" Text="Save" onclick="Button1_Click" 
                    Width="52px" />               
                </td>
            <td>
               </td>
        </tr>
        <tr>
            <td style="width: 110px" >
                </td>
            <td >
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:cn %>" 
                    DeleteCommand="DELETE FROM [tbeventcat] WHERE [Eventcatid] = @Eventcatid" 
                    InsertCommand="INSERT INTO [tbeventcat] ([Eventcatid], [Eventcatdetail]) VALUES (@Eventcatid, @Eventcatdetail)" 
                    SelectCommand="SELECT [Eventcatid], [Eventcatdetail] FROM [tbeventcat] ORDER BY [Eventcatdetail]" 
                    
                    UpdateCommand="UPDATE [tbeventcat] SET [Eventcatdetail] = @Eventcatdetail WHERE [Eventcatid] = @Eventcatid">
                    <DeleteParameters>
                        <asp:Parameter Name="Eventcatid" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="Eventcatid" Type="Int32" />
                        <asp:Parameter Name="Eventcatdetail" Type="String" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Eventcatdetail" Type="String" />
                        <asp:Parameter Name="Eventcatid" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                </td>
        </tr>
        <tr>
            <td colspan='3'>
            <asp:Panel ID="pp" runat="server" ScrollBars="Auto" Height="300px">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" DataKeyNames="Eventid" 
                    EmptyDataText="There are no data records to display." ForeColor="#333333" 
                    GridLines="None" onrowediting="GridView1_RowEditing" Width="865px">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Eventlogo" SortExpression="Eventlogo">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Eventlogo") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Image ID="Label1" runat="server" ImageUrl='<%# "~/event/" + Eval("Eventlogo") %>' Height='30px' Width='30'></asp:Image>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="EventNAme" HeaderText="Name" 
                            SortExpression="EventNAme" />
                        <asp:TemplateField HeaderText="Detail" SortExpression="eventdetail">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("eventdetail") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Literal ID="Label2" runat="server" Text='<%# Bind("eventdetail") %>'></asp:Literal>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="eventfrmdat" HeaderText="Start Date" 
                            SortExpression="eventfrmdat" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="eventtodat" HeaderText="End Date" 
                            SortExpression="eventtodat" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="eventvenue" HeaderText="Venue" 
                            SortExpression="eventvenue" />
                        <asp:BoundField DataField="Eventcatdetail" HeaderText="Category" 
                            SortExpression="Eventcatdetail" />
                        <asp:CommandField ShowEditButton="True" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView></asp:Panel>
            </td>
        </tr>
    </table>


</asp:Content>

