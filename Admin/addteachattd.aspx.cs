﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_addteachattd : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            seserp.DropdownWebService obj = new seserp.DropdownWebService();
            obj.sum(Convert.ToInt32(Session["other"]));
        }
    }
    private void gridbind()
    {
        SqlCommand cmd = new SqlCommand("SELECT tbteacher.teacherid, tbteacher.teacheruserid, tbteacher.teacherclgid, tbteacher.teachersalutation + ' ' + tbteacher.teacherfname + ' ' + tbteacher.teachermname + ' ' + tbteacher.teacherlname AS name, tbdep.depname, tbpost.postdesc, tbteacher.teacherphoto FROM            tbteacher INNER JOIN                          tbdep ON tbteacher.teacherdepid = tbdep.depid INNER JOIN                         tbpost ON tbteacher.teacherpostid = tbpost.postid WHERE        (tbteacher.teacherdelsts = 0) AND (tbteacher.teacherTORNTsts = 1) AND (tbteacher.teacherdepid = @did)", con);
        
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = Convert.ToInt32(DropDownList2.SelectedValue);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        //GridView1.DataSource = dr;
        //GridView1.DataBind();
        DataList1.DataSource = dr;
        DataList1.DataBind();
        dr.Close();
        cmd.Dispose();
        con.Close();
    }
    

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        // foreach (DataListItem item in DataList1.Items)        
        //{
        //    HtmlInputText strmybid = (HtmlInputText)item.FindControl("txtMyBid");        
        SqlCommand cmd1 = new SqlCommand("SELECT   COUNT(*) AS cnt FROM   tbteacherattd INNER JOIN tbteacher ON tbteacherattd.teachid = tbteacher.teacherid WHERE    (tbteacherattd.teachattddate = @d) AND (tbteacher.teacherdepid = @dp)", con);
        cmd1.Parameters.Add("@d", SqlDbType.Date).Value = Convert.ToDateTime(TextBox1.Text);
        cmd1.Parameters.Add("@dp", SqlDbType.Int).Value = Convert.ToInt32(DropDownList2.SelectedValue);
        Int32 check = Convert.ToInt32(cmd1.ExecuteScalar());
        cmd1.Dispose();
        if (check == 0)
        {
            Application.Lock();
            for (int i = 0; i < DataList1.Items.Count; i++)
            {
                Int32 tid = Convert.ToInt32(DataList1.DataKeys[i]);
                Int32 at = Convert.ToInt32(((RadioButtonList)(DataList1.Items[i].FindControl("RadioButtonList1"))).SelectedValue);
                SqlCommand cmd = new SqlCommand("INSERT INTO tbteacherattd  (teachid, teachattddate, teachattdtypeid) VALUES     (@tid,@date,@attdtype)", con);
                cmd.Parameters.Add("@tid", SqlDbType.Int).Value = tid;
                cmd.Parameters.Add("@attdtype", SqlDbType.Int).Value = at;
                cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(TextBox1.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            Application.UnLock();
            Label4.Text = "Attendance has been filled for Date: " + TextBox1.Text;
            TextBox1.Text = string.Empty;
        }
        else
        {
            Label4.Text = "Attendance Already been submitted for Date: " +TextBox1.Text;
            TextBox1.Text = string.Empty;
        }

        //for (int i = 0; i < GridView1.Rows.Count; i++)
        //{
        //    GridView1.SelectRow(i);
        //    Int32 tid = Convert.ToInt32(GridView1.SelectedDataKey.Value);
        //    Int32 tid = Convert.ToInt32(GridView1.DataKeys[i].Value);
        //    Int32 at = Convert.ToInt32(((RadioButtonList)(GridView1.Rows[i].FindControl("RadioButtonList1"))).SelectedValue);
        //    SqlCommand cmd = new SqlCommand("INSERT INTO tbteacherattd  (teachid, teachattddate, teachattdtypeid) VALUES     (@tid,@date,@attdtype)", con);
        //    cmd.Parameters.Add("@tid", SqlDbType.Int).Value = tid;
        //    cmd.Parameters.Add("@attdtype", SqlDbType.Int).Value = at;
        //    cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(TextBox1.Text);
        //    cmd.ExecuteNonQuery();
        //    cmd.Dispose();

        //}
        con.Close();
    }
    //protected void GridView1_DataBound(object sender, EventArgs e)
    //{
    //    for (int i = 0; i < GridView1.Rows.Count; i++)
    //    {
    //        ((RadioButtonList)(GridView1.Rows[i].FindControl("RadioButtonList1"))).Items[0].Selected = true;
    //    }
    //}
    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        for (int i = 0; i < DataList1.Items.Count; i++)
        {
            ((RadioButtonList)(DataList1.Items[i].FindControl("RadioButtonList1"))).Items[0].Selected = true;
        }
    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {
         gridbind();
         Label4.Text = string.Empty;
        if (DataList1.Items.Count == 0)
        {
            Label4.Text = "No Records Found For Department "+DropDownList2.SelectedItem;
            pp.Visible = false;
        }
        else
        {
            
            pp.Visible = true;
        }
    }
}