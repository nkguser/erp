﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class dir_StudentSessionals : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    static Int32 did;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            DDLSessionBind();
        }
    }
    private void DDLSessionBind()
    {
        DDLSession.DataTextField = "session";
        DDLSession.DataValueField = "sessionid";
        DDLSession.DataSource = ERP.AllSession();
        DDLSession.DataBind();
        DDLSession.Items.Insert(0, "-- Select session --");
    }
    protected void DDLSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSession.SelectedIndex == 0)
        {
            sessionviewclear();
        }
        else
        {
            sessionviewclear();
            classbind();
        }
    }
    private void sessionviewclear()
    {
        DDLSessional.Enabled = false;
        DDLSessional.SelectedIndex = 0;
        DDLClass.Items.Clear();
        DDLSubject.Items.Clear();
        LBLDate.Text = string.Empty;
        LBLMaxMarks.Text = string.Empty;
        Lblviewmsg.Text = string.Empty;
        DDLClass.Enabled = false;
        DDLSubject.Enabled = false;
        DDLClass.Items.Insert(0, "--Select Class--");
        DDLSubject.Items.Insert(0, "--Select Subject--");
        submittedmarkpanel.Visible = false;
    }
    private void classbind()
    {
        DDLClass.DataTextField = "Class";
        DDLClass.DataValueField = "groupid";
        DDLClass.DataSource = ERP.AllClassDIR(Convert.ToInt32(DDLSession.SelectedValue));
        DDLClass.DataBind();
        DDLClass.Enabled = true;
        DDLClass.Items.Insert(0, "-- Select Class --");
    }
    protected void DDLClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLClass.SelectedIndex == 0)
        {
            classclear();
        }
        else
        {
            classclear();
            subjectbind();
        }
    }
    private void classclear()
    {
        DDLSubject.Items.Clear();
        LBLDate.Text = string.Empty;
        LBLMaxMarks.Text = string.Empty;
        DDLSubject.Enabled = false;
        DDLSessional.Enabled = false;
        DDLSubject.Items.Insert(0, "--Select Subject--");
        DDLSessional.SelectedIndex = 0;
        Lblviewmsg.Text = string.Empty;
        submittedmarkpanel.Visible = false;
    }
    private void subjectbind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmdfind = new SqlCommand("SELECT   tbdep.depid FROM   tbdep INNER JOIN tbclass ON tbdep.depid = tbclass.classdepid INNER JOIN tbgroup ON tbclass.classid = tbgroup.groupclassid WHERE  (tbgroup.groupid = @gid)", con);
        cmdfind.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        Int32 did = Convert.ToInt32(cmdfind.ExecuteScalar());
        SqlCommand cmd = new SqlCommand("SELECT   tbsubject.subjecttitle + '(' + tbsubjectype.subjecttypename + ')' AS subject, tbsubject.subjectid FROM   tbsubject INNER JOIN tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid WHERE (tbsubject.subjectdepid = @did)", con);
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
        SqlDataReader dr = cmd.ExecuteReader();
        DDLSubject.DataTextField = "subject";
        DDLSubject.DataValueField = "Subjectid";
        DDLSubject.DataSource = dr;
        DDLSubject.DataBind();
        DDLSubject.Items.Insert(0, "-- Select Subject --");
        DDLSubject.Enabled = true;
        dr.Close();
        cmd.Dispose();
        con.Close();
    }
    protected void DDLSubject_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSubject.SelectedIndex == 0)
        {
            subjectclear();
        }
        else
        {
            subjectclear();
            DDLSessional.Enabled = true;
        }
    }
    private void subjectclear()
    {
        DDLSessional.Enabled = false;
        DDLSessional.SelectedIndex = 0;
        LBLDate.Text = string.Empty;
        LBLMaxMarks.Text = string.Empty;
        Lblviewmsg.Text = string.Empty;
        submittedmarkpanel.Visible = false;
    }
    protected void DDLSessional_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 sid;
        if (DDLSessional.SelectedIndex == 0)
        {
            LBLMaxMarks.Text = string.Empty;
            LBLDate.Text = string.Empty;
            Lblviewmsg.Text = string.Empty;
            submittedmarkpanel.Visible = false;
        }
        else
        {
            LBLMaxMarks.Text = string.Empty;
            LBLDate.Text = string.Empty;
            Lblviewmsg.Text = string.Empty;
            submittedmarkpanel.Visible = false;
            Int32 sesno = Convert.ToInt32(DDLSessional.SelectedValue);
            Int32 sbid = Convert.ToInt32(DDLSubject.SelectedValue);
            Int32 gid = Convert.ToInt32(DDLClass.SelectedValue);
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("SELECT  top 1  studentid FROM tbstudent WHERE (studentgroupid =@gid )", con);
            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                sid = Convert.ToInt32(dr["studentid"]);
                SqlCommand cmdcheck = new SqlCommand("SELECT COUNT(*) AS cnt FROM tbstudentsessional WHERE (studentsessionalsubjectid = @sbid) AND (studentsessionalstudentid = @sid) AND (studentsessionalno = @sesno)", con);
                cmdcheck.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
                cmdcheck.Parameters.Add("@sbid", SqlDbType.Int).Value = sbid;
                cmdcheck.Parameters.Add("@sesno", SqlDbType.Int).Value = sesno;
                dr.Close();
                Int32 check = Convert.ToInt32(cmdcheck.ExecuteScalar());
                if (check == 0)
                {
                    Lblviewmsg.Text = sesno + " Sessional Marks not yet filled ";
                }
                else
                {
                    SqlCommand submitcheck = new SqlCommand("SELECT COUNT(*) AS cnt FROM tbstudentsessional WHERE (studentsessionalsubjectid = @sbid) AND (studentsessionalstudentid = @sid) AND (studentsessionalno = @sesno) AND (studentsessionalreqlvlid = 3) AND  (studentsessionaldelsts = 0)", con);
                    submitcheck.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
                    submitcheck.Parameters.Add("@sbid", SqlDbType.Int).Value = sbid;
                    submitcheck.Parameters.Add("@sesno", SqlDbType.Int).Value = sesno;
                    Int32 subcheck = Convert.ToInt32(submitcheck.ExecuteScalar());
                    if (subcheck == 0)
                    {
                        submittedmarkpanel.Visible = false;
                        Lblviewmsg.Text = sesno + " Sessional Marks not submitted till now ";
                    }
                    else
                    {
                        
                        GridShowSubmitBind();
                        GridshowSubmit.Visible = true;
                        submittedmarkpanel.Visible = true;
                        Lblviewmsg.Text = string.Empty;
                    }

                }
            }
            else
            {
                Lblviewmsg.Text = "There are no records exists for Class " + DDLClass.SelectedItem;
            }
            cmd.Dispose();
        }

    }
    private void GridShowSubmitBind()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT     tbstudentsessional.studentsessionaldate, tbstudentsessional.studentsessionalmmarks, tbstudent.studentid FROM tbstudentsessional INNER JOIN                 tbstudent ON tbstudentsessional.studentsessionalstudentid = tbstudent.studentid INNER JOIN tbgroup ON tbstudent.studentgroupid = tbgroup.groupid WHERE (tbstudentsessional.studentsessionalno = @sno) AND (tbstudentsessional.studentsessionalsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND    (tbstudentsessional.studentsessionaldelsts = 0) AND (tbgroup.groupdelsts = 0) AND (tbstudent.studentdelsts = 0) AND (tbstudentsessional.studentsessionalreqlvlid = 3)", con);
        cmd.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DDLSessional.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            LBLDate.Text = Convert.ToDateTime(dr["studentsessionaldate"]).ToShortDateString();
            LBLMaxMarks.Text = dr["studentsessionalmmarks"].ToString();
        }
        cmd.Dispose();
        dr.Close();
        con.Close();
        GridshowSubmit.DataSource = ERP.DispSessionalDIR(Convert.ToInt32(DDLSessional.SelectedValue), Convert.ToInt32(DDLSubject.SelectedValue), Convert.ToInt32(DDLClass.SelectedValue));
        GridshowSubmit.DataBind();
    }
    protected void GridShow_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            //Label l = (Label)(GridShow.Rows[e.Row.RowIndex].FindControl("LBLAttend"));
            Label l = (Label)(e.Row.FindControl("LBLAttend"));
            if (l.Text.Equals("True"))
                l.Text = "Present";
            else
                l.Text = "Absent";
        }
    }
}