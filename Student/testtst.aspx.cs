﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using AjaxControlToolkit;


public partial class Student_testtst : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if ((Convert.ToInt32(Session["tout"]) < 0)||Session["tout"]==null)
        //    Server.Transfer("expire.aspx");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        ((ToolkitScriptManager)Master.FindControl("ToolkitScriptManager1")).RegisterAsyncPostBackControl(GridView1);
        ((ToolkitScriptManager)Master.FindControl("ToolkitScriptManager1")).RegisterAsyncPostBackControl(DataList1);
        //ScriptManager1.RegisterAsyncPostBackControl(GridView1);
        //ScriptManager1.RegisterAsyncPostBackControl(DataList1);
        if (Page.IsPostBack == false)
        {
            Session["tout"] = 45;
            DataTable tb = new DataTable("tst");
            tb.Columns.Add(new DataColumn("qstcod", Type.GetType("System.Int32")));
            tb.Columns.Add(new DataColumn("qstdsc", Type.GetType("System.String")));
            tb.Columns.Add(new DataColumn("qstpic", Type.GetType("System.String")));
            ot.clsqst obj = new ot.clsqst();
            List<ot.clsqstprp> k = obj.Display_Rec(Convert.ToInt32(Session["tcod"]));
            Int32 p = k.Count;
            Int32 i;
            for (i = 0; i < k.Count; i++)
            {
                DataRow r = tb.NewRow();
                r[0] = k[i].p_qstcod;
                r[1] = k[i].p_qstdsc;
                r[2] = k[i].p_qstpic;
                tb.Rows.Add(r);
            }
            Label1.Text = Session["tout"].ToString();
            ViewState["tst"] = tb;
            GridView1.DataSource = tb;
            GridView1.DataBind();
        }
        else
        {
            Session["tout"] = null;
        }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    public Int32 r;
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
            r = 0;
        else if (e.Row.RowType == DataControlRowType.DataRow)
        {
            r = r + 1;
            Int32 a = Convert.ToInt32(GridView1.DataKeys[e.Row.RowIndex][0]);
            String s = GridView1.DataKeys[e.Row.RowIndex][1].ToString();
            RadioButtonList rbl = (RadioButtonList)(e.Row.FindControl("RadioButtonList1"));
            ot.clsopt obj = new ot.clsopt();
            rbl.DataSource = obj.Display_Rec(a);
            rbl.DataTextField = "p_optdsc";
            rbl.DataValueField = "p_optsts";
            rbl.DataBind();
            Image img = (Image)(e.Row.FindControl("img1"));
            if (s == "")
                img.Visible = false;
            else
                img.ImageUrl = "~/qstpics/" + a.ToString() + s;
        }
    }
    protected void GridView1_PageIndexChanged(object sender, EventArgs e)
    {

    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = (DataTable)(ViewState["tst"]);
        GridView1.DataBind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SubmitTest();
    }

    private void SubmitTest()
    {
        Int32 tot = 0;
        for (Int32 i = 0; i < GridView1.Rows.Count; i++)
        {
            RadioButtonList rbl = (RadioButtonList)(GridView1.Rows[i].FindControl
                ("RadioButtonList1"));
            if (rbl.SelectedValue == "T")
                tot = tot + 1;
        }
        ot.clsscr obj = new ot.clsscr();
        ot.clsscrprp objprp = new ot.clsscrprp();
        objprp.p_scrcod = Convert.ToInt32(Session["key"]);
        objprp.p_scrval = tot;
        obj.Upd_Rec(objprp);
        Session["tout"] = null;
        Response.Redirect("testmyscr.aspx");
    }
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Label1.Text) == 1)
        {
            Int32 tot = 0;
            for (Int32 i = 0; i < GridView1.Rows.Count; i++)
            {
                RadioButtonList rbl = (RadioButtonList)(GridView1.Rows[i].FindControl
                    ("RadioButtonList1"));
                if (rbl.SelectedValue == "T")
                    tot = tot + 1;
            }
            ot.clsscr obj = new ot.clsscr();
            ot.clsscrprp objprp = new ot.clsscrprp();
            objprp.p_scrdat = DateTime.Now.ToShortDateString();
            objprp.p_scrregcod = Convert.ToInt32(Session["stid"]);
            objprp.p_scrteccod = Convert.ToInt32(Session["tcod"]);
            objprp.p_scrval = tot;
            obj.Save_Rec(objprp);
            Session["tout"] = -1;
            Response.Redirect("testmyscr.aspx");
        }
        else if (Convert.ToInt32(Label1.Text) <= 5 && Convert.ToInt32(Label1.Text) >= 1)
        {
            Label1.Text = (Convert.ToInt32(Label1.Text) - 1).ToString();// +" Hurry Up!! "

        }
        else if (Convert.ToInt32(Label1.Text) <= 0)
        {
            Response.Redirect("expire.aspx");
        }
        else
        {
            Label1.Text = (Convert.ToInt32(Label1.Text) - 1).ToString();
        }
        Session["tout"] = Convert.ToInt32(Session["tout"]) - 1;
    }

    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        List<int> ar = new List<int>();
        for (Int32 i = 0; i < GridView1.Rows.Count; i++)
        {
            if (((CheckBox)(GridView1.Rows[i].FindControl("CheckBox1"))).Checked == true)
            {
                ar.Add(i + 1);
            }
        }
        DataList1.DataSource = ar;
        DataList1.DataBind();

    }
    //public void GeneratePageToken()
    //{
    //    SessionVariables currSession = new SessionVariables();
    //    hfMasterPageToken.Value = System.DateTime.Now.ToString();
    //    currSession.PageToken = hfMasterPageToken.Value;
    //}

    //public string GetLastPageToken
    //{
    //    get
    //    {
    //        SessionVariables currSession = new SessionVariables();
    //        return currSession.PageToken;
    //    }
    //}

    //public bool TokensMatch
    //{
    //    get
    //    {
    //        SessionVariables currSession = new SessionVariables();
    //        return (currSession.PageToken != null
    //            && currSession.PageToken == hfMasterPageToken.Value);
    //    }
    //}
//    if (this.TokensMatch == false)
//{
//    //Reload the data.
//    //Generates a NewPageToken (this.GeneratePageToken();)
//    ReloadDataMethod();
//    this.MessageToUser =
//     "Data reloaded.  Click Edit or Insert button to change.";
//    this.MessageType = MessageToUserType.Success;
//    this.DisplayMessageToUser = true;
//    return;
//}
}


