﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class Admin_Teacherview : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (Page.IsPostBack == false)
        {
            seserp.DropdownWebService obj = new seserp.DropdownWebService();
            obj.sum(Convert.ToInt32(Session["other"]));
        }
    }
    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        Int64 p1;// grosspay;
        Int32 did, cid, pid; //p2;
        //string salutation, fname, mname, lname, mtname, ftname, corres, perma, qualification, experience, email, photo, resrchjour, resrchcnfrncs, workshop, otheracts, specialization, project, subundertaken, cnfrnconduct, cnfrnattend, otherinfo, msts, memofprofbody, pan, pf, sbankname, sbranchname, baccnumber, isfc, payscale, apptype, uniappletternumber,religion;
        string salutation, fname, mtname, ftname, corres;
        DateTime dob, doj;// uniappdate;
        Boolean g1, tornt, msts;
        Label tcid = ((Label)(DetailsView1.Rows[0].FindControl("Label100")));
        did = Convert.ToInt32(((DropDownList)(DetailsView1.Rows[2].FindControl("ddlDepartment"))).SelectedValue);
        salutation = ((DropDownList)(DetailsView1.Rows[3].FindControl("DropDownList1"))).SelectedValue;
        fname = ((TextBox)(DetailsView1.Rows[4].FindControl("TextBox1"))).Text;
        TextBox mname = ((TextBox)(DetailsView1.Rows[5].FindControl("TextBox3")));
        TextBox lname = ((TextBox)(DetailsView1.Rows[6].FindControl("TextBox5")));
        dob = Convert.ToDateTime(((TextBox)(DetailsView1.Rows[7].FindControl("TextBox7"))).Text);
        doj = Convert.ToDateTime(((TextBox)(DetailsView1.Rows[8].FindControl("TextBox9"))).Text);
        ftname = ((TextBox)(DetailsView1.Rows[9].FindControl("TextBox11"))).Text;
        mtname = ((TextBox)(DetailsView1.Rows[10].FindControl("TextBox13"))).Text;
        corres = ((TextBox)(DetailsView1.Rows[11].FindControl("TextBox15"))).Text;
        TextBox perma = ((TextBox)(DetailsView1.Rows[12].FindControl("TextBox17")));
        pid = Convert.ToInt32(((DropDownList)(DetailsView1.Rows[13].FindControl("DropDownList7"))).SelectedValue);
        TextBox qualification = ((TextBox)(DetailsView1.Rows[14].FindControl("TextBox19")));
        TextBox experience = ((TextBox)(DetailsView1.Rows[15].FindControl("TextBox21")));
        p1 = Convert.ToInt64(((TextBox)(DetailsView1.Rows[16].FindControl("TextBox23"))).Text);
        TextBox p2 = ((TextBox)(DetailsView1.Rows[17].FindControl("TextBox25")));
        TextBox email = ((TextBox)(DetailsView1.Rows[18].FindControl("TextBox27")));
        FileUpload photo = ((FileUpload)(DetailsView1.Rows[19].FindControl("FileUpload1")));
        CheckBox g = ((CheckBox)(DetailsView1.Rows[20].FindControl("CheckBox1")));
        if (g.Checked == true)
        {
            g1 = true;
        }
        else
        {
            g1 = false;
        }
        TextBox resrchjour = ((TextBox)(DetailsView1.Rows[21].FindControl("TextBox31")));
        TextBox resrchcnfrncs = ((TextBox)(DetailsView1.Rows[22].FindControl("TextBox33")));
        TextBox workshop = ((TextBox)(DetailsView1.Rows[23].FindControl("TextBox35")));
        TextBox otheracts = ((TextBox)(DetailsView1.Rows[24].FindControl("TextBox37")));
        TextBox specialization = ((TextBox)(DetailsView1.Rows[25].FindControl("TextBox39")));
        TextBox project = ((TextBox)(DetailsView1.Rows[26].FindControl("TextBox41")));
        TextBox subundertaken = ((TextBox)(DetailsView1.Rows[27].FindControl("TextBox43")));
        TextBox cnfrnconduct = ((TextBox)(DetailsView1.Rows[28].FindControl("TextBox45")));
        TextBox cnfrnattend = ((TextBox)(DetailsView1.Rows[29].FindControl("TextBox47")));
        TextBox otherinfo = ((TextBox)(DetailsView1.Rows[30].FindControl("TextBox49")));
        cid = Convert.ToInt32(((DropDownList)(DetailsView1.Rows[31].FindControl("DropDownList9"))).Text);
        CheckBox mst = ((CheckBox)(DetailsView1.Rows[32].FindControl("CheckBox3")));
        if (mst.Checked == true)
        {
            msts = true;
        }
        else
        {
            msts = false;
        }
        TextBox memofprofbody = ((TextBox)(DetailsView1.Rows[33].FindControl("TextBox51")));
        TextBox pan = ((TextBox)(DetailsView1.Rows[34].FindControl("TextBox53")));
        TextBox pf = ((TextBox)(DetailsView1.Rows[35].FindControl("TextBox55")));
        TextBox sbankname = ((TextBox)(DetailsView1.Rows[36].FindControl("TextBox57")));
        TextBox sbranchname = ((TextBox)(DetailsView1.Rows[37].FindControl("TextBox59")));
        TextBox baccnumber = ((TextBox)(DetailsView1.Rows[38].FindControl("TextBox61")));
        TextBox isfc = ((TextBox)(DetailsView1.Rows[39].FindControl("TextBox63")));
        TextBox grosspay = ((TextBox)(DetailsView1.Rows[40].FindControl("TextBox65")));
        TextBox payscale = ((TextBox)(DetailsView1.Rows[41].FindControl("TextBox67")));

        CheckBox tornt1 = ((CheckBox)(DetailsView1.Rows[42].FindControl("CheckBox5")));
        if (tornt1.Checked == true)
        {
            tornt = true;
        }
        else
        {
            tornt = false;
        }
        TextBox apptype = ((TextBox)(DetailsView1.Rows[43].FindControl("TextBox69")));
        TextBox uniappletternumber = ((TextBox)(DetailsView1.Rows[44].FindControl("TextBox71")));
        TextBox uniappdate = ((TextBox)(DetailsView1.Rows[45].FindControl("TextBox73")));
        TextBox religion = ((TextBox)(DetailsView1.Rows[46].FindControl("TextBox75")));
        SqlCommand cmd = new SqlCommand("updteacher", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(DetailsView1.DataKey[0].ToString());
        cmd.Parameters.Add("@salutation", SqlDbType.VarChar, 8).Value = salutation;
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
        cmd.Parameters.Add("@fname", SqlDbType.VarChar, 50).Value = fname;
        if (mname.Text == null)
        {
            cmd.Parameters.Add("@mname", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@mname", SqlDbType.VarChar, 50).Value = mname.Text;
        }
        if (lname.Text == null)
        {
            cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50).Value = lname.Text;
        }
        cmd.Parameters.Add("@dob", SqlDbType.DateTime).Value = dob;
        cmd.Parameters.Add("@doj", SqlDbType.DateTime).Value = doj;
        cmd.Parameters.Add("@mtname", SqlDbType.VarChar, 50).Value = mtname;
        cmd.Parameters.Add("@ftname", SqlDbType.VarChar, 50).Value = ftname;
        cmd.Parameters.Add("@corres", SqlDbType.VarChar, 500).Value = corres;
        if (perma.Text == null)
        {
            cmd.Parameters.Add("@perma", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@perma", SqlDbType.VarChar, 500).Value = perma.Text;
        }
        cmd.Parameters.Add("@pid", SqlDbType.Int).Value = pid;
        if (qualification.Text == null)
        {
            cmd.Parameters.Add("@qualification", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@qualification", SqlDbType.VarChar, 50).Value = qualification.Text;
        }
        if (experience.Text == null)
        {
            cmd.Parameters.Add("@experience", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@experience", SqlDbType.VarChar, 50).Value = experience.Text;
        }
        cmd.Parameters.Add("@p1", SqlDbType.BigInt).Value = p1;
        if (p2.Text == null || p2.Text == string.Empty || p2.Text == "")
        {
            cmd.Parameters.Add("@p2", SqlDbType.BigInt).Value = Convert.DBNull;
        }
        else
        {
            cmd.Parameters.Add("@p2", SqlDbType.BigInt).Value = Convert.ToInt64(p2.Text);
        }
        if (email.Text == null)
        {
            cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = email.Text;
        }
        if (photo.FileName == null || photo.FileName == string.Empty)
        {
            SqlCommand cmd1 = new SqlCommand("select teacherphoto from tbteacher where teacherid=" + Convert.ToInt32(DetailsView1.DataKey[0].ToString()), con);
            SqlDataReader dr = cmd1.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                string abc = dr[0].ToString();
                if (abc != null || abc != string.Empty)
                {
                    cmd.Parameters.Add("@photo", SqlDbType.VarChar, 100).Value = abc;
                }
                else
                {
                    cmd.Parameters.Add("@photo", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
                }
            }
            dr.Close();
            cmd1.Dispose();

        }
        else
        {
            SqlCommand cmd1 = new SqlCommand("select teacherphoto from tbteacher where teacherid=" + Convert.ToInt32(DetailsView1.DataKey[0].ToString()), con);
            SqlDataReader dr = cmd1.ExecuteReader();

            if (dr.HasRows)
            {
                dr.Read();
                string abc = dr[0].ToString();
                if (abc == "")
                {

                }
                else
                {
                    File.Delete(Server.MapPath("~\\Teacherpic") + "\\" + abc);
                }
            }
            dr.Close();
            cmd1.Dispose();
            DirectoryInfo di = new DirectoryInfo(Server.MapPath("~\\Teacherpic"));
            if (di.Exists == false)
                di.Create();
            string fn = tcid.Text.Substring(2, 5) + Path.GetExtension(photo.PostedFile.FileName);
            string sp = Server.MapPath("~\\Teacherpic");
            if (sp.EndsWith("\\") == false)
                sp += "\\";
            sp += fn;
            photo.PostedFile.SaveAs(sp);
            cmd.Parameters.Add("@photo", SqlDbType.VarChar, 100).Value = fn;
        }
        cmd.Parameters.Add("@g1", SqlDbType.Bit).Value = g1;
        if (resrchjour.Text == null)
        {
            cmd.Parameters.Add("@resrchjour", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@resrchjour", SqlDbType.VarChar, 500).Value = resrchjour.Text;
        }
        if (resrchcnfrncs.Text == null)
        {
            cmd.Parameters.Add("@resrchcnfrncs", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@resrchcnfrncs", SqlDbType.VarChar, 500).Value = resrchcnfrncs.Text;
        }
        if (workshop.Text == null)
        {
            cmd.Parameters.Add("@workshop", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@workshop", SqlDbType.VarChar, 500).Value = workshop.Text;
        }
        if (otheracts.Text == null)
        {
            cmd.Parameters.Add("@otheracts", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@otheracts", SqlDbType.VarChar, 500).Value = otheracts.Text;
        }
        if (specialization.Text == null)
        {
            cmd.Parameters.Add("@specialization", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@specialization", SqlDbType.VarChar, 500).Value = specialization.Text;
        }
        if (project.Text == null)
        {
            cmd.Parameters.Add("@project", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@project", SqlDbType.VarChar, 500).Value = project.Text;
        }
        if (subundertaken.Text == null)
        {
            cmd.Parameters.Add("@subundertaken", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@subundertaken", SqlDbType.VarChar, 500).Value = subundertaken.Text;
        }
        if (cnfrnconduct.Text == null)
        {
            cmd.Parameters.Add("@cnfrnconduct", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@cnfrnconduct", SqlDbType.VarChar, 500).Value = cnfrnconduct.Text;
        }
        if (cnfrnattend.Text == null)
        {
            cmd.Parameters.Add("@cnfrnattend", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@cnfrnattend", SqlDbType.VarChar, 500).Value = cnfrnattend.Text;
        }
        if (otherinfo.Text == null)
        {
            cmd.Parameters.Add("@otherinfo", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@otherinfo", SqlDbType.VarChar, 500).Value = otherinfo.Text;
        }
        cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
        cmd.Parameters.Add("@msts", SqlDbType.Bit).Value = msts;
        if (memofprofbody.Text == null)
        {
            cmd.Parameters.Add("@memofprofbody ", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@memofprofbody ", SqlDbType.VarChar, 500).Value = memofprofbody.Text;
        }
        if (pan.Text == null)
        {
            cmd.Parameters.Add("@pan ", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@pan ", SqlDbType.VarChar, 50).Value = pan.Text;
        }
        if (religion.Text == null)
        {
            cmd.Parameters.Add("@religion ", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@religion ", SqlDbType.VarChar, 50).Value = religion.Text;
        }
        if (pf.Text == null)
        {
            cmd.Parameters.Add("@pf ", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@pf ", SqlDbType.VarChar, 500).Value = pf.Text;
        }
        if (sbankname.Text == null)
        {
            cmd.Parameters.Add("@sbankname ", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@sbankname ", SqlDbType.VarChar, 100).Value = sbankname.Text;
        }
        if (sbranchname.Text == null)
        {
            cmd.Parameters.Add("@sbranchname ", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@sbranchname ", SqlDbType.VarChar, 100).Value = sbranchname.Text;
        }
        if (baccnumber.Text == null)
        {
            cmd.Parameters.Add("@baccnumber", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@baccnumber", SqlDbType.VarChar, 50).Value = baccnumber.Text;
        }
        if (isfc.Text == null)
        {
            cmd.Parameters.Add("@isfc ", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@isfc ", SqlDbType.VarChar, 50).Value = isfc.Text;
        }

        if (grosspay.Text == null || grosspay.Text == "")
        {
            cmd.Parameters.Add("@grosspay", SqlDbType.BigInt).Value = Convert.DBNull;
        }
        else
        {
            cmd.Parameters.Add("@grosspay", SqlDbType.BigInt).Value = Convert.ToInt64(grosspay.Text);
        }

        if (payscale.Text == null)
        {
            cmd.Parameters.Add("@payscale ", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@payscale ", SqlDbType.VarChar, 50).Value = payscale.Text;
        }
        cmd.Parameters.Add("@tornt", SqlDbType.Bit).Value = tornt;
        if (apptype.Text == null)
        {
            cmd.Parameters.Add("@apptype ", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@apptype ", SqlDbType.VarChar, 50).Value = apptype.Text;
        }
        if (uniappletternumber.Text == null)
        {
            cmd.Parameters.Add("@uniappletternumber ", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@uniappletternumber ", SqlDbType.VarChar, 50).Value = uniappletternumber.Text;
        }
        if (uniappdate.Text == null || uniappdate.Text == "")
        {
            cmd.Parameters.Add("@uniappdate", SqlDbType.DateTime).Value = Convert.DBNull;
        }
        else
        {
            cmd.Parameters.Add("@uniappdate", SqlDbType.DateTime).Value = Convert.ToDateTime(uniappdate.Text);
        }
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly);
        det_bind();
    }
    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        DetailsView1.ChangeMode(e.NewMode);
        det_bind();
    }
    protected void DetailsView1_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
    {
        Int32 a = Convert.ToInt32(DetailsView1.DataKey[0].ToString());
        SqlCommand cmd = new SqlCommand("UPDATE  tbteacher SET  teacherdelsts = 1 WHERE  (teacherid = @tid)", con);
        cmd.Parameters.Add("@tid", SqlDbType.VarChar, 50).Value = a;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        Response.Redirect("~/admin/teacherview.aspx");
    }
    private void det_bind()
    {
        SqlCommand cmd = new SqlCommand("dispteacher1", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@tid", SqlDbType.VarChar, 50).Value = TextBox77.Text;
        cmd.Parameters.Add("@clgid", SqlDbType.Int).Value = Convert.ToInt32(Session["other"]); 
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DetailsView1.DataSource = ds;
        DetailsView1.DataBind();
        DetailsView1.Visible = true;
    }
    protected void TextBox77_TextChanged(object sender, EventArgs e)
    {

        SqlCommand cmd = new SqlCommand("select count(*) as cnt from tbteacher where teacherclgid=@u", con);
        cmd.Parameters.Add("@u", SqlDbType.VarChar, 50).Value = TextBox77.Text;
        Int32 a1 = Convert.ToInt32(cmd.ExecuteScalar());
        if (a1 == 0)
        {

        }
        else
        {
            det_bind();
        }
        det_bind();
    }
}
    //protected void abc(object sender, EventArgs e)
    //{
    //    DropDownList d = ((DropDownList)(DetailsView1.Rows[1].FindControl("DropDownList3")));
    //    DropDownList d1 = ((DropDownList)(DetailsView1.Rows[2].FindControl("DropDownList5")));
    //    Int32 a = Convert.ToInt32(d.SelectedValue);
    //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    //    con.Open();
    //    SqlCommand cmd = new SqlCommand("SELECT tbdep.depid, tbdep.depname FROM tbcourse INNER JOIN tbdep ON tbcourse.courseid = @c", con);
    //    cmd.Parameters.Add("@c", SqlDbType.Int).Value = a;
    //    SqlDataReader dr = cmd.ExecuteReader();
    //    d1.DataSource = dr;
    //    dr.Read();
    //    d1.DataTextField = "depname";
    //    d1.DataValueField = "depid";
    //    d1.DataBind();
    //    dr.Close();
    //    cmd.Dispose();


    //}
//    protected void DetailsView1_DataBound(object sender, EventArgs e)
//    {
//        //foreach (DetailsViewRow dr in this.DetailsView1.Rows)
//        //{
//        //    if (dr.RowType == DataControlRowType.DataRow)
//        //    {
//        //        if (dr.RowState == DataControlRowState.Edit || dr.RowState == DataControlRowState.Insert)
//                //{
//        if (DetailsView1.CurrentMode == DetailsViewMode.Edit) 
//{ 
//                    DropDownList d;
//                    DropDownList d1;
            
//                    //DropDownList DDL = (DropDownList)dr.FindControl("DropDownListID");
//                    d = (DropDownList)DetailsView1.FindControl("DropDownList3");
//                    d1 = (DropDownList)DetailsView1.FindControl("DropDownList5");
//                    SqlDataAdapter adp = new SqlDataAdapter("select * from tbcourse;select * from tbdep", ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
//                    DataSet ds = new DataSet();
//                    adp.Fill(ds);
//                    d.DataSource = ds.Tables[0];
//                    d.DataBind();
//                    d1.DataSource = ds.Tables[1];
//                    d1.DataBind();
                    
                  
                   
//                    //d.Items.FindByValue(r["courseid"].ToString()).Selected = true;
//                    //d1.Items.FindByValue(r["depid"].ToString()).Selected = true;
//                }
//            }
        