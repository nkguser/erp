﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="mksreq.aspx.cs" Inherits="HOD_mksreq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <asp:Label ID="l1" runat="server" ForeColor="Red"  Visible="false"/><br />
 <table width="100%" style="text-align: left; font-weight: bold;">
    
            <tr>
                <td 
                >
                
                    <strong>Roll no:
                </strong>
                </td>
                <td 
                >
                    <asp:TextBox ID="TextBox1" runat="server" Width="300px"></asp:TextBox><asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToValidate="TextBox1" Operator="DataTypeCheck" Display="Dynamic" 
                        ErrorMessage="*" Font-Bold="True" ForeColor="Red" Type="Integer">*</asp:CompareValidator>
                &nbsp;<asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                        Text="Continue"  style="font-weight: 700" Width="73px" />
                </td>
            </tr>

            </table>
             <asp:Panel ID="Panel1" runat="server" Visible="false">
       
            <table width="100%" style="text-align: left; font-weight: bold;">
            <tr>
                <td style="width: 101px" 
                >
                    <strong>Sessional</strong></td>
                <td 
                >
                    <asp:DropDownList ID="DropDownList2" runat="server" 
                        DataTextFormatString="{0:d}">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 101px" 
                >
                    <strong>Subject</strong></td>
                <td 
                                >
                    <asp:DropDownList ID="DropDownList1" runat="server" Width="300px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 101px" >
                    <strong>Reason</strong></td>
                <td >
                    <asp:TextBox ID="TextBox2" runat="server" Height="100px" TextMode="MultiLine" 
                        Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 101px" >
                    <strong>Marks</strong></td>
                <td >
                   <asp:TextBox ID="TextBox3" runat="server" Width="300px"></asp:TextBox>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="width: 101px" >
                    <strong>Attendance</strong>&nbsp;&nbsp; </td>
                <td align=left>
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                        RepeatDirection="Horizontal" style="font-weight: 700">
                        <asp:ListItem Selected="True" Value="True">Present</asp:ListItem>
                        <asp:ListItem Value="False">Absent</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td style="width: 101px" >
                    </td>
                <td >
                    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                        style="font-weight: 700" Text="Submit Request" Height="39px" 
                        Width="151px" />
                </td>
            </tr>
        </table>
    
    </asp:Panel>
</asp:Content>

