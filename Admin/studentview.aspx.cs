﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using kitmerp;

public partial class Admin_studentview : System.Web.UI.Page
{


    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (Page.IsPostBack == false)
        {
            seserp.DropdownWebService obj = new seserp.DropdownWebService();
            obj.sum(Convert.ToInt32(Session["other"]));
        }

    }
    protected void DetailsView1_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
    {
        SqlCommand cmd = new SqlCommand("update tbstudent set studentdelsts=1 where studentid=@rno", con);
        cmd.Parameters.Add("rno", SqlDbType.Int).Value = Convert.ToInt32(DetailsView1.DataKey[0].ToString());
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        Response.Redirect("~/admin/studentview.aspx");
    }
    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        TextBox rno1 = ((TextBox)(DetailsView1.Rows[0].FindControl("TextBox1")));
        Int32 rno, gid, cid, tenth, rollcheck;
        Int64 p1;
        string fname, mtname, ftname, corres;
        DateTime d1;
        Boolean hostler;

        rno = Convert.ToInt32(((TextBox)(DetailsView1.Rows[0].FindControl("TextBox1"))).Text);
        TextBox regno = ((TextBox)(DetailsView1.Rows[1].FindControl("TextBox3")));
        gid = Convert.ToInt32(((DropDownList)(DetailsView1.Rows[6].FindControl("ddlGroup"))).SelectedValue);
        fname = ((TextBox)(DetailsView1.Rows[7].FindControl("TextBox5"))).Text;
        TextBox mname = ((TextBox)(DetailsView1.Rows[8].FindControl("TextBox7")));
        TextBox lname = ((TextBox)(DetailsView1.Rows[9].FindControl("TextBox9")));
        mtname = ((TextBox)(DetailsView1.Rows[10].FindControl("TextBox11"))).Text;
        ftname = ((TextBox)(DetailsView1.Rows[11].FindControl("TextBox13"))).Text;
        d1 = Convert.ToDateTime(((TextBox)(DetailsView1.Rows[12].FindControl("TextBox15"))).Text);
        cid = Convert.ToInt32(((DropDownList)(DetailsView1.Rows[13].FindControl("dropdownlist9"))).SelectedValue);
        corres = ((TextBox)(DetailsView1.Rows[14].FindControl("TextBox17"))).Text;
        TextBox perma = ((TextBox)(DetailsView1.Rows[15].FindControl("TextBox19")));
        p1 = Convert.ToInt64(((TextBox)(DetailsView1.Rows[16].FindControl("TextBox21"))).Text);
        TextBox p2 = ((TextBox)(DetailsView1.Rows[17].FindControl("TextBox23")));
        TextBox email = ((TextBox)(DetailsView1.Rows[18].FindControl("TextBox25")));
        FileUpload photo = ((FileUpload)(DetailsView1.Rows[19].FindControl("FileUpload1")));
        tenth = Convert.ToInt32(((TextBox)(DetailsView1.Rows[20].FindControl("TextBox27"))).Text);
        TextBox twelth = ((TextBox)(DetailsView1.Rows[21].FindControl("TextBox29")));
        TextBox blood = ((TextBox)(DetailsView1.Rows[22].FindControl("TextBox31")));
        TextBox achieve = ((TextBox)(DetailsView1.Rows[23].FindControl("TextBox33")));
        TextBox skill = ((TextBox)(DetailsView1.Rows[24].FindControl("TextBox35")));
        TextBox training = ((TextBox)(DetailsView1.Rows[25].FindControl("TextBox37")));
        TextBox hobbies = ((TextBox)(DetailsView1.Rows[26].FindControl("TextBox39")));
        FileUpload resume = ((FileUpload)(DetailsView1.Rows[27].FindControl("FileUpload3")));
        //hostler = Convert.ToBoolean(((RadioButtonList)(DetailsView1.Rows[29].FindControl("RBLgender"))).SelectedValue);
        CheckBox hostl = ((CheckBox)(DetailsView1.Rows[28].FindControl("CheckBox1")));

        if (hostl.Checked == true)
        {
            hostler = true;
        }
        else
            hostler = false;
        SqlCommand cmd = new SqlCommand("updstudent", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(DetailsView1.DataKey[0].ToString());
        SqlCommand cmdcheck = new SqlCommand("select studentrollno from tbstudent where studentid=@u", con);
        cmdcheck.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(DetailsView1.DataKey[0].ToString());
        SqlDataReader drcheck = cmdcheck.ExecuteReader();
        if (drcheck.HasRows)
        {
            drcheck.Read();
            rollcheck = Convert.ToInt32(drcheck[0]);
            if (rno == rollcheck)
            {
                drcheck.Close();
                cmdcheck.Dispose();
                RollCheck(rno1, rno, gid, cid, tenth, p1, fname, mtname, ftname, corres, d1, hostler, regno, mname, lname, perma, p2, email, photo, twelth, blood, achieve, skill, training, hobbies, resume, cmd);
            }
            else
            {
                drcheck.Close();
                cmdcheck.Dispose();
                SqlCommand cmdcheck1 = new SqlCommand("select count(*) as cnt from tbstudent where studentrollno=@u ", con);
                cmdcheck1.Parameters.Add("@u", SqlDbType.Int).Value = rno;

                Int32 check = Convert.ToInt32(cmdcheck1.ExecuteScalar());
                if (check == 0)
                {
                    RollCheck(rno1, rno, gid, cid, tenth, p1, fname, mtname, ftname, corres, d1, hostler, regno, mname, lname, perma, p2, email, photo, twelth, blood, achieve, skill, training, hobbies, resume, cmd);
                }
                else
                {
                    TextBox roll = ((TextBox)(DetailsView1.Rows[0].FindControl("TextBox1")));
                    roll.Text = string.Empty;
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Roll Number Not Available !');", true);
                }
            }
        }

    }
    private void RollCheck(TextBox rno1, Int32 rno, Int32 gid, Int32 cid, Int32 tenth, Int64 p1, string fname, string mtname, string ftname, string corres, DateTime d1, Boolean hostler, TextBox regno, TextBox mname, TextBox lname, TextBox perma, TextBox p2, TextBox email, FileUpload photo, TextBox twelth, TextBox blood, TextBox achieve, TextBox skill, TextBox training, TextBox hobbies, FileUpload resume, SqlCommand cmd)
    {
        cmd.Parameters.Add("@rno", SqlDbType.Int).Value = rno;
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
        cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
        cmd.Parameters.Add("@p1", SqlDbType.BigInt).Value = p1;
        if (p2.Text == null || p2.Text == "")
        {
            cmd.Parameters.Add("@p2", SqlDbType.Int).Value = Convert.DBNull;
        }
        else
        {
            cmd.Parameters.Add("@p2", SqlDbType.Int).Value = Convert.ToInt32(p2.Text);
        }
        cmd.Parameters.Add("@tenth", SqlDbType.Int).Value = tenth;
        if (twelth.Text == null || twelth.Text == "")
        {
            cmd.Parameters.Add("@twelth", SqlDbType.Int).Value = Convert.DBNull;
        }
        else
        {
            cmd.Parameters.Add("@twelth", SqlDbType.Int).Value = Convert.ToInt32(twelth.Text);
        }
        if (regno.Text == null)
        {
            cmd.Parameters.Add("@regno", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@regno", SqlDbType.VarChar, 50).Value = regno.Text;
        }
        cmd.Parameters.Add("@fname", SqlDbType.VarChar, 50).Value = fname;
        if (mname.Text == null)
        {
            cmd.Parameters.Add("@mname", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@mname", SqlDbType.VarChar, 50).Value = mname.Text;
        }
        if (lname.Text == null)
        {
            cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50).Value = " ";
        }
        else
        {
            cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50).Value = lname.Text;
        }
        cmd.Parameters.Add("@mtname", SqlDbType.VarChar, 50).Value = mtname;
        cmd.Parameters.Add("@ftname", SqlDbType.VarChar, 50).Value = ftname;
        cmd.Parameters.Add("@corres", SqlDbType.VarChar, 300).Value = corres;
        if (perma.Text == null)
        {
            cmd.Parameters.Add("@perma", SqlDbType.VarChar, 300).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@perma", SqlDbType.VarChar, 300).Value = perma.Text;
        }
        if (photo.FileName == null || photo.FileName == string.Empty)
        {
            SqlCommand cmd1 = new SqlCommand("select studentphoto from tbstudent where studentid=" + Convert.ToInt32(DetailsView1.DataKey[0].ToString()), con);
            SqlDataReader dr = cmd1.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                string abc = dr[0].ToString();
                if (abc != null || abc != string.Empty)
                {
                    cmd.Parameters.Add("@photo", SqlDbType.VarChar, 100).Value = abc;
                }
                else
                {
                    cmd.Parameters.Add("@photo", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
                }
            }
            dr.Close();
            cmd1.Dispose();
        }
        else
        {
            SqlCommand cmd1 = new SqlCommand("select studentphoto from tbstudent where studentid=" + Convert.ToInt32(DetailsView1.DataKey[0].ToString()), con);
            SqlDataReader dr = cmd1.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                string abc = dr[0].ToString();
                if (abc == "")
                {

                }
                else
                {

                    File.Delete(Server.MapPath("~\\Studentpic") + "\\" + abc);
                }
            }
            dr.Close();
            cmd1.Dispose();
            DirectoryInfo di = new DirectoryInfo(Server.MapPath("~\\Studentpic" + "\\"));
            if (di.Exists == false)
                di.Create();

            string fn = rno1.Text.Substring(2, 5) + Path.GetExtension(photo.PostedFile.FileName);
            string sp = Server.MapPath("~\\Studentpic" + "\\");
            if (sp.EndsWith("\\") == false)
                sp += "\\";
            sp += fn;
            photo.PostedFile.SaveAs(sp);
            cmd.Parameters.Add("@photo", SqlDbType.VarChar, 100).Value = fn;
        }
        if (email.Text == null)
        {
            cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = email.Text;
        }
        if (blood.Text == null)
        {
            cmd.Parameters.Add("@blood", SqlDbType.VarChar, 4).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@blood", SqlDbType.VarChar, 4).Value = blood.Text;
        }
        if (achieve.Text == null)
        {
            cmd.Parameters.Add("@achieve", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@achieve", SqlDbType.VarChar, 200).Value = achieve.Text;
        }
        if (skill.Text == null)
        {
            cmd.Parameters.Add("@skill", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@skill", SqlDbType.VarChar, 200).Value = skill.Text;
        }
        if (training.Text == null)
        {
            cmd.Parameters.Add("@training", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@training", SqlDbType.VarChar, 200).Value = training.Text;
        }
        if (hobbies.Text == null)
        {
            cmd.Parameters.Add("@hobbies", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@hobbies", SqlDbType.VarChar, 200).Value = hobbies.Text;
        }
        if (resume.FileName == null || resume.FileName == string.Empty)
        {
            SqlCommand cmd1 = new SqlCommand("select studentresumefile from tbstudent where studentid=" + Convert.ToInt32(DetailsView1.DataKey[0].ToString()), con);
            SqlDataReader dr = cmd1.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                string abc = dr[0].ToString();
                cmd.Parameters.Add("@resume", SqlDbType.VarChar, 100).Value = abc;
            }
            else
            {
                cmd.Parameters.Add("@resume", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
            }
            dr.Close();
            cmd1.Dispose();

        }
        else
        {
            SqlCommand cmd1 = new SqlCommand("select studentresumefile from tbstudent where studentid=" + Convert.ToInt32(DetailsView1.DataKey[0].ToString()), con);
            SqlDataReader dr = cmd1.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                string abc = dr[0].ToString();
                if (abc == "")
                {

                }
                else
                {
                    File.Delete(Server.MapPath("~\\StudentResume") + "\\" + abc);
                }
            }
            dr.Close();
            cmd1.Dispose();
            DirectoryInfo di1 = new DirectoryInfo(Server.MapPath("~\\Studentresume" + "\\"));
            if (di1.Exists == false)
                di1.Create();
            string fn1 = rno1.Text.Substring(2, 5) + Path.GetExtension(resume.PostedFile.FileName);
            string sp1 = Server.MapPath("~\\Studentresume" + "\\");
            if (sp1.EndsWith("\\") == false)
                sp1 += "\\";
            sp1 += fn1;
            resume.PostedFile.SaveAs(sp1);
            cmd.Parameters.Add("@resume", SqlDbType.VarChar, 100).Value = fn1;
        }
        cmd.Parameters.Add("@d1", SqlDbType.DateTime).Value = d1;
        cmd.Parameters.Add("@hostler", SqlDbType.Bit).Value = hostler;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly);
        det_bind();
    }
    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        DetailsView1.ChangeMode(e.NewMode);
        det_bind();
    }
    protected void TextBox41_TextChanged(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand("select count(*) as cnt from tbstudent where studentrollno=@rno", con);
        cmd.Parameters.Add("@rno", SqlDbType.Int).Value = Convert.ToInt32(TextBox41.Text);
        Int32 a1 = Convert.ToInt32(cmd.ExecuteScalar());
        if (a1 == 0)
        {

        }
        else
        {
            det_bind();
        }
        det_bind();
    }
    private void det_bind()
    {
        SqlCommand cmd = new SqlCommand("dispstudent1", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@rno", SqlDbType.Int).Value = Convert.ToInt32(TextBox41.Text);
        cmd.Parameters.Add("@clgid", SqlDbType.Int).Value = Convert.ToInt32(Session["other"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DetailsView1.DataSource = ds;

        DetailsView1.DataBind();
        DetailsView1.Visible = true;
    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        //DDLSessionBind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox rno = ((TextBox)(DetailsView1.Rows[0].FindControl("TextBox2")));
            if (rno.Text == string.Empty || rno.Text == "" || rno.Text == null)
            {
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Enter Roll Number in the given field !');", true);
            }
            else
            {
                SqlCommand cmdcheck = new SqlCommand("select studentrollno from tbstudent where studentid=@u", con);
                cmdcheck.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(DetailsView1.DataKey[0].ToString());
                SqlDataReader drcheck = cmdcheck.ExecuteReader();
                if (drcheck.HasRows)
                {
                    drcheck.Read();
                    Int32 rollcheck = Convert.ToInt32(drcheck[0]);
                    if (Convert.ToInt32(rno.Text) == rollcheck)
                    {
                        drcheck.Close();
                        cmdcheck.Dispose();
                        ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Roll Number is Acceptable !');", true);
                    }
                    else
                    {
                        drcheck.Close();
                        cmdcheck.Dispose();
                        SqlCommand cmdcheck1 = new SqlCommand("select count(*) as cnt from tbstudent where studentrollno=@u", con);
                        cmdcheck1.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(rno.Text);
                        Int32 check = Convert.ToInt32(cmdcheck1.ExecuteScalar());
                        if (check == 0)
                        {
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Roll Number Available !');", true);
                        }
                        else
                        {
                            rno.Text = string.Empty;
                            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Roll Number Not Available !');", true);
                        }
                    }
                }
            }
        }

        catch (Exception exp)
        {
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Enter numeric number only !');", true);
        }
    }
    
}