﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class HOD_Icard : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        frmbind();
        bari();
    }
    private void frmbind()
    {
        SqlCommand cmd = new SqlCommand("teachericard", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        FormView1.DataSource = ds;
        FormView1.DataBind();
    }
    private void bari()
    {
        string name = ((Label)(FormView1.FindControl("LBLName"))).Text;
        ((System.Web.UI.WebControls.Image)FormView1.FindControl("Image1")).ImageUrl = "~/HOD/barcode.aspx?a=" + name;
    }
}