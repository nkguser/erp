﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class dir_attdrep : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();

    }    

    protected void TextBox41_TextChanged(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand("select count(*) as cnt from tbstudent where studentrollno=@rno", con);
        cmd.Parameters.Add("@rno", SqlDbType.Int).Value = Convert.ToInt32(TextBox41.Text);
        Int32 a1 = Convert.ToInt32(cmd.ExecuteScalar());
        if (a1 == 0)
        {

        }
        else
        {
            det_bind();
        }
        det_bind();
    }
    private void det_bind()
    {
        SqlCommand cmd = new SqlCommand("dispstudent1", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("rno", SqlDbType.Int).Value = Convert.ToInt32(TextBox41.Text);
        cmd.Parameters.Add("clgid", SqlDbType.Int).Value = Convert.ToInt32(Session["other"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DetailsView1.DataSource = ds;
        DetailsView1.DataBind();
        DetailsView1.Visible = true;
        if (ds.Tables[0].Rows.Count == 0)
        {
            Panel1.Visible = false;

        }
        else
        {
            Panel1.Visible = true;
            GetData();
        }
    }

    private void GetData()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        DDlSem.Items.Clear();
        SqlCommand cmd = new SqlCommand("SELECT tbcourse.coursesemester FROM tbcourse INNER JOIN tbdep ON tbcourse.courseid = tbdep.depcourseid INNER JOIN tbclass ON tbdep.depid = tbclass.classdepid INNER JOIN tbgroup ON tbclass.classid = tbgroup.groupclassid INNER JOIN tbstudent ON tbgroup.groupid = tbstudent.studentgroupid WHERE (tbcourse.coursedelsts = 0) AND (tbdep.depdelsts = 0) AND (tbgroup.groupdelsts = 0) AND (tbclass.classdelsts = 0) AND (tbstudent.studentdelsts = 0) AND (tbstudent.studentid = @stid)", con);
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(((Label)(DetailsView1.FindControl("Label2"))).Text);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        int count = Int32.Parse(dr["coursesemester"].ToString());
        DDlSem.Items.Insert(0, "-- Choose Semester --");
        for (int i = 1; i <= count; i++)
        {
            DDlSem.Items.Add(i.ToString());
        }
        dr.Close();
        cmd.Dispose();
        con.Close();
    }
    private void GridShowBind()
    {
        SqlCommand cmd = new SqlCommand("DispSessionalStdnt", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(((Label)(DetailsView1.FindControl("Label2"))).Text);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDlSem.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShow.DataSource = ds;
        GridShow.DataBind();
        cmd.Dispose();
    }
    protected void GridShow_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            Label l = (Label)(e.Row.FindControl("LBLAttend"));
            if (l.Text.Equals("True"))
                l.Text = "Present";
            else
                l.Text = "Absent";
        }
    }
    protected void DDLSem_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridShowBind();
        GridShowSummaryBind();
        GridShow.Visible = true;
        GridShowSummary.Visible = true;
    }
    private void GridShowSummaryBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT distinct tbstudentattnd.studentattndsubjectid, tbsubject.subjecttitle + ' - ' + tbsubjectype.subjecttypename AS subject FROM         tbsubject INNER JOIN tbstudentattnd ON tbsubject.subjectid = tbstudentattnd.studentattndsubjectid INNER JOIN  tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid WHERE     (tbstudentattnd.studentattndstudentid = @stid) AND (tbsubject.subjectsem = @sno) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbsubject.subjectdelsts = 0) AND (tbsubjectype.subjectdelsts = 0) ORDER BY subject", con);
        cmd.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DDlSem.SelectedValue);
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(((Label)(DetailsView1.FindControl("Label2"))).Text);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShowSummary.DataSource = ds;
        GridShowSummary.DataBind();
        cmd.Dispose();
        con.Close();
    }
    protected void GridShowSummary_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("SELECT     COUNT(*) AS count FROM         tbstudentattnd WHERE     (studentattndstudentid = @stid) AND (studentattndattend = 'True') AND (studentattndsubjectid = @sid) AND (studentattnddelsts = 0)", con);
            SqlCommand cmd1 = new SqlCommand("SELECT COUNT(*) AS count FROM tbstudentAttnd WHERE (studentattndstudentid = @stid) AND (studentattndsubjectid = @sid) AND (studentattnddelsts = 0)", con);
            cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(((Label)(DetailsView1.FindControl("Label2"))).Text);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSummary.DataKeys[e.Row.RowIndex].Values["studentattndsubjectid"]);
            cmd1.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(((Label)(DetailsView1.FindControl("Label2"))).Text);
            cmd1.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSummary.DataKeys[e.Row.RowIndex].Values["studentattndsubjectid"]);
            Label l1 = (Label)(e.Row.FindControl("LblAt"));
            Label l2 = (Label)(e.Row.FindControl("LblTotal"));
            Label l3 = (Label)(e.Row.FindControl("Lblpercent"));
            l1.Text = cmd.ExecuteScalar().ToString();
            l2.Text = cmd1.ExecuteScalar().ToString();
            if (Convert.ToInt32(l2.Text) != 0)
                l3.Text = (100 * Convert.ToInt32(l1.Text) / Convert.ToInt32(l2.Text)).ToString() + "%";
            else
                l3.Text = "--";
            cmd.Dispose();
            cmd1.Dispose();
            con.Close();
        }
    }
    protected void DDlSem_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridShowSummaryBind();
        GridShowBind();
    }
}