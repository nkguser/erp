﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/Test.master" AutoEventWireup="true" CodeFile="testtst.aspx.cs" Inherits="Student_testtst" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table style="width: 100%">
                <tr>
                    <td>
                        <div style="FONT-WEIGHT: bold; color: #003300;">
                            Instructions :</div>
                        <ul sizcache="5" sizset="45" style="color: #009900">
                            <li><strong>Large bank of questions per test, questions regularly refreshed</strong></li>
                            <li><strong>Users can take any test for free, can retake a test typically after 10 
                                days Only</strong></li>
                            <li><strong>Typically 45 minutes to complete test, </strong></li>
                            <li><strong>Do not Close/Refresh your browser window during test sessions </strong>
                            </li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table ID="TABLE_9">
                            <tr>
                                <td>
                                    <span class="header" style="color: #003300"><strong>Rules for taking the test</strong></span></td>
                            </tr>
                        </table>
                        <table ID="TABLE_10" border="1" cellpadding="3" cellspacing="0" 
                            style="border: 1px solid #999999; WIDTH: 100%; FONT-SIZE: inherit; border-collapse: collapse; border-spacing: 0; text-indent: 0;">
                            <tr style="color: #FF66CC">
                                <td style="border-style: none; border-color: inherit; border-width: medium; color: #CC0066;">
                                    Duration:
                                </td>
                                <td style="color: #CC0066">
                                    45 minutes
                                </td>
                            </tr>
                            <tr style="color: #FF66CC">
                                <td style="border-style: none; border-color: inherit; border-width: medium; color: #CC0066;" 
                                    valign="top">
                                    Number of Questions:
                                </td>
                                <td style="color: #CC0066">
                                    35 Multiple Choice questions.<br /> Each question has between 1 and 6 options of 
                                    which 1 or more may be correct
                                </td>
                            </tr>
                            <tr style="color: #FF66CC">
                                <td colspan="2" 
                                    style="border-style: none; border-color: inherit; border-width: medium; color: #CC0066; text-align: center; font-size: small;" 
                                    valign="top">
                                    &nbsp;</td>
                            </tr>
                        </table>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                    </td>
                </tr>
            </table>
            <asp:Label ID="Label1" runat="server" Font-Bold="True" ForeColor="Red" 
                Text="123"></asp:Label>
            &nbsp;Minutes Remaining<br />
            <asp:Timer ID="Timer1" runat="server" ontick="Timer1_Tick">
            </asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        onselectedindexchanged="GridView1_SelectedIndexChanged" 
        Width=100% DataKeyNames="qstcod,qstpic" 
        Height="186px" onpageindexchanged="GridView1_PageIndexChanged" 
        onpageindexchanging="GridView1_PageIndexChanging" 
        onrowdatabound="GridView1_RowDataBound" PageSize="1" GridLines="Horizontal">
        <Columns>
            <asp:TemplateField>                
                <ItemTemplate>                
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 42px">
                            <%#Container.DataItemIndex +1 %>
                            </td>
                            <td style="width: 61px">
                                <asp:Image ID="img1" runat="server" Height="60px" Width="60px" />
                             </td>
                            <td style="width: 700px">
                                <asp:Literal ID="l1" runat="server" Text='<%#Eval("qstdsc") %>' />
                            </td>
                            <td>
                                <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" 
                                    Font-Bold="True" oncheckedchanged="CheckBox1_CheckedChanged" 
                                    Text="Review Later" />
                             </td>
                        </tr>
                             
                        <tr>
                            <td style="width: 42px; height: 30px;">
                                </td>
                            <td style="height: 30px; width: 61px;">
                                &nbsp;</td>
                            <td style="height: 30px; width: 341px;">
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                                </asp:RadioButtonList>
                            </td>
                           
                        </tr>
                    </table>                    
                </ItemTemplate>
            </asp:TemplateField>
   
        </Columns>
        <AlternatingRowStyle BackColor="White" />
         <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#64B8ED" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
        <PagerSettings Mode="NextPrevious" NextPageText="Next" 
            PreviousPageText="Previous" />
    </asp:GridView>
    
    <div class="contentbox" style="background-position:fixed">
        <div class="head">
            Review Questions!!!</div>
        <div class="text">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:DataList ID="DataList1" runat="server" Height="16px" RepeatColumns="5" 
                        Width="267px">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Underline="True" 
                                ForeColor="Red" Text="<%#Container.DataItem.ToString()%>"></asp:Label>
                        </ItemTemplate>
                    </asp:DataList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    
    <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button1" runat="server" Text="Submit Test" 
        onclick="Button1_Click" />
&nbsp;&nbsp;&nbsp;

</asp:Content>

