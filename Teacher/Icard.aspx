﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="Icard.aspx.cs" Inherits="Teacher_Icard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <asp:FormView ID="FormView1" runat="server" Width="80%" 
        style="text-align: left">
        <HeaderTemplate><center>
            <span style="color: #FF0000; font-size: x-large"><strong>KURUKSHETRA INSTITUTE 
            OF TECHNOLOGY &amp; MANAGEMENT</strong></span>
            <br /><span style="color: #333399">Kurukshetra -Pehowa Road, Bhor Saidan<br /> 
            KURUKSHETRA - 136 119, Haryana<br /> Ph.:01741-283841-42, Fax.: -1741-283843<br /> 
            E.mail: </span><a href="mailto:kitm@sify.com" style="color: #333399">
            kitm@sify.com</a><span style="color: #333399">,&nbsp;&nbsp;&nbsp; Web: www.kitm.in</span>
            </center>
        </HeaderTemplate>


        <ItemTemplate>
            <table class="styledmenu">
                <tr>
                    <td>
                        <asp:Label ID="LBLName" runat="server" Font-Bold="True" 
                            Text='<%#Eval("name") %>'></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td rowspan="4">
                        <asp:Image ID="Image2" runat="server" Height="100px" Width="100px" ImageUrl='<%#"~/Teacherpic/"+ Eval("teacherphoto") %>'/>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        <asp:Label ID="LBLDesignation" runat="server" Font-Bold="True"><%#Eval("postdesc") %></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        <asp:Label ID="LBLdep" runat="server" Font-Bold="True" 
                            Text='<%# Eval("depname") %>'></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                               
                <tr>
                    <td style="font-weight: 700">
                        ADDRESS:</td>
                    <td style="font-weight: 700">
                        <asp:Literal ID="Litaddress" runat="server" Text='<%#Eval("address") %>'></asp:Literal>                        
                    </td>
                    <td rowspan="4">
                        <asp:Image ID="Image1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        DATE OF BIRTH:</td>
                    <td>
                        <asp:Label ID="LBLDob" runat="server" Font-Bold="True"><%#Eval("teacherdob","{0:d}") %></asp:Label>
                    </td>
                </tr>
                
                
                <tr>
                    <td style="font-weight: 700">
                        PHONE.:</td>
                    <td>
                        <asp:Label ID="LBLphone" runat="server" Font-Bold="True"><%#Eval("teacherphone1") %></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        INSTRUCTIONS:</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        1. This card is applicable only in the case of non-issuement/lost of original 
                        Identity Card.</td>
                </tr>
                <tr>
                    <td colspan="3">
                        2.This card must be produced on demand.</td>
                </tr>
                <tr>
                    <td colspan="3">
                        3.This card is not transerrable and if misused holder will be responsible.</td>
                </tr>
                <tr>
                    <td colspan="3">
                        4. this card is valid after being attested by Institute Office only.</td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </ItemTemplate>


    </asp:FormView>
</asp:Content>

