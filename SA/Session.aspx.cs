﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class SA_Session : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    Int32 count,sp;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
            DDLUpdSessionBind();
    }
    private void RegSession(Int32 sstart)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("select count(*) as cnt from tbsession where sessionstart="+sstart, con);
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            count = Convert.ToInt32(dr[0]);
        }
        dr.Close();
        cmd.Dispose();
        if (count == 0)
        {
            SqlCommand cmd1 = new SqlCommand("insert into tbsession(sessionstart,sessionphase,sessiondelsts) values(@sesnstart,1,0)", con);
            cmd1.Parameters.Add("@Sesnstart", SqlDbType.Int).Value = sstart;
            cmd1.ExecuteNonQuery();
            DDLUpdSessionBind();
            cmd1.Dispose();
            con.Close();
        }
        else
        {
            Lblmsg.Text = sstart + " Already Registered";
        }
    }
    private void UpdSession(int sid)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("update tbsession set sessionphase=2 where Sessionid=@sid", con);
        cmd.Parameters.Add("@Sid", SqlDbType.Int).Value = sid;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
    }
    private DataSet AllSession()
    {
        SqlDataAdapter adp = new SqlDataAdapter("SELECT Sessionid, CONVERT(varchar(12), Sessionstart) + ' - ' + CONVERT(varchar(12), Sessionphase) AS session FROM tbSession where sessiondelsts=0 ORDER BY Sessionstart ", con);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    private void DDLUpdSessionBind()
    {
        DataSet ds = AllSession();
        DDLUpdateSession.DataValueField = "Sessionid";
        DDLUpdateSession.DataTextField = "session";
        DDLUpdateSession.DataSource = ds;
        DDLUpdateSession.DataBind();
        DDLUpdateSession.Enabled = true;
        DDLUpdateSession.Items.Insert(0, "--Select Session --");
    }
    protected void BtnRegSession_Click(object sender, EventArgs e)
    {
        RegSession(Convert.ToInt32(TxtNewSessionStart.Text));
        Lblmsg.Text = TxtNewSessionStart.Text  + " Registered Successfully";
        Lblmsg.ForeColor = System.Drawing.Color.Green;
        TxtNewSessionStart.Text = "";
        gridbind();
        changepahse();
    }
    private void gridbind()
    {
        SqlDataAdapter adp = new SqlDataAdapter("select classid,classcursem,classsessionstart,classsessionend from tbclass",con);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShowSummary.DataSource = ds;
        GridShowSummary.DataBind();
    }
    private void changepahse()
    {
        Application.Lock();
        Int32 c = GridShowSummary.Rows.Count;
        for (Int32 i = 0; i < c; i++)
        {
            GridShowSummary.SelectRow(i);
            Int32 cid = Convert.ToInt32(GridShowSummary.SelectedDataKey.Value);
            Int32 cs = Convert.ToInt32(((Label)(GridShowSummary.SelectedRow.FindControl("Lblcursem"))).Text);
            Int32 ss = Convert.ToInt32(((Label)(GridShowSummary.SelectedRow.FindControl("Lblsessionstart"))).Text);
            Int32 se = Convert.ToInt32(((Label)(GridShowSummary.SelectedRow.FindControl("Lblsessionend"))).Text);
            if (con.State == ConnectionState.Closed)
                con.Open();
            if (cs != 2 * (se - ss))
            {
                cs = cs + 1;
                SqlCommand cmd = new SqlCommand("update  tbclass set classcursem=@cs where classid=@cid", con);
                cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
                cmd.Parameters.Add("@cs", SqlDbType.Int).Value = cs;
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
            }
            else
            {
                continue;
            }
        }
        Application.UnLock();
    }
    protected void BtnUpdateSession_Click(object sender, EventArgs e)
    {
        
        if (DDLUpdateSession.SelectedIndex == 0)
        {
            Lblmsg.Text = "Choose Session";
            Lblmsg.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd=new SqlCommand("select sessionphase from tbsession where sessionid=@sid",con);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLUpdateSession.SelectedValue);
            SqlDataReader dr=cmd.ExecuteReader();
            if(dr.HasRows)
            {
                dr.Read();
               sp=Convert.ToInt32(dr[0]);
            
            }
            dr.Close();
            cmd.Dispose();
            if (sp == 2)
            {
                Lblmsg.Text = "The Session is Already updated";
            }
            else
            {
                UpdSession(Convert.ToInt32(DDLUpdateSession.SelectedValue));
                Lblmsg.Text = DDLUpdateSession.SelectedItem + " Updated Successfully";
                Lblmsg.ForeColor = System.Drawing.Color.Green;
                gridbind();
                changepahse();
                DDLUpdSessionBind();
            }
        }
    }
}