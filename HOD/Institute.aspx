﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="Institute.aspx.cs" Inherits="HOD_Institute" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <div id="leftColumn">
   
      <div class="styledmenu">
        <h5 class="headerbar">KITM</h5>
        <ul>
          <li><asp:LinkButton ID="LinkButton5" runat="server" onclick="LinkButton1_Click">About KItm</asp:LinkButton></li>
           
             <li> <asp:LinkButton ID="LinkButton9" runat="server" onclick="LinkButton2_Click">Chairman Message</asp:LinkButton></li>
            
             <li>    <asp:LinkButton ID="LinkButton10" runat="server" onclick="LinkButton3_Click">Director Message</asp:LinkButton></li>
             
               <li>  <asp:LinkButton ID="LinkButton11" runat="server" onclick="LinkButton4_Click">Vision & MIssion</asp:LinkButton></li>
           
               <li>  <asp:LinkButton ID="LinkButton12" runat="server" onclick="LinkButton8_Click">Why KITM</asp:LinkButton></li>
        
          
        </ul>        
      
     
   </div>
   </div>
      <div id="content">
  <table>
       
        <tr>            
            <td rowspan="7">
                <asp:Panel ID="Panel1" runat="server">
                    <table>
                     
                        <tr>
                            <td style="width: 312px">
                                <span><b><i><font color="#006699" size="5">Education</font></i>
                                <table ID="table2" border="0" cellpadding="3" cellspacing="3" style="width: 96%">
                                    <tr>
                                        <td height="24" width="91%">
                                            <span><b><font color="#006699" size="4">&nbsp;leads to</font>
                                            </b></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="91%">
                                            <span><b><i><font color="#006699" size="5">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enlightenment</font> </i></b></span>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                </b></span>
                            </td>
                            <td style="width: 53px">
                                <img alt="" src="../images/shantidevi1.jpg" 
                style="width: 116px; height: 156px" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 312px">
                                &nbsp;</td>
                            <td >
                                
                                    <font color="#006699"><b>Late Smt. Shanti Devi</b></font></td>
                        </tr>
                        <tr>
                            <td style="width: 312px">
                                &nbsp;</td>
                            <td style="width: 53px">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" >
                                <div align="justify">
                                    <blockquote>
                                        <p>
                                            Shanti Devi Educational Trust (SDET) is established to pay homage to the fond 
                                            memories of Late Smt. Shanti Devi, a noble soul who had spent her whole life for 
                                            the upliftment of the underprivileged. Taking inspiration from her esteemed 
                                            vision of academically and morally strong children, we at Kurukshetra Institute 
                                            of Technology and Management (KITM) which is an endeavour of SDET, devote 
                                            ourselves to the relentless pursuit of excellence in the field of education. Our 
                                            vision is to develop this into a superior institute responsible for grooming 
                                            future leaders for the country.<br />
                                        </p>
                                    </blockquote>
                                </div>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 312px">
                                &nbsp;</td>
                            <td style="width: 53px">
                                &nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
                 <asp:Panel ID="Panel2" runat="server">
                     <table >
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td colspan="5">
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td align="left">
                                 <img 
                src="../images/chairmanbanner.jpg" />
                             </td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td align="left">
                                 <font color="#006699" size="2"><b>Chairman&#39;s Message</b></font></td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td colspan="4">
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td align="left">
                                 Dear Students,<br /> </td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td colspan="4">
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td colspan="4">
                             <div align="justify">
                                    <blockquote>
                                        
                                <p > Your interest in KITM is indeed heartening and appreciable. Today the 
                                 pre-requisite to embark on the journey towards a bright career is to secure 
                                 admission in one of the best institutions.<br />
                                 <br />
                                 We at KITM provide quality education to our students and prepare them as future 
                                 technocrats and managers of our country. We provide the best faculty and 
                                 infrastructural support. Our faculty can impart the best professional training 
                                 and infuse in you cutting edge technical knowledge
                                 <br />
                                 <br />
                                 I have always harboured a two-pronged philosophy in life. First, to promote 
                                 meaningful education and the second to inculcate values that are rich and highly 
                                 humanistic.<br />
                                 <br />
                                 Under the management of SDET, this Institute testifies the same philosophy. 
                                 Welcome to KITM ! I am sure that your personal visit will be more convincing.
                                 <br />
                                 <blockquote>
                                 </div >
                                    
                             </td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td align="left">
                                 <b><font color="#006699">Satyapal Aggarwal<br /> Chairman</font></b>&nbsp;<br /></td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td colspan="4">
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                     </table>
                </asp:Panel>
                 <asp:Panel ID="Panel3" runat="server">
                     <table class="styledmenu">
                         <tr>                             
                             <td colspan="2" align="left">
                                 <img src="../images/pj.jpg" />
                             </td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                         <td></td>
                             
                             <td align="left">
                                 <strong><font color="#006699" size="2">Message from the Director Principal</font>
                                 </strong>
                                 <br />
                             </td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                           
                             <td colspan="2">
                             <div align="justify">
                                    <blockquote>
                                        
                                <p >
                                 As India continues to make giant strides in the global arena, be it in medicine, 
                                 science &amp; technology or management, the need for very high standards of 
                                 professional education is becoming increasingly critical.
                                 <br />
                                 <br />
                                 The opening up of the Indian economy has ushered in a plethora of MNCs and big 
                                 professionally managed organizations, creating immense opportunities and 
                                 lucrative career options for the present, as well as, for the future 
                                 generations.<br />
                                 <br />
                                 By blending traditional values and modern methodologies, we at KITM present the 
                                 very best of the academic world to our students so that they can achieve great 
                                 heights in their life.<br />
                                 <br />
                                 In today’s society, where the moral dimensions of the day to day living are 
                                 losing out to the forces of the market place, we strive to make this institute 
                                 into an incubator for the meaningful transformation of mind and spirit. Your 
                                 stay with us will be an enriching and memorable experience in your life.
                                 </p>
                                 </blockquote>
                                 </div>
                             </td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td align="left">
                                 <font color="#006699"><b>Dr. P. J. George<br /> Director-Principal</b> </font></td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td align="left">
                                 <asp:LinkButton ID="LinkButton6" runat="server" onclick="LinkButton6_Click">More&gt;&gt;</asp:LinkButton></td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                        
                     </table>
                </asp:Panel>
                 <asp:Panel ID="Panel4" runat="server">
                     <table class="styledmenu">
                         <tr>
                             <td align="left" colspan="2" >
                                 &nbsp;</td>
                             <td align="left" colspan="2">
                                 <font color="#006699"><strong>About Dr. P. J .George </strong></font>
                             </td>
                             <td style="height: 18px">
                                 &nbsp;</td>
                             <td style="height: 18px">
                                 </td>
                         </tr>
                         <tr>
                             <td >
                                 &nbsp;</td>
                             <td colspan="2" align="left">
                                 <img 
                                     src="../images/georgeimg.jpg" width="500px" />
                             </td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td style="width: 101px">
                                 &nbsp;</td>
                             <td align="left" colspan="3">
                                 <font color="#663d3b"><strong><font color="#006699">Dr. P. J. George</font></strong><br />
                                 </font></td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                            
                             <td colspan="3">
                             <div align="justify">
                                    <blockquote>
                                        
                                <p > 
                                 M.Sc. (Kerala University), M.Tech. (IIT Delhi), Ph.D. (IIT Delhi).<br /> Dr. P. 
                                 J. George is Professor in the Electronic Science Department, Kurukshetra 
                                 University from 1993 onwards. After completing his M.Sc. from Kerala University, 
                                 he joined IIT Delhi for further studies and completed his M.Tech. in 1971 and 
                                 Ph. D. in 1975. During his M. Tech. and Ph. D., he worked in the area of 
                                 Semiconductor Electronics with emphasis on physics and technology of 
                                 semiconductor devices and ICs. From 1975 to 1978, he worked as Post-Doctoral 
                                 Fellow and CSIR Pool Scientist in Thin Film Technology Group in IIT Delhi to 
                                 develop thin film solar cells for terrestrial applications.
                                 <br />
                                 </p>
                                 </blockquote>
                                 </div>
                             </td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             
                             <td colspan="3">
                                 <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                     <tr>
                                         <td valign="top" width="60%">
                                             <div align="justify">
                                             <blockquote>
                                             <p>
                                                 Dr. George joined Kurukshetra University as Lecturer in 1978, promoted as reader 
                                                 in 1985 and as Professor in 1993. He was instrumental in establishing Electronic 
                                                 Science Department and University Institute of Engineering and Technology in 
                                                 Kurukshetra University.
                                                 <br />
                                                 <br />
                                                 He was the Chairman of Electronic Science Department for more than 10 years and 
                                                 also the Director-Coordinator of UIET for 3 years. He was Dean, Faculty of 
                                                 Engineering and Technology and Dean, Faculty of Science in the University. He 
                                                 was a member of University Court, Executive Council, Academic Council, Faculty 
                                                 of Science and many committees. He is also member of various academic bodies of 
                                                 many other universities and institutes.
                                                 </p>
                                                 </blockquote>
                                             </div>
                                         </td>
                                     </tr>
                                 </table>
                             </td>
                             <td align="right">
                                 
                             </td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                           
                             <td colspan="3">
                             <div align="justify">
                             <blockquote>
                             <p>
                                 Dr. George is a Fellow of Institute of Electronics and Telecommunication 
                                 Engineers, Vice-Chairman of IEEE-ED/MTT India Chapter and Vice-President of 
                                 Indian Microelectronics Society. He was a member of high power delegation of the 
                                 Haryana Govt. and Kurukshetra Univ. which visited USA, UK, Australia, Singapore 
                                 and Malaysia to establish academic link. He worked in the Harwell Laboratories 
                                 of United Kingdom Atomic Energy Establishment as Vacation Assistant in 1986 and 
                                 1987. He visited ICTP, Trieste, Italy as a Visiting Scientist. He worked as 
                                 Visiting Research Professor, Solar Energy Laboratory, UNAM, Mexico for 1&amp;1/2 
                                 years.<br />
                                 <br />
                                 Dr. George has guided 16 Ph.D students and presently guiding another 6 students. 
                                 He has more than 60 research publications to his credit. In addition he has 
                                 supervised a number of post-graduate project works. He has undertaken many 
                                 research and development projects funded by various agencies. He has 35 years of 
                                 teaching, research and administrative experience.<br /></p></blockquote></div> </td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td style="width: 101px">
                                 <asp:LinkButton ID="LinkButton7" runat="server" onclick="LinkButton7_Click">&lt;&lt;Back</asp:LinkButton>
                             </td>
                             <td colspan="3">
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td style="width: 101px">
                                 &nbsp;</td>
                             <td colspan="3">
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td style="width: 101px">
                                 &nbsp;</td>
                             <td colspan="3">
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                     </table>
                     
                </asp:Panel>
                 <asp:Panel ID="Panel5" runat="server">
                     <table class="styledmenu">
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td colspan="2">
                                 <img 
                src="../images/vision.jpg" />
                             </td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td align="left">
                                 <span class="hd"><font color="#006699" size="2"><b><strong>Our Mission</strong></b></font></span></td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td>
                             <div align="justify">
                             <blockquote>

                                 <p>
                                     To provide exceptional professional training to students to enable them to 
                                     successfully meet the challenging demands the global market.</p></blockquote></div>
                             </td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td align="left">
                                 <span class="hd"><font color="#006699" size="2"><b><strong>Our Destination</strong></b></font></span>&nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td>
                             <div align="justify">
                             <blockquote>
                                 <p>
                                     To reach the forefront of technical and management education by creating highly targeted professional leadership by meaningful transformation of mind and spirit.
</p></blockquote></div>
                             </td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                         <tr>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                             <td>
                                 &nbsp;</td>
                         </tr>
                     </table>
                </asp:Panel>
                 <asp:Panel ID="Panel6" runat="server">                     
                                 <img src="../images/why.jpg" width="100%" />                            
                </asp:Panel>

            </td>
        </tr>
       
        
        
    </table>
    <//div>
 
</asp:Content>

