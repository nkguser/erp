﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class Teacher_studentattend : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            DDLSessionBind();
        }
    }
    private void DDLSessionBind()
    {        
        DDLSession.DataTextField = "session";
        DDLSession.DataValueField = "sessionid";
        DDLSession.DataSource = ERP.AllSession();
        DDLSession.DataBind();
        DDLSession.Items.Insert(0, "-- Select session --");
    }
    protected void DDLSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSession.SelectedIndex == 0)
        {
            sessionclear();
        }
        else
        {
            sessionclear();
            classbind();
        }
    }
    private void sessionclear()
    {
        DDLClass.Items.Clear();
        DDLClass.Enabled = false;
        DDLClass.Items.Insert(0, "--Select Class--");
        DDLSubject.Items.Clear();
        DDLSubject.Enabled = false;
        DDLSubject.Items.Insert(0, "--Select Subject--");
        LBLTotalDelivered.Text = string.Empty;
        RBLOpt.ClearSelection();
        RBLOpt.Enabled = false;
        LBLmsg.Text = string.Empty;
        TxtDate.Text = string.Empty;
        Panelfill.Visible = false;
        Panelview.Visible = false;
        RadioButtonList1.ClearSelection();
        DDLDate.SelectedIndex = 0;
        Panel1.Visible = false;
        Panel2.Visible = false;
    }
    private void classbind()
    {
        DDLClass.DataTextField = "Class";
        DDLClass.DataValueField = "subjectallotGroupid";
        DDLClass.DataSource = ERP.TeacherClass(Convert.ToInt32(Session["stid"]), Convert.ToInt32(DDLSession.SelectedValue));
        DDLClass.DataBind();
        DDLClass.Enabled = true;
        DDLClass.Items.Insert(0, "-- Select Class --");
    }
    protected void DDLClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLClass.SelectedIndex == 0)
        {
            classclear();
        }
        else
        {
            classclear();
            subjectbind();
        }
    }
    private void classclear()
    {
        DDLSubject.Items.Clear();
        DDLSubject.Enabled = false;
        DDLSubject.Items.Insert(0, "--Select Subject--");
        LBLTotalDelivered.Text = string.Empty;
        RBLOpt.ClearSelection();
        RBLOpt.Enabled = false;
        LBLmsg.Text = string.Empty;
        TxtDate.Text = string.Empty;
        Panelfill.Visible = false;
        Panelview.Visible = false;
        RadioButtonList1.ClearSelection();
        DDLDate.SelectedIndex = 0;
        Panel1.Visible = false;
        Panel2.Visible = false;
    }
    private void subjectbind()
    {
        DDLSubject.DataTextField = "subject";
        DDLSubject.DataValueField = "subjectallotSubjectid";
        DDLSubject.DataSource = ERP.TeacherSubject(Convert.ToInt32(Session["stid"]), Convert.ToInt32(DDLClass.SelectedValue));
        DDLSubject.DataBind();
        DDLSubject.Enabled = true;
        DDLSubject.Items.Insert(0, "-- Select Subject --");
    }
    protected void DDLSubject_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSubject.SelectedIndex == 0)
        {
            subjectclear();
        }
        else
        {
            subjectclear();
            LBLTotalDeliveredBind();
            RBLOpt.Enabled = true;
        }
    }
    private void subjectclear()
    {
        LBLTotalDelivered.Text = string.Empty;
        RBLOpt.ClearSelection();
        RBLOpt.Enabled = false;
        LBLmsg.Text = string.Empty;
        TxtDate.Text = string.Empty;
        Panelfill.Visible = false;
        Panelview.Visible = false;
        RadioButtonList1.ClearSelection();
        DDLDate.SelectedIndex = 0;
        Panel1.Visible = false;
        Panel2.Visible = false;
    }
    private void LBLTotalDeliveredBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT COUNT(DISTINCT tbstudentattnd.studentattnddate) AS count FROM   tbstudentattnd INNER JOIN   tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE        (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0)", con);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        LBLTotalDelivered.Text = cmd.ExecuteScalar().ToString();
        cmd.Dispose();
        con.Close();
    }
    protected void RBLOpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RBLOpt.SelectedIndex == 0)
        {
            DDLDate.Items.Clear();
            DDLDate.Items.Insert(0,"-- Choose Date --");
            Panelview.Visible = false;
            Panel1.Visible = false;
            Panel2.Visible = false;
            TxtDate.Text = string.Empty;
            LBLmsg.Text = string.Empty;
            RadioButtonList1.ClearSelection();
            Panelfill.Visible = true;
            GridFillBind();
        }
        else
        {
            if (Convert.ToInt32(LBLTotalDelivered.Text) == 0)
            {
                LBLmsg.Text = "No Lecture/Tutorial/Lab delievered till Date";
                TxtDate.Text = string.Empty;
                Panelfill.Visible = false;
                Panelview.Visible = false;
                DDLDate.Items.Clear();
                DDLDate.Items.Insert(0, "-- Choose Date --");
                Panel1.Visible = false;
                Panel2.Visible = false;
                RadioButtonList1.ClearSelection();
            }
            else
            {
                TxtDate.Text = string.Empty;
                LBLmsg.Text = string.Empty;      
                Panelfill.Visible = false;
                Panel1.Visible = false;
                Panel2.Visible = false;
                RadioButtonList1.ClearSelection();
                DDLDate.Items.Clear();
                DDLDate.Items.Insert(0, "-- Choose Date --");
                Panelview.Visible = true; 
            }
       }
    }
    private void GridFillBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT studentid,studentRollno FROM tbStudent where studentgroupid=@gid", con);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridFill.DataSource = ds;
        GridFill.DataBind();
    }
    protected void GridFill_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) AS count FROM tbstudentattnd WHERE (studentattndattend = 'True') AND (studentattndsubjectid = @sid) AND (studentattndstudentid = @rid) AND (studentattnddelsts = 0)", con);
            //set datakaey fiel student id in grid
            cmd.Parameters.Add("@rid", SqlDbType.Int).Value = Convert.ToInt32(GridFill.DataKeys[e.Row.RowIndex].Value);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
            Label l1 = (Label)(e.Row.FindControl("LblAt"));
            Label l2 = (Label)(e.Row.FindControl("Lblpercent"));
            l1.Text = cmd.ExecuteScalar().ToString();
            if (Convert.ToInt32(LBLTotalDelivered.Text) != 0)
                l2.Text = (100 * Convert.ToInt32(l1.Text) / Convert.ToInt32(LBLTotalDelivered.Text)).ToString() + "%";
            else
                l2.Text = "--";
            cmd.Dispose();
            con.Close();
        }
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        DateTime sdate = Convert.ToDateTime(TxtDate.Text);
        Int32 c = GridFill.Rows.Count;
        if (c != 0)
        {
            GridFill.SelectRow(0);
            Int32 l1 = Convert.ToInt32(GridFill.SelectedDataKey.Value);
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmddate = new SqlCommand("SELECT COUNT(*) AS cnt FROM tbstudentattnd WHERE (studentattndstudentid = @sid) AND (studentattndsubjectid = @ssid) AND (studentattnddate = @sadate)", con);
            cmddate.Parameters.Add("@sid", SqlDbType.Int).Value = l1;
            cmddate.Parameters.Add("@ssid", SqlDbType.Int).Value = DDLSubject.SelectedValue;
            cmddate.Parameters.Add("@sadate", SqlDbType.Date).Value = Convert.ToDateTime(sdate.ToShortDateString());
            Int32 check = Convert.ToInt32(cmddate.ExecuteScalar());
            if (check == 0)
            {
                for (Int32 i = 0; i < c; i++)
                {
                    GridFill.SelectRow(i);
                    Int32 l = Convert.ToInt32(GridFill.SelectedDataKey.Value);
                    Boolean b = Convert.ToBoolean(((RadioButtonList)(GridFill.SelectedRow.FindControl("RBLAttendance"))).SelectedValue);
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    Application.Lock();
                    SqlCommand cmd = new SqlCommand("INSERT INTO tbstudentattnd (studentattndstudentid, studentattndsubjectid, studentattnddate, studentattndattend, studentattndapprovlvid, studentattnddelsts) VALUES (@rid,@sid,@date,@at,1,0)", con);
                    cmd.Parameters.Add("@rid", SqlDbType.Int).Value = l;
                    cmd.Parameters.Add("@sid", SqlDbType.Int).Value = DDLSubject.SelectedValue;
                    cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(sdate.ToShortDateString());
                    cmd.Parameters.Add("@at", SqlDbType.Bit).Value = b;
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    con.Close();
                    Application.UnLock();
                }
                LBLTotalDeliveredBind();
                GridFillBind();
                LBLmsg.Text = "Records saved successfully for date : " + sdate.ToShortDateString();
                TxtDate.Text = string.Empty;
            }
            else
                LBLmsg.Text = "Records already submitted for date : " + sdate.ToShortDateString();
                TxtDate.Text = string.Empty;
        }
        else
            LBLmsg.Text = " There are no records for Submission........... ";
    }
    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RadioButtonList1.SelectedIndex == 0)
        {
            DDLDateBindnotapproved();
            DDLDatesubmithodBind();
            Panel1.Visible = false;
            Panel2.Visible = false;
            LBLmsg.Text = string.Empty;

        }
        else
        {
            DDLDateBindapproved();
            Panel1.Visible = false;
            Panel2.Visible = false;
            LBLmsg.Text = string.Empty;
        }
    }
    protected void DDLdatesubmithod_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLdatesubmithod.SelectedIndex == 0)
        {
            LnkBtmSubmitHod.Visible = false;

        }
        else
        {
            LnkBtmSubmitHod.Visible = true;
        }
    }
    private void DDLDateBindnotapproved()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT DISTINCT tbstudentattnd.studentattnddate FROM tbstudentattnd INNER JOIN tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE     (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattndapprovlvid = 1) AND (tbstudent.studentdelsts = 0)", con);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DDLDate.DataTextField = "studentattndDate";
        DDLDate.DataValueField = "studentattndDate";
        DDLDate.DataSource = ds;
        DDLDate.DataBind();
        DDLDate.Items.Insert(0, "-- Choose Date --");
    }
    protected void DDLDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLDate.SelectedIndex == 0)
        {
            Panel1.Visible = false;
            Panel2.Visible = false;
            LBLmsg.Text = string.Empty;
            LnkBtmSubmitHod.Visible = false;
        }
        else
        {
            if (RadioButtonList1.SelectedIndex == 0)
            {
                Panel1.Visible = true;
                Panel2.Visible = false;
                GridShowBind();
                LBLmsg.Text = string.Empty;
                LnkBtmSubmitHod.Visible = false;
            }
            else
            {
                Panel1.Visible = false;
                Panel2.Visible =true;
                GridShowSubmitBind();
                LBLmsg.Text = string.Empty;
                LnkBtmSubmitHod.Visible = false;
            }
        }

    }
    private void GridShowBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT  studentid, tbstudent.studentrollno, tbstudentattnd.studentattndattend FROM  tbstudent INNER JOIN   tbstudentattnd ON tbstudent.studentid = tbstudentattnd.studentattndstudentid WHERE     (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudentattnd.studentattnddate = @date) AND (tbstudent.studentgroupid = @gid) AND (tbstudent.studentdelsts = 0)  AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattndapprovlvid = 1)", con);
        cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(DDLDate.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShow.DataSource = ds;
        GridShow.DataBind();
        cmd.Dispose();
        con.Close();
    }
    protected void GridShow_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridShow.EditIndex = e.NewEditIndex;
        GridShowBind();
    }
    protected void GridShow_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        Int32 rno = Convert.ToInt32(GridShow.DataKeys[e.RowIndex].Value);
        Boolean at = Convert.ToBoolean(((RadioButtonList)(GridShow.Rows[e.RowIndex].FindControl("RBLAttendance"))).SelectedValue);
        if (con.State == ConnectionState.Closed)
            con.Open();
        // set datakeyfield to student id in grid view
        SqlCommand cmd = new SqlCommand("UPDATE   tbstudentattnd SET  studentattndattend = @a FROM  tbstudentattnd INNER JOIN   tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE        (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudentattnd.studentattnddate = @d) AND (tbstudentattnd.studentattnddelsts = 0) AND                          (tbstudent.studentdelsts = 0) AND (tbstudentattnd.studentattndstudentid = @rid) AND (tbstudentattnd.studentattndapprovlvid = 1)", con);
        cmd.Parameters.Add("@a", SqlDbType.Bit).Value = at;
        cmd.Parameters.Add("@rid", SqlDbType.Int).Value = rno;
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@d", SqlDbType.Date).Value = Convert.ToDateTime(DDLDate.SelectedValue);
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        GridShow.EditIndex = -1;
        GridShowBind();
    }
    protected void GridShow_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridShow.EditIndex = -1;
        GridShowBind();
    }
    protected void GridShow_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) AS count FROM  tbstudentattnd WHERE (studentattndsubjectid = @sid) AND (studentattndstudentid = @rid) AND (studentattndattend = 'True') AND (studentattnddelsts = 0)", con);
            // set gridview datakey
            cmd.Parameters.Add("@rid", SqlDbType.Int).Value = Convert.ToInt32(GridShow.DataKeys[e.Row.RowIndex].Value);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
            Label l = (Label)(e.Row.FindControl("LBLAttend"));
            Label l1 = (Label)(e.Row.FindControl("LblAt"));
            Label l2 = (Label)(e.Row.FindControl("Lblpercent"));
            l1.Text = cmd.ExecuteScalar().ToString();
            if (Convert.ToInt32(LBLTotalDelivered.Text) != 0 || LBLTotalDelivered.Text != "")
                l2.Text = (100 * Convert.ToInt32(l1.Text) / Convert.ToInt32(LBLTotalDelivered.Text)).ToString() + "%";
            else
                l2.Text = "--";
            cmd.Dispose();
            con.Close();
            if (l.Text.Equals("True"))
                l.Text = "Present";
            else
                l.Text = "Absent";
        }
    }
    private void DDLDatesubmithodBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT DISTINCT tbstudentattnd.studentattnddate FROM tbstudentattnd INNER JOIN tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE     (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattndapprovlvid = 1) AND (tbstudent.studentdelsts = 0)", con);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DDLdatesubmithod.DataTextField = "studentattndDate";
        DDLdatesubmithod.DataValueField = "studentattndDate";
        DDLdatesubmithod.DataSource = ds;
        DDLdatesubmithod.DataBind();
        DDLdatesubmithod.Items.Insert(0, "-- Choose Date --");
    }
    private void DDLDateBindapproved()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT DISTINCT tbstudentattnd.studentattnddate FROM tbstudentattnd INNER JOIN tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE     (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattndapprovlvid = 2) AND (tbstudent.studentdelsts = 0)", con);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DDLDate.DataTextField = "studentattndDate";
        DDLDate.DataValueField = "studentattndDate";
        DDLDate.DataSource = ds;
        DDLDate.DataBind();
        DDLDate.Items.Insert(0, "-- Choose Date --");
    }
    private void GridShowSubmitBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT studentid, tbstudent.studentrollno, tbstudentattnd.studentattndattend FROM tbstudent INNER JOIN tbstudentattnd ON tbstudent.studentid = tbstudentattnd.studentattndstudentid WHERE (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudentattnd.studentattnddate = @date) AND (tbstudent.studentgroupid = @gid) AND (tbstudent.studentdelsts = 0) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattndapprovlvid >1)", con);
        cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(DDLDate.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShowSubmit.DataSource = ds;
        GridShowSubmit.DataBind();
        cmd.Dispose();
        con.Close();

    } 
    protected void GridShowSubmit_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) AS count FROM  tbstudentattnd WHERE (studentattndsubjectid = @sid) AND (studentattndstudentid = @rid) AND (studentattndattend = 'True') AND (studentattnddelsts = 0) ", con);
            // set gridview datakey
            cmd.Parameters.Add("@rid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSubmit.DataKeys[e.Row.RowIndex].Value);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
            Label l = (Label)(e.Row.FindControl("LBLAttend"));
            Label l1 = (Label)(e.Row.FindControl("LblAt"));
            Label l2 = (Label)(e.Row.FindControl("Lblpercent"));
            l1.Text = cmd.ExecuteScalar().ToString();
            l2.Text = (100 * Convert.ToInt32(l1.Text) / Convert.ToInt32(LBLTotalDelivered.Text)).ToString() + "%";
            cmd.Dispose();
            con.Close();
            if (l.Text.Equals("True"))
                l.Text = "Present";
            else
                l.Text = "Absent";
        }
    }
    protected void LnkBtmSubmitHod_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("UPDATE tbstudentattnd SET   studentattndapprovlvid = 2 FROM    tbstudentattnd INNER JOIN  tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE     (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattnddate <= @date) AND  (tbstudentattnd.studentattndsubjectid = @sid) and (studentattndapprovlvid = 1)", con);
        cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(DDLdatesubmithod.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        Panel1.Visible = false;
        RadioButtonList1.SelectedIndex = 1;
        DDLDateBindapproved();
        DateTime sdate = Convert.ToDateTime(Convert.ToString(DDLdatesubmithod.SelectedItem));
        LBLmsg.Text = "Attendance Submitted Successfully for the Date " + sdate;
    }  
}