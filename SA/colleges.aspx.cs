﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;
using System.IO;

public partial class SA_colleges : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    Int32 id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            gridbind();
        }
    }
    private void gridbind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlDataAdapter adp = new SqlDataAdapter("select * from tbclg", con);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        Gridclg.DataSource = ds;
        Gridclg.DataBind();
    }
    private void clear()
    {
        TxtClgname.Text = string.Empty;
        TxtEY.Text = string.Empty;
        TxtCY.Text = string.Empty;
        TxtAID.Text = string.Empty;
        
        TxtFname.Text = string.Empty;
        TxtLname.Text = string.Empty;
        Txtemail.Text = string.Empty;
        Txtphone.Text = string.Empty;
        
    }
    private void disable()
    {
        TxtEY.Enabled = false;
        TxtCY.Enabled = false;
        TxtAID.Enabled = false;
        TxtFname.Enabled =false;
        TxtLname.Enabled = false;
        Txtemail.Enabled = false;
        Txtphone.Enabled = false;
        LBreset.Enabled = false;
    }
    private void enable()
    {
        TxtEY.Enabled = true;
        TxtCY.Enabled = true;
        
        TxtFname.Enabled = true;
        TxtLname.Enabled = true;
        Txtemail.Enabled = true;
        Txtphone.Enabled = true;
        LBreset.Enabled = true;
    }
    protected void Rblclg_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Rblclg.SelectedIndex == 0)
        {
            P1.Visible = true;
            Lblclgname.Text = "Reg. College Name";
            TxtClgname.Visible = true;
            DDLclgname.Visible = false;
            Imglogo.Visible = false;
            Btnregupd.Text = "Register";
            Btnregupd.Visible = true;
            FUlogo.Visible = true;
            p2.Visible = false;
            Lblmsg.Text = string.Empty;
            BtnCancel.Visible = false;
            Gridclg.Visible = true;
            clear();
            enable();
        }
        else
        {
            Gridclg.Visible = false;
            P1.Visible = true;
            Lblclgname.Text = "Choose College";
            DDLclgname.Visible = true;
            TxtClgname.Visible = false;
            FUlogo.Visible = false;
            Imglogo.Visible = true;
            Btnregupd.Visible = false;
            Btnregupd.Text = "Update College Info";
            p2.Visible = true;
            disable();
            clear();
            ddlbind();
            
        }
    }
    private void ddlbind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("select * from tbclg",con);
        SqlDataReader dr = cmd.ExecuteReader();
        DDLclgname.DataTextField = "clgname";
        DDLclgname.DataValueField = "clgid";
        DDLclgname.DataSource = dr;
        DDLclgname.DataBind();
        DDLclgname.Items.Insert(0, "--Select Course --");
        dr.Close();
        cmd.Dispose();
    }
    protected void LBreset_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        Int32 a= Convert.ToInt32(ViewState["id"]);
        SqlCommand cmd = new SqlCommand("update tbuser set userpwd=@userlogid where userid="+a,con);
        cmd.Parameters.Add("@userlogid", SqlDbType.VarChar,50).Value = TxtAID.Text;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        Lblmsg.Text = "Password Resetted Successfully";
    }
    protected void DDLclgname_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLclgname.SelectedIndex == 0)
        {
            Lblmsg.Text = "College Not Selected";
            clear();
            disable();
            Imglogo.Visible = true;
            Imglogo.ImageUrl = "";
            Btnregupd.Visible = false;
            BtnCancel.Visible = false;
        }
        else
        {
            find();
        }
    }

    private void find()
    {
        Lblmsg.Text = string.Empty;
        disable();
        FUlogo.Visible = false;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("select * from tbclg where clgid=@clgid", con);
        cmd.Parameters.Add("@clgid", SqlDbType.Int).Value = Convert.ToInt32(DDLclgname.SelectedValue);
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            Imglogo.ImageUrl = "~/collegelogo/" + dr["clglogo"].ToString();
            Imglogo.Visible = true;
            TxtEY.Text = dr["clgstart"].ToString();
            TxtCY.Text = dr["clgend"].ToString();
            id = Convert.ToInt32(dr["clgadminid"]);
            ViewState["id"] = id;
            TxtFname.Text = dr["clgfname"].ToString();
            TxtLname.Text = dr["clglname"].ToString();
            Txtemail.Text = dr["clgemail"].ToString();
            Txtphone.Text = dr["clgphone"].ToString();
            TxtClgname.Text = dr["clgname"].ToString();
        }
        dr.Close();
        cmd.Dispose();
        SqlCommand adminid = new SqlCommand("select userlogid from tbuser where userid=@clgadminid", con);
        adminid.Parameters.Add("@clgadminid", SqlDbType.Int).Value = id;
        SqlDataReader dr1 = adminid.ExecuteReader();
        if (dr1.HasRows)
        {
            dr1.Read();
            TxtAID.Text = dr1[0].ToString();
        }
        dr1.Close();
        adminid.Dispose();
        con.Close();
        Btnregupd.Visible = true;
        Btnregupd.Text = "Edit College Info";
    }
    protected void Btnregupd_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (Btnregupd.Text =="Register")
        {
            SqlCommand cmd = new SqlCommand("insclg",con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@clgn", SqlDbType.VarChar,200).Value = TxtClgname.Text;
            cmd.Parameters.Add("@clgaid", SqlDbType.VarChar, 50).Value = TxtFname.Text;
            cmd.Parameters.Add("@clgs", SqlDbType.Int).Value = Convert.ToInt32(TxtEY.Text);
            if (TxtCY.Text == null || TxtCY.Text == string.Empty)
            {
                cmd.Parameters.Add("@clge", SqlDbType.Int).Value = Convert.DBNull;
            }
            else
            {
                cmd.Parameters.Add("@clge", SqlDbType.Int).Value = Convert.ToInt32(TxtCY.Text);
            }
            cmd.Parameters.Add("@clgfn", SqlDbType.VarChar, 50).Value = TxtFname.Text;
            cmd.Parameters.Add("@clgln", SqlDbType.VarChar, 50).Value = TxtLname.Text;
            cmd.Parameters.Add("@clgem", SqlDbType.VarChar, 100).Value = Txtemail.Text;
            
            cmd.Parameters.Add("@clgp", SqlDbType.BigInt).Value = Convert.ToInt64(Txtphone.Text);
            cmd.Parameters.Add("@uid", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            if (FUlogo.FileName == null || FUlogo.FileName == string.Empty)
            {
                Lblmsg.Text = "Please choose logo  for College";
            }
            else
            {
                cmd.Parameters.Add("@clgl", SqlDbType.VarChar, 100).Value = Path.GetExtension(FUlogo.PostedFile.FileName);
                cmd.ExecuteNonQuery();
                Int32 rowid = Convert.ToInt32(cmd.Parameters["@uid"].Value);
                cmd.Dispose();
                string a1 = rowid.ToString() + Path.GetExtension(FUlogo.PostedFile.FileName);
                string sp = Server.MapPath("~\\collegelogo" + "\\");
                if (sp.EndsWith("\\") == false)
                    sp += "\\";
                sp += a1;
                FUlogo.PostedFile.SaveAs(sp);
                Lblmsg.Text = TxtClgname.Text + " Added Successfully";
                clear();
                gridbind();
            }
            
            
        }
        else if (Btnregupd.Text == "Edit College Info")
        {
            LBreset.Enabled = true;
            Btnregupd.Text = "Update College Info";
            BtnCancel.Visible = true;
            enable();
            TxtEY.Enabled = false;
            Imglogo.Visible = false;
            FUlogo.Visible = true;
            DDLclgname.Visible = false;
            TxtClgname.Visible = true;
        }
        else
        {
            SqlCommand cmd = new SqlCommand("updclg", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@clgid", SqlDbType.Int).Value = Convert.ToInt32(DDLclgname.SelectedValue);
            cmd.Parameters.Add("@clgn", SqlDbType.VarChar, 200).Value = TxtClgname.Text;
            if (TxtCY.Text == null || TxtCY.Text == string.Empty)
            {
                cmd.Parameters.Add("@clge", SqlDbType.Int).Value = Convert.DBNull;
            }
            else
            {
                cmd.Parameters.Add("@clge", SqlDbType.Int).Value = Convert.ToInt32(TxtCY.Text);
            }
            cmd.Parameters.Add("@clgfn", SqlDbType.VarChar, 50).Value = TxtFname.Text;
            cmd.Parameters.Add("@clgln", SqlDbType.VarChar, 50).Value = TxtLname.Text;
            cmd.Parameters.Add("@clgem", SqlDbType.VarChar, 100).Value = Txtemail.Text;
            cmd.Parameters.Add("@clgp", SqlDbType.BigInt).Value = Convert.ToInt64(Txtphone.Text);
            cmd.Parameters.Add("@clgaid", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();
            Int32 aid = Convert.ToInt32(cmd.Parameters["@clgaid"].Value);
            cmd.Dispose();
            Int32 a;
            string a1 = aid.ToString() + Path.GetExtension(FUlogo.PostedFile.FileName);
            if (FUlogo.FileName == null || FUlogo.FileName == string.Empty)
            {
                
            }
            else
            {
                
                SqlCommand cmd1 = new SqlCommand("select clglogo from tbclg where clgid=" + Convert.ToInt32(DDLclgname.SelectedValue), con);
                SqlDataReader dr = cmd1.ExecuteReader();
                dr.Read();
                string abc = dr[0].ToString();
                File.Delete(Server.MapPath("~\\collegelogo") + "\\" + abc);
                string sp = Server.MapPath("~\\collegelogo" + "\\");
                if (sp.EndsWith("\\") == false)
                    sp += "\\";
                sp += a1;
                FUlogo.PostedFile.SaveAs(sp);
                dr.Close();
                cmd1.Dispose();

            }
            DDLclgname.Visible = true;
            TxtClgname.Visible = false;
            disable();
            ddlbind();
            Btnregupd.Visible = false;
            BtnCancel.Visible = false;
            Lblmsg.Text = TxtClgname.Text + " Updated Successfully";
            clear();
            
        }
    }
    protected void BtnCancel_Click(object sender, EventArgs e)
    {
        
            find();
            DDLclgname.Visible = true;
            TxtClgname.Visible = false;
            BtnCancel.Visible = false;
        
    }
}