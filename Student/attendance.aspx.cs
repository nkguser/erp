﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using kitmerp;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Student_attendance : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {       
        if (Page.IsPostBack == false)
        {
            GetData();
        }
    }
    private void GetData()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        DDLSem.Items.Clear();
        SqlCommand cmd = new SqlCommand("SELECT     tbcourse.coursesemester FROM  tbcourse INNER JOIN tbdep ON tbcourse.courseid = tbdep.depcourseid INNER JOIN tbclass ON tbdep.depid = tbclass.classdepid INNER JOIN                      tbgroup ON tbclass.classid = tbgroup.groupclassid INNER JOIN                      tbstudent ON tbgroup.groupid = tbstudent.studentgroupid WHERE     (tbcourse.coursedelsts = 0) AND (tbdep.depdelsts = 0) AND (tbgroup.groupdelsts = 0) AND (tbclass.classdelsts = 0) AND (tbstudent.studentdelsts = 0) AND                       (tbstudent.studentid = @stid)", con);
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        int count = Int32.Parse(dr["coursesemester"].ToString());
        DDLSem.Items.Insert(0, "-- Choose Semester --");
        for (int i = 1; i <= count; i++)
        {
            DDLSem.Items.Add(i.ToString());
        }
        dr.Close();
        cmd.Dispose();
        con.Close();
    }
    private void GridShowDetailBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        String order = RBLDetailOpt.SelectedValue;
        SqlCommand cmd = new SqlCommand("SELECT  distinct  tbstudentattnd.studentattndsubjectid ,tbsubject.subjecttitle + ' - ' + tbsubjectype.subjecttypename as subject, tbstudentattnd.studentattnddate, tbstudentattnd.studentattndattend FROM   tbsubject INNER JOIN  tbstudentattnd ON tbsubject.subjectid = tbstudentattnd.studentattndsubjectid INNER JOIN                       tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid WHERE (tbstudentattnd.studentattndstudentid = @stid) AND (tbsubject.subjectsem = @sno) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbsubject.subjectdelsts = 0) AND (tbsubjectype.subjectdelsts = 0) ORDER BY " + order, con);
        cmd.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DDLSem.SelectedValue);
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShowDetail.DataSource = ds;
        GridShowDetail.DataBind();
        cmd.Dispose();
        con.Close();
    }
    protected void GridShowDetail_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            Label l = (Label)(e.Row.FindControl("LBLAttend"));
            if (l.Text.Equals("True"))
                l.Text = "Present";
            else
                l.Text = "Absent";
        }
    }
    private void GridShowSummaryBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT distinct tbstudentattnd.studentattndsubjectid, tbsubject.subjecttitle + ' - ' + tbsubjectype.subjecttypename AS subject FROM         tbsubject INNER JOIN tbstudentattnd ON tbsubject.subjectid = tbstudentattnd.studentattndsubjectid INNER JOIN  tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid WHERE     (tbstudentattnd.studentattndstudentid = @stid) AND (tbsubject.subjectsem = @sno) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbsubject.subjectdelsts = 0) AND (tbsubjectype.subjectdelsts = 0) ORDER BY subject", con);
        cmd.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DDLSem.SelectedValue);
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShowSummary.DataSource = ds;
        GridShowSummary.DataBind();
        cmd.Dispose();
        con.Close();
    }
    protected void GridShowSummary_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("SELECT     COUNT(*) AS count FROM         tbstudentattnd WHERE     (studentattndstudentid = @stid) AND (studentattndattend = 'True') AND (studentattndsubjectid = @sid) AND (studentattnddelsts = 0)", con);
            SqlCommand cmd1 = new SqlCommand("SELECT COUNT(*) AS count FROM tbstudentAttnd WHERE (studentattndstudentid = @stid) AND (studentattndsubjectid = @sid) AND (studentattnddelsts = 0)", con);
            cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSummary.DataKeys[e.Row.RowIndex].Values["studentattndsubjectid"]);
            cmd1.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
            cmd1.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSummary.DataKeys[e.Row.RowIndex].Values["studentattndsubjectid"]);
            Label l1 = (Label)(e.Row.FindControl("LblAt"));
            Label l2 = (Label)(e.Row.FindControl("LblTotal"));
            Label l3 = (Label)(e.Row.FindControl("Lblpercent"));
            l1.Text = cmd.ExecuteScalar().ToString();
            l2.Text = cmd1.ExecuteScalar().ToString();
            if (Convert.ToInt32(l2.Text) != 0)
                l3.Text = (100 * Convert.ToInt32(l1.Text) / Convert.ToInt32(l2.Text)).ToString() + "%";
            else
                l3.Text = "--";
            cmd.Dispose();
            cmd1.Dispose();
            con.Close();
        }
    }
    protected void DDLSem_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSem.SelectedIndex == 0)
        {
            DDLSem.SelectedIndex = 0;
            GridShowSummary.Visible = false;
            GridShowDetail.Visible = false;
            RBLDetailOpt.Visible = false;
        }
        else
        {
            GridShowSummary.Visible = false;
            GridShowDetail.Visible = false;
            RBLDetailOpt.Visible = false;
            GridShowSummaryBind();
            if (GridShowSummary.Rows.Count == 0)
            {
                return;
            }
            else
            GridShowSummary.Visible = true;
        }
    }
    protected void RBLOpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RBLOpt.SelectedIndex == 0 && DDLSem.SelectedIndex==0)
        {
            RBLDetailOpt.Visible = false;
        }
        else if (RBLOpt.SelectedIndex == 0)
        {
            GridShowSummaryBind();
            GridShowSummary.Visible = true;
            GridShowDetail.Visible = false;
            RBLDetailOpt.Visible = false;
        }
        else
        {
            RBLDetailOpt.Visible = true;
            GridShowSummary.Visible = false;

        }
    }
    protected void RBLDetailOpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSem.SelectedIndex == 0)
        {
            GridShowDetail.Visible = false;
            GridShowSummary.Visible = false;
        }
        else
        {
            GridShowDetailBind();
            GridShowDetail.Visible = true;
        }
    }
}