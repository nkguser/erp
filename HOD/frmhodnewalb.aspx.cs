﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class HOD_frmhodnewalb : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            if (Request.QueryString["acod"] != null)
            {
                Int32 a = Convert.ToInt32(Request.QueryString["acod"]);
                nsphotosnak.clsalb obj = new nsphotosnak.clsalb();
                List<nsphotosnak.clsalbprp> k = obj.Find_rec(a);
                TextBox1.Text = k[0].p_albnam;
                DropDownList1.DataBind();
                DropDownList1.SelectedIndex = -1;
                DropDownList1.Items.FindByValue(k[0].p_albtmpcod
                    .ToString()).Selected = true;
                RadioButtonList1.Items.FindByValue(k[0].p_albsts.ToString())
                    .Selected = true;
                Button1.Text = "Update";
                Label1.Text = "Edit Album";
            }
            else
                Label1.Text = "New Album";

        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        nsphotosnak.clsalb obj = new nsphotosnak.clsalb();
        nsphotosnak.clsalbprp objprp = new nsphotosnak.clsalbprp();
        objprp.p_albnam = TextBox1.Text;
        objprp.p_albregcod = Convert.ToInt32(Session["depid"]);
        objprp.p_albtmpcod = Convert.ToInt32(DropDownList1.SelectedValue);
        objprp.p_albcvrpic = "nopic.jpg";
        objprp.p_albsts = Convert.ToChar(RadioButtonList1.SelectedValue);
        if (Button1.Text == "Create")
        {
            Int32 a = obj.Save_Rec(objprp);
            DirectoryInfo di = new DirectoryInfo(Server.MapPath("../albums") + "//" + a.ToString());
            if (di.Exists == false)
                di.Create();
            Response.Redirect("frmhodmyalb.aspx");
        }
        else
        {
            objprp.p_albcod = Convert.ToInt32(Request.QueryString["acod"]);
            obj.Update_Rec(objprp);
            Response.Redirect("frmhodmyalb.aspx");
        }
    }
}