﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;


public partial class Teacher_Studentview : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();

    }
    protected void DetailsView1_ItemDeleting(object sender, DetailsViewDeleteEventArgs e)
    {
        SqlCommand cmd = new SqlCommand("insert into tbstudent(studentdelsts) values(1) where student where studentrollid=@rno");
        cmd.Parameters.Add("rno", SqlDbType.Int).Value = Convert.ToInt32(ViewState["cod"]);
        cmd.ExecuteNonQuery();
        cmd.Dispose();
    }
    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        Int32 rno, gid, cid, p1, tenth;
        string fname, lname, mtname, ftname, corres;
        DateTime d1;
        Boolean hostler;
        rno = Convert.ToInt32(((TextBox)(DetailsView1.Rows[0].FindControl("TextBox1"))).Text);
        TextBox regno = ((TextBox)(DetailsView1.Rows[1].FindControl("TextBox3")));
        gid = Convert.ToInt32(((DropDownList)(DetailsView1.Rows[7].FindControl("DropDownList7"))).SelectedValue);
        fname = ((TextBox)(DetailsView1.Rows[8].FindControl("TextBox5"))).Text;
        TextBox mname = ((TextBox)(DetailsView1.Rows[9].FindControl("TextBox7")));
        lname = ((TextBox)(DetailsView1.Rows[10].FindControl("TextBox9"))).Text;
        mtname = ((TextBox)(DetailsView1.Rows[11].FindControl("TextBox11"))).Text;
        ftname = ((TextBox)(DetailsView1.Rows[12].FindControl("TextBox13"))).Text;
        d1 = Convert.ToDateTime(((TextBox)(DetailsView1.Rows[13].FindControl("TextBox15"))).Text);
        cid = Convert.ToInt32(((DropDownList)(DetailsView1.Rows[14].FindControl("dropdownlist9"))).SelectedValue);
        corres = ((TextBox)(DetailsView1.Rows[15].FindControl("TextBox17"))).Text;
        TextBox perma = ((TextBox)(DetailsView1.Rows[16].FindControl("TextBox19")));
        p1 = Convert.ToInt32(((TextBox)(DetailsView1.Rows[17].FindControl("TextBox21"))).Text);
        TextBox p2 = ((TextBox)(DetailsView1.Rows[18].FindControl("TextBox23")));
        TextBox email = ((TextBox)(DetailsView1.Rows[19].FindControl("TextBox25")));
        FileUpload photo = ((FileUpload)(DetailsView1.Rows[20].FindControl("FileUpload1")));
        tenth = Convert.ToInt32(((TextBox)(DetailsView1.Rows[21].FindControl("TextBox27"))).Text);
        TextBox twelth = ((TextBox)(DetailsView1.Rows[22].FindControl("TextBox29")));
        TextBox blood = ((TextBox)(DetailsView1.Rows[23].FindControl("TextBox31")));
        TextBox achieve = ((TextBox)(DetailsView1.Rows[24].FindControl("TextBox33")));
        TextBox skill = ((TextBox)(DetailsView1.Rows[25].FindControl("TextBox35")));
        TextBox training = ((TextBox)(DetailsView1.Rows[26].FindControl("TextBox37")));
        TextBox hobbies = ((TextBox)(DetailsView1.Rows[27].FindControl("TextBox39")));
        FileUpload resume = ((FileUpload)(DetailsView1.Rows[28].FindControl("FileUpload3")));
        //hostler = Convert.ToBoolean(((RadioButtonList)(DetailsView1.Rows[29].FindControl("RBLgender"))).SelectedValue);
        CheckBox hostl = ((CheckBox)(DetailsView1.Rows[29].FindControl("CheckBox1")));
        if (hostl.Checked == true)
        {
            hostler = true;
        }
        else
            hostler = false;
        SqlCommand cmd = new SqlCommand("updstudent", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@rno", SqlDbType.Int).Value = rno;
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
        cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
        cmd.Parameters.Add("@p1", SqlDbType.Int).Value = p1;
        if (p2.Text == null || p2.Text == "")
        {
            cmd.Parameters.Add("@p2", SqlDbType.Int).Value = Convert.DBNull;
        }
        else
        {
            cmd.Parameters.Add("@p2", SqlDbType.Int).Value = Convert.ToInt32(p2.Text);
        }
        cmd.Parameters.Add("@tenth", SqlDbType.Int).Value = tenth;
        if (twelth.Text == null || twelth.Text == "")
        {
            cmd.Parameters.Add("@twelth", SqlDbType.Int).Value = Convert.DBNull;
        }
        else
        {
            cmd.Parameters.Add("@twelth", SqlDbType.Int).Value = Convert.ToInt32(twelth.Text);
        }
        if (regno.Text == null)
        {
            cmd.Parameters.Add("@regno", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@regno", SqlDbType.VarChar, 50).Value = regno.Text;
        }
        cmd.Parameters.Add("@fname", SqlDbType.VarChar, 50).Value = fname;
        if (mname.Text == null)
        {
            cmd.Parameters.Add("@mname", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@mname", SqlDbType.VarChar, 50).Value = mname.Text;
        }
        cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50).Value = lname;
        cmd.Parameters.Add("@mtname", SqlDbType.VarChar, 50).Value = mtname;
        cmd.Parameters.Add("@ftname", SqlDbType.VarChar, 50).Value = ftname;
        cmd.Parameters.Add("@corres", SqlDbType.VarChar, 300).Value = corres;
        if (perma.Text == null)
        {
            cmd.Parameters.Add("@perma", SqlDbType.VarChar, 300).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@perma", SqlDbType.VarChar, 300).Value = perma.Text;
        }
        if (photo.FileName == null)
        {
            cmd.Parameters.Add("@photo", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@photo", SqlDbType.VarChar, 100).Value = photo.FileName;
        }
        if (email.Text == null)
        {
            cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = email.Text;
        }
        if (blood.Text == null)
        {
            cmd.Parameters.Add("@blood", SqlDbType.VarChar, 4).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@blood", SqlDbType.VarChar, 4).Value = blood.Text;
        }
        if (achieve.Text == null)
        {
            cmd.Parameters.Add("@achieve", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@achieve", SqlDbType.VarChar, 200).Value = achieve.Text;
        }
        if (skill.Text == null)
        {
            cmd.Parameters.Add("@skill", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@skill", SqlDbType.VarChar, 200).Value = skill.Text;
        }
        if (training.Text == null)
        {
            cmd.Parameters.Add("@training", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@training", SqlDbType.VarChar, 200).Value = training.Text;
        }
        if (hobbies.Text == null)
        {
            cmd.Parameters.Add("@hobbies", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@hobbies", SqlDbType.VarChar, 200).Value = hobbies.Text;
        }
        if (resume.FileName == null)
        {
            cmd.Parameters.Add("@resume", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@resume", SqlDbType.VarChar, 100).Value = resume.FileName;
        }
        //cmd.Parameters.Add("@resume", SqlDbType.VarChar, 100).Value = resume;
        cmd.Parameters.Add("@d1", SqlDbType.DateTime).Value = d1;
        cmd.Parameters.Add("@hostler", SqlDbType.Bit).Value = hostler;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly);
        det_bind();
    }
    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        DetailsView1.ChangeMode(e.NewMode);
        det_bind();
    }

    protected void TextBox41_TextChanged(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand("select count(*) as cnt from tbstudent where studentrollno=@rno", con);
        cmd.Parameters.Add("@rno", SqlDbType.Int).Value = Convert.ToInt32(TextBox41.Text);
        Int32 a1 = Convert.ToInt32(cmd.ExecuteScalar());
        if (a1 == 0)
        {

        }
        else
        {
            det_bind();
        }
        det_bind();
    }
    private void det_bind()
    {
        SqlCommand cmd = new SqlCommand("dispstudent1", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("rno", SqlDbType.Int).Value = Convert.ToInt32(TextBox41.Text);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DetailsView1.DataSource = ds;
        DetailsView1.DataBind();
        DetailsView1.Visible = true;
    }
   
}