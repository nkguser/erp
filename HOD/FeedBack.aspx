﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="FeedBack.aspx.cs" Inherits="HOD_FeedBack" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table >
    <tr>
       
        <td>
            <strong>Subject :  </strong>  </td>
        <td>
            <asp:TextBox ID="TextBox1" runat="server" Height="38px" 
                style="margin-left: 0px" Width="100%" CssClass="unwatermarked"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ControlToValidate="TextBox1" Display="Dynamic" ErrorMessage="*" 
                Font-Bold="True" ForeColor="Red" ValidationGroup="abc"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
             <td>
                 <strong>Detail  :</strong></td>
        <td>
            <cc1:Editor ID="Editor1" runat="server" Height="300px" Width="100%" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                ControlToValidate="Editor1" Display="Dynamic" ErrorMessage="*" Font-Bold="True" 
                ForeColor="Red" ValidationGroup="abc"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        
        <td>
            &nbsp;</td>
        <td>
            <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Send" 
                ValidationGroup="abc" />
            <asp:Label ID="lblfeedback" runat="server" Font-Bold="True" ForeColor="#339933"></asp:Label>
        </td>
        <td>
            &nbsp;</td>
    </tr>
</table>
</asp:Content>

