﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="testopt.aspx.cs" Inherits="HOD_testopt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table style="width: 100%">
        <tr>
            <td colspan="2">
                <h1>
                    Add Options</h1>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h3>
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </h3>
            </td>
        </tr>
        <tr>
            <td style="width: 121px">
                Option Description</td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" Height="66px" TextMode="MultiLine" 
                    Width="473px" CssClass="field2"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 121px">
                Status</td>
            <td>
                <asp:CheckBox ID="CheckBox1" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width: 121px">
                Picture</td>
            <td>
                <asp:FileUpload ID="FileUpload1" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width: 121px">
                &nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Submit" />
&nbsp;&nbsp;
                <asp:Button ID="Button2" runat="server" Text="Cancel" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="p_optcod,p_optpic" DataSourceID="ObjectDataSource1" 
                    Width="594px" onrowdatabound="GridView1_RowDataBound" 
                    onrowdeleting="GridView1_RowDeleting">
                    <Columns>
                        <asp:TemplateField HeaderText="Picture">
                        <ItemTemplate>
                        <asp:Image ID="img1" runat="server" Height="60px" Width="60px" />
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="p_optdsc" HeaderText="Description" 
                            SortExpression="p_optdsc" />
                        <asp:BoundField DataField="p_optsts" HeaderText="Status" 
                            SortExpression="p_optsts" />
                        <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                </asp:GridView>
                <br />
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                    SelectMethod="Display_Rec" TypeName="ot.clsopt">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="qstcod" QueryStringField="qcod" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                <br />
            </td>
        </tr>
    </table>
</asp:Content>

