﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="testrpt.aspx.cs" Inherits="Teacher_testrpt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
  <table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td colspan="2">
                <h1>
                    Test Reports :</h1>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <h3>
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </h3>
            </td>
        </tr>
        <tr>
            <td>
                Choose Test</td>
            <td align=left>
               <asp:DropDownList ID="DDLTest" runat="server" AutoPostBack="True" 
                    DataTextField="tecnam" DataValueField="teccod" 
                        onselectedindexchanged="DropDownList1_SelectedIndexChanged" 
                    DataTextFormatString="{0:D}" Width="200px">
                    </asp:DropDownList>

            </td>
        </tr>
        <tr>
            <td>
                Choose Date</td>
            <td>
               <asp:DropDownList ID="DDLDate" runat="server" AutoPostBack="True" 
                    DataTextField="scrdat" DataValueField="scrdat" 
                        onselectedindexchanged="DropDownList2_SelectedIndexChanged" 
                    DataTextFormatString="{0:D}" Width="200px" Enabled="False">
                    </asp:DropDownList>

                    </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" align=center>
            <asp:Panel ID="p1" runat="server" Height=300px ScrollBars=Auto Visible=false>
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    CellPadding="4"  ForeColor="#333333" 
                    GridLines="None" Width="100%" 
                    EmptyDataText="There are no records to display" ShowHeaderWhenEmpty="True" 
                    DataKeyNames="scrcod" onrowdeleting="GridView1_RowDeleting" >
                     
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                    <asp:TemplateField >
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkAll" runat="server"   />
                        </HeaderTemplate> 
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField> 
                        <asp:BoundField DataField="studentrollno" HeaderText="Roll number" 
                            SortExpression="studentrollno" />
                        <asp:BoundField DataField="name" HeaderText="Name" 
                            SortExpression="name" />                             
                        <asp:BoundField DataField="scrdat" HeaderText="Date" 
                            SortExpression="scrdat" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="scrval" HeaderText="Score" 
                            SortExpression="scrval" />
                     <asp:TemplateField >
                        
                        <ItemTemplate>
                            <asp:LinkButton ID="LBD" runat="server" Text="Delete" CommandName="delete" />
                        </ItemTemplate>
                    </asp:TemplateField> 
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView></asp:Panel>
                
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                    SelectMethod="Display_Rec" TypeName="ot.clstec"></asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
                    SelectMethod="Disp_Rpt" TypeName="ot.clsscr"></asp:ObjectDataSource>
            </td>
        </tr>
    </table>
</asp:Content>
