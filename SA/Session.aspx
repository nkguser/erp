﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SA/InnerSA.master" AutoEventWireup="true" CodeFile="Session.aspx.cs" Inherits="SA_Session" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

                 
    <table class="styledmenu">
      <tr><td>
      
      </td><td align=left>
             <asp:Label ID="Lblmsg" runat="server" Font-Bold="True" Font-Size="Large"></asp:Label>
             </td><td></td></tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                
                    <table class="style2">
                        <tr>
                            <td class="style6" colspan="3">
                                <strong>Register Session</strong></td>
                        </tr>
                        <tr>
                            <td class="style32" nowrap="nowrap">
                                &nbsp;</td>
                            <td class="style9">
                                &nbsp;</td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style32">
                                Session Start Year</td>
                            <td class="style9">
                                <asp:TextBox Width="200px" ID="TxtNewSessionStart" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                    ControlToValidate="TxtNewSessionStart" Display="Dynamic" 
                                    ErrorMessage="RequiredFieldValidator" ForeColor="Red" 
                                    ValidationGroup="ghi">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator4" runat="server" 
                                    ControlToValidate="TxtNewSessionStart" Display="Dynamic" 
                                    ErrorMessage="CompareValidator" ForeColor="Red" Operator="DataTypeCheck" 
                                    Type="Integer" ValidationGroup="ghi">*</asp:CompareValidator>
                            </td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style32">
                                &nbsp;</td>
                            <td class="style9">
                                &nbsp;</td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style33">
                            </td>
                            <td class="style10">
                                <asp:Button ID="BtnRegSession" runat="server" Text="Register" 
                                    onclick="BtnRegSession_Click" ValidationGroup="ghi" />
                            </td>
                        </tr>
                    </table>
                
                 </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
              
                    <table class="style2">
                        <tr>
                            <td class="style6" colspan="3" nowrap="nowrap">
                                <strong>Change Session</strong></td>
                        </tr>
                        <tr>
                            <td class="style6" colspan="3" nowrap="nowrap">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style36" nowrap="nowrap">
                                Session </td>
                            <td>
                                <asp:DropDownList  Width="200px"  ID="DDLUpdateSession" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style37" nowrap="nowrap">
                            </td>
                            <td class="style5">
                                <asp:Button ID="BtnUpdateSession" runat="server" Text="Update" 
                                    onclick="BtnUpdateSession_Click" ValidationGroup="abc" />
                                &nbsp;</td>
                            <td class="style5">
                                &nbsp;</td>
                        </tr>
                    </table>
                
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:GridView ID="GridShowSummary" runat="server" AutoGenerateColumns="False" 
                                        DataKeyNames="classid" GridLines="None" 
                                        
                    ShowHeader="False" Width="320px" Visible="False" 
                                         >
                                        <Columns>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="Lblcursem" runat="server" Font-Bold="True" 
                                                        Text='<%# Eval("classcursem") %>'></asp:Label>
                                                    
                                                </ItemTemplate>
                                                <ItemStyle Wrap="False"  />
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False" 
                                                >
                                                <ItemTemplate>
                                                    <asp:Label ID="Lblsessionstart" Text='<%# Eval("classsessionstart") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField  ShowHeader="False" 
                                                >
                                                <ItemTemplate>
                                                    <asp:Label ID="Lblsessionend" Text='<%# Eval("classsessionend") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            
                                        </Columns>
                                    </asp:GridView>
                </td>
            <td>
                &nbsp;</td>
        </tr>
        </table>

                 
</asp:Content>

