﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class HOD_testqst : System.Web.UI.Page
{
    protected void Button1_Click(object sender, EventArgs e)
    {
        ot.clsqst obj = new ot.clsqst();
        ot.clsqstprp objprp = new ot.clsqstprp();
        objprp.p_qstdsc = Editor1.Content;
        objprp.p_qstlvl = 'B';
        objprp.p_qstteccod = Convert.ToInt32(DropDownList1.SelectedValue);
        String s = FileUpload1.FileName;
        if (s != "")
        {
            s = s.Substring(s.LastIndexOf('.'));
        }
        if (Button1.Text == "Submit")
        {
            objprp.p_qstpic = s;
            Int32 a = obj.Save_Rec(objprp);
            if (s != "")
            {
                FileUpload1.PostedFile.SaveAs(Server.MapPath("~/qstpics") + "//" + a.ToString() + s);
            }
        }
        else
        {
            objprp.p_qstcod = Convert.ToInt32(ViewState["cod"]);
            if (s == "")
                objprp.p_qstpic = ViewState["pic"].ToString();
            else
            {
                objprp.p_qstpic = s;
                FileUpload1.PostedFile.SaveAs(Server.MapPath("~/qstpics") + "//" + ViewState["cod"].ToString() + s);
            }
            obj.Update_Rec(objprp);
            Button1.Text = "Submit";
        }
        Editor1.Content = String.Empty;
        GridView1.DataBind();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Int32 a = Convert.ToInt32(GridView1.DataKeys[e.Row.RowIndex][0]);
            String s = GridView1.DataKeys[e.Row.RowIndex][1].ToString();
            Image img = (Image)(e.Row.Cells[0].FindControl("img1"));
            if (s != "")
                img.ImageUrl = "~/qstpics/" + a.ToString() + s;
            else
                img.Visible = false;
        }
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        ot.clsqst obj = new ot.clsqst();
        ot.clsqstprp objprp = new ot.clsqstprp();
        objprp.p_qstcod = Convert.ToInt32(GridView1.DataKeys[e.RowIndex][0]);
        obj.Delete_Rec(objprp);
        GridView1.DataBind();
        e.Cancel = true;
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        Int32 a = Convert.ToInt32(GridView1.DataKeys[e.RowIndex][0]);
        Response.Redirect("testopt.aspx?qcod=" + a.ToString());
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Int32 a = Convert.ToInt32(GridView1.DataKeys[e.NewEditIndex][0]);
        ViewState["cod"] = a;
        String s = GridView1.DataKeys[e.NewEditIndex][1].ToString();
        ViewState["pic"] = s;
        ot.clsqst obj = new ot.clsqst();
        List<ot.clsqstprp> k = obj.Find_Rec(a);
        if (k.Count > 0)
        {
            Editor1.Content = k[0].p_qstdsc;
        }
        Button1.Text = "Update";
        e.Cancel = true;
    } 
}