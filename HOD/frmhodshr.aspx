﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="frmhodshr.aspx.cs" Inherits="HOD_frmhodshr" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <h2>
        Share Album</h2>
    <table class="styledmenu">
        <tr>
            <td>
                Send To:</td>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="LevelNotice" 
                    DataTextField="leveldesc" DataValueField="levelid">
                </asp:DropDownList>
                <asp:SqlDataSource ID="LevelNotice" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:cn %>" 
                    SelectCommand="SELECT [levelid], [leveldesc] FROM [tblevel] WHERE ([leveldelsts] = @leveldelsts)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="false" Name="leveldelsts" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td colspan="2">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
            DataSourceID="ObjectDataSource1" Width="742px" DataKeyNames="depid">
            <Columns>
                <asp:TemplateField>
                <ItemTemplate>
                <asp:CheckBox ID="ck1" runat=server />
                </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="depname" HeaderText="Department" 
                    SortExpression="depname" />
            </Columns>
        </asp:GridView>
            </td>
        </tr>
    </table>
    <p>
        <asp:Button ID="Button1" runat="server" Text="Share" Width="146px" 
            onclick="Button1_Click" />
    </p>
    <p>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            SelectMethod="AllDepartment1" TypeName="kitmerp.ERP">
        </asp:ObjectDataSource>
    </p>
</asp:Content>

