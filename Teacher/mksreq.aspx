﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="mksreq.aspx.cs" Inherits="Teacher_mksreq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Label ID="l1" runat="server" ForeColor="Red"  Visible="false"/><br />
    <table width="100%" style="text-align: left; font-weight: bold;">
    
            <tr>
                <td style="width: 185px">
                
                    <strong>Roll no:
                </strong>
                </td>
                <td style="margin-left: 40px">
                    <asp:TextBox ID="TextBox1" runat="server" Width="300px"></asp:TextBox><asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToValidate="TextBox1" Operator="DataTypeCheck" Display="Dynamic" 
                        ErrorMessage="*" Font-Bold="True" ForeColor="Red" Type="Integer">*</asp:CompareValidator>
                &nbsp;<asp:Button ID="Button2" runat="server" onclick="Button2_Click" 
                        Text="Continue" style="font-weight: 700" Width="100px" />
                </td>
            </tr>

            </table>
             <asp:Panel ID="Panel1" runat="server" Visible="false">
       
            <table width="100%" style="text-align: left; font-weight: bold;">
            <tr>
                <td>
                    <strong>Sessional</strong></td>
                <td style="margin-left: 40px">
                    <asp:DropDownList ID="DropDownList2" runat="server" 
                        DataTextFormatString="{0:d}" Width="300px">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Subject</strong></td>
                <td style="margin-left: 40px">
                    <asp:DropDownList ID="DropDownList1" runat="server" Width="300px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Reason</strong></td>
                <td style="margin-left: 40px">
                    <asp:TextBox ID="TextBox2" runat="server" Height="53px" TextMode="MultiLine" 
                        Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Marks</strong></td>
                <td style="margin-left: 40px">
                    &nbsp;<asp:TextBox ID="TextBox3" runat="server" Width="300px"></asp:TextBox>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Attendance</strong>&nbsp;&nbsp; </td>
                <td align="left" style="margin-left: 40px">
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                        RepeatDirection="Horizontal" style="font-weight: 700">
                        <asp:ListItem Selected="True" Value="True">Present</asp:ListItem>
                        <asp:ListItem Value="False">Absent</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td style="margin-left: 40px">
                    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                        style="font-weight: 700" Text="Submit Request" Height="39px" 
                        Width="151px" />
                </td>
            </tr>
        </table>
    
    </asp:Panel>
</asp:Content>

