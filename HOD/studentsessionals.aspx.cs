﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class HOD_studentsessionals : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
    
    }
    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RadioButtonList1.SelectedIndex == 0)
        {
            fillpanel.Visible = true;
            viewpanel.Visible = false;
            DDLSessionfBind();
            datemarks.Visible = false;
            fillmarkpanel.Visible = false;
            TxtDate.Text = string.Empty;
            TXTMaxMarks.Text = string.Empty;
            lblfillmarksmsg.Text = string.Empty;
            DDLClassf.Enabled = false;
            DDLClassf.Items.Clear();
            DDLClassf.Items.Insert(0, "-- Select Class --");
            DDLSubjectf.Enabled = false;
            DDLSubjectf.Items.Clear();
            DDLSubjectf.Items.Insert(0, "-- Select Subject --");
            
        }
        else
        {
            
            fillpanel.Visible = false;
            viewpanel.Visible = true;
            DDLSessionBind();
            DDLSessional.Enabled = false;
            DDLSessional.SelectedIndex = 0;
            DDLClass.Items.Clear();
            DDLSubject.Items.Clear();
            LBLDate.Text = string.Empty;
            LBLMaxMarks.Text = string.Empty;
            Lblviewmsg.Text = string.Empty;
            DDLClass.Enabled = false;
            DDLSubject.Enabled = false;
            DDLClass.Items.Insert(0, "--Select Class--");
            DDLSubject.Items.Insert(0, "--Select Subject--");
            viewmarkpanel.Visible = false;
            submittedmarkpanel.Visible = false;
        }
    }
    private void DDLSessionfBind()
    {
        DDLSessionf.DataTextField = "session";
        DDLSessionf.DataValueField = "sessionid";
        DDLSessionf.DataSource = ERP.AllSession();
        DDLSessionf.DataBind();
        DDLSessionf.Items.Insert(0, "-- Select session --");
    }
    protected void DDLSessionf_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(DDLSessionf.SelectedIndex==0)
        {
            datemarks.Visible = false;
            fillmarkpanel.Visible = false;
            TxtDate.Text = string.Empty;
            TXTMaxMarks.Text = string.Empty;
            lblfillmarksmsg.Text = string.Empty;
            DDLClassf.Enabled = false;
            DDLClassf.Items.Clear();
            DDLClassf.Items.Insert(0, "-- Select Class --");
            DDLSubjectf.Enabled = false;
            DDLSubjectf.Items.Clear();
            DDLSubjectf.Items.Insert(0, "-- Select Subject --");
        }
        else
        {
            datemarks.Visible = false;
            fillmarkpanel.Visible = false;
            TxtDate.Text = string.Empty;
            TXTMaxMarks.Text = string.Empty;
            lblfillmarksmsg.Text = string.Empty;
            DDLClassf.Enabled = false;
            DDLClassf.Items.Clear();
            DDLClassf.Items.Insert(0, "-- Select Class --");
            DDLSubjectf.Enabled = false;
            DDLSubjectf.Items.Clear();
            DDLSubjectf.Items.Insert(0, "-- Select Subject --");
            DDLClassf.DataTextField = "Class";
            DDLClassf.DataValueField = "subjectallotgroupid";
            DDLClassf.DataSource = ERP.TeacherClass(Convert.ToInt32(Session["stid"]), Convert.ToInt32(DDLSessionf.SelectedValue));
            DDLClassf.DataBind();
            DDLClassf.Enabled = true;
            //DDLSubject.Enabled = true;
            DDLClassf.Items.Insert(0, "-- Select Class --");
        }
    }
    protected void DDLClassf_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLClassf.SelectedIndex == 0)
        {
            datemarks.Visible = false;
            fillmarkpanel.Visible = false;
            TxtDate.Text = string.Empty;
            TXTMaxMarks.Text = string.Empty;
            lblfillmarksmsg.Text = string.Empty;
            DDLSubjectf.Enabled = false;
            DDLSubjectf.Items.Clear();
            DDLSubjectf.Items.Insert(0,"-- Select Subject --");
        }
        else
        {
            datemarks.Visible = false;
            fillmarkpanel.Visible = false;
            TxtDate.Text = string.Empty;
            TXTMaxMarks.Text = string.Empty;
            DDLSubjectf.Enabled = true;
            lblfillmarksmsg.Text = string.Empty;
            DDLSubjectf.DataTextField = "subject";
            DDLSubjectf.DataValueField = "subjectallotsubjectid";
            DDLSubjectf.DataSource = ERP.TeacherSubject(Convert.ToInt32(Session["stid"]), Convert.ToInt32(DDLClassf.SelectedValue));
            DDLSubjectf.DataBind();
            DDLSubjectf.Items.Insert(0, "-- Select Subject --");
            GridFillBind();
        }
    }
    private void GridFillBind()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT studentid,studentrollno FROM tbStudent where studentgroupid=@gid", con);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClassf.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridFill.DataSource = ds;
        GridFill.DataBind();
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        Application.Lock();
        Int32 c = GridFill.Rows.Count;
            for (Int32 i = 0; i < c; i++)
            {
                GridFill.SelectRow(i);
                Int32 l = Convert.ToInt32(GridFill.SelectedDataKey.Value);
                Boolean b = Convert.ToBoolean(((RadioButtonList)(GridFill.SelectedRow.FindControl("RBLAttendance"))).SelectedValue);
                Decimal d = Convert.ToDecimal(((TextBox)(GridFill.SelectedRow.FindControl("TxtMarks"))).Text);
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlCommand cmd = new SqlCommand("INSERT INTO tbstudentsessional (studentsessionalstudentid, studentsessionalsubjectid, studentsessionalno, studentsessionaldate, studentsessionalattnd, studentsessionalmarks, studentsessionalmmarks, studentsessionalreqlvlid) VALUES (@rid,@sid,@sesn_no,@date,@at,@m,@mm, 2)", con);
                cmd.Parameters.Add("@rid", SqlDbType.Int).Value = l;
                cmd.Parameters.Add("@sid", SqlDbType.Int).Value = DDLSubjectf.SelectedValue;
                DateTime sdate = Convert.ToDateTime(TxtDate.Text);
                cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(sdate.ToShortDateString());
                cmd.Parameters.Add("@sesn_no", SqlDbType.Int).Value = Convert.ToInt32(DDLSessionalf.SelectedValue);
                cmd.Parameters.Add("@at", SqlDbType.Bit).Value = b;
                cmd.Parameters.Add("@m", SqlDbType.Decimal).Value = d;
                cmd.Parameters.Add("@mm", SqlDbType.Int).Value = Convert.ToInt32(TXTMaxMarks.Text);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
            }
            Application.UnLock();
            if (Convert.ToInt32(DDLSessionalf.SelectedValue) == 3)
            {
                Response.Redirect("~/hod/studentsessionals.aspx");
                lblfillmarksmsg.Text = Convert.ToInt32(DDLSessionalf.SelectedValue) + " Sessional Marks has been Submitted Successfully!!! ";
            }
            else
            {
                lblfillmarksmsg.Text = Convert.ToInt32(DDLSessionalf.SelectedValue) + " Sessional Marks has been Submitted Successfully!!! ";
                fillmarkpanel.Visible = false;
                datemarks.Visible = false;
                DDLSubjectf.SelectedIndex = 0;
               
            }
        
    }
    private Int32 SessionalNo()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SessionalNot", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClassf.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubjectf.SelectedValue);
        //cmd.Parameters.Add("@s1", SqlDbType.Int).Direction = ParameterDirection.Output;
        //cmd.Parameters.Add("@s2", SqlDbType.Int).Direction = ParameterDirection.Output;
        cmd.Parameters.Add("@s", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        //cmd.Parameters.Add("@s1", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        //cmd.Parameters.Add("@s2", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.ExecuteScalar();
        Int32 k = Convert.ToInt32(cmd.Parameters["@s"].Value);
        //Int32 k1=Convert.ToInt32(cmd.Parameters["@s1"].Value);
        //Int32 k2 = Convert.ToInt32(cmd.Parameters["@s2"].Value);
        cmd.Dispose();
        con.Close();
        return k;
    }
    private Int32 SessionalNoHOD()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SessionalNoHodsubmit", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClassf.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubjectf.SelectedValue);
        cmd.Parameters.Add("@s", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.ExecuteScalar();
        Int32 k = Convert.ToInt32(cmd.Parameters["@s"].Value);
        cmd.Dispose();
        con.Close();
        return k;
    }
    protected void DDLSubjectf_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(DDLSubjectf.SelectedIndex==0)
        {
            datemarks.Visible = false;
            fillmarkpanel.Visible = false;
            TxtDate.Text = string.Empty;
            TXTMaxMarks.Text = string.Empty;
            lblfillmarksmsg.Text = string.Empty;
        }
        else
        {
            //There is Problem in getting sessionalNo. if we choose directly 3rd sessional to fill it 
            //instead of 1 and 2 it does not show the 1 and 2 in ddl again when we choose sessional no.
            DDLSessionalf.Items.Clear();
            //for (Int32 i = SessionalNo() + 1; i <= 3; i++)
            //{
            Int32 i = SessionalNo() + 1;
            if(i<=3)
            {
            DDLSessionalf.Items.Add(i.ToString());
            }
            //}
            //Int32 m = SessionalNo();
            if (DDLSessionalf.Text == string.Empty)
            {
                lblfillmarksmsg.Text = "All Sessional Marks have been filled by you!!!";
                fillmarkpanel.Visible = false;
                datemarks.Visible = false;
            }
            else
            {
                GridFillBind();
                if (GridFill.Rows.Count == 0)
                {
                    lblfillmarksmsg.Text = "There are no records for submission of marks..";
                }
                else
                {
                    fillmarkpanel.Visible = true;
                    DDLSessionalf.Enabled = true;
                    GridFillBind();
                    GridFill.Visible = true;
                    LinkButton2.Visible = true;
                    datemarks.Visible = true;
                    lblfillmarksmsg.Text = string.Empty;
                }
            }
    }    
    }
    private void DDLSessionBind()
    {
        DDLSession.DataTextField = "session";
        DDLSession.DataValueField = "sessionid";
        DDLSession.DataSource = ERP.AllSession();
        DDLSession.DataBind();
        DDLSession.Items.Insert(0, "-- Select session --");
    }
    protected void DDLSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSession.SelectedIndex == 0)
        {
            DDLSessional.Enabled = false;
            DDLSessional.SelectedIndex = 0;
            DDLClass.Items.Clear();
            DDLSubject.Items.Clear();
            LBLDate.Text = string.Empty;
            LBLMaxMarks.Text = string.Empty;
            Lblviewmsg.Text = string.Empty;
            DDLClass.Enabled = false;
            DDLSubject.Enabled = false;
            DDLClass.Items.Insert(0, "--Select Class--");
            DDLSubject.Items.Insert(0, "--Select Subject--");
            viewmarkpanel.Visible = false;
            submittedmarkpanel.Visible = false;
        }
        else
        {
            submittedmarkpanel.Visible = false;
            viewmarkpanel.Visible = false;
            Lblviewmsg.Text = string.Empty;
            viewmarkpanel.Visible = false;
            DDLSessional.Enabled = false;
            DDLSessional.SelectedIndex = 0;
            DDLClass.Items.Clear();
            DDLSubject.Items.Clear();
            LBLDate.Text = string.Empty;
            LBLMaxMarks.Text = string.Empty;
            DDLClass.Items.Insert(0, "--Select Class--");
            DDLSubject.Items.Insert(0, "--Select Subject--");
            DDLSubject.Enabled = false;
            classbind();
        }

    }
    private void classbind()
    {
        DDLClass.DataTextField = "Class";
        DDLClass.DataValueField = "subjectallotgroupid";
        DDLClass.DataSource = ERP.AllClassHOD(Convert.ToInt32(Session["depid"]), Convert.ToInt32(DDLSession.SelectedValue), Convert.ToInt32(Session["stid"]));
        DDLClass.DataBind();
        DDLClass.Enabled = true;
        DDLClass.Items.Insert(0, "-- Select Class --");
    }
    protected void DDLClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLClass.SelectedIndex == 0)
        {
            DDLSubject.Items.Clear();
            LBLDate.Text = string.Empty;
            LBLMaxMarks.Text = string.Empty;
            DDLSubject.Enabled = false;
            DDLSessional.Enabled = false;
            DDLSubject.Items.Insert(0, "--Select Subject--");
            DDLSessional.SelectedIndex = 0;
            Lblviewmsg.Text = string.Empty;
            viewmarkpanel.Visible = false;
            submittedmarkpanel.Visible = false;
        }
        else
        {
            submittedmarkpanel.Visible = false;
            viewmarkpanel.Visible = false;
            Lblviewmsg.Text = string.Empty;
            DDLSessional.SelectedIndex = 0;
            DDLSessional.Enabled = false;
            DDLSubject.Items.Clear();
            LBLDate.Text = string.Empty;
            LBLMaxMarks.Text = string.Empty;
            DDLSubject.Enabled = true;
            DDLSubject.Items.Insert(0, "--Select Subject--");
            subjectbind();
        }
    }
    private void subjectbind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        //SqlCommand cmd = new SqlCommand("SELECT tbSubject.Subjectid, tbSubject.Subjecttitle + ' (' + tbSubjectype.SubjectTypename + ')' AS subject FROM tbSubject INNER JOIN tbSubjectype ON tbSubject.Subjecttypeid = tbSubjectype.SubjectTypeid WHERE (Subjectdepid = @did) AND (tbsubject.Subjectdelsts = 0) AND (tbsubjectype.Subjectdelsts = 0)", con);
        //SqlCommand cmd = new SqlCommand("SELECT  tbcourse.coursename + ' (' + tbdep.depname + ') -' + tbsubject.subjecttitle AS subject, tbsubject.subjectid FROM            tbsubject INNER JOIN tbsubjectallot ON tbsubject.subjectid = tbsubjectallot.subjectallotsubjectid INNER JOIN tbdep ON tbsubject.subjectdepid = tbdep.depid INNER JOIN tbcourse ON tbdep.depcourseid = tbcourse.courseid WHERE        (tbsubject.subjectdepid <> @did) AND (tbsubjectallot.subjectallotteacherid = @tid) ORDER BY tbsubject.subjectdepid", con);
        SqlCommand cmdsubject = new SqlCommand("dispsubjecthod", con);
        cmdsubject.CommandType = CommandType.StoredProcedure;
        cmdsubject.Parameters.Add("@did", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        cmdsubject.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        cmdsubject.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataReader dr = cmdsubject.ExecuteReader();
        DDLSubject.DataTextField = "subject";
        DDLSubject.DataValueField = "Subjectid";
        DDLSubject.DataSource = dr;
        DDLSubject.DataBind();
        DDLSubject.Items.Insert(0, "-- Select Subject --");
        DDLSubject.Items[0].Attributes.Add("disabled", "disabled");
        dr.Close();
        cmdsubject.Dispose();
        con.Close();
    }
    protected void DDLSubject_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSubject.SelectedIndex == 0)
        {
            DDLSessional.Enabled = false;
            DDLSessional.SelectedIndex = 0;
            LBLDate.Text = string.Empty;
            LBLMaxMarks.Text = string.Empty;
            viewmarkpanel.Visible = false;
            Lblviewmsg.Text = string.Empty;
            submittedmarkpanel.Visible = false;
        }
        else
        {
            Lblviewmsg.Text = string.Empty;
            viewmarkpanel.Visible = false;
            DDLSessional.Enabled = true;
            DDLSessional.SelectedIndex = 0;
            LBLDate.Text = string.Empty;
            LBLMaxMarks.Text = string.Empty;
            DDLSessional.Enabled = true;
            submittedmarkpanel.Visible = false;
        }

    }
    protected void DDLSessional_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 sid;
        //DDLSessional.Items.Clear();
        //for (Int32 i = 1; i <= SessionalNo(); i++)
        //{
        //    DDLSessional.Items.Add(i.ToString());
        //}
        if (DDLSessional.SelectedIndex == 0)
        {
            //DDLSessional.Enabled = true;
            LBLMaxMarks.Text = string.Empty;
            LBLDate.Text = string.Empty;
            Lblviewmsg.Text = string.Empty;
            viewmarkpanel.Visible = false;
            submittedmarkpanel.Visible = false;
        }
        else
        {

            LBLMaxMarks.Text = string.Empty;
            LBLDate.Text = string.Empty;
            Lblviewmsg.Text = string.Empty;
            viewmarkpanel.Visible = false;
            submittedmarkpanel.Visible = false;
            Int32 sesno = Convert.ToInt32(DDLSessional.SelectedValue);
            Int32 sbid = Convert.ToInt32(DDLSubject.SelectedValue);
            Int32 gid = Convert.ToInt32(DDLClass.SelectedValue);
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("SELECT  top 1  studentid FROM tbstudent WHERE (studentgroupid =@gid )", con);
            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                sid = Convert.ToInt32(dr["studentid"]);
                SqlCommand cmdcheck = new SqlCommand("SELECT COUNT(*) AS cnt FROM tbstudentsessional WHERE (studentsessionalsubjectid = @sbid) AND (studentsessionalstudentid = @sid) AND (studentsessionalno = @sesno)", con);
                cmdcheck.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
                cmdcheck.Parameters.Add("@sbid", SqlDbType.Int).Value = sbid;
                cmdcheck.Parameters.Add("@sesno", SqlDbType.Int).Value = sesno;
                dr.Close();
                Int32 check = Convert.ToInt32(cmdcheck.ExecuteScalar());
                if (check == 0)
                {
                    Lblviewmsg.Text = sesno + " Sessional Marks not yet filled ";
                }
                else
                {
                    SqlCommand submitcheck = new SqlCommand("SELECT COUNT(*) AS cnt FROM tbstudentsessional WHERE (studentsessionalsubjectid = @sbid) AND (studentsessionalstudentid = @sid) AND (studentsessionalno = @sesno) AND (studentsessionalreqlvlid = 3) AND  (studentsessionaldelsts = 0)", con);
                    submitcheck.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
                    submitcheck.Parameters.Add("@sbid", SqlDbType.Int).Value = sbid;
                    submitcheck.Parameters.Add("@sesno", SqlDbType.Int).Value = sesno;
                    Int32 subcheck = Convert.ToInt32(submitcheck.ExecuteScalar());
                    if (subcheck == 0)
                    {
                        GridShowBind();
                        viewmarkpanel.Visible = true;
                        submittedmarkpanel.Visible = false;
                        if (GridShow.Rows.Count != 0)
                            LnkBtnSubmitHod.Visible = true;
                        else
                        {
                            submittedmarkpanel.Visible = false;
                            LBLMaxMarks.Text = string.Empty;
                            LBLDate.Text = string.Empty;
                            viewmarkpanel.Visible = true;
                            GridShow.Visible = false;
                            LnkBtnSubmitHod.Visible = false;
                            Lblviewmsg.Text = sesno + " Sessional Marks has been Submit";
                        }
                    }
                    else
                    {
                        viewmarkpanel.Visible = false;
                        GridShowSubmitBind();
                        GridshowSubmit.Visible = true;
                        submittedmarkpanel.Visible = true;
                        Lblviewmsg.Text = sesno + " Sessional Marks already been Submitted";
                        if (GridshowSubmit.Rows.Count != 0)

                            GridshowSubmit.Visible = true;
                        else
                        {
                            viewmarkpanel.Visible = false;
                            LBLDate.Text = string.Empty;
                            LBLMaxMarks.Text = string.Empty;
                            Lblviewmsg.Text = "No records ";
                            GridshowSubmit.Visible = false;
                            submittedmarkpanel.Visible = false;
                        }
                    }

                }
            }
            else
            {
                Lblviewmsg.Text = "There are no records exists for Class " + DDLClass.SelectedItem;
            }
            cmd.Dispose();
        }

    }
    private void GridShowSubmitBind()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT     tbstudentsessional.studentsessionaldate, tbstudentsessional.studentsessionalmmarks, tbstudent.studentid FROM tbstudentsessional INNER JOIN                 tbstudent ON tbstudentsessional.studentsessionalstudentid = tbstudent.studentid INNER JOIN tbgroup ON tbstudent.studentgroupid = tbgroup.groupid WHERE (tbstudentsessional.studentsessionalno = @sno) AND (tbstudentsessional.studentsessionalsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND    (tbstudentsessional.studentsessionaldelsts = 0) AND (tbgroup.groupdelsts = 0) AND (tbstudent.studentdelsts = 0) AND (tbstudentsessional.studentsessionalreqlvlid = 3)", con);
        cmd.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DDLSessional.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            LBLDate.Text = Convert.ToDateTime(dr["studentsessionaldate"]).ToShortDateString();
            LBLMaxMarks.Text = dr["studentsessionalmmarks"].ToString();
        }
        cmd.Dispose();
        dr.Close();
        con.Close();
        GridshowSubmit.DataSource = ERP.DispSessionalDIR(Convert.ToInt32(DDLSessional.SelectedValue), Convert.ToInt32(DDLSubject.SelectedValue), Convert.ToInt32(DDLClass.SelectedValue));
        GridshowSubmit.DataBind();
    }
    private void GridShowBind()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT tbstudentsessional.studentsessionaldate, tbstudentsessional.studentsessionalmmarks, tbstudent.studentid FROM         tbstudentsessional INNER JOIN                 tbstudent ON tbstudentsessional.studentsessionalstudentid = tbstudent.studentid INNER JOIN                      tbgroup ON tbstudent.studentgroupid = tbgroup.groupid WHERE     (tbstudentsessional.studentsessionalno = @sno) AND (tbstudentsessional.studentsessionalsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND                       (tbstudentsessional.studentsessionaldelsts = 0) AND (tbgroup.groupdelsts = 0) AND (tbstudent.studentdelsts = 0) AND (tbstudentsessional.studentsessionalreqlvlid = 2)", con);
        cmd.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DDLSessional.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            LBLDate.Text = Convert.ToDateTime(dr["studentsessionaldate"]).ToShortDateString();
            LBLMaxMarks.Text = dr["studentsessionalmmarks"].ToString();
        }
        cmd.Dispose();
        dr.Close();
        con.Close();
        GridShow.DataSource = ERP.DispSessionalHOD(Convert.ToInt32(DDLSessional.SelectedValue), Convert.ToInt32(DDLSubject.SelectedValue), Convert.ToInt32(DDLClass.SelectedValue));
        GridShow.DataBind();
    }
    protected void GridShow_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridShow.EditIndex = e.NewEditIndex;
        GridShowBind();
    }
    protected void GridShow_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        Int32 rno = Convert.ToInt32(GridShow.DataKeys[e.RowIndex].Value);
        Boolean at = Convert.ToBoolean(((RadioButtonList)(GridShow.Rows[e.RowIndex].FindControl("RBLAttendance"))).SelectedValue);
        Decimal mks = Convert.ToDecimal(((TextBox)(GridShow.Rows[e.RowIndex].FindControl("TxtMarks"))).Text);
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("UPDATE tbstudentSessional SET studentSessionalattnd =@a, studentsessionalMarks =@m where studentsessionalstudentid=@rid and studentsessionalsubjectid=@sid and studentsessionaldate=@d and studentsessionalno=@sno and studentsessionaldelsts=0 and studentsessionalreqlvlid=2", con);
        cmd.Parameters.Add("@a", SqlDbType.Bit).Value = at;
        cmd.Parameters.Add("@m", SqlDbType.Decimal).Value = mks;
        cmd.Parameters.Add("@rid", SqlDbType.Int).Value = rno;
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@d", SqlDbType.Date).Value = Convert.ToDateTime(LBLDate.Text);
        cmd.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DDLSessional.SelectedValue);
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        GridShow.EditIndex = -1;
        GridShowBind();
    }
    protected void GridShow_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridShow.EditIndex = -1;
        GridShowBind();
    }
    protected void GridShow_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType==DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Edit)
        //{
        //    GridViewRow r=e.Row;
        //    RadioButtonList rd = (RadioButtonList)(e.Row.FindControl("RBLAttendance"));
        //    rd.Items.FindByValue(e.Row.["Sessional_Attendance"]).Selected = true;

        //}
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            //Label l = (Label)(GridShow.Rows[e.Row.RowIndex].FindControl("LBLAttend"));
            Label l = (Label)(e.Row.FindControl("LBLAttend"));
            if (l.Text.Equals("True"))
                l.Text = "Present";
            else
                l.Text = "Absent";
        }
    }
    protected void LnkBtnSubmitHod_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("UPDATE    tbstudentsessional SET studentsessionalreqlvlid =3 FROM  tbstudentsessional INNER JOIN tbstudent ON tbstudentsessional.studentsessionalstudentid = tbstudent.studentid WHERE   (tbstudent.studentgroupid = @gid) and   (studentsessionaldate = @date) AND (studentsessionalno = @sno) AND (studentsessionalsubjectid = @sid) AND (studentsessionaldelsts = 0) and studentsessionalreqlvlid=2", con);
        cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(LBLDate.Text);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DDLSessional.SelectedValue);
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        DDLSessional.SelectedIndex = 0;
        LBLDate.Text = string.Empty;
        LBLMaxMarks.Text = string.Empty;
        Lblviewmsg.Text = "Marks has been Submitted Successfully";
        viewmarkpanel.Visible = false;
    }
}