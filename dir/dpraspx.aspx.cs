﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class dir_dpraspx : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    static Int32 did;
    string lbld;
    protected void Page_Load(object sender, EventArgs e)
    {
        GridShowSubmit.Visible = false;
        if (Page.IsPostBack == false)
        {
            DDLSessionBind();
        }
    }

    private void DDLSessionBind()
    {
        DDLSession.DataTextField = "session";
        DDLSession.DataValueField = "sessionid";
        DDLSession.DataSource = ERP.AllSession();
        DDLSession.DataBind();
        DDLSession.Items.Insert(0, "-- Select session --");
    }
    protected void DDLSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(DDLSession.SelectedIndex==0)
        {
            DDLClass.Enabled = false;
            DDLClass.Items.Clear();
            DDLClass.Items.Insert(0,"-- Select Class --");
            p1.Visible = false;
            GridShowSubmit.Visible = false;
            lbdate.Visible = false;

        }
        else
        {
            DDLClass.Enabled = false;
            DDLClass.Items.Clear();
            DDLClass.Items.Insert(0, "-- Select Class --");
            p1.Visible = false;
            lbdate.Visible = false;
            GridShowSubmit.Visible = false;
            DDLAllClassBind();
        }
    }
    private void DDLAllClassBind()
    {
        DDLClass.DataTextField = "Class";
        DDLClass.DataValueField = "groupid";
        DDLClass.DataSource = ERP.AllClassDIR(Convert.ToInt32(Convert.ToInt32(DDLSession.SelectedValue)));
        DDLClass.DataBind();
        DDLClass.Enabled = true;
        DDLClass.Items.Insert(0, "-- Select Class --");
    }
    protected void DDLClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLClass.SelectedIndex == 0)
        {
            p1.Visible = false;
            GridShowSubmit.Visible = false;
            lbdate.Visible = false;

        }
        else
        {
            p1.Visible = false;
            GridShowSubmit.Visible = false;
            lbdate.Visible = true;
            GridShowSubmitBind();
        }
        
    }
    private void GridShowSubmitBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT tbsubject.subjectid,  tbsubject.subjecttitle+' ('+tbsubjectype.subjecttypename+')' as subject, tbsubjectallot.subjectallotgroupid FROM  tbsubjectallot INNER JOIN tbsubject ON tbsubjectallot.subjectallotsubjectid = tbsubject.subjectid INNER JOIN tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid WHERE (tbsubjectallot.subjectallotgroupid = @gid) AND (tbsubject.subjectsem = @semid)", con);
        //cmd.Parameters.Add("@date", SqlDbType.Date).Value =Convert.ToDateTime("04/04/2012");// System.DateTime.Today;
        //cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.Parameters.Add("@semid", SqlDbType.Int).Value = 6;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShowSubmit.DataSource = ds;
        GridShowSubmit.DataBind();
        GridShowSubmit.Visible = true;
        cmd.Dispose();
        con.Close();
    }
    protected void GridShowSubmit_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       
        string s= "" + System.DateTime.Today.Day + "/" + System.DateTime.Today.Month + "/" + System.DateTime.Today.Year + ""; 
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) AS cnt FROM  (SELECT  tbstudentattnd.studentattndstudentid FROM  tbstudentattnd INNER JOIN tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudentattnd.studentattnddate = @did) AND  (tbstudentattnd.studentattndattend = 1)) AS derivedtbl_1", con);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSubmit.DataKeys[e.Row.RowIndex].Value);
            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
            if (TextBox1.Text == "" || TextBox1.Text == string.Empty || TextBox1.Text == null)
            {
                cmd.Parameters.Add("@did", SqlDbType.Date).Value = System.DateTime.Today;//Convert.ToDateTime(s); 
            }
            else
            {
                cmd.Parameters.Add("@did", SqlDbType.Date).Value = Convert.ToDateTime(TextBox1.Text);
            }
            Label l = (Label)(e.Row.FindControl("LBLD"));
            Label l1 = (Label)(e.Row.FindControl("LBLS"));
            Label l2 = (Label)(e.Row.FindControl("LBLAP"));
            Label l3 = (Label)(e.Row.FindControl("LBLP"));
            Label l4 = (Label)(e.Row.FindControl("LBLT"));
            l2.Text = cmd.ExecuteScalar().ToString();
            SqlCommand cmd1 = new SqlCommand("SELECT COUNT(DISTINCT tbstudentattnd.studentattnddate) AS count FROM  tbstudentattnd INNER JOIN tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE        (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0)", con);
            cmd1.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
            cmd1.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSubmit.DataKeys[e.Row.RowIndex].Value);
            l.Text = cmd1.ExecuteScalar().ToString();
            cmd1.Dispose();
            SqlCommand cmd2 = new SqlCommand("SELECT COUNT(*) AS count FROM  tbstudent where studentgroupid=@gid", con);
            cmd2.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
            l1.Text = cmd2.ExecuteScalar().ToString();
            if (Convert.ToInt32(l1.Text) != 0)
                l3.Text = (100 * Convert.ToInt32(l2.Text) / Convert.ToInt32(l1.Text)).ToString() + "%";
            else
                l3.Text = "--";
            SqlCommand cmd3 = new SqlCommand("SELECT tbteacher.teacherfname + ' ' + tbteacher.teacherlname AS tname FROM   tbsubjectallot INNER JOIN tbteacher ON tbsubjectallot.subjectallotteacherid = tbteacher.teacherid WHERE (tbsubjectallot.subjectallotgroupid = @gid) AND (tbsubjectallot.subjectallotsubjectid = @sid)", con);
            cmd3.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
            cmd3.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSubmit.DataKeys[e.Row.RowIndex].Value);
            l4.Text = cmd3.ExecuteScalar().ToString();
            cmd3.Dispose();
            cmd2.Dispose();
            cmd1.Dispose();
            cmd.Dispose();
            con.Close();
        }
    }
    protected void lbdate_Click(object sender, EventArgs e)
    {
        p1.Visible = true;
    }
    protected void lbenter_Click(object sender, EventArgs e)
    {
        GridShowSubmitBind();
        p1.Visible = false;
        TextBox1.Text = string.Empty;
    }
}