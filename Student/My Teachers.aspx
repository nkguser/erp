﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/InnerStudent.master" AutoEventWireup="true" CodeFile="My Teachers.aspx.cs" Inherits="Student_My_Teachers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<head>
    <style type="text/css"> 
    .abc
    {
        font-size:larger;
        font-weight:bold;
        cursor:pointer
    }
    .xyz
    {
        display:none;
        visibility:hidden
    }
    .black_overlay
    {
        display:none;
        position: absolute;
        top: 4%;
        left: 0%;
        width: 100%;
        height:155%;
        background-color:black;
        z-index:auto;
        overflow: auto;
        -moz-opacity: 0.8;
        opacity:100;
        filter: alpha(opacity=100);
    }
    .white_content 
    {
        display:none;
        position: absolute;
        top: 10%;
        left: 19%;
        width:880px;
        padding: 0px;
        border: 0px solid #a6c25c;
        background-color: white;
        z-index:1002;
        overflow: auto;
     }
     .headertext
     {
        font-family:Arial, Helvetica, sans-serif;
        font-size:14px;
        color:#f19a19;
        font-weight:bold;
     }

    </style>
    <script type="text/javascript" language=javascript>
        //        function GetData(id) {
        //            var e;
        //            e = document.getElementById('d' + id);
        //            if (e) {

        //                if (e.style.display != "block") {
        //                    e.style.display = "block";
        //                    e.style.visibility = "visible";
        //                }
        //                else {
        //                    e.style.display = "none";
        //                    e.style.visibility = "hidden";
        //                }
        //            }

        //        }
        //        function GetData1(id) {
        //            var e1;
        //            e1 = document.getElementById('g' + id);
        //            if (e1) {

        //                if (e1.style.display != "block") {
        //                    e1.style.display = "block";
        //                    e1.style.visibility = "visible";
        //                }
        //                else {
        //                    e1.style.display = "none";
        //                    e1.style.visibility = "hidden";
        //                }
        //            }

        //        }
        function ShowImages() {
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block'
            return false;
        }
    </script>
</head>
      <asp:DataList ID="DataList2" runat="server"  
                       
                          
                    
                         RepeatColumns="7" 
                         RepeatDirection="Horizontal" Width="850px" 
          style="text-align: left" 
                  DataKeyField="teacherid" onitemcommand="DataList2_ItemCommand">
                         <ItemTemplate>
                             <table style="width: 100%">
                                 <tr>
                                     <td align="center">
                                         <asp:Image ID="img1" runat="server" ImageUrl='<%# "~/Teacherpic/" + Eval("teacherphoto") %>' Height="80px" Width="80px" />
                                     </td>
                                 </tr>
                                 
                                 <tr>
                                     <td align="center">
                                         &nbsp;</td>
                                 </tr>
                                 <tr>
                                     <td align="center">
                                         <asp:LinkButton ID="lk3" runat="server" 
                                             
                                             Text='<%#Eval("teacherfname") %>' CommandName="Show"> </asp:LinkButton>
                                     </td>
                                 </tr>
                                 <tr>
                                     <td align="center">
                                         <asp:LinkButton ID="lk4" runat="server" style="font-size:10px;font-style:italic" 
                                             
                                             Text='<%#Eval("subjecttitle") %>'> </asp:LinkButton>
                                         </td>
                                 </tr>
                                 <tr>
                                     <td align="center">
                                         (<asp:LinkButton ID="lk5" runat="server" Text='<%#Eval("subjecttypename") %>'> </asp:LinkButton>
                                         )</td>
                                 </tr>
                             </table>
                         </ItemTemplate>
                     </asp:DataList>
                     <div id="light" class="white_content">
     <table cellpadding=0 cellspacing=0 border=0 style="background-color:#a6c25c;" width="100%">
        <tr>
            <td height="16px"><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"><img src="../close.gif" style="border :0px"  width="13px" align="right" height="13px"/></a></td>
        </tr>
        <tr>
            <td style="padding-left:16px;padding-right:16px;padding-bottom:16px"> 
                <table align="center"  border="0" cellpadding="0" cellspacing="0" style="background-color:#fff" width="100%">
                    <tr>
                        <td align="center" colspan="2" class="headertext" ></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Repeater ID="imglightbox" runat="server">
                                <ItemTemplate>
                                <table><tr>
                                    <td><asp:Image ID="Image1" runat="server" Height=200px Width=200px ImageUrl='<%#"~/teacherpic/"+Eval("teacherphoto") %>' /></td>
                                    <td><br /><br /><br /><br /><%#Eval("name") %><br />
                                     <%#Eval("coursename") %><br />
                                     <%#Eval("depname") %> <br />
                                     <%#Eval("postdesc") %><br />
                                     <%#Eval("teacherexp") %><br />
                                     <%#Eval("teacherspecialization")%><br />
                                     <%#Eval("teachercorresadd")%><br /><%#Eval("teacherphone1") %><br /><%#Eval("teacheremailid") %><br />
                                     <i><%#Eval("teacherdob","{0:d}") %></i><br /><i><%#Eval("teacherdoj","{0:d}") %></i><br /><br /></td></tr></table>
                                </ItemTemplate>
                                
                            </asp:Repeater>
                        </td>
                            <asp:LinkButton id="sendmsg" runat="server">Send Message</asp:LinkButton>
                        <td></td>
                    </tr>
                    <tr>
                        <td height="10px"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div align="center" class=" headertext">
    </div>
</div>
<div id="fade" class="black_overlay">

</asp:Content>

