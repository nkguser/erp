﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Common.master" AutoEventWireup="true" CodeFile="frmhodshralb.aspx.cs" Inherits="HOD_frmhodshralb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <h2 >
        Shared Albums</h2>
    <p>
    
        <asp:DataList ID="DataList1" runat="server" DataSourceID="ObjectDataSource1" 
            Width="850px" RepeatColumns="4" RepeatDirection="Horizontal" 
            DataKeyField="p_albcvrpic" onitemdatabound="DataList1_ItemDataBound"  
            oncancelcommand="DataList1_CancelCommand" 
           >
            <ItemTemplate>
    
                <table style="width: 100%">
                    <tr>
                        <td align="center">
               <asp:Image ID="img1" runat="server"  Height="95px" Width="95px"/>                    
                         </td>
                    </tr>
                    <tr>
                        <td align="center">
                        <%#Eval("p_albnam") %>
                           </td>
                    </tr>
             
                    </tr>
              
                    <td align="center">
                        <asp:LinkButton ID="lk3" runat="server" 
                            CommandArgument='<%#Eval("p_albcod") %>' CommandName="cancel" 
                            Text="Preview Album"></asp:LinkButton>
                        &nbsp;
                        <asp:LinkButton ID="lk5" runat="server" 
                            CommandArgument='<%#Eval("p_albcod") %>'>Share Album</asp:LinkButton>
                    </td>
              
                </table>
    
            </ItemTemplate>
        </asp:DataList>
    </p>
    <p>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            SelectMethod="Dspshralb" TypeName="nsphotosnak.clsalb" 
            OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:SessionParameter DefaultValue="" Name="regcod" SessionField="depid" 
                    Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </p>
</asp:Content>

