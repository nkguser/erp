﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/InnerAdmin.master" AutoEventWireup="true" CodeFile="DetTeacher.aspx.cs" Inherits="Admin_DetTeacher" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" 
        Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
        WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%">
        <LocalReport ReportPath="Admin\admintdetrpt.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="admintdetrpt" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:cn %>" SelectCommand="admin_Rpt_tdet" 
        SelectCommandType="StoredProcedure" ProviderName="System.Data.SqlClient">
        <SelectParameters>
            <asp:SessionParameter Name="clg" SessionField="other" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>

