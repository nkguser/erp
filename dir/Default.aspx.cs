﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class dir_Default : System.Web.UI.Page
{
    
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    DataView dv = new DataView();
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (_cal.SelectedDate == DateTime.MinValue)
            _cal.SelectedDate = _cal.TodaysDate;
        if (Page.IsPostBack == false)
        {
            grd();
            dleventbind();
            Dlist_bind();
            Dlist_bind2();
            gridbind();
        }
    }
    void Page_PreRender()
    {
        dv = (DataView)SqlDataSource2.Select(DataSourceSelectArguments.Empty);
        dv.Sort = "entrydate";
    }

    protected void calSchedule_DayRender(object sender, DayRenderEventArgs e)
    {
        if (dv.FindRows(e.Day.Date).Length > 0)
            e.Cell.BackColor = System.Drawing.Color.GreenYellow;
    }
   
    
    private void gridbind()
    {
        SqlCommand cmd = new SqlCommand("select * from tbapp where appdate>=@date",con);
        cmd.Parameters.Add("@date", SqlDbType.Date).Value = System.DateTime.Today;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridTeacher.DataSource = ds;
        GridTeacher.DataBind();
    }    
   
    private void grd()
    {
        //DateTime dt = Convert.ToDateTime("9/5/12");
        SqlCommand cmd = new SqlCommand("SELECT     tbteacherattd.teachid, tbteacherattd.teachattddate, tbteacherattd.teachattdtypeid, tbteacher.teacherfname + ' ' + tbteacher.teacherlname AS name,tbteacher.teacherphoto,tbteachattdtype.teachattdtypedesc FROM         tbteacherattd INNER JOIN     tbteachattdtype ON tbteacherattd.teachattdtypeid = tbteachattdtype.teachtattdtypeid INNER JOIN   tbteacher ON tbteacherattd.teachid = tbteacher.teacherid WHERE     (tbteacherattd.teachattdtypeid <> 1) AND (tbteacherattd.teachattddate = @date) ", con);
        cmd.Parameters.Add("@date", SqlDbType.Date).Value = System.DateTime.Today;
        //cmd.Parameters.Add("@depid", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        if (ds.Tables[0].Rows.Count == 0)
        {
            Label3.Text = "All Teachers are Present today...";
            Label3.Visible = true;
        }
        DataList4.DataSource = ds;
        DataList4.DataBind();
        cmd.Dispose();
        ds.Dispose();
    }
    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        Image img = (Image)(e.Item.FindControl("Image1"));
        Label lbl = (Label)(e.Item.FindControl("lbl1"));
        if (Convert.ToInt32(lbl.Text) == 5)
            img.BorderColor = System.Drawing.Color.Red;
        else
            img.BorderColor = System.Drawing.Color.Gold;

    }
    private void Dlist_bind()
    {
        SqlCommand cmd = new SqlCommand("SELECT tbnotice.noticeid,  tbnotice.noticesubject, tbnotice.noticecontent, tbnotice.noticedate FROM tbnotice INNER JOIN tblevel ON tbnotice.noticelevelid = tblevel.levelid ", con);
        //cmd.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DataList2.DataSource = ds;
        DataList2.DataBind();
        cmd.Dispose();
    }
    private void Dlist_bind2()
    {
        SqlCommand cmd = new SqlCommand("SELECT tbnotice.noticeid,  tbnotice.noticesubject, tbnotice.noticecontent, tbnotice.noticedate FROM tbnotice INNER JOIN tblevel ON tbnotice.noticelevelid = tblevel.levelid AND tblevel.levelid = 3 ", con);
       // cmd.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DataList1.DataSource = ds;
        DataList1.DataBind();
        cmd.Dispose();
    }
    private void rep_bind(Int32 a)
    {
        SqlCommand cmd = new SqlCommand("SELECT   tbnotice.noticesubject, tbnotice.noticecontent, tbnotice.noticedate FROM tbnotice where noticeid=@u", con);
        cmd.Parameters.Add("@u", SqlDbType.Int).Value = a;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        imglightbox.DataSource = ds;
        imglightbox.DataBind();
        cmd.Dispose();
    }
    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "Show")
        {
            //Button imgbtn = sender as Button;
            ////Find Image button in gridview
            //Button imgbtntxt = (Button)Repeater1.FindControl("imgbtn");
            //Assign imagebutton url to image field in lightbox
            Int32 a = Convert.ToInt32(DataList1.DataKeys[e.Item.ItemIndex]);
            rep_bind(a);
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:ShowImages();", true);
        }
    }
    protected void DataList2_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "Show")
        {
            //Button imgbtn = sender as Button;
            ////Find Image button in gridview
            //Button imgbtntxt = (Button)Repeater1.FindControl("imgbtn");
            //Assign imagebutton url to image field in lightbox
            Int32 a = Convert.ToInt32(DataList2.DataKeys[e.Item.ItemIndex]);
            rep_bind(a);
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:ShowImages();", true);
        }
    }

    private void dleventbind()
    {
        //click="MM_openBrWindow('Default3.aspx?Id=749','win2','scrollbars=yes,width=33320,height=500,resizable=no,top=150' )"
        SqlDataAdapter adp = new SqlDataAdapter("SELECT     TOP (8) Eventid, EventNAme, eventdetail, eventfrmdat, eventtodat, eventvenue, Eventcatid, Eventlogo FROM         tbevent", con);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DataList3.DataSource = ds;
        DataList3.DataBind();

    }
    protected void DataListE_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            Int32 key = Convert.ToInt32(DataList3.DataKeys[e.Item.ItemIndex]);
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation","javascript:window.open('Default3.aspx?Id=" + key + "','win2', 'scrollbars=no,width=350,height=320,resizable=no,top=300,left=500' );", true); 
            //Response.Write("<script language='javascript'>window.open('Default3.aspx?Id=" + key + "','win2', 'scrollbars=yes,width=620,height=500,resizable=no,top=150' );</script>");
        }
    }
    protected void DataList4_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}