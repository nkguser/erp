﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/InnerStudent.master" AutoEventWireup="true" CodeFile="FeedbackStudent.aspx.cs" Inherits="Student_FeedbackStudent" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table class="styledmenu" width="900px">
    <tr>
        <td>
            &nbsp;</td>
        <td>
            Subject Header</td>
        <td>
            <asp:TextBox ID="TextBox1" runat="server" Height="38px" 
                style="margin-left: 0px" Width="800px"></asp:TextBox>
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                ControlToValidate="TextBox1" Display="Dynamic" ErrorMessage="*" 
                Font-Bold="True" ForeColor="Red" ValidationGroup="abc"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            Subject Detail</td>
        <td>
            <cc1:Editor ID="Editor1" runat="server" Height="300px" Width="800px" />
        </td>
        <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                ControlToValidate="Editor1" Display="Dynamic" ErrorMessage="*" Font-Bold="True" 
                ForeColor="Red" ValidationGroup="abc"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Send" 
                ValidationGroup="abc" />
            <asp:Label ID="lblfeedback" runat="server"></asp:Label>
        </td>
        <td>
            &nbsp;</td>
    </tr>
</table>
</asp:Content>

