﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="studentsessionals.aspx.cs" Inherits="HOD_studentsessionals" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <center>
    <table width=100% style="text-align: left; font-weight: bold;">
    <tr>
    <td></td>
    <td style="width: 181px"></td>
    <td style="width: 204px">
        <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
            RepeatDirection="Horizontal" AutoPostBack="True" 
            onselectedindexchanged="RadioButtonList1_SelectedIndexChanged">
            <asp:ListItem Value="0">Fill</asp:ListItem>
            <asp:ListItem Value="1">View</asp:ListItem>
        </asp:RadioButtonList>  
    </td>
    <td></td>
    <td></td>
    </tr>
    </table></center>
    <asp:Panel ID="fillpanel" runat="server" Visible="False">
     <center>
     <table>
     <tr>
     <td>
    <table width=100% style="text-align: left; font-weight: bold;" >
      <%--  <tr>
            <td>
                Course</td>
            <td>
                <asp:DropDownList ID="DDLCourse" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLCourse_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Department</td>
            <td>
                <asp:DropDownList ID="DDLDept" runat="server">
                    <asp:ListItem>-- Select Department --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>--%>
        <tr>
            <td style="width: 152px">
                Session</td>
            <td>
                <asp:DropDownList ID="DDLSessionf" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLSessionf_SelectedIndexChanged" Width="300px">
                </asp:DropDownList>
            </td>
            
        </tr>
        <tr>
            <td style="width: 152px">
                Class</td>
            <td>
                <asp:DropDownList ID="DDLClassf" runat="server" AutoPostBack="True" 
                    Enabled="False" onselectedindexchanged="DDLClassf_SelectedIndexChanged" 
                    Width="300px">
                    <asp:ListItem>-- Select Class --</asp:ListItem>
                </asp:DropDownList>
            </td>
            
        </tr>
        <tr>
            <td style="width: 152px">
                Subject</td>
            <td>
                <asp:DropDownList ID="DDLSubjectf" runat="server" Enabled="False" 
                    AutoPostBack="True" 
                    onselectedindexchanged="DDLSubjectf_SelectedIndexChanged" Width="300px">
                    <asp:ListItem>-- Choose Subject --</asp:ListItem>
                </asp:DropDownList>
            </td>
            
        </tr>
        
      </table>
     
        <asp:Panel ID="datemarks" runat="server" Visible="False" >
                <table width=100%  style="text-align: left; font-weight: bold;" >
                <tr>
            <td style="width: 152px">
                Sessional</td>
            <td>
                <asp:DropDownList ID="DDLSessionalf" runat="server" Enabled="False" 
                    AutoPostBack="True" Width="300px"/>
                
            
                
                <%--<asp:LinkButton ID="LinkButton10" runat="server" onclick="LinkButton8_Click">Show Submitted Marks</asp:LinkButton>--%>
            </td>
        </tr>
        <tr>
            <td style="width: 152px">
                Date(DD/MM/YY)</td>
            <td>
                <asp:TextBox ID="TxtDate" runat="server" Width="300px"></asp:TextBox>
                <asp:CalendarExtender Format="dd/MM/yyyy " ID="TxtDate_CalendarExtender" runat="server" 
                        Enabled="True"  TargetControlID="TxtDate">
                    </asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="TxtDate" Display="Dynamic" 
                    ErrorMessage="RequiredFieldValidator" Font-Bold="True" ForeColor="Red" 
                    ValidationGroup="abc">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator2" runat="server" 
                    ControlToValidate="TxtDate" ErrorMessage="CompareValidator" Font-Bold="True" 
                    ForeColor="Red" Operator="DataTypeCheck" Type="Date" ValidationGroup="abc">*</asp:CompareValidator>
                <asp:Label ID="LBLDatef" runat="server"></asp:Label>
            </td>
            
        </tr>
        <tr>
            <td style="width: 152px">
                Max Marks
            </td>
            <td>
                <asp:TextBox ID="TXTMaxMarks" runat="server" Width="300px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="TXTMaxMarks" Display="Dynamic" 
                    ErrorMessage="RequiredFieldValidator" Font-Bold="True" ForeColor="Red" 
                    ValidationGroup="abc">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator3" runat="server" 
                    ControlToValidate="TXTMaxMarks" ErrorMessage="CompareValidator" 
                    Font-Bold="True" ForeColor="Red" Operator="DataTypeCheck" Type="Integer" 
                    ValidationGroup="abc">*</asp:CompareValidator>
                <asp:Label ID="LBLMaxMarksf" runat="server" Visible="False"></asp:Label>
            </td>
            
        </tr>
    </table></asp:Panel>
    </td>
    </tr>
    <tr>
    <td>
    <asp:Label ID="lblfillmarksmsg" runat="server" Font-Bold="True" 
                  ForeColor="#003300">
                  </asp:Label>
                  </td></tr>
                  </table>
    </center>    
    <asp:Panel ID="fillmarkpanel" runat="server" Visible="False">
        <table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td colspan="3">
            <div  style="overflow:scroll;height:300px;visibility:inherit">
                <asp:GridView ID="GridFill" runat="server" 
                    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
                    GridLines="None" DataKeyNames="studentid" Width="100%" 
                    EmptyDataText="There are no records for display" ShowHeaderWhenEmpty="True">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Roll No">                        
                            <ItemTemplate>
                                <asp:Label ID="LblRollNo" runat="server" Text='<%# Eval("studentRollno") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Attendance">
                            <ItemTemplate>
                                <asp:RadioButtonList ID="RBLAttendance" runat="server" 
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="True">Present</asp:ListItem>
                                    <asp:ListItem Value="false">Absent</asp:ListItem>
                                </asp:RadioButtonList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Marks Obtained">
                            <ItemTemplate>
                                <asp:TextBox ID="TxtMarks" runat="server"></asp:TextBox>
                                <%--<asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" 
                                    ErrorMessage="Invalid Data" MinimumValue="0" Type="Double" 
                                    ControlToValidate="TxtMarks">*</asp:RangeValidator>--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TxtMarks" ErrorMessage="Invalid Data" ValidationGroup="abc">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                    ErrorMessage="Invalid Data" Operator="DataTypeCheck" Type="Double" 
                                    ControlToValidate="TxtMarks" ValidationGroup="abc">*</asp:CompareValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                </div>
            </td>
        </tr>
          <tr>
              <td >
                  
                <asp:LinkButton ID="LinkButton2" runat="server" onclick="LinkButton2_Click" 
                    Visible="False" ValidationGroup="abc">Save</asp:LinkButton>
                  
                  &nbsp;
                  
                  </td>
              
          </tr>
          <tr>
          <td>
              &nbsp;</td>
          </tr>
    </table></asp:Panel>
    </asp:Panel>
    <asp:Panel ID="viewpanel" runat="server" Visible="False">
     <center>
     <table>
     <tr>
     <td>
<table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td>
                Session</td>
            <td>
                <asp:DropDownList ID="DDLSession" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLSession_SelectedIndexChanged" Width="300px">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
              <tr>
            <td>
                Class</td>
            <td>
                <asp:DropDownList ID="DDLClass" runat="server" AutoPostBack="True" 
                    Enabled="False" onselectedindexchanged="DDLClass_SelectedIndexChanged" 
                    Width="300px" >
                    <asp:ListItem>-- Select Class --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
  
    <%--    <tr>
            <td>
                Group</td>
            <td>
                <asp:DropDownList ID="DDLGrp" runat="server" style="height: 22px" >
                    <asp:ListItem>-- Select Group --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>--%>
        <tr>
            <td>
                Subject</td>
            <td>
                <asp:DropDownList ID="DDLSubject" runat="server" Enabled="False" 
                    AutoPostBack="True" 
                    onselectedindexchanged="DDLSubject_SelectedIndexChanged" Width="300px">
                    <asp:ListItem>-- Choose Subject --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Sessional</td>
            <td>
                <asp:DropDownList ID="DDLSessional" runat="server" Enabled="False" 
                    AutoPostBack="True" 
                    onselectedindexchanged="DDLSessional_SelectedIndexChanged" Width="300px">
                    <asp:ListItem Value="0">-- Select Sessional No --</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td>
                &nbsp;&nbsp;&nbsp;
                </td>
        </tr>
        <tr>
            <td>
                Date</td>
            <td>
                <asp:Label ID="LBLDate" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Max Marks
            </td>
            <td>
                <asp:Label ID="LBLMaxMarks" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="Lblviewmsg" runat="server" Font-Bold="True" 
                    ForeColor="#003300"></asp:Label></td>
            <td>
                &nbsp;</td>
        </tr>
        
    </table>
    </td></tr>
                  </table>
    </center>  
     <asp:Panel ID="viewmarkpanel" runat="server" Visible="False"> 
    <table width="100%" style="text-align: left; font-weight: bold;" >
    <tr>
            <td colspan="2">
              <div  style="overflow:scroll;height:300px;visibility:inherit">
                <asp:GridView ID="GridShow" runat="server" 
                    AutoGenerateColumns="False" onrowcancelingedit="GridShow_RowCancelingEdit" 
                    onrowdatabound="GridShow_RowDataBound" onrowediting="GridShow_RowEditing" 
                    onrowupdating="GridShow_RowUpdating" CellPadding="4" ForeColor="#333333" 
                    GridLines="None" DataKeyNames="studentid" 
                    
                    EmptyDataText="Either You Have Submitted the Marks or Marks have not been entered in system" 
                    Width="100%">
                
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Roll No">                            
                            <ItemTemplate>
                                <asp:Label ID="LblRollNo" runat="server" Text='<%# Eval("studentrollno") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Attendance">
                            <EditItemTemplate>
                                <asp:RadioButtonList ID="RBLAttendance" runat="server" 
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="True" Selected="True">Present</asp:ListItem>
                                    <asp:ListItem Value="false">Absent</asp:ListItem>
                                </asp:RadioButtonList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LBLAttend" runat="server" Text='<%# Eval("studentSessionalattnd") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Marks Obtained">
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtMarks" runat="server" Text='<%# Eval("studentSessionalMarks") %>'></asp:TextBox>
                                <%--<asp:RangeValidator ID="RangeValidator1" runat="server" 
                                    ControlToValidate="TxtMarks" Display="Dynamic" ErrorMessage="Invalid Data" 
                                    MinimumValue="0" MaximumValue="20" Type="Double">range</asp:RangeValidator>--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TxtMarks" ErrorMessage="Invalid Data" ValidationGroup="abc">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                    ControlToValidate="TxtMarks" ErrorMessage="Invalid Data" 
                                    Operator="DataTypeCheck" Type="Double" ValidationGroup="abc">*</asp:CompareValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LBLMarks" runat="server" Text='<%# Eval("studentSessionalMarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:LinkButton ID="LBUpdate" runat="server" CommandName="Update" 
                                    Text="Update"></asp:LinkButton>
                                &nbsp;&nbsp;&nbsp;
                                <asp:LinkButton ID="LBCancel" runat="server" CommandName="Cancel" 
                                    Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LBEdit" runat="server" CommandName="Edit"
                                    Text="Edit" ValidationGroup="abc"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView></div>
            </td>
            
        </tr>
          <tr>
              <td >
                  
                  <asp:LinkButton ID="LnkBtnSubmitHod" runat="server" 
                      onclick="LnkBtnSubmitHod_Click">Submit Class Marks to Director </asp:LinkButton>
              </td>
             
          </tr>
    </table></asp:Panel>
     <asp:Panel ID="submittedmarkpanel" runat="server" Visible="False">   
    <table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td colspan="2">
              <div  style="overflow:scroll;height:300px;visibility:inherit">
               <asp:GridView ID="GridshowSubmit" runat="server" 
                    AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" 
                    GridLines="None" DataKeyNames="studentid" EmptyDataText="No Data" 
                    Width="100%">
                
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Roll No">                            
                            <ItemTemplate>
                                <asp:Label ID="LblRollNo" runat="server" Text='<%# Eval("studentrollno") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Attendance">                         
                            <ItemTemplate>
                                <asp:Label ID="LBLAttend" runat="server" Text='<%# Eval("studentSessionalattnd") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Marks Obtained">                        
                            <ItemTemplate>
                                <asp:Label ID="LBLMarks" runat="server" Text='<%# Eval("studentSessionalMarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView></div>             
            </td>
        </tr>
          <tr>
              <td>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>
              <td>
                  &nbsp;</td>
          </tr>
    </table></asp:Panel>
    </asp:Panel>
 </asp:Content>

