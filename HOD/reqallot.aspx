﻿<%@ Page Title="" Language="C#" EnableEventValidation="false"  MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="reqallot.aspx.cs" Inherits="HOD_reqallot" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <table  width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td style="width: 572px">
                &nbsp;</td><td>
               <asp:RadioButtonList ID="RadioButtonList2" runat="server" 
                    RepeatDirection="Horizontal" Width="266px" AutoPostBack="True" 
                    onselectedindexchanged="RadioButtonList2_SelectedIndexChanged" 
                    RepeatColumns="2" >
                    <asp:ListItem Value="0">Make Request</asp:ListItem>
                    <asp:ListItem Value="1">Approved Request</asp:ListItem>
                    <asp:ListItem Value="2">View Request</asp:ListItem>
                    <asp:ListItem Value="3">Alotted Requested</asp:ListItem>
                </asp:RadioButtonList>
                </td>
            <td style="width: 387px"  >
                &nbsp;</td>
                <td colspan="2">
                    &nbsp;
                </td>
                <td>
                </td>
            
            
        </tr>
        <tr><td></td>
        </tr>
        <tr><td></td></tr>
   </table>
   <asp:Panel ID="p1" ScrollBars="Auto" width="100%"  runat="server" 
        Visible="False">
    <table class="styledmenu"  width="100%" style="text-align: left; font-weight: bold;">
         <tr>
         <td></td><td>Class</td>
         <td>Group</td><td></td><td></td></tr>
         <tr>
            <td align=left style="width: 572px"   >
            Choose Group
                </td>
            <td  >
                
                <asp:DropDownList ID="DDLClass" runat="server" Height="20px" 
                    style="font-weight: bold" Width="200px" 
                    onselectedindexchanged="DDLClass_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList>
                <%--<ajax:CascadingDropDown ID="DDLClass_CascadingDropDown" runat="server" 
                    Category="Class" LoadingText="Loading Class..." ParentControlID="DDLDept" 
                    PromptText="Select Class" ServiceMethod="BindClassdropdown" 
                    ServicePath="DropdownWebService.asmx" TargetControlID="DDLClass">
                </ajax:CascadingDropDown>--%>
                <asp:RequiredFieldValidator ID="ddlClass3" runat="server" 
                    ControlToValidate="DDLClass" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="abc"></asp:RequiredFieldValidator>
                
            </td>
            
            <td  >
                
            <asp:DropDownList ID="DDLGroup" runat="server" Height="20px" 
                     style="font-weight: bold" Width="200px" Enabled="False"  
                    >
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ddlGroup7" runat="server" 
                    ControlToValidate="DDLGroup" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="abc"></asp:RequiredFieldValidator>
                
                </td>
            <td >
                &nbsp;</td><td >
                <%--<ajax:CascadingDropDown ID="DDLGroup_CascadingDropDown" runat="server" 
                    Category="Group" LoadingText="Loading Group..." ParentControlID="DDLClass" 
                    PromptText="Select Group" ServiceMethod="BindGroupdropdown" 
                    ServicePath="DropdownWebService.asmx" TargetControlID="DDLGroup">
                </ajax:CascadingDropDown>--%>
                 </td>
            
        </tr>
         <tr>
            <td style="width: 572px" >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td>
                </td>
        </tr>
         <tr>
            <td style="width: 572px" >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td>
                </td>
        </tr>
         <tr>
    
     
           <td style="width: 572px">Subject</td>
            <td  > 
            <asp:DropDownList ID="DDlSubject" runat="server" Height="20px" 
                    style="font-weight: bold" Width="200px" 
                    onselectedindexchanged="DDlSubject_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ddlGroup5" runat="server" 
                    ControlToValidate="DDlSubject" ErrorMessage="*" ForeColor="Red" 
                    TabIndex="1" ValidationGroup="abc"></asp:RequiredFieldValidator>
               
            </td>
            <td style="width: 318px" >
            
            </td >
            <td style="width: 205px">
            </td>
            <td>
             </td>
            
                
            
            
        </tr>
         <tr>
            <td style="width: 572px">
                </td>
            <td style="width: 387px">
                </td>
            <td style="width: 318px">
                </td>
            <td style="width: 205px">
                </td>
            <td>
                </td>
        </tr>
         <tr>
        <td style="width: 572px">
        </td>
        <td style="width: 387px">Course</td>
        <td style="width: 318px">Department</td>
            <td style="width: 205px">
                </td>
            <td>
                </td>
        </tr>
         <tr>
            <td style="width: 572px" >
                Choose Department for Request</td>
            <td  >
                <asp:DropDownList ID="DDLCoursed" runat="server" Height="20px" 
                    style="font-weight: bold" Width="200px">
                   
                </asp:DropDownList>
                <ajax:CascadingDropDown ID="CascadingDropDown4" runat="server" 
                    Category="Course" LoadingText="Loading Course..." PromptText="Select Course" 
                    ServiceMethod="BindCoursedropdown" ServicePath="DropdownWebService.asmx" 
                    TargetControlID="DDLCoursed">
                </ajax:CascadingDropDown>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="DDLCoursed" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="abc"></asp:RequiredFieldValidator>
            </td>
            <td  >
                <asp:DropDownList ID="DDLDeptd" runat="server" Height="20px" 
                    style="font-weight: bold" Width="200px">
                </asp:DropDownList>
                <ajax:CascadingDropDown ID="CascadingDropDown2" runat="server" 
                    Category="Department" LoadingText="Loading Department..." 
                    ParentControlID="DDLCoursed" PromptText="Select Department" 
                    ServiceMethod="BindDepartmentdropdown" ServicePath="DropdownWebService.asmx" 
                    TargetControlID="DDLDeptd">
                </ajax:CascadingDropDown>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="DDLDeptd" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="abc"></asp:RequiredFieldValidator>
                </td>
            <td style="width: 205px">
                
               
           
            </td>
            <td>
                </td>
        </tr>
         <tr>
            <td  >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td>
                </td>
        </tr>
         <tr>
            <td  >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td>
                </td>
        </tr>
         <tr>
            <td  >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td>
                </td>
        </tr>
         <tr>
            <td style="width: 572px" >
                </td>
            <td >
                 
                 <asp:Button ID="BTNSubAllot0" runat="server" onclick="BTNSubAllot_Click" 
                     style="font-weight: 700" Text="Allot" ValidationGroup="abc" 
                     Width="150px" />
                 
                </td>
            <td >
                 <asp:Label ID="lblmsg" runat="server" Font-Bold="True" ForeColor="#003300" ></asp:Label></td>
            <td style="width: 205px">
                </td>
            <td>
                </td>
        </tr>
         <tr>
            <td  >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td >
                </td>
            <td>
                </td>
        </tr>
         <tr>
            <td colspan="5" align="center">
            <div style="overflow:scroll;height:300px;">
                <asp:GridView ID="GridAllTeacher" runat="server" AutoGenerateColumns="False" 
                    EmptyDataText="There are no data records to display." 
                    AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" 
                    width="100%" ShowHeaderWhenEmpty="True" DataKeyNames="reqallotid" onrowdeleting="GridAllTeacher_RowDeleting"
                      >
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                       <asp:TemplateField HeaderText="Session">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLSession" runat="server" 
                                    DataTextField="Session" DataValueField="Session_id">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("sessionstart") %>'></asp:Label>
                            </ItemTemplate>
                           
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Class">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLClass" runat="server" AutoPostBack="True" 
                                    DataTextField="Class_name" DataValueField="Class_id">
                                </asp:DropDownList>
                                <br />
                                <asp:DropDownList ID="DDLGroup" runat="server" 
                                    DataTextField="Group_Title" DataValueField="Group_id">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("class") %>'></asp:Label>
                            </ItemTemplate>
                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Subject">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLDept" runat="server">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Requested Department">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLSub" runat="server" DataTextField="Subject_title" 
                                    DataValueField="subject_id">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Text='<%# Eval("depname") %>'></asp:Label>
                            </ItemTemplate>
                           
                        </asp:TemplateField>
                        
                      
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName='<%# "Update" %>' 
                                    Text="Update"></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton5" runat="server" CommandName='<%# "Cancel" %>' 
                                    Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LnkBtnDelete" Text="Cancel Request" CommandName="Delete" runat="server"/>
                               
                            </ItemTemplate>
                           
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </div>
            </td>
           
        </tr>
         <tr>
            <td >
            </td>
            <td >
            <br />
            <br />
            
           
            </td>
            
        </tr>
    </table>
    </asp:Panel>
    <asp:Panel ID="p2" ScrollBars="Auto"  width="100%"  runat="server" 
        Visible="False">
     <table width="100%" style="text-align: left; font-weight: bold;">
    <tr>
    <td align="center">
     
               
                <asp:GridView ID="GridTeacher" runat="server" 
                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                     
                    EmptyDataText="There are no data records to display." ForeColor="Black" 
                    GridLines="None" 
                     style="font-weight: bold;"
                   Font-Bold="False" ShowHeaderWhenEmpty="True" Width="100%" 
                    DataKeyNames="subjectid,groupid,reqallotid" 
                    onrowcancelingedit="GridTeacher_RowCancelingEdit" 
                    onrowdatabound="GridTeacher_RowDataBound" onrowediting="GridTeacher_RowEditing" onrowupdating="GridTeacher_RowUpdating" 
                    >
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Department">
                            <EditItemTemplate>
                                <asp:Label ID="Label9" runat="server" Text='<%# Eval("depname") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Text='<%# Eval("depname") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Session">
                            <EditItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("sessionstart") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("sessionstart") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Class">
                            <EditItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("class") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("class") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Subject">
                            <EditItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Teacher">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLTeacher" runat="server" 
                                    Enabled="true" >
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="DDLTeacherd" runat="server" 
                                     Enabled="false" Width="100px">
                                    
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName='<%# "Update" %>' 
                                    Text="Allot"></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton5" runat="server" CommandName='<%# "Cancel" %>' 
                                    Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LnkBtnDelete" runat="server" CommandName="Edit" 
                                    Text="Allot" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <EmptyDataRowStyle Font-Bold="True" ForeColor="#FF3300" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle CssClass="grid" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
              
 
    </td>
    </tr>
    </table> </asp:Panel>
    <asp:Panel ID="p3" ScrollBars="Auto"  width="100%"  runat="server" 
        Visible="False">
     <table width="100%" style="text-align: left; font-weight: bold;">
    <tr>
    <td align="center">
     <div style="overflow:scroll;height:300px;">
                <asp:GridView ID="Gridapproved" runat="server" AutoGenerateColumns="False" 
                    EmptyDataText="There are no data records to display." 
                    AllowSorting="True" CellPadding="4" ForeColor="#333333" GridLines="None" 
                    width="100%" ShowHeaderWhenEmpty="True" 
                      >
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                       <asp:TemplateField HeaderText="Session">
                          
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("sessionstart") %>'></asp:Label>
                            </ItemTemplate>
                           
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Class">
                            
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("class") %>'></asp:Label>
                            </ItemTemplate>
                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Subject">
                            
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Teacher">
                           
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                            </ItemTemplate>
                           
                        </asp:TemplateField>
         
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </div>
            <</td></tr></table></asp:Panel>
    <asp:Panel ID="p4" ScrollBars="Auto"  width="100%"  runat="server" 
        Visible="False">
     <table width="100%" style="text-align: left; font-weight: bold;">
    <tr>
    <td align="center">
     
               
                <asp:GridView ID="Gridallot" runat="server" 
                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                     
                    EmptyDataText="There are no data records to display." ForeColor="Black" 
                    GridLines="None" 
                     style="font-weight: bold;"
                   Font-Bold="False" ShowHeaderWhenEmpty="True" Width="100%" 
                    DataKeyNames="subjectid,groupid,reqallotid" 
                    
                    
                    >
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Department">
                            <EditItemTemplate>
                                <asp:Label ID="Label9" runat="server" Text='<%# Eval("depname") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Text='<%# Eval("depname") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Session">
                            <EditItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("sessionstart") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("sessionstart") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Class">
                            <EditItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("class") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("class") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Subject">
                            <EditItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Teacher">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLTeacher" runat="server" 
                                    Enabled="true">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Lblteacher" runat="server" Text='<%# Eval("name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName='<%# "Update" %>' 
                                    Text="Allot"></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton5" runat="server" CommandName='<%# "Cancel" %>' 
                                    Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                           <%-- <ItemTemplate>
                                <asp:LinkButton ID="LnkBtnDelete" runat="server" CommandName="Edit" 
                                    Text="Allot" />
                            </ItemTemplate>--%>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <EmptyDataRowStyle Font-Bold="True" ForeColor="#FF3300" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle CssClass="grid" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
              
 
    </td>
    </tr>
    </table> </asp:Panel>
</asp:Content>
