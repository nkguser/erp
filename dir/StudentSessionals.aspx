﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dir/MasterPage.master" AutoEventWireup="true" CodeFile="StudentSessionals.aspx.cs" Inherits="dir_StudentSessionals" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
     <center>
     <table>
     <tr>
     <td>
<table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td>
                Session</td>
            <td>
                <asp:DropDownList ID="DDLSession" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLSession_SelectedIndexChanged" Width="300px">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
              <tr>
            <td>
                Class</td>
            <td>
                <asp:DropDownList ID="DDLClass" runat="server" AutoPostBack="True" 
                    Enabled="False" onselectedindexchanged="DDLClass_SelectedIndexChanged" 
                    Width="300px" >
                    <asp:ListItem>-- Select Class --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
  
    <%--    <tr>
            <td>
                Group</td>
            <td>
                <asp:DropDownList ID="DDLGrp" runat="server" style="height: 22px" >
                    <asp:ListItem>-- Select Group --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>--%>
        <tr>
            <td>
                Subject</td>
            <td>
                <asp:DropDownList ID="DDLSubject" runat="server" Enabled="False" 
                    AutoPostBack="True" 
                    onselectedindexchanged="DDLSubject_SelectedIndexChanged" Width="300px">
                    <asp:ListItem>-- Choose Subject --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Sessional</td>
            <td>
                <asp:DropDownList ID="DDLSessional" runat="server" Enabled="False" 
                    AutoPostBack="True" 
                    onselectedindexchanged="DDLSessional_SelectedIndexChanged" Width="300px">
                    <asp:ListItem Value="0">-- Select Sessional No --</asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td>
                &nbsp;&nbsp;&nbsp;
                </td>
        </tr>
        <tr>
            <td>
                Date</td>
            <td>
                <asp:Label ID="LBLDate" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Max Marks
            </td>
            <td>
                <asp:Label ID="LBLMaxMarks" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="Lblviewmsg" runat="server" Font-Bold="True" 
                    ForeColor="#003300"></asp:Label></td>
            <td>
                &nbsp;</td>
        </tr>
        
    </table>
    </td></tr>
                  </table>
    </center>  
     <asp:Panel ID="submittedmarkpanel" runat="server" Visible="False">   
    <table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td colspan="2">
              <div  style="overflow:scroll;height:300px;visibility:inherit">
               <asp:GridView ID="GridshowSubmit" runat="server" 
                    AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" 
                    GridLines="None" DataKeyNames="studentid" EmptyDataText="No Data" 
                    Width="100%">
                
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Roll No">                            
                            <ItemTemplate>
                                <asp:Label ID="LblRollNo" runat="server" Text='<%# Eval("studentrollno") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Attendance">                         
                            <ItemTemplate>
                                <asp:Label ID="LBLAttend" runat="server" Text='<%# Eval("studentSessionalattnd") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Marks Obtained">                        
                            <ItemTemplate>
                                <asp:Label ID="LBLMarks" runat="server" Text='<%# Eval("studentSessionalMarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView> </div>            
            </td>
            <td> 
                &nbsp;</td>
        </tr>
          <tr>
              <td colspan="2">
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>
              <td>
                  &nbsp;</td>
          </tr>
    </table></asp:Panel>
   
</asp:Content>

