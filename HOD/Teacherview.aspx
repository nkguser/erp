﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="Teacherview.aspx.cs" Inherits="Admin_Teacherview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
         <table class="style1">
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    <strong>Teacher ID</strong></td>
                <td>
                    <asp:TextBox ID="TextBox77" runat="server" 
                        ontextchanged="TextBox77_TextChanged" Width="297px" ></asp:TextBox>
                        <asp:Label ID="lblmsg" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:RequiredFieldValidator ID="txtrequired" runat="server" 
                        ControlToValidate="TextBox77" Display="Dynamic" ErrorMessage="**" ValidationGroup="abc" 
                        Font-Bold="True" ForeColor="Red" 
                       ></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td class="style2">
    
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
             <tr>
                 <td class="style2">
                     &nbsp;</td>
                 <td colspan="2">
                     <asp:DataList ID="DataList2" runat="server"  
                         oncancelcommand="DataList1_CancelCommand" 
                          
                         onitemdatabound="DataList1_ItemDataBound" 
                         RepeatColumns="7" 
                         RepeatDirection="Horizontal" Width="850px" style="text-align: left">
                         <ItemTemplate>
                             <table style="width: 100%">
                                 <tr>
                                     <td align="center">
                                         <asp:Image ID="img1" runat="server" ImageUrl='<%# "~/Teacherpic/" + Eval("teacherphoto") %>' Height="100px" Width="100px" />
                                     </td>
                                 </tr>
                                 
                                 <tr>
                                     <td align="center">
                                         &nbsp;</td>
                                 </tr>
                                 <tr>
                                     <td align="center">
                                         <asp:LinkButton ID="lk3" runat="server" 
                                             CommandArgument='<%#Eval("teacherid") %>' CommandName="cancel" 
                                             Text='<%#Eval("teacherfname") %>' > </asp:LinkButton>
                                     </td>
                                 </tr>
                                 <tr>
                                     <td align="center">
                                         &nbsp;</td>
                                 </tr>
                             </table>
                         </ItemTemplate>
                     </asp:DataList>
                 </td>
             </tr>
             <tr>
                 <td class="style2">
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
             </tr>
             <tr>
                 <td class="style2">
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
             </tr>
             <tr>
                 <td class="style2">
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td style="text-align: left">
                     <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" 
                         CellPadding="4" DataKeyNames="teacherid" ForeColor="#333333" GridLines="None" 
                         Height="16px" onitemdeleting="DetailsView1_ItemDeleting" 
                         onitemupdating="DetailsView1_ItemUpdating" 
                         onmodechanging="DetailsView1_ModeChanging" 
                         style="margin-right: 0px; margin-top: 15px; text-align: left;" 
                         Visible="False" Width="438px">
                         <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                         <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
                         <EditRowStyle BackColor="#999999" />
                         <EmptyDataTemplate>
                             No Record&nbsp; Found<br />
                         </EmptyDataTemplate>
                         <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
                         <Fields>
                             <asp:TemplateField HeaderText="Teacher ID">
                                 <EditItemTemplate>
                                     <asp:Label ID="Label100" runat="server" Text='<%# Eval("teacherclgid") %>'></asp:Label>
                                 </EditItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="techerlabelview" runat="server" 
                                         Text='<%# Eval("teacherclgid") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Course">
                                 <ItemTemplate>
                                     <asp:Label ID="Label2" runat="server" Text='<%# Eval("Coursename") %>'></asp:Label>
                                 </ItemTemplate>
                                 <EditItemTemplate>
                                     <asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack="True" 
                                         DataSourceID="Course" DataTextField="coursename" DataValueField="courseid" 
                                         onselectedindexchanged="DropDownList4_SelectedIndexChanged">
                                     </asp:DropDownList>
                                     <asp:SqlDataSource ID="Course" runat="server" 
                                         ConnectionString="<%$ ConnectionStrings:cn %>" 
                                         SelectCommand="SELECT [courseid], [coursename] FROM [tbcourse] WHERE ([coursedelsts] = @coursedelsts)">
                                         <SelectParameters>
                                             <asp:Parameter DefaultValue="false" Name="coursedelsts" Type="Boolean" />
                                         </SelectParameters>
                                     </asp:SqlDataSource>
                                 </EditItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Department" SortExpression="teacherdepid">
                                 <ItemTemplate>
                                     <asp:Label ID="Label3" runat="server" Text='<%# Eval("Depname") %>'></asp:Label>
                                 </ItemTemplate>
                                 <EditItemTemplate>
                                     <asp:DropDownList ID="DropDownList5" runat="server">
                                     </asp:DropDownList>
                                 </EditItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Salutation" SortExpression="teachersalutation">
                                 <EditItemTemplate>
                                     <asp:DropDownList ID="DropDownList1" runat="server">
                                         <asp:ListItem>Mr.</asp:ListItem>
                                         <asp:ListItem>Mrs.</asp:ListItem>
                                         <asp:ListItem>Miss</asp:ListItem>
                                         <asp:ListItem>Dr.</asp:ListItem>
                                     </asp:DropDownList>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:DropDownList ID="DropDownList2" runat="server">
                                         <asp:ListItem>Mr.</asp:ListItem>
                                         <asp:ListItem>Mrs.</asp:ListItem>
                                         <asp:ListItem>Miss</asp:ListItem>
                                         <asp:ListItem>Dr.</asp:ListItem>
                                     </asp:DropDownList>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label1" runat="server" Text='<%# Eval("teachersalutation") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="First Name" SortExpression="teacherfname">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("teacherfname") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                         ControlToValidate="TextBox1" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox2" runat="server" Text='<%# Eval("teacherfname") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                         ControlToValidate="TextBox2" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label4" runat="server" Text='<%# Eval("teacherfname") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Middle Name" SortExpression="teachermname">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox3" runat="server" Text='<%# Eval("teachermname") %>'></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox4" runat="server" Text='<%# Eval("teachermname") %>'></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label5" runat="server" Text='<%# Eval("teachermname") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Last Name" SortExpression="teacherlname">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox5" runat="server" Text='<%# Eval("teacherlname") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                         ControlToValidate="TextBox5" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox6" runat="server" Text='<%# Eval("teacherlname") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                         ControlToValidate="TextBox6" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label6" runat="server" Text='<%# Eval("teacherlname") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Date Of Birth" SortExpression="teacherdob">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox7" runat="server" 
                                         Text='<%# Eval("teacherdob","{0:d}") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                         ControlToValidate="TextBox7" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                     <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                         ControlToValidate="TextBox7" ErrorMessage="**" ForeColor="Red" 
                                         Operator="DataTypeCheck" Type="Date" ValidationGroup="abc"></asp:CompareValidator>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox8" runat="server" Text='<%# Eval("teacherdob") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                         ControlToValidate="TextBox8" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                     <asp:CompareValidator ID="CompareValidator2" runat="server" 
                                         ControlToValidate="TextBox8" ErrorMessage="**" ForeColor="Red" 
                                         Operator="DataTypeCheck" Type="Date" ValidationGroup="abc"></asp:CompareValidator>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label7" runat="server" Text='<%# Eval("teacherdob","{0:d}") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Date Of Joining" SortExpression="teacherdoj">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox9" runat="server" 
                                         Text='<%# Eval("teacherdoj","{0:d}") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                         ControlToValidate="TextBox9" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                     <asp:CompareValidator ID="CompareValidator3" runat="server" 
                                         ControlToValidate="TextBox9" ErrorMessage="**" ForeColor="Red" 
                                         Operator="DataTypeCheck" Type="Date" ValidationGroup="abc"></asp:CompareValidator>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox10" runat="server" Text='<%# Eval("teacherdoj") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                                         ControlToValidate="TextBox10" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                     <asp:CompareValidator ID="CompareValidator4" runat="server" 
                                         ControlToValidate="TextBox10" ErrorMessage="**" ForeColor="Red" 
                                         Operator="DataTypeCheck" Type="Date" ValidationGroup="abc"></asp:CompareValidator>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label8" runat="server" Text='<%# Eval("teacherdoj","{0:d}") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Father's name" 
                                 SortExpression="teacherfathername">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox11" runat="server" 
                                         Text='<%# Eval("teacherfathername") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                                         ControlToValidate="TextBox11" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox12" runat="server" 
                                         Text='<%# Eval("teacherfathername") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                                         ControlToValidate="TextBox12" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label9" runat="server" Text='<%# Eval("teacherfathername") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Mother's Name" 
                                 SortExpression="teachermothername">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox13" runat="server" 
                                         Text='<%# Eval("teachermothername") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" 
                                         ControlToValidate="TextBox13" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox14" runat="server" 
                                         Text='<%# Eval("teachermothername") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" 
                                         ControlToValidate="TextBox14" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label10" runat="server" Text='<%# Eval("teachermothername") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Correspondence Address" 
                                 SortExpression="teachercorresadd">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox15" runat="server" Height="92px" 
                                         Text='<%# Eval("teachercorresadd") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" 
                                         ControlToValidate="TextBox15" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox16" runat="server" Height="92px" 
                                         Text='<%# Eval("teachercorresadd") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                                         ControlToValidate="TextBox16" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label11" runat="server" Text='<%# Eval("teachercorresadd") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Permanent Address" 
                                 SortExpression="teacherpermaadd">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox17" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherpermaadd") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox18" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherpermaadd") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label12" runat="server" Text='<%# Eval("teacherpermaadd") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Post" SortExpression="teacherpostid">
                                 <EditItemTemplate>
                                     <asp:DropDownList ID="DropDownList7" runat="server" DataSourceID="Post" 
                                         DataTextField="postdesc" DataValueField="postid">
                                     </asp:DropDownList>
                                     <asp:SqlDataSource ID="Post" runat="server" 
                                         ConnectionString="<%$ ConnectionStrings:cn %>" 
                                         SelectCommand="SELECT [postid], [postdesc] FROM [tbpost] WHERE ([postdelsts] = @postdelsts)">
                                         <SelectParameters>
                                             <asp:Parameter DefaultValue="false" Name="postdelsts" Type="Boolean" />
                                         </SelectParameters>
                                     </asp:SqlDataSource>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:DropDownList ID="DropDownList8" runat="server" DataSourceID="Post" 
                                         DataTextField="postdesc" DataValueField="postid">
                                     </asp:DropDownList>
                                     <asp:SqlDataSource ID="Post0" runat="server" 
                                         ConnectionString="<%$ ConnectionStrings:cn %>" 
                                         SelectCommand="SELECT [postid], [postdesc] FROM [tbpost] WHERE ([postdelsts] = @postdelsts)">
                                         <SelectParameters>
                                             <asp:Parameter DefaultValue="false" Name="postdelsts" Type="Boolean" />
                                         </SelectParameters>
                                     </asp:SqlDataSource>
                                 </InsertItemTemplate>
                                 <%--<ItemTemplate>
                        <asp:Label ID="Label13" runat="server" Text='<%# Eval("teacherpostid") %>'></asp:Label>
                    </ItemTemplate>--%></asp:TemplateField>
                             <asp:TemplateField HeaderText="Qualification" 
                                 SortExpression="teacherqualification">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox19" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherqualification") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox20" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherqualification") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label14" runat="server" 
                                         Text='<%# Eval("teacherqualification") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Experience" SortExpression="teacherexp">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox21" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherexp") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox22" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherexp") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label15" runat="server" Text='<%# Eval("teacherexp") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Phone Number" SortExpression="teacherphone1">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox23" runat="server" Text='<%# Eval("teacherphone1") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" 
                                         ControlToValidate="TextBox23" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                     <asp:CompareValidator ID="CompareValidator5" runat="server" 
                                         ControlToValidate="TextBox23" ErrorMessage="**" ForeColor="Red" 
                                         Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox24" runat="server" Text='<%# Eval("teacherphone1") %>'></asp:TextBox>
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" 
                                         ControlToValidate="TextBox24" ErrorMessage="*" ForeColor="Red" 
                                         ValidationGroup="abc"></asp:RequiredFieldValidator>
                                     <asp:CompareValidator ID="CompareValidator6" runat="server" 
                                         ControlToValidate="TextBox24" ErrorMessage="**" ForeColor="Red" 
                                         Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label16" runat="server" Text='<%# Eval("teacherphone1") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Alternate Phone Number" 
                                 SortExpression="teacherphone2">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox25" runat="server" Text='<%# Eval("teacherphone2") %>'></asp:TextBox>
                                     <asp:CompareValidator ID="CompareValidator7" runat="server" 
                                         ControlToValidate="TextBox25" ErrorMessage="**" ForeColor="Red" 
                                         Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox26" runat="server" Text='<%# Eval("teacherphone2") %>'></asp:TextBox>
                                     <asp:CompareValidator ID="CompareValidator8" runat="server" 
                                         ControlToValidate="TextBox26" ErrorMessage="**" ForeColor="Red" 
                                         Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label17" runat="server" Text='<%# Eval("teacherphone2") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="E_mail" SortExpression="teacheremailid">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox27" runat="server" Text='<%# Eval("teacheremailid") %>'></asp:TextBox>
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                         ControlToValidate="TextBox27" ErrorMessage="***" ForeColor="Red" 
                                         ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                         ValidationGroup="abc"></asp:RegularExpressionValidator>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox28" runat="server" Text='<%# Eval("teacheremailid") %>'></asp:TextBox>
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                                         ControlToValidate="TextBox28" ErrorMessage="***" ForeColor="Red" 
                                         ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                         ValidationGroup="abc"></asp:RegularExpressionValidator>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label18" runat="server" Text='<%# Eval("teacheremailid") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Picture" SortExpression="teacherphoto">
                                 <EditItemTemplate>
                                     <asp:FileUpload ID="FileUpload1" runat="server" />
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:FileUpload ID="FileUpload2" runat="server" />
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <%-- <asp:Image ID="Image1" runat="server" Height="80px" 
                                        ImageUrl='<%# Eval("teacherphoto") %>' Width="80px" />--%>
                                     <asp:Image ID="Image1" runat="server" Height="80px" 
                                         ImageUrl='<%# "~/Teacherpic/" + Eval("teacherphoto")%>' Width="80px" />
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Gender" SortExpression="teachergender">
                                 <EditItemTemplate>
                                     <asp:CheckBox ID="CheckBox1" runat="server" 
                                         Text='<%# Eval("teachergender") %>' />
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:CheckBox ID="CheckBox2" runat="server" 
                                         Text='<%# Eval("teachergender") %>' />
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label20" runat="server" Text='<%# Eval("teachergender") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Research Journals" 
                                 SortExpression="teacherresrchjrnls">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox31" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherresrchjrnls") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox32" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherresrchjrnls") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label21" runat="server" Text='<%# Eval("teacherresrchjrnls") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Research Conferences" 
                                 SortExpression="teacherresrchcnfrncs">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox33" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherresrchcnfrncs") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox34" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherresrchcnfrncs") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label22" runat="server" 
                                         Text='<%# Eval("teacherresrchcnfrncs") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Workshop/Seminar" 
                                 SortExpression="teacherworkshopseminar">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox35" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherworkshopseminar") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox36" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherworkshopseminar") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label23" runat="server" 
                                         Text='<%# Eval("teacherworkshopseminar") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Other Acts" SortExpression="teacherotheracts">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox37" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherotheracts") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox38" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherotheracts") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label24" runat="server" Text='<%# Eval("teacherotheracts") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Specialization" 
                                 SortExpression="teacherspecialization">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox39" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherspecialization") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox40" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherspecialization") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label25" runat="server" 
                                         Text='<%# Eval("teacherspecialization") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Project/Award" 
                                 SortExpression="teacherprojectaward">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox41" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherprojectaward") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox42" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherprojectaward") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label26" runat="server" 
                                         Text='<%# Eval("teacherprojectaward") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Subject Undertaken" 
                                 SortExpression="teachersubjectundertaken">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox43" runat="server" Height="92px" 
                                         Text='<%# Eval("teachersubjectundertaken") %>' TextMode="MultiLine" 
                                         Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox44" runat="server" Height="92px" 
                                         Text='<%# Eval("teachersubjectundertaken") %>' TextMode="MultiLine" 
                                         Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label27" runat="server" 
                                         Text='<%# Eval("teachersubjectundertaken") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Conferences Conducted" 
                                 SortExpression="teachercnfrncconducted">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox45" runat="server" Height="92px" 
                                         Text='<%# Eval("teachercnfrncconducted") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox46" runat="server" Height="92px" 
                                         Text='<%# Eval("teachercnfrncconducted") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label28" runat="server" 
                                         Text='<%# Eval("teachercnfrncconducted") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Conferences Attended" 
                                 SortExpression="teachercnfrncattend">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox47" runat="server" Height="92px" 
                                         Text='<%# Eval("teachercnfrncattend") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox48" runat="server" Height="92px" 
                                         Text='<%# Eval("teachercnfrncattend") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label29" runat="server" 
                                         Text='<%# Eval("teachercnfrncattend") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Other Info" SortExpression="teacherotherinfo">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox49" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherotherinfo") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox50" runat="server" Height="92px" 
                                         Text='<%# Eval("teacherotherinfo") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label30" runat="server" Text='<%# Eval("teacherotherinfo") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Category" SortExpression="teachercategoryid">
                                 <EditItemTemplate>
                                     <asp:DropDownList ID="DropDownList9" runat="server" DataSourceID="Ctegory" 
                                         DataTextField="categoryname" DataValueField="categoryid">
                                     </asp:DropDownList>
                                     <asp:SqlDataSource ID="Ctegory" runat="server" 
                                         ConnectionString="<%$ ConnectionStrings:cn %>" 
                                         SelectCommand="SELECT [categoryid], [categoryname] FROM [tbcategory] WHERE ([categorydelsts] = @categorydelsts)">
                                         <SelectParameters>
                                             <asp:Parameter DefaultValue="false" Name="categorydelsts" Type="Boolean" />
                                         </SelectParameters>
                                     </asp:SqlDataSource>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:DropDownList ID="DropDownList10" runat="server" DataSourceID="Ctegory" 
                                         DataTextField="categoryname" DataValueField="categoryid">
                                     </asp:DropDownList>
                                     <asp:SqlDataSource ID="Ctegory0" runat="server" 
                                         ConnectionString="<%$ ConnectionStrings:cn %>" 
                                         SelectCommand="SELECT [categoryid], [categoryname] FROM [tbcategory] WHERE ([categorydelsts] = @categorydelsts)">
                                         <SelectParameters>
                                             <asp:Parameter DefaultValue="false" Name="categorydelsts" Type="Boolean" />
                                         </SelectParameters>
                                     </asp:SqlDataSource>
                                 </InsertItemTemplate>
                                 <%--<ItemTemplate>
                        <asp:Label ID="Label31" runat="server" Text='<%# Eval("teachercategoryid") %>'></asp:Label>
                    </ItemTemplate>--%></asp:TemplateField>
                             <asp:TemplateField HeaderText="Maritial Status" 
                                 SortExpression="teachermaritalstatus">
                                 <EditItemTemplate>
                                     <asp:CheckBox ID="CheckBox3" runat="server" 
                                         Text='<%# Eval("teachermaritalstatus") %>' />
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:CheckBox ID="CheckBox4" runat="server" 
                                         Text='<%# Eval("teachermaritalstatus") %>' />
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label32" runat="server" 
                                         Text='<%# Eval("teachermaritalstatus") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Membership Of Professional Bodies" 
                                 SortExpression="teachermembershipofprofessionalbodies">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox51" runat="server" Height="92px" 
                                         Text='<%# Eval("teachermembershipofprofessionalbodies") %>' 
                                         TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox52" runat="server" Height="92px" 
                                         Text='<%# Eval("teachermembershipofprofessionalbodies") %>' 
                                         TextMode="MultiLine" Width="361px"></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label33" runat="server" 
                                         Text='<%# Eval("teachermembershipofprofessionalbodies") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Pan Number" SortExpression="teacherpanno">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox53" runat="server" Text='<%# Eval("teacherpanno") %>'></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox54" runat="server" Text='<%# Eval("teacherpanno") %>'></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label34" runat="server" Text='<%# Eval("teacherpanno") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="PF Number" SortExpression="teacherpfno">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox55" runat="server" Text='<%# Eval("teacherpfno") %>'></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox56" runat="server" Text='<%# Eval("teacherpfno") %>'></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label35" runat="server" Text='<%# Eval("teacherpfno") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Salary Bank Name" 
                                 SortExpression="teachersalaryBankname">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox57" runat="server" 
                                         Text='<%# Eval("teachersalaryBankname") %>'></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox58" runat="server" 
                                         Text='<%# Eval("teachersalaryBankname") %>'></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label36" runat="server" 
                                         Text='<%# Eval("teachersalaryBankname") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Salary Branch Name" 
                                 SortExpression="teachersalarybranchname">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox59" runat="server" 
                                         Text='<%# Eval("teachersalarybranchname") %>'></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox60" runat="server" 
                                         Text='<%# Eval("teachersalarybranchname") %>'></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label37" runat="server" 
                                         Text='<%# Eval("teachersalarybranchname") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Bank Account Number" 
                                 SortExpression="teacherbankaccno">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox61" runat="server" 
                                         Text='<%# Eval("teacherbankaccno") %>'></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox62" runat="server" 
                                         Text='<%# Eval("teacherbankaccno") %>'></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label38" runat="server" Text='<%# Eval("teacherbankaccno") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="ISFC Code" SortExpression="teacherISFCcode">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox63" runat="server" 
                                         Text='<%# Eval("teacherISFCcode") %>'></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox64" runat="server" 
                                         Text='<%# Eval("teacherISFCcode") %>'></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label39" runat="server" Text='<%# Eval("teacherISFCcode") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Gross Pay" SortExpression="teachergrosspay">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox65" runat="server" 
                                         Text='<%# Eval("teachergrosspay") %>'></asp:TextBox>
                                     <asp:CompareValidator ID="CompareValidator9" runat="server" 
                                         ControlToValidate="TextBox65" ErrorMessage="**" ForeColor="Red" 
                                         Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox66" runat="server" 
                                         Text='<%# Eval("teachergrosspay") %>'></asp:TextBox>
                                     <asp:CompareValidator ID="CompareValidator10" runat="server" 
                                         ControlToValidate="TextBox66" ErrorMessage="**" ForeColor="Red" 
                                         Operator="DataTypeCheck" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label40" runat="server" Text='<%# Eval("teachergrosspay") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Pay Scale" SortExpression="teacherpayscale">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox67" runat="server" 
                                         Text='<%# Eval("teacherpayscale") %>'></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox68" runat="server" 
                                         Text='<%# Eval("teacherpayscale") %>'></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label41" runat="server" Text='<%# Eval("teacherpayscale") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Teacher Or Non Teaching" 
                                 SortExpression="teacherTORNTsts">
                                 <EditItemTemplate>
                                     <asp:CheckBox ID="CheckBox5" runat="server" 
                                         Text='<%# Eval("teacherTORNTsts") %>' />
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:CheckBox ID="CheckBox6" runat="server" 
                                         Text='<%# Eval("teacherTORNTsts") %>' />
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label42" runat="server" Text='<%# Eval("teacherTORNTsts") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Appointment Type" 
                                 SortExpression="teacherappointtype">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox69" runat="server" 
                                         Text='<%# Eval("teacherappointtype") %>'></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox70" runat="server" 
                                         Text='<%# Eval("teacherappointtype") %>'></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label43" runat="server" Text='<%# Eval("teacherappointtype") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="University Approvel Letter Number" 
                                 SortExpression="teacherUnivaprroveletterno">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox71" runat="server" 
                                         Text='<%# Eval("teacherUnivaprroveletterno") %>'></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox72" runat="server" 
                                         Text='<%# Eval("teacherUnivaprroveletterno") %>'></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label44" runat="server" 
                                         Text='<%# Eval("teacherUnivaprroveletterno") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="University Approvel Date" 
                                 SortExpression="teacherunivapproveDate">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox73" runat="server" 
                                         Text='<%# Eval("teacherunivapproveDate","{0:d}") %>'></asp:TextBox>
                                     <asp:CompareValidator ID="CompareValidator11" runat="server" 
                                         ControlToValidate="TextBox73" ErrorMessage="**" ForeColor="Red" 
                                         Operator="DataTypeCheck" Type="Date" ValidationGroup="abc"></asp:CompareValidator>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox74" runat="server" 
                                         Text='<%# Eval("teacherunivapproveDate") %>'></asp:TextBox>
                                     <asp:CompareValidator ID="CompareValidator12" runat="server" 
                                         ControlToValidate="TextBox74" ErrorMessage="**" ForeColor="Red" 
                                         Operator="DataTypeCheck" Type="Date" ValidationGroup="abc"></asp:CompareValidator>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label45" runat="server" 
                                         Text='<%# Eval("teacherunivapproveDate","{0:d}") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField HeaderText="Religion">
                                 <EditItemTemplate>
                                     <asp:TextBox ID="TextBox75" runat="server" 
                                         Text='<%# Eval("teacherreligion") %>'></asp:TextBox>
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     <asp:TextBox ID="TextBox76" runat="server" 
                                         Text='<%# Eval("teacherreligion") %>'></asp:TextBox>
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     <asp:Label ID="Label46" runat="server" Text='<%# Eval("teacherreligion") %>'></asp:Label>
                                 </ItemTemplate>
                             </asp:TemplateField>
                             <asp:TemplateField>
                                 <EditItemTemplate>
                                     &nbsp;
                                 </EditItemTemplate>
                                 <InsertItemTemplate>
                                     &nbsp;
                                 </InsertItemTemplate>
                                 <ItemTemplate>
                                     &nbsp;&nbsp;
                                 </ItemTemplate>
                             </asp:TemplateField>
                         </Fields>
                         <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                         <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                         <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                         <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                     </asp:DetailsView>
                 </td>
             </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>

