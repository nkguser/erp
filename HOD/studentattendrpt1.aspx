﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="studentattendrpt1.aspx.cs" Inherits="HOD_studentattendrpt1" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td>

            <table width="100%">
         
        <tr>
            <td style="width: 400px">
                Session</td>
            <td>
                <asp:DropDownList ID="DDlSession" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDlSessionf_SelectedIndexChanged" Width="200px">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 400px">
                Class</td>
            <td>
                <asp:DropDownList ID="DDLclass" runat="server" AutoPostBack="True" 
                    Enabled="False" onselectedindexchanged="DDLClassf_SelectedIndexChanged" 
                    Width="200px">
                    <asp:ListItem>-- Select Class --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
                <tr>
                    <td style="width: 400px">
                        Semester</td>
                    <td>
                <asp:DropDownList ID="DDLSem" runat="server" AutoPostBack="True" 
                    Enabled="False" onselectedindexchanged="DDLSem_SelectedIndexChanged" 
                    Width="200px">
                    <asp:ListItem Value="0">-- Select Semester --</asp:ListItem>
                </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr></table>
                <asp:Panel ID="panelfill" runat="server" Visible="False">
                <table width="100%" style="text-align: left; font-weight: bold;">
                <tr>
                    <td align="center" colspan="2">
           <div  style="overflow:scroll;height:300px;visibility:inherit">
                        <asp:GridView ID="GridShowSummary" runat="server" 
                    AutoGenerateColumns="False" Width="100%" DataKeyNames="studentid" CellPadding="4" 
                            ForeColor="#333333" onrowdatabound="GridShowSummary_RowDataBound">
                
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                
                    <Columns>
                        <asp:TemplateField HeaderText="Roll-No">
                        
                        <ItemTemplate>
                        <asp:Label ID="Lblrollno" runat="server" Text='<%# Eval("studentrollno") %>' 
                                CssClass="left"></asp:Label>
                        </ItemTemplate>
                            <HeaderStyle CssClass="left" Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:DataList ID="subjectno" runat="server" 
                                    Font-Bold="True" 
                                    Font-Italic="False" Font-Overline="False" Font-Strikeout="False" 
                                    Font-Underline="False" HorizontalAlign="Left" 
                                    RepeatDirection="Horizontal" style="margin-right: 0px" 
                                    onitemdatabound="subjectno_ItemDataBound">
                                    <ItemTemplate>
                                        <table align="left">
                                            <tr>
                                                <td style="border: 1px solid #000000; width: 1px">
                                                    <asp:Label ID="LBLT" runat="server"   CssClass="left"></asp:Label>
                                                </td>
                                                <td style="border: 1px solid #000000; width: 8px">
                                                   <asp:Label ID="LBLA" runat="server"  CssClass="left"></asp:Label>
                                                </td>
                                                <td style="border: 1px solid #000000; width: 8px">
                                                    <asp:Label ID="LBLP" runat="server"  CssClass="left"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                    <%--<SeparatorStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                        Font-Strikeout="False" Font-Underline="False" />--%>
                                </asp:DataList>
                            </ItemTemplate>
                            <HeaderTemplate>
                                <asp:DataList ID="subject" runat="server" CellPadding="4" 
                                    DataKeyField="subjectid" DataSourceID="SqlDataSource1" Font-Bold="True" 
                                    Font-Italic="False" Font-Overline="False" Font-Strikeout="False" 
                                    Font-Underline="False" ForeColor="#333333" HorizontalAlign="Left" 
                                    RepeatDirection="Horizontal" style="margin-right: 0px" 
                                    onitemdatabound="subject_ItemDataBound">
                                    <AlternatingItemStyle BackColor="White" />
                                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                    <ItemStyle BackColor="#EFF3FB" />
                                    <ItemTemplate>
                                        
                                            <table style="border-width:thin">
                                                <tr>
                                                    <td align="left" colspan="3">
                                                        <asp:Label ID="LBLID" runat="server" Text='<%# Eval("Subjectid") %>' 
                                                            Visible="False"></asp:Label>
                                                        <asp:Label ID="LblSN" runat="server" Text='<%# Eval("Subjectshorttitle") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="LBlST" runat="server" Text='<%# Eval("Subjecttypename") %>'></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table align="left">
                                                <tr>
                                                    <td style="border: 1px solid #000000; width: 8px">
                                                        T
                                                    </td>
                                                    <td style="border: 1px solid #000000; width: 8px">
                                                        A
                                                    </td>
                                                    <td style="border: 1px solid #000000; width: 8px">
                                                        %
                                                    </td>
                                                </tr>
                                            </table>
                                        
                                    </ItemTemplate>
                                    <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SeparatorStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                                        Font-Strikeout="False" Font-Underline="False" />
                                </asp:DataList>
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                    ConnectionString="Data Source=.\SQLEXPRESS;AttachDbFilename=|DataDirectory|\Kitmerp.mdf;Integrated Security=True;User Instance=True" 
                                    ProviderName="System.Data.SqlClient" 
                                    SelectCommand="SELECT DISTINCT tbsubject.subjectid,tbsubject.subjectshorttitle,tbsubjectype.subjecttypename  FROM            tbsubjectallot INNER JOIN tbsubject ON tbsubjectallot.subjectallotsubjectid = tbsubject.subjectid INNER JOIN tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid WHERE        (tbsubjectallot.subjectallotgroupid = @gid) AND (tbsubject.subjectsem = @sno)">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="DDLclass" Name="gid" 
                                            PropertyName="SelectedValue" />
                                        <asp:ControlParameter ControlID="DDLSem" Name="sno" 
                                            PropertyName="SelectedValue" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </HeaderTemplate>
                        </asp:TemplateField>
                        
                    </Columns>
                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView></div>
                    
                    
                    </td>
               </tr>
                
      </table>
      </asp:Panel>
      
            </asp:Panel></td></tr></table>
</asp:Content>

