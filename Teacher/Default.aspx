﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Teacher_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            height: 26px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td>
                Course</td>
            <td>
                <asp:DropDownList ID="DDLCourse" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLCourse_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Department</td>
            <td>
                <asp:DropDownList ID="DDLDept" runat="server">
                    <asp:ListItem>-- Select Department --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Session</td>
            <td>
                <asp:DropDownList ID="DDLSession" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLSession_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="style1">
                Class</td>
            <td class="style1">
                <asp:DropDownList ID="DDLClass" runat="server" AutoPostBack="True" 
                    Enabled="False" onselectedindexchanged="DDLClass_SelectedIndexChanged">
                    <asp:ListItem>-- Select Class --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="style1">
                </td>
        </tr>
        <tr>
            <td>
                Subject</td>
            <td>
                <asp:DropDownList ID="DDLSubject" runat="server" Enabled="False">
                    <asp:ListItem>-- Choose Subject --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>     
        <tr>
            <td>
                Sessional</td>
            <td>
                <asp:DropDownList ID="DDLSessional" runat="server" Enabled="False" 
                    AutoPostBack="True"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td>
                &nbsp;&nbsp;&nbsp;
                <%--<asp:LinkButton ID="LinkButton10" runat="server" onclick="LinkButton8_Click">Show Submitted Marks</asp:LinkButton>--%>
            </td>
        </tr>
        <tr>
            <td>
                Date</td>
            <td>
                <asp:TextBox ID="TxtDate" runat="server" Visible="False"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="TxtDate" Display="Dynamic" 
                    ErrorMessage="RequiredFieldValidator" Font-Bold="True" ForeColor="Red" 
                    ValidationGroup="abc">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator2" runat="server" 
                    ControlToValidate="TxtDate" ErrorMessage="CompareValidator" Font-Bold="True" 
                    ForeColor="Red" Operator="DataTypeCheck" Type="Date" ValidationGroup="abc">*</asp:CompareValidator>
                <asp:Label ID="LBLDate" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Max Marks
            </td>
            <td>
                <asp:TextBox ID="TXTMaxMarks" runat="server" Visible="False"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="TXTMaxMarks" Display="Dynamic" 
                    ErrorMessage="RequiredFieldValidator" Font-Bold="True" ForeColor="Red" 
                    ValidationGroup="abc">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator3" runat="server" 
                    ControlToValidate="TXTMaxMarks" ErrorMessage="CompareValidator" 
                    Font-Bold="True" ForeColor="Red" Operator="DataTypeCheck" Type="Integer" 
                    ValidationGroup="abc">*</asp:CompareValidator>
                <asp:Label ID="LBLMaxMarks" runat="server" Visible="False"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridFill" runat="server" Visible="False" 
                    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
                    GridLines="None" DataKeyNames="studentid">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Roll No">                        
                            <ItemTemplate>
                                <asp:Label ID="LblRollNo" runat="server" Text='<%# Eval("studentRollno") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Attendance">
                            <ItemTemplate>
                                <asp:RadioButtonList ID="RBLAttendance" runat="server" 
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="True">Present</asp:ListItem>
                                    <asp:ListItem Value="false">Absent</asp:ListItem>
                                </asp:RadioButtonList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Marks Obtained">
                            <ItemTemplate>
                                <asp:TextBox ID="TxtMarks" runat="server"></asp:TextBox>
                                <%--<asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" 
                                    ErrorMessage="Invalid Data" MinimumValue="0" Type="Double" 
                                    ControlToValidate="TxtMarks">*</asp:RangeValidator>--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TxtMarks" ErrorMessage="Invalid Data" ValidationGroup="abc">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                    ErrorMessage="Invalid Data" Operator="DataTypeCheck" Type="Double" 
                                    ControlToValidate="TxtMarks" ValidationGroup="abc">*</asp:CompareValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <asp:LinkButton ID="LinkButton2" runat="server" onclick="LinkButton2_Click" 
                    Visible="False" ValidationGroup="abc">Save</asp:LinkButton>
            </td>
            <td> 
                &nbsp;</td>
        </tr>
          <tr>
              <td colspan="2">
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  </td>
              <td>
                  &nbsp;</td>
          </tr>
    </table>
    </div>
    </form>
</body>
</html>
