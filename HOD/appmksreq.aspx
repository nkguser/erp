﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="appmksreq.aspx.cs" Inherits="HOD_appmksreq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Label ID="Label4" runat="server" Text="Label" Visible="false"></asp:Label>
        <div  style="overflow:scroll;height:500px;width:100%;visibility:inherit">
    <asp:DataList ID="DataList1" runat="server" DataKeyField="marksreqid" 
            CellPadding="4" ForeColor="#333333" 
            ondeletecommand="DataList1_DeleteCommand" OnItemDataBound="DataList1_ItemDataBound" oneditcommand="DataList1_EditCommand"  Width="100%">
            <AlternatingItemStyle BackColor="White" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <ItemStyle BackColor="#9BB4E6" Font-Bold="False" Font-Italic="False" 
                Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                Wrap="False" />
            <ItemTemplate>
            <table>
               <tr><td></td><td colspan='2'>
                    <p align="right">
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Delete" CommandArgument='<%# Eval("marksreqid") %>'>Reject</asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Edit" CommandArgument='<%# Eval("marksreqid") %>'>Approve</asp:LinkButton>
                    </p>
            </td>
            </tr>
            <tr>
            <td width="150px">
           <asp:Image ID="Image1" runat="server"  ImageUrl='<%#"~/Studentpic/"+Eval("studentphoto") %>' Height="100" Width="100"/>
            </td>
                

            <td width="150px">
            <b><asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>'/></b><br />
                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Studentrollno") %>'/><br />
                <asp:Label ID="Label3" runat="server" Text='<%# Eval("class") %>'/><br />
                <br />                
                </td>
                <td>                   
                 <asp:Label ID="Label5" runat="server" Text='<%# Eval("marksreqreqlvlid") %>' Visible="false"/>
                <b>  
                <asp:Label ID="subjectLabel" runat="server" Text='<%# Eval("subject") %>' /></b>
                    <br />
                    <b>Sessional </b>
                <asp:Label ID="marksreqsesnoLabel" runat="server" 
                    Text='<%# Eval("marksreqsesno") %>' /><br />
                <br />
                
                <b>New Attendance : </b>
                <asp:Label ID="marksreqnewattdLabel" runat="server" 
                    Text='<%# Eval("marksreqnewattd") %>' /> | 
                    <b>Prev Attendance : </b>
                <asp:Label ID="marksreqoldattdLabel" runat="server" 
                    Text='<%# Eval("marksreqoldattd") %>' />
                <br />              
                
                <b>Changed Marks : </b>
                <asp:Label ID="marksreqnewmarksLabel" runat="server" 
                    Text='<%# Eval("marksreqnewmarks") %>' /> | 
                    <b>Prev Marks : </b>
                <asp:Label ID="marksreqoldmaRKSLabel" runat="server" 
                    Text='<%# Eval("marksreqoldmaRKS") %>' />
                <br />
                <br />
                
                <b>Request Status </b>
                <asp:Label ID="marksreqreqstsLabel" runat="server" 
                    Text='<%# Eval("marksreqreqsts") %>' />
                <br />
                <b>Response Date :  </b>
                <asp:Label ID="marksreqapprovedateLabel" runat="server" 
                    Text='<%# Eval("marksreqapprovedate") %>' />                     
                </td>
                </tr>
                <tr>
                <td colspan="3">
                <b>Description :</b>
                <asp:Label ID="marksreqdescLabel" runat="server" 
                    Text='<%# Eval("marksreqdesc") %>' Wrap = "true" rows = "3" ReadOnly = "true" TextMode = "multiline" BorderStyle = "None" BorderWidth = "0"/>
                    <br /></td></tr>
                <tr>
            <td></td>
            <td colspan='2' align="right"><b>By : <i><u>
            <asp:Label ID="Label6" runat="server" Text='<%# Eval("tName") %>'/></u></i></b> </td></tr>
            
            </table>   
            </ItemTemplate>
            <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        </asp:DataList></div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

