﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/InnerStudent.master" AutoEventWireup="true" CodeFile="Messages.aspx.cs" Inherits="Student_Messages" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<head id="Head1">
<style type="text/css">
.fancy-green .ajax__tab_header
{
	background: url(../green_bg_Tab.gif) repeat-x;
	cursor:pointer;
}
.fancy-green .ajax__tab_hover .ajax__tab_outer, .fancy-green .ajax__tab_active .ajax__tab_outer
{
	background: url(../green_left_Tab.gif) no-repeat left top;
}
.fancy-green .ajax__tab_hover .ajax__tab_inner, .fancy-green .ajax__tab_active .ajax__tab_inner
{
	background: url(../green_right_Tab.gif) no-repeat right top;
}
.fancy .ajax__tab_header
{
	font-size: 13px;
	font-weight: bold;
	color: #000;
	font-family: sans-serif;
}
.fancy .ajax__tab_active .ajax__tab_outer, .fancy .ajax__tab_header .ajax__tab_outer, .fancy .ajax__tab_hover .ajax__tab_outer
{
	height: 46px;
}
.fancy .ajax__tab_active .ajax__tab_inner, .fancy .ajax__tab_header .ajax__tab_inner, .fancy .ajax__tab_hover .ajax__tab_inner
{
	height: 46px;
	margin-left: 16px; /* offset the width of the left image */
}
.fancy .ajax__tab_active .ajax__tab_tab, .fancy .ajax__tab_hover .ajax__tab_tab, .fancy .ajax__tab_header .ajax__tab_tab
{
	margin: 16px 16px 0px 0px;
}
.fancy .ajax__tab_hover .ajax__tab_tab, .fancy .ajax__tab_active .ajax__tab_tab
{
	color: #fff;
}
.fancy .ajax__tab_body
{
	font-family: Arial;
	font-size: 10pt;
	border-top: 0;
	height:400px;
	border:1px solid #999999;
	padding: 8px;
	background-color: #ffffff;
}

</style>
</head>
<div>

<ajax:TabContainer ID="TabContainer1" runat="server" CssClass="fancy fancy-green">

<ajax:TabPanel ID="tbpnluser" runat="server" >
<HeaderTemplate>
Inbox
</HeaderTemplate>
<ContentTemplate>
<asp:Panel ID="UserReg" runat="server">
<center> <img src="../Updation.jpg" /></center>
</asp:Panel>
</ContentTemplate>
</ajax:TabPanel>

<ajax:TabPanel ID="tbpnlusrdetails" runat="server" >
<HeaderTemplate>
Outbox
</HeaderTemplate>
<ContentTemplate>
<asp:Panel ID="Panel1" runat="server"><center> <img src="../Updation.jpg" /></center>
</asp:Panel>
</ContentTemplate>
</ajax:TabPanel>
<ajax:TabPanel ID="tbpnljobdetaiddls" runat="server" >
<HeaderTemplate>
Compose
</HeaderTemplate>
<ContentTemplate>
<asp:Panel ID="Panel2" runat="server">
   <center> <img src="../Updation.jpg" /></center>
</asp:Panel>
</ContentTemplate>
</ajax:TabPanel>
</ajax:TabContainer>
</div>
</asp:Content>

