﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="StudentSessional.aspx.cs" Inherits="Teacher_StudentSessional" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
      <table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td style="width: 265px">
                Session</td>
            <td colspan="2">
                <asp:DropDownList ID="DDLSession" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLSession_SelectedIndexChanged" Width="300px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 265px">
                Class</td>
            <td colspan="2">
                <asp:DropDownList ID="DDLClass" runat="server" AutoPostBack="True" 
                    Enabled="False" onselectedindexchanged="DDLClass_SelectedIndexChanged" 
                    Width="300px">
                    <asp:ListItem>-- Select Class --</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 265px">
                Subject</td>
            <td colspan="2">
                <asp:DropDownList ID="DDLSubject" runat="server" Enabled="False" Width="300px" 
                    AutoPostBack="True" 
                    onselectedindexchanged="DDLSubject_SelectedIndexChanged">
                    <asp:ListItem>-- Choose Subject --</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 265px">
                Choose Option</td>
            <td align="left" colspan="2">
                <asp:RadioButtonList ID="RBLOpt" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="RBLOpt_SelectedIndexChanged" 
                    RepeatDirection="Horizontal" Enabled="False">
                    <asp:ListItem Value="0">Fill</asp:ListItem>
                    <asp:ListItem Value="1">View</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr></table >
        <asp:Panel ID="fillshowpanel" runat="server" Visible="false">
        <table width="100%" style="text-align: left; font-weight: bold;">
        
        <tr>
            <td style="width: 265px">
                Sessional</td>
            <td colspan="2">
                <asp:DropDownList ID="DDLSessional" runat="server" 
                    AutoPostBack="True" Width="300px" 
                    onselectedindexchanged="DDLSessional_SelectedIndexChanged"/>
               
            </td>
        </tr>
        <tr>
            <td style="width: 265px">
                Date</td>
            <td colspan="2">
                <asp:Label ID="LBLDate" runat="server"></asp:Label>
                <asp:TextBox ID="TxtDate" runat="server" Visible="False" Width="300px"></asp:TextBox>
                <asp:CalendarExtender Format="dd/MM/yyyy "  ID="TxtDate_CalendarExtender" runat="server" 
                    Enabled="True" TargetControlID="TxtDate">
                </asp:CalendarExtender>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="TxtDate" Display="Dynamic" 
                    ErrorMessage="RequiredFieldValidator" Font-Bold="True" ForeColor="Red" 
                    ValidationGroup="abc">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator2" runat="server" 
                    ControlToValidate="TxtDate" ErrorMessage="CompareValidator" Font-Bold="True" 
                    ForeColor="Red" Operator="DataTypeCheck" Type="Date" ValidationGroup="abc">*</asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td style="width: 265px">
                Max Marks
            </td>
            <td colspan="2">
                <asp:Label ID="LBLMaxMarks" runat="server"></asp:Label>
                <asp:TextBox ID="TXTMaxMarks" runat="server" Visible="False" Width="300px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="TXTMaxMarks" Display="Dynamic" 
                    ErrorMessage="RequiredFieldValidator" Font-Bold="True" ForeColor="Red" 
                    ValidationGroup="abc">*</asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator3" runat="server" 
                    ControlToValidate="TXTMaxMarks" ErrorMessage="CompareValidator" 
                    Font-Bold="True" ForeColor="Red" Operator="DataTypeCheck" Type="Integer" 
                    ValidationGroup="abc">*</asp:CompareValidator>
            </td>
        </tr>
        </table>
        </asp:Panel>
        <table  width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td style="height: 18px;" colspan="3">
                <asp:Label ID="LBLmsg" runat="server" Font-Bold="True" ForeColor="#003300"></asp:Label>
                </td>
        </tr>
        </table>
        <asp:Panel ID="fillpanel" runat="server" Visible="false">
      <table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td ><div  style="overflow:scroll;height:300px;visibility:inherit">
                <asp:GridView ID="GridFill" runat="server" 
                    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
                    GridLines="None" DataKeyNames="studentid" 
                    EmptyDataText="No students are  Present in this Group." Width="100%">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Roll No">                        
                            <ItemTemplate>
                                <asp:Label ID="LblRollNo" runat="server" Text='<%# Eval("studentRollno") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Attendance">
                            <ItemTemplate>
                                <asp:RadioButtonList ID="RBLAttendance" runat="server" 
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True" Value="True">Present</asp:ListItem>
                                    <asp:ListItem Value="false">Absent</asp:ListItem>
                                </asp:RadioButtonList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Marks Obtained">
                            <ItemTemplate>
                                <asp:TextBox ID="TxtMarks" runat="server"></asp:TextBox>
                                <%--<asp:RangeValidator ID="RangeValidator1" runat="server" Display="Dynamic" 
                                    ErrorMessage="Invalid Data" MinimumValue="0" Type="Double" 
                                    ControlToValidate="TxtMarks">*</asp:RangeValidator>--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TxtMarks" ErrorMessage="Invalid Data" ValidationGroup="abc">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                    ErrorMessage="Invalid Data" Operator="DataTypeCheck" Type="Double" 
                                    ControlToValidate="TxtMarks" ValidationGroup="abc">*</asp:CompareValidator>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView></div>
            </td>
            </tr>
            <tr>
            <td>
            <asp:LinkButton ID="LinkButton2" runat="server" onclick="LinkButton2_Click" 
                    ValidationGroup="abc">Save</asp:LinkButton>
            </td>
            </tr>
            </table>
            </asp:Panel>
            <asp:Panel ID="viewpanel" runat="server" Visible="false">
      <table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
        <td><div  style="overflow:scroll;height:300px;visibility:inherit">
             <asp:GridView ID="GridShow" runat="server" 
                    AutoGenerateColumns="False" onrowcancelingedit="GridShow_RowCancelingEdit" 
                    onrowdatabound="GridShow_RowDataBound" onrowediting="GridShow_RowEditing" 
                    onrowupdating="GridShow_RowUpdating" CellPadding="4" ForeColor="#333333" 
                    GridLines="None" DataKeyNames="studentid" Width="100%">
                
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Roll No">                            
                            <ItemTemplate>
                                <asp:Label ID="LblRollNo" runat="server" Text='<%# Eval("studentrollno") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Attendance">
                            <EditItemTemplate>
                                <asp:RadioButtonList ID="RBLAttendance" runat="server" 
                                    RepeatDirection="Horizontal">
                                    <asp:ListItem Value="True" Selected="True">Present</asp:ListItem>
                                    <asp:ListItem Value="false">Absent</asp:ListItem>
                                </asp:RadioButtonList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LBLAttend" runat="server" Text='<%# Eval("studentSessionalattnd") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Marks Obtained">
                            <EditItemTemplate>
                                <asp:TextBox ID="TxtMarks" runat="server" Text='<%# Eval("studentSessionalMarks") %>'></asp:TextBox>
                                <%--<asp:RangeValidator ID="RangeValidator1" runat="server" 
                                    ControlToValidate="TxtMarks" Display="Dynamic" ErrorMessage="Invalid Data" 
                                    MinimumValue="0" MaximumValue="20" Type="Double">range</asp:RangeValidator>--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TxtMarks" ErrorMessage="Invalid Data" ValidationGroup="abc">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                    ControlToValidate="TxtMarks" ErrorMessage="Invalid Data" 
                                    Operator="DataTypeCheck" Type="Double" ValidationGroup="abc">*</asp:CompareValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="LBLMarks" runat="server" Text='<%# Eval("studentSessionalMarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:LinkButton ID="LBUpdate" runat="server" CommandName="Update" 
                                    Text="Update"></asp:LinkButton>
                                &nbsp;&nbsp;&nbsp;
                                <asp:LinkButton ID="LBCancel" runat="server" CommandName="Cancel" 
                                    Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LBEdit" runat="server" CommandName="Edit"
                                    Text="Edit" ValidationGroup="abc"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView></div>
           </td>
           </tr>
           <tr>
           <td>
           <asp:LinkButton ID="LnkBtnSubmitHod" runat="server" 
                      onclick="LnkBtnSubmitHod_Click">Submit to Head Of Deptt.</asp:LinkButton>
           </td>
           </tr>
           </table>
           </asp:Panel>
           <asp:Panel ID="submitpanel" runat="server" Visible="false">
      <table width="100%" style="text-align: left; font-weight: bold;">
           <tr>
            <td> <div  style="overflow:scroll;height:300px;visibility:inherit">
               <asp:GridView ID="GridshowSubmit" runat="server" 
                    AutoGenerateColumns="False" onrowdatabound="GridShow_RowDataBound" 
                    CellPadding="4" ForeColor="#333333" 
                    GridLines="None" DataKeyNames="studentid" Width="100%">
                
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Roll No">                            
                            <ItemTemplate>
                                <asp:Label ID="LblRollNo" runat="server" Text='<%# Eval("studentrollno") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Attendance">                         
                            <ItemTemplate>
                                <asp:Label ID="LBLAttend" runat="server" Text='<%# Eval("studentSessionalattnd") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Marks Obtained">                        
                            <ItemTemplate>
                                <asp:Label ID="LBLMarks" runat="server" Text='<%# Eval("studentSessionalMarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                        
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView></div>             
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>

