﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="frmhodnewalb.aspx.cs" Inherits="HOD_frmhodnewalb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="width: 100%">
        <tr>
            <td align=left colspan="2">
                <h1>
                    <asp:Label ID="Label1" runat="server"></asp:Label>
                </h1>
            </td>
        </tr>
        <tr>
            <td style="width: 205px" align=left>
                Album Name</td>
            <td align=left>
                <asp:TextBox ID="TextBox1" runat="server" Width="500px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 205px" align=left>
                Template</td>
            <td align=left>
                <asp:DropDownList ID="DropDownList1" runat="server" 
                    DataSourceID="ObjectDataSource1" DataTextField="p_tmpnam" 
                    DataValueField="p_tmpcod">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="width: 205px" align=left>
                Status</td>
            <td align=left>
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" Height="16px" 
                    RepeatDirection="Horizontal" Width="320px">
                    <asp:ListItem Value="P">Public</asp:ListItem>
                    <asp:ListItem Selected="True" Value="R">Private</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td style="width: 205px">
                &nbsp;</td>
            <td align=left>
                <asp:Button ID="Button1" runat="server" Text="Create" onclick="Button1_Click" />
&nbsp;
                <asp:Button ID="Button2" runat="server" Text="Cancel" />
            </td>
        </tr>
        <tr>
            <td style="width: 205px">
                &nbsp;</td>
            <td>
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                    SelectMethod="Disp_rec" TypeName="nsphotosnak.clstmp">
                </asp:ObjectDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

