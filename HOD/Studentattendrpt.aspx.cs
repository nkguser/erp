﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class HOD_Studentattendrpt : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            GetData();
            DDLSessionBind();
        }
    }
    private void GetData()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        DDLSem.Items.Clear();
        SqlCommand cmd = new SqlCommand("SELECT tbCourse.coursesemester FROM tbDep INNER JOIN tbCourse ON tbDep.depCourseid = tbCourse.Courseid WHERE (tbDep.Depid = @did)", con);
        Int32 a = Convert.ToInt32(Session["depid"]);
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        int count = Int32.Parse(dr["Coursesemester"].ToString());
        DDLSem.Items.Insert(0, "-- Choose Semester --");
        for (int i = 1; i <= count; i++)
        {
            DDLSem.Items.Add(i.ToString());
        }
        dr.Close();
        cmd.Dispose();
        con.Close();
    }
    private void DDLSessionBind()
    {
        DDLSession.DataTextField = "session";
        DDLSession.DataValueField = "sessionid";
        DDLSession.DataSource = ERP.AllSession();
        DDLSession.DataBind();
        DDLSession.Items.Insert(0, "-- Select session --");
    }
    protected void DDLSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDLClass.DataTextField = "Classname";
        DDLClass.DataValueField = "classid";
        DDLClass.DataSource = ERP.AllClass(Convert.ToInt32(Session["depid"]), Convert.ToInt32(DDLSession.SelectedValue));
        DDLClass.DataBind();
        DDLClass.Enabled = true;        
        DDLClass.Items.Insert(0, "-- Select Class --");
    }
    protected void DDLClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDLGrp.DataTextField = "Groupname";
        DDLGrp.DataValueField = "Groupid";
        DDLGrp.DataSource = ERP.AllGroup(Convert.ToInt32(DDLClass.SelectedValue));
        DDLGrp.DataBind();
        DDLGrp.Enabled = true;
        DDLGrp.Items.Insert(0, "-- Select Group --");

    }   
    protected void DDLSem_SelectedIndexChanged(object sender, EventArgs e)
    {
       
    }
}