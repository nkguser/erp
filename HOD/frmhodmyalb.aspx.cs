﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class HOD_frmhodmyalb : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Image img = (Image)(e.Item.FindControl("img1"));
            LinkButton lk = (LinkButton)(e.Item.FindControl("lk5"));
            Int32 a = Convert.ToInt32(lk.CommandArgument);
            nsphotosnak.clsalb obj = new nsphotosnak.clsalb();
            List<nsphotosnak.clsalbprp> k = obj.Find_rec(a);
            if (k[0].p_albsts == 'P')
                lk.Visible = true;
            else
                lk.Visible = false;
            if (k[0].p_albcvrpic == "nopic.jpg")
                img.ImageUrl = "~/nopic.jpg";
            else
                img.ImageUrl = "~/albums/" + a.ToString() + "//" + k[0].p_albcvrpic;
        }
    }
    protected void DataList1_UpdateCommand(object source, DataListCommandEventArgs e)
    {
        Int32 a = Convert.ToInt32(e.CommandArgument);
        Response.Redirect("frmhodalbdet.aspx?acod=" + a.ToString());
    }
    protected void DataList1_CancelCommand(object source, DataListCommandEventArgs e)
    {
        Int32 a = Convert.ToInt32(e.CommandArgument);
        nsphotosnak.clsalb obj = new nsphotosnak.clsalb();
        List<nsphotosnak.clsalbprp> k = obj.Find_rec(a);
        Response.Redirect("~/Common/frmhodgallerypage.aspx?acod=" + a.ToString() + "&id=" + k[0].p_albtmpcod);
    }
    protected void DataList1_EditCommand(object source, DataListCommandEventArgs e)
    {
        Int32 a = Convert.ToInt32(e.CommandArgument);
        Response.Redirect("frmhodnewalb.aspx?acod=" + a.ToString());
    }
    protected void DataList1_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        Int32 a = Convert.ToInt32(e.CommandArgument);
        nsphotosnak.clsalb obj = new nsphotosnak.clsalb();
        nsphotosnak.clsalbprp objprp = new nsphotosnak.clsalbprp();
        objprp.p_albcod = a;
        obj.Delete_Rec(objprp);
        DataList1.DataBind();
        DirectoryInfo di = new DirectoryInfo(Server.MapPath("../albums") + "//" + a.ToString());
        if (di.Exists == true)
            di.Delete(true);
    }
    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "save")
        {
            Int32 a = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("frmhodshr.aspx?acod=" + a.ToString());
        }
    }
}