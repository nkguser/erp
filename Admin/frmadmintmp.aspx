﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/InnerAdmin.master" AutoEventWireup="true" CodeFile="frmadmintmp.aspx.cs" Inherits="Admin_frmadmintmp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <table style="width: 100%">
        <tr>
            <td style="width: 237px">
                <h1>
                    Display Templates</h1>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 237px">
                Template Name</td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 237px">
                PageID</td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server" Width="300px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 237px">
                &nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Submit" />
&nbsp;
                <asp:Button ID="Button2" runat="server" Text="Cancel" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="p_tmpcod" DataSourceID="ObjectDataSource1" 
                    onrowdeleting="GridView1_RowDeleting" onrowediting="GridView1_RowEditing" 
                    Width="697px">
                    <Columns>
                        <asp:BoundField DataField="p_tmpnam" HeaderText="Template Name" 
                            SortExpression="p_tmpnam" />
                        <asp:BoundField DataField="p_tmpdsppag" HeaderText="PageID" 
                            SortExpression="p_tmpdsppag" />
                        <asp:CommandField ShowEditButton="True" />
                        <asp:CommandField ShowDeleteButton="True" />
                    </Columns>
                </asp:GridView>
                <br />
                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                    SelectMethod="Disp_rec" TypeName="nsphotosnak.clstmp">
                </asp:ObjectDataSource>
            </td>
        </tr>
    </table>
</asp:Content>

