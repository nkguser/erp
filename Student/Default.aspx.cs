﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;

public partial class _Default : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();

        Draw(Convert.ToInt32(Request.QueryString["Prec"]));
    }
   
    private void Draw(Int32 p)
    {
        Bitmap objBitmap = new Bitmap(201, 18);
        Graphics objGraphics = Graphics.FromImage(objBitmap);
        objGraphics.FillRectangle(new SolidBrush(Color.Beige), 0, 0, 1050, 18);
        Rectangle rec = new Rectangle(0, 0, 199, 17);
        objGraphics.FillRectangle(new SolidBrush(Color.Transparent), rec);
        Pen pn = new Pen(Color.Green,17);
        if (p >= 75)
            pn.Color = Color.Green;

        else if (p >= 60 && p < 75)
        {
            pn.Color =Color.Gold;
        }
        else
        {
            pn.Color = Color.Red;
        }
       
        objGraphics.DrawLine(pn, 1, 8, p*2, 8);
        objGraphics.DrawRectangle(new Pen(Color.Black), rec);
        Response.Clear();
        Response.ContentType = "image/Jpeg";
        objBitmap.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
        objGraphics.Dispose();
        objBitmap.Dispose();
    }
}