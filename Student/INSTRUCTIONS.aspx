﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/InnerStudent.master" AutoEventWireup="true" CodeFile="INSTRUCTIONS.aspx.cs" Inherits="Student_INSTRUCTIONS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <table style="width: 100%">
        <tr>
            <td>
                <div style="FONT-WEIGHT: bold; color: #003300;">
                    Instructions :</div>
                <ul sizcache="5" sizset="45" style="color: #009900">                    
                    <li><strong>Large bank of questions per test, questions regularly refreshed</strong></li>
                    <li><strong>Users can take any test for free, can retake a test typically after 10 days Only</strong></li>
                    <li><strong>Typically 45 minutes to complete test, </strong> </li>
                        <li><strong>Do not Close/Refresh your browser window during test sessions </strong> </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td>
                <table id="TABLE_9">
                    <tr>
                        <td>
                            <span class="header" style="color: #003300"><strong>Rules for taking the test</strong></span></td>
                    </tr>
                </table>
                <table id="TABLE_10" border="1" cellpadding="3" cellspacing="0" 
                    style="border: 1px solid #999999; WIDTH: 100%; FONT-SIZE: inherit; border-collapse: collapse; border-spacing: 0; text-indent: 0;">
                    <tr style="color: #FF66CC">
                        <td style="border-style: none; border-color: inherit; border-width: medium; color: #CC0066;">
                            Duration:
                        </td>
                        <td style="color: #CC0066">
                            45 minutes
                        </td>
                    </tr>
                    <tr style="color: #FF66CC">
                        <td style="border-style: none; border-color: inherit; border-width: medium; color: #CC0066;" 
                            valign="top">
                            Number of Questions:
                        </td>
                        <td style="color: #CC0066">
                            35 Multiple Choice questions.<br />
                            Each question has between 1 and 6 options of which 1 or more may be correct
                        </td>
                    </tr>
                    <tr style="color: #FF66CC">
                        <td style="border-style: none; border-color: inherit; border-width: medium; color: #CC0066; text-align: center; font-size: small;" 
                            valign="top" colspan="2">
                            &nbsp;</td>
                    </tr>
                </table>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
            </td>
        </tr>
    </table>
</asp:Content>

