﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="frmhodalbdet.aspx.cs" Inherits="HOD_frmhodalbdet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h2 >
        <asp:Label ID="Label1" runat="server"></asp:Label>
    </h2>
    <table style="width: 100%" align="center">
        <tr>
            <td style="width: 143px" align="left">
                <h4>
                    Upload Picture</h4>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 143px" align="left">
                Select Picture</td>
            <td align="left">
                <asp:FileUpload ID="FileUpload1" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width: 143px" align="left">
                Description</td>
            <td align="left">
                <asp:TextBox ID="TextBox1" runat="server" Height="67px" TextMode="MultiLine" 
                    Width="518px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 143px">
                &nbsp;</td>
            <td align="left">
                <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Upload" />
&nbsp;&nbsp;
                <asp:Button ID="Button2" runat="server" Text="Cancel" />
            </td>
        </tr>
    </table>
    <p>
        <asp:DataList ID="DataList1" runat="server" DataSourceID="ObjectDataSource1" 
            RepeatColumns="5" RepeatDirection="Horizontal" Width="916px" 
            DataKeyField="p_albdetcod" ondeletecommand="DataList1_DeleteCommand" 
            oneditcommand="DataList1_EditCommand">
            <ItemTemplate>
             <centre>
             <img src='../albums/<%#Eval("p_albdetalbcod") %>/<%#Eval("p_albdetcod") %><%#Eval("p_albdetpic") %>' height="95px"
             width="95px" /><br />
             <asp:LinkButton id="lk1" runat=server CommandName="Edit"
             Text="Set as cover pic" commandargument='<%#Eval("p_albdetpic") %>' /><br />
             <asp:LinkButton id="lk2" runat=server commandname="Delete"
             Text="Delete"  commandargument='<%#Eval("p_albdetpic") %>'/>
             </centre>
            </ItemTemplate>
        </asp:DataList>
    </p>
    <p>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            SelectMethod="Display_Rec" TypeName="nsphotosnak.clsalbdet" 
            OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:QueryStringParameter Name="albcod" QueryStringField="acod" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </p>
</asp:Content>

