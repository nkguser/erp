﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/InnerStudent.master" AutoEventWireup="true" CodeFile="attendance.aspx.cs" Inherits="Student_attendance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      <table width="100%" style="text-align: left; font-weight: bold;">
       <tr>
            <td>
                Semester</td>
            <td>
                <asp:DropDownList ID="DDLSem" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLSem_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                </td>
        </tr>
        <tr>
            <td>
                Choose Option</td>
            <td>
                <asp:RadioButtonList ID="RBLOpt" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="RBLOpt_SelectedIndexChanged">
                    <asp:ListItem Selected="True">Summary</asp:ListItem>
                    <asp:ListItem>Detailed</asp:ListItem>
                </asp:RadioButtonList>
                <asp:RadioButtonList ID="RBLDetailOpt" runat="server" AutoPostBack="True" 
                    Visible="False" onselectedindexchanged="RBLDetailOpt_SelectedIndexChanged">
                    <asp:ListItem Selected="True" Value="subject">Subject-wise</asp:ListItem>
                    <asp:ListItem Value=" tbstudentattnd.studentattndDate">Date-wise</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
               </td>
        </tr>
        </table>        
        <table>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridShowDetail" runat="server" AutoGenerateColumns="False" 
                    EmptyDataText="No Records Found" 
                    onrowdatabound="GridShowDetail_RowDataBound" Visible="False" 
                    CellPadding="4" DataKeyNames="studentattndsubjectid" ForeColor="#333333" 
                    GridLines="None" Width="865px">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Subject">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("studentattnddate","{0:d}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Attendance">
                            <ItemTemplate>
                                <asp:Label ID="LBLAttend" runat="server" 
                                    Text='<%# Eval("studentattndattend") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <asp:GridView ID="GridShowSummary" runat="server" Visible="False" 
                    AutoGenerateColumns="False" onrowdatabound="GridShowSummary_RowDataBound" 
                    CellPadding="4" ForeColor="#333333" 
                    GridLines="None" DataKeyNames="studentattndsubjectid" Width="865px">
                
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Subject">
                            <ItemTemplate>
                                <asp:Label ID="LblSubject" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Total Attended">
                             <ItemTemplate>
                                 <asp:Label ID="LblAt" runat="server"></asp:Label>
                             </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Delivered">
                            <ItemTemplate>
                                <asp:Label ID="LblTotal" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="%age">
                            <ItemTemplate>
                                <asp:Label ID="Lblpercent" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td>              
            </td>
        </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

