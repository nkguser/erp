﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" MasterPageFile="~/Student/InnerStudent.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Student_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <head>
    <style type="text/css"> 
    .abc
    {
        font-size:larger;
        font-weight:bold;
        cursor:pointer
    }
    .xyz
    {
        display:none;
        visibility:hidden
    }
    .black_overlay
    {
        display:none;
        position: absolute;
        top: 4%;
        left: 0%;
        width: 100%;
        height:155%;
        background-color:black;
        z-index:auto;
        overflow: auto;
        -moz-opacity: 0.8;
        opacity:100;
        filter: alpha(opacity=100);
    }
    .white_content 
    {
        display:none;
        position: absolute;
        top: 10%;
        left: 19%;
        width:880px;
        padding: 0px;
        border: 0px solid #a6c25c;
        background-color: white;
        z-index:1002;
        overflow: auto;
     }
     .headertext
     {
        font-family:Arial, Helvetica, sans-serif;
        font-size:14px;
        color:#f19a19;
        font-weight:bold;
     }

    </style>
    <script type="text/javascript" language=javascript>
//        function GetData(id) {
//            var e;
//            e = document.getElementById('d' + id);
//            if (e) {

//                if (e.style.display != "block") {
//                    e.style.display = "block";
//                    e.style.visibility = "visible";
//                }
//                else {
//                    e.style.display = "none";
//                    e.style.visibility = "hidden";
//                }
//            }

//        }
//        function GetData1(id) {
//            var e1;
//            e1 = document.getElementById('g' + id);
//            if (e1) {

//                if (e1.style.display != "block") {
//                    e1.style.display = "block";
//                    e1.style.visibility = "visible";
//                }
//                else {
//                    e1.style.display = "none";
//                    e1.style.visibility = "hidden";
//                }
//            }

//        }
        function ShowImages() {
            document.getElementById('light').style.display = 'block';
            document.getElementById('fade').style.display = 'block'
            return false;
        }
    </script>
</head>
    <table>
 <tr>
        <td style="border:1px solid #2D4262; width:865px;height:125px">
        <asp:Panel ID="Panel1" runat="server">
                    <table class="styledmenu">
                        <tr>
                            <td align="left">
                                <asp:Image ID="Image2" runat="server" Width="80px"  Height="80px" />
                            </td>
                            <td style="height: 95px">
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table class="styledmenu">
                                    <tr>
                                        <td  align="left">
                                            <asp:Label ID="lblname" runat="server" ></asp:Label>
                                            <br />
                                            <asp:Label ID="lblroll" runat="server"></asp:Label>
                                            <br />
                                            <asp:Label ID="lbldep" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
        </td>
        </tr>
  </table>
    <table >
        <tr>
        <td style="border:1px solid #2D4262; width:865px;height:500px">
        
            <table >
            <th  style="border:1px solid #2D4262; width:260px" align=center bgcolor="#2D4262"><b style="color:White">NoticeBoard</b></th>
             <th   style="border:1px solid #2D4262; width:260px" align=center bgcolor="#2D4262"><b style="color:White">Branch Alerts</b></th>
             <th align=center   style="border:1px solid #2D4262; width:320px" bgcolor="#2D4262"><b style="color:White">Events</b></th>
                <tr>
                    <td  align="left"  valign="top" 
                        style="border:1px solid #2D4262; width:260px;height:200px;float:none ">
                                                     <center>
                   <asp:DataList ID="DataList1" runat="server"  DataKeyField="noticeid" onitemcommand="DataList1_ItemCommand" style="text-align: left" Width="100%"  >
                                        <ItemTemplate>
                                            <table width="100%" style="text-align: left; font-weight: bold;"><tr>
                                                <td><asp:Label ID="LBLS" runat="server" Text='<%#Eval("noticesubject") %>'></asp:Label></td>    
                                                    <td><asp:Label ID="LBLD" runat="server" Text='<%#Eval("noticedate","{0:d}") %>'></asp:Label></td>
                                                        <td><asp:ImageButton ID="imgbtn" ImageUrl="~/images/info.png" Height="15px" Width="15px" runat="server" CommandName="Show" />
                                                                    <%-- onclick="imgbtn_Click" />--%></td></tr></table>
                                        </ItemTemplate>
                            </asp:DataList></center></td>
                    <td style="border:1px solid #2D4262; width:260px;height:200px" align="left" valign="top">
                         <center><asp:Label ID="LBLattdshrt" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                         <asp:DataList ID="DataList2" runat="server" 
                             onitemcommand="DataList2_ItemCommand" DataKeyField="noticeid" 
                             style="text-align: left" Width="100%" >
                <ItemTemplate>
                <table width="100%" style="text-align: left; font-weight: bold;"><tr>
                <td><asp:Label ID="LBLS1" runat="server" Text='<%#Eval("noticesubject") %>'></asp:Label> </td>
                <td><asp:Label ID="LBLD1" runat="server" Text='<%#Eval("noticedate","{0:d}") %>'></asp:Label></td>
                <td><asp:ImageButton ID="imgbtn1" ImageUrl="~/images/info.png" Height="15px" Width="15px" runat="server" CommandName="Show" />
                </td></tr></table></ItemTemplate>
               
                </asp:DataList></center></td>
                    <td style="border:1px solid #2D4262; width:280px;height:250px">
                        <table border="0" cellpadding="2" cellspacing="2" style="margin-top: 0px; border: 1px solid #CCCCCC;
        padding: 1px;" width="300px">
                            <tr>
                                <td valign="top" style="width:320px;height:250px">
                                    
                                        <asp:DataList ID="DataList3" runat="server" DataKeyField="Eventid" 
                                            onitemcommand="DataListE_ItemCommand" RepeatColumns="3" 
                                            RepeatDirection="Horizontal" ShowFooter="False" Width="100%">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="Image1" runat="server" CommandName="Select" Height="50px" 
                                                                ImageUrl='<%#"~/event/"+Eval("Eventlogo") %>' 
                                                                ToolTip='<%# Eval("EventNAme") %>' Width="50px" />
                                                        </td>
                                                        <td>
                                                            <h2>
                                                            </h2>
                                        <br />
                                        <br />
                                                        </td>
                                                    </tr>
                                                </table>
                            <br />
                            <br />
                                            </ItemTemplate>
                                        </asp:DataList>
                                   
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <table width="100%" style="text-align: left; font-weight: bold;">
                <th align="center" style="border:1px solid #2D4262; width:570px"  bgcolor="#2D4262"><b style="color:White">Resources & Updates</b></th>
                <th align="center" style="border:1px solid #2D4262; width:330px"  bgcolor="#2D4262"><b style="color:White">Attendance</b></th>
                <tr>
                <td style="border:1px solid #2D4262; width:570px;height:250px">
                      <b>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  No Record Found</b>
                      </td>
                    <td style="border:1px solid #2D4262; width:330px;height:250px" align=right>
                        <table style="text-align: left; font-weight: bold;  width: 100%;">
                            <tr>
                                <td>
                                    &nbsp;</td>
                                <td> 
                                </td>
                                <td align="right">
                                   <asp:DropDownList ID="DDLSem" runat="server" AutoPostBack="True" 
                                        onselectedindexchanged="DDLSem_SelectedIndexChanged" style="text-align: right">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            </table>
                            <table style="text-align: left; font-weight: bold; height: 241px;" width="320px">
                            <tr >
                                <td valign="top"  >
                                
                                    <asp:GridView ID="GridShowSummary" runat="server" AutoGenerateColumns="False" 
                                        DataKeyNames="studentattndsubjectid" GridLines="None" 
                                        onrowdatabound="GridShowSummary_RowDataBound" ShowHeader="False" Width="320px" 
                                         >
                                        <Columns>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblSubject" runat="server" Font-Bold="True" 
                                                        Text='<%# Eval("Subject") %>'></asp:Label>
                                                    
                                                </ItemTemplate>
                                                <ItemStyle Wrap="False"  />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Attended" ShowHeader="False" 
                                                Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblAt" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Delivered" ShowHeader="False" 
                                                Visible="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblTotal" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    <asp:Image ID="imggrp" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="False">
                                                <ItemTemplate>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Lblpercent" runat="server" Font-Bold="True"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                                
                            </tr>
                        </table>
                    </td>
                </tr>
                </table>
            </table>
        
        </td>
            
        </tr>
    </table>
    
<div id="light" class="white_content">
     <table cellpadding=0 cellspacing=0 border=0 style="background-color:#a6c25c;" width="100%">
        <tr>
            <td height="16px"><a href = "javascript:void(0)" onclick = "document.getElementById('light').style.display='none';document.getElementById('fade').style.display='none'"><img src="../close.gif" style="border :0px"  width="13px" align="right" height="13px"/></a></td>
        </tr>
        <tr>
            <td style="padding-left:16px;padding-right:16px;padding-bottom:16px"> 
                <table align="center"  border="0" cellpadding="0" cellspacing="0" style="background-color:#fff" width="100%">
                    <tr>
                        <td align="center" colspan="2" class="headertext" ></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Repeater ID="imglightbox" runat="server">
                                <ItemTemplate>
                
                                    <p align="left" ><%#Eval("noticesubject") %><br />       
                                    <i><%#Eval("noticedate","{0:d}") %></i></p><br /><br />
                                    <%#Eval("noticecontent") %><br />
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td height="10px"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div align="center" class=" headertext">
    </div>
</div>
<div id="fade" class="black_overlay">
</div>   
</asp:Content>

