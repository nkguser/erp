﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class dir_ViewSyllabusDir : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
    }
    private void dlist_bind()
    {
        SqlCommand cmd = new SqlCommand("select depsyl from tbdep where depcourseid=@cid", con);
        cmd.Parameters.Add("cid", SqlDbType.Int).Value = Convert.ToInt32(ViewState["cod"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DataList1.DataSource = ds;
        DataList1.DataBind();
        DataList1.Visible = true;
    }
    protected void DataList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["cod"] = Convert.ToInt32(DropDownList1.SelectedValue);
        dlist_bind();
    }
}