﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/InnerStudent.master" AutoEventWireup="true" CodeFile="Profile.aspx.cs" Inherits="Student_Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <table width="100%">
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
    
        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" 
            CellPadding="4" ForeColor="#333333" 
            GridLines="None" Height="16px" onitemupdating="DetailsView1_ItemUpdating" 
             Width="865px" onmodechanging="DetailsView1_ModeChanging" DataKeyNames="studentid">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
            <EditRowStyle BackColor="#999999" />
            <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
            <Fields>
                <asp:TemplateField HeaderText="Roll Number" SortExpression="studentrollno">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("studentrollno") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Registration Number" 
                    SortExpression="studentregno">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("studentregno") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Course">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("coursename") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Department">
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("depname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Session">
                    <ItemTemplate>
                        <asp:Label ID="Label28" runat="server" Text='<%# Eval("sessionstart") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Class">
                   <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Eval("classname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Group">
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Eval("groupname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="First Name" SortExpression="studentfname">  
                    <ItemTemplate>
                        <asp:Label ID="Label7" runat="server" Text='<%# Eval("studentfname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Middle Name" SortExpression="studentmname">
                    <ItemTemplate>
                        <asp:Label ID="Label8" runat="server" Text='<%# Eval("studentmname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Last Name" SortExpression="studentlname">
                    <ItemTemplate>
                        <asp:Label ID="Label9" runat="server" Text='<%# Eval("studentlname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mother Name" SortExpression="studentmothername">
                    <ItemTemplate>
                        <asp:Label ID="Label10" runat="server" Text='<%# Eval("studentmothername") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Father Name" SortExpression="studentfathername">
                    <ItemTemplate>
                        <asp:Label ID="Label11" runat="server" Text='<%# Eval("studentfathername") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date Of Birth" SortExpression="studentdob">
                    <ItemTemplate>
                        <asp:Label ID="Label12" runat="server" Text='<%# Eval("studentdob") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Category" SortExpression="studentcategoryid">
                    <ItemTemplate>
                        <asp:Label ID="Label13" runat="server" Text='<%# Eval("categoryname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Correspondence Address" 
                    SortExpression="studentcorresadd">
                     <ItemTemplate>
                        <asp:Label ID="Label14" runat="server" Text='<%# Eval("studentcorresadd") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Permanent Address" 
                    SortExpression="studentpermaadd">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox19" runat="server" Text='<%# Eval("studentpermaadd") %>' 
                            Height="92px" TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("studentpermaadd") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Phone Number" SortExpression="studentphone1">
                    <ItemTemplate>
                        <asp:Label ID="Label16" runat="server" Text='<%# Eval("studentphone1") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Alternate Phone Number" 
                    SortExpression="studentphone2">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox23" runat="server" Text='<%# Eval("studentphone2") %>'></asp:TextBox>
                        <asp:CompareValidator ID="CompareValidator50" runat="server" ErrorMessage="**" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox23" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label17" runat="server" Text='<%# Eval("studentphone2") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Email_ID" SortExpression="studentemail_id">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox25" runat="server" 
                            Text='<%# Eval("studentemail_id") %>'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="abc" runat="server" ErrorMessage="***" ControlToValidate="TextBox25" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red"></asp:RegularExpressionValidator>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label18" runat="server" Text='<%# Eval("studentemail_id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Picture" SortExpression="studentphoto">
                    <ItemTemplate>
                        <%--<asp:Image ID="Image1" runat="server" Height="80px" Width="80px" 
                            ImageUrl='<%# Eval("studentphoto") %>' />--%>
                            <asp:Image ID="Image1" runat="server" Height="80px" Width="80px" 
                            ImageUrl='<%#"~/Studentpic/"+Eval("Studentphoto") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="10 Marks" SortExpression="student10marks">
                    <ItemTemplate>
                        <asp:Label ID="Label19" runat="server" Text='<%# Eval("student10marks") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="12 Marks" SortExpression="student12marks">
                    <ItemTemplate>
                        <asp:Label ID="Label20" runat="server" Text='<%# Eval("student12marks") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Blood Group" SortExpression="studentbloodgroup">
                    <ItemTemplate>
                        <asp:Label ID="Label21" runat="server" Text='<%# Eval("studentbloodgroup") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Achievement" SortExpression="studentachievement">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox33" runat="server" 
                            Text='<%# Eval("studentachievement") %>' Height="92px" 
                            TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label22" runat="server" Text='<%# Eval("studentachievement") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Skill" SortExpression="studentskill">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox35" runat="server" Text='<%# Eval("studentskill") %>' 
                            Height="92px" TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label23" runat="server" Text='<%# Eval("studentskill") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Training" SortExpression="studenttraining">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox37" runat="server" 
                            Text='<%# Eval("studenttraining") %>' Height="92px" TextMode="MultiLine" 
                            Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    
                    <ItemTemplate>
                        <asp:Label ID="Label24" runat="server" Text='<%# Eval("studenttraining") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Hobbies" SortExpression="studenthobbies">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox39" runat="server" Text='<%# Eval("studenthobbies") %>' 
                            Height="92px" TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    
                    <ItemTemplate>
                        <asp:Label ID="Label25" runat="server" Text='<%# Eval("studenthobbies") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Resume File" SortExpression="studentresumefile">
                    <EditItemTemplate>
                        <asp:FileUpload ID="FileUpload3" runat="server" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="Lk5" runat="server" Text='<%#Eval("studentresumefile") %>' NavigateUrl='<%#"~/Studentresume/"+ Eval("studentresumefile") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="IS Hostler" SortExpression="studentishostler">
                      <ItemTemplate>
                    <asp:Label ID="Lblgender" runat="server" Text='<%# Eval("studentishostler") %>'></asp:Label>
                     </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <EditItemTemplate>
                        <asp:LinkButton ID="LinkButton6" runat="server" CommandName="update" ValidationGroup="abc">Update</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="LinkButton7" runat="server" CommandName="cancel" >Cancel</asp:LinkButton>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="edit">Edit</asp:LinkButton>
                        &nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
            </Fields>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        </asp:DetailsView>
    
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>

