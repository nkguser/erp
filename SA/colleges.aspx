﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SA/InnerSA.master" AutoEventWireup="true" CodeFile="colleges.aspx.cs" Inherits="SA_colleges" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <table class="styledmenu" >
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" align="center">
                <asp:RadioButtonList ID="Rblclg" runat="server" AutoPostBack="True" 
                    Font-Bold="True" Font-Strikeout="False" Font-Underline="False" 
                    ForeColor="#003366" RepeatDirection="Horizontal" 
                    onselectedindexchanged="Rblclg_SelectedIndexChanged">
                    <asp:ListItem Value="0">Register College</asp:ListItem>
                    <asp:ListItem Value="1">Update College</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        </table>
    <asp:Panel ID="P1" runat="server" Visible="False">
    
        <table>
        <tr>
            <td>
                &nbsp;</td>
            <td colspan="2" align=left>
                <asp:Label ID="Lblmsg" runat="server" Font-Bold="True"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td >
            </td>
            <td align=left >
                <asp:Label ID="Lblclgname" runat="server"></asp:Label>
            </td>
            <td align="left" >
                <asp:TextBox ID="TxtClgname" runat="server" Width="400px" Visible="False"></asp:TextBox>
                <br />
                <asp:DropDownList ID="DDLclgname" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLclgname_SelectedIndexChanged" Visible="False" 
                    Width="400px">
                </asp:DropDownList>
            </td>
            <td style="height: 18px">
                <asp:RequiredFieldValidator ID="lblname" runat="server" 
                    ControlToValidate="TxtClgname" ErrorMessage="*" Font-Bold="True" 
                    ForeColor="#CC0000" ValidationGroup="pqr"></asp:RequiredFieldValidator>
            </td>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
                Logo</td>
            <td align="left">
                <asp:FileUpload ID="FUlogo" runat="server" />
                <asp:Image ID="Imglogo" runat="server" Height="75px" Width="75px" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
                Establishment Year</td>
            <td align="left">
                <asp:TextBox ID="TxtEY" runat="server"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:RequiredFieldValidator ID="EY" runat="server" ControlToValidate="TxtEY" 
                    Display="Dynamic" ErrorMessage="*" Font-Bold="True" ForeColor="#CC0000" 
                    ValidationGroup="pqr"></asp:RequiredFieldValidator>
                &nbsp;<asp:CompareValidator ID="EY1" runat="server" ControlToValidate="TxtEY" 
                    Display="Dynamic" ErrorMessage="**" Font-Bold="True" ForeColor="#CC0000" 
                    Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td >
                &nbsp;</td>
            <td align="left">
                Closing Year</td>
            <td align="left">
                <asp:TextBox ID="TxtCY" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:CompareValidator ID="CY2" runat="server" ControlToValidate="TxtCY" 
                    ErrorMessage="**" Font-Bold="True" ForeColor="#CC0000" Operator="DataTypeCheck" 
                    Type="Integer"></asp:CompareValidator>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <asp:Panel ID=p2 runat="server">
        <tr> 
        
        <td>
        &nbsp;</td>
               <td  align="left"> 
               
                Administrator ID</td>
            <td align="left">
                <asp:TextBox ID="TxtAID" runat="server"></asp:TextBox>
            </td>
            
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
                Password</td>
            <td align="left">
                <asp:LinkButton ID="LBreset" runat="server" Text="Reset" onclick="LBreset_Click" 
                    Width="100px" Font-Strikeout="True" Font-Size="Medium" />
            </td>
            
        </tr>
        </asp:Panel>
        
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
                First Name</td>
            <td align="left">
                <asp:TextBox ID="TxtFname" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="FN" runat="server" ControlToValidate="TxtFname" 
                    ErrorMessage="*" Font-Bold="True" ForeColor="#CC0000" ValidationGroup="pqr"></asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align=left >
                Last Name</td>
            <td align="left">
                <asp:TextBox ID="TxtLname" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="LN" runat="server" ControlToValidate="TxtLname" 
                    ErrorMessage="*" Font-Bold="True" ForeColor="#CC0000" ValidationGroup="pqr"></asp:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
                Email-ID</td>
            <td align="left">
                <asp:TextBox ID="Txtemail" runat="server"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:RequiredFieldValidator ID="EM" runat="server" ControlToValidate="Txtemail" 
                    Display="Dynamic" ErrorMessage="*" Font-Bold="True" ForeColor="#CC0000" 
                    ValidationGroup="pqr"></asp:RequiredFieldValidator>
                &nbsp;<asp:RegularExpressionValidator ID="EM1" runat="server" 
                    ControlToValidate="Txtemail" Display="Dynamic" ErrorMessage="***" 
                    Font-Bold="True" ForeColor="#CC0000" 
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                    ValidationGroup="pqr"></asp:RegularExpressionValidator>
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
                Phone</td>
            <td align="left">
                <asp:TextBox ID="Txtphone" runat="server"></asp:TextBox>
            </td>
            <td colspan="2">
                <asp:RequiredFieldValidator ID="PH" runat="server" ControlToValidate="Txtphone" 
                    Display="Dynamic" ErrorMessage="*" Font-Bold="True" ForeColor="#CC0000" 
                    ValidationGroup="pqr"></asp:RequiredFieldValidator>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                <asp:Button ID="Btnregupd" runat="server" onclick="Btnregupd_Click" 
                    Width="150px" ValidationGroup="pqr" />
            
                &nbsp;
            
                <asp:Button ID="BtnCancel" runat="server" onclick="BtnCancel_Click" 
                    Width="150px" Text="Cancel" Visible="False" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td align="left">
                    &nbsp;</td>
                <td align="left">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td align="left" colspan="6">
                    
                </td>
            </tr>
    </table></asp:Panel>
    <tabe>
    <tr>
    <td>
    <asp:GridView ID="Gridclg" runat="server" AutoGenerateColumns="False" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" 
                        Width="865px">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <Columns>
                           
                            <asp:TemplateField HeaderText="Collge Name" SortExpression="clgname">
                            <ItemTemplate>
                            <asp:Label ID="Lblclgname" runat="server" Font-Bold="True" 
                                                        Text='<%# Eval("clgname") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Logo" SortExpression="clglogo">
                             <ItemTemplate>
                             <asp:Image ID="Imglogo" Height="50px" Width="50px" runat="server" Font-Bold="True" 
                                                        ImageUrl='<%# "~/collegelogo/" + Eval("clglogo") %>'></asp:Image>
                           </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Establish In" SortExpression="clgstart">
                             <ItemTemplate>
                            <asp:Label ID="Lblclgstart" runat="server" Font-Bold="True" 
                                                        Text='<%# Eval("clgstart") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Closing Yr" SortExpression="clgend">
                             <ItemTemplate>
                            <asp:Label ID="Lblclgend" runat="server" Font-Bold="True" 
                                                        Text='<%# Eval("clgend") %>'></asp:Label>
                            </ItemTemplate>
                            </asp:TemplateField>
                           
                        </Columns>
                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White"  />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>
    </td>
    </tr>
    </tabe>

</asp:Content>

