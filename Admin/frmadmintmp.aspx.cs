﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_frmadmintmp : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        nsphotosnak.clstmp obj = new nsphotosnak.clstmp();
        nsphotosnak.clstmpprp objprp = new nsphotosnak.clstmpprp();
        objprp.p_tmpnam = TextBox1.Text;
        objprp.p_tmpdsppag = TextBox2.Text;
        if (Button1.Text == "Submit")
        {
            obj.Save_Rec(objprp);
        }
        else
        {
            objprp.p_tmpcod = Convert.ToInt32(ViewState["cod"]);
            obj.Update_Rec(objprp);
            Button1.Text = "Submit";
        }
        TextBox1.Text = String.Empty;
        TextBox2.Text = String.Empty;
        GridView1.DataBind();
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Int32 a = Convert.ToInt32(GridView1.DataKeys[e.NewEditIndex][0]);
        ViewState["cod"] = a;
        nsphotosnak.clstmp obj = new nsphotosnak.clstmp();
        List<nsphotosnak.clstmpprp> k = obj.Find_rec(a);
        if (k.Count > 0)
        {
            TextBox1.Text = k[0].p_tmpnam;
            TextBox2.Text = k[0].p_tmpdsppag;
            Button1.Text = "Update";
        }
        e.Cancel = true;
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        nsphotosnak.clstmp obj = new nsphotosnak.clstmp();
        nsphotosnak.clstmpprp objprp = new nsphotosnak.clstmpprp();
        objprp.p_tmpcod = Convert.ToInt32(GridView1.DataKeys[e.RowIndex][0]);
        obj.Delete_Rec(objprp);
        GridView1.DataBind();
        e.Cancel = true;
    }
}