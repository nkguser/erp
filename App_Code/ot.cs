﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;
namespace ot
{
    //*******************************Connection Class
    public partial class clscon
    {
        protected SqlConnection con = new SqlConnection();
        public clscon()
        {
            con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        }
    }
    //*********************************************1
    public interface inttec
    {
        Int32 p_teccod
        {
            get;
            set;
        }
        Int32 p_tecteacherid
        {
            get;
            set;
        }
        String p_tecnam
        {
            get;
            set;
        }
    }
    public class clstecprp : inttec
    {
        private Int32 a, c;
        private String b;
        public int p_teccod
        {
            get
            {
                return a;
            }
            set
            {
                a = value;
            }
        }

        public string p_tecnam
        {
            get
            {
                return b;
            }
            set
            {
                b = value;
            }
        }


        public int p_tecteacherid
        {
            get
            {
                return c;
            }
            set
            {
                c = value;
            }
        }
    }
    public class clstec : clscon
    {
        public void Save_Rec(clstecprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("instec", con);
            cmd.CommandType = CommandType.StoredProcedure;
            // cmd.Parameters.Add("@tecod", SqlDbType.Int).Value = p.p_teccod;
            cmd.Parameters.Add("@tecnam", SqlDbType.VarChar, 50).Value = p.p_tecnam;
            cmd.Parameters.Add("@tecnam", SqlDbType.Int).Value = p.p_tecteacherid;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clstecprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updtec", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@teccod", SqlDbType.Int).Value = p.p_teccod;
            cmd.Parameters.Add("@tecnam", SqlDbType.VarChar, 50).Value = p.p_tecnam;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clstecprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("deltec", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@teccod", SqlDbType.Int).Value = p.p_teccod;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clstecprp> Display_Rec(Int32 tid)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dsptec", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@tid", SqlDbType.Int).Value = tid;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clstecprp> obj = new List<clstecprp>();
            while (dr.Read())
            {
                clstecprp k = new clstecprp();
                k.p_teccod = Convert.ToInt32(dr[0]);
                k.p_tecnam = dr[1].ToString();
                obj.Add(k);
            }

            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;


        }
        public List<clstecprp> Display_RecST()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dsptecST", con);
            cmd.CommandType = CommandType.StoredProcedure;            
            SqlDataReader dr = cmd.ExecuteReader();
            List<clstecprp> obj = new List<clstecprp>();
            while (dr.Read())
            {
                clstecprp k = new clstecprp();
                k.p_teccod = Convert.ToInt32(dr[0]);
                k.p_tecnam = dr[1].ToString();
                obj.Add(k);
            }

            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;


        }
        public List<clstecprp> Find_Rec(Int32 teccod)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndtec", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@teccod", SqlDbType.Int).Value = teccod;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clstecprp> obj = new List<clstecprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clstecprp k = new clstecprp();
                k.p_teccod = Convert.ToInt32(dr[0]);
                k.p_tecnam = dr[1].ToString();
                obj.Add(k);
            }

            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;

        }
    }




    //*********************************************2
    public interface intqst
    {
        Int32 p_qstcod
        {
            get;
            set;
        }
        String p_qstdsc
        {
            get;
            set;
        }
        Int32 p_qstteccod
        {
            get;
            set;
        }
        Char p_qstlvl
        {
            get;
            set;
        }

        String p_qstpic
        {
            get;
            set;
        }

    }
    public class clsqstprp : intqst
    {
        private Int32 qstcod, qstteccod;
        private String qstdsc, qstpic;
        private Char qstlvl;
        public int p_qstcod
        {
            get
            {
                return qstcod;
            }
            set
            {
                qstcod = value;
            }
        }

        public string p_qstdsc
        {
            get
            {
                return qstdsc;
            }
            set
            {
                qstdsc = value;
            }
        }

        public int p_qstteccod
        {
            get
            {
                return qstteccod;
            }
            set
            {
                qstteccod = value;
            }
        }

        public char p_qstlvl
        {
            get
            {
                return qstlvl;
            }
            set
            {
                qstlvl = value;
            }
        }

        public string p_qstpic
        {
            get
            {
                return qstpic;
            }
            set
            {
                qstpic = value;
            }
        }
    }
    public class clsqst : clscon
    {
        public Int32 Save_Rec(clsqstprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insqst", con);
            cmd.CommandType = CommandType.StoredProcedure;
            // cmd.Parameters.Add("@tecod", SqlDbType.Int).Value = p.p_teccod;
            cmd.Parameters.Add("@qstdsc", SqlDbType.NText).Value = p.p_qstdsc;
            cmd.Parameters.Add("@qstteccod ", SqlDbType.Int).Value = p.p_qstteccod;
            cmd.Parameters.Add("@qstlbl", SqlDbType.Char, 1).Value = p.p_qstlvl;
            cmd.Parameters.Add("@qstpic", SqlDbType.VarChar, 50).Value = p.p_qstpic;
            cmd.Parameters.Add("@r", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();
            Int32 a = Convert.ToInt32(cmd.Parameters["@r"].Value);
            cmd.Dispose();
            con.Close();
            return a;
        }
        public void Update_Rec(clsqstprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updqst", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@qstcod", SqlDbType.Int).Value = p.p_qstcod;
            cmd.Parameters.Add("@qstdsc", SqlDbType.NText).Value = p.p_qstdsc;
            cmd.Parameters.Add("@qstteccod ", SqlDbType.Int).Value = p.p_qstteccod;
            cmd.Parameters.Add("@qstlbl", SqlDbType.Char, 1).Value = p.p_qstlvl;
            cmd.Parameters.Add("@qstpic", SqlDbType.VarChar, 50).Value = p.p_qstpic;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsqstprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delqst", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@qstcod", SqlDbType.Int).Value = p.p_qstcod;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsqstprp> Display_Rec(Int32 teccod)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspqst", con);
            cmd.Parameters.Add("@teccod", SqlDbType.Int).Value = teccod;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsqstprp> obj = new List<clsqstprp>();
            while (dr.Read())
            {
                clsqstprp k = new clsqstprp();
                k.p_qstcod = Convert.ToInt32(dr[0]);
                k.p_qstdsc = dr[1].ToString();
                k.p_qstteccod = Convert.ToInt32(dr[2]);
                k.p_qstlvl = Convert.ToChar(dr[3]);
                k.p_qstpic = dr[4].ToString();
                obj.Add(k);
            }

            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
        public List<clsqstprp> Find_Rec(Int32 qstcod)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndqst", con);
            cmd.Parameters.Add("@qstcod", SqlDbType.Int).Value = qstcod;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsqstprp> obj = new List<clsqstprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsqstprp k = new clsqstprp();
                k.p_qstcod = Convert.ToInt32(dr[0]);
                k.p_qstdsc = dr[1].ToString();
                k.p_qstteccod = Convert.ToInt32(dr[2]);
                k.p_qstlvl = Convert.ToChar(dr[3]);
                k.p_qstpic = dr[4].ToString();
                obj.Add(k);
            }

            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }





    //*********************************************3
    public interface intopt
    {
        Int32 p_optcod
        {
            get;
            set;
        }

        Int32 p_optqstcod
        {
            get;
            set;
        }

        String p_optdsc
        {
            get;
            set;
        }

        Char p_optsts
        {
            get;
            set;
        }

        String p_optpic
        {
            get;
            set;
        }
    }
    public class clsoptprp : intopt
    {
        private Int32 optcod, optqstcod;
        private String optdsc, optpic;
        private Char optsts;
        public int p_optcod
        {
            get
            {
                return optcod;
            }
            set
            {
                optcod = value;
            }
        }

        public int p_optqstcod
        {
            get
            {
                return optqstcod;
            }
            set
            {
                optqstcod = value;
            }
        }

        public string p_optdsc
        {
            get
            {
                return optdsc;
            }
            set
            {
                optdsc = value;
            }
        }

        public char p_optsts
        {
            get
            {
                return optsts;
            }
            set
            {
                optsts = value;
            }
        }

        public string p_optpic
        {
            get
            {
                return optpic;
            }
            set
            {
                optpic = value;
            }
        }
    }
    public class clsopt : clscon
    {
        public Int32 Save_Rec(clsoptprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insopt", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@optdsc", SqlDbType.VarChar, 200).Value = p.p_optdsc;
            cmd.Parameters.Add("@optqstcod", SqlDbType.Int).Value = p.p_optqstcod;
            cmd.Parameters.Add("@optsts", SqlDbType.Char, 1).Value = p.p_optsts;
            cmd.Parameters.Add("@optpic", SqlDbType.VarChar, 50).Value = p.p_optpic;
            cmd.Parameters.Add("@ret", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();
            Int32 a = Convert.ToInt32(cmd.Parameters["@ret"].Value);
            cmd.Dispose();
            con.Close();
            return a;
        }
        public void Update_Rec(clsoptprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updopt", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@optcod ", SqlDbType.Int).Value = p.p_optcod;
            cmd.Parameters.Add("@optdsc", SqlDbType.VarChar, 200).Value = p.p_optdsc;
            cmd.Parameters.Add("@optqstcod ", SqlDbType.Int).Value = p.p_optqstcod;
            cmd.Parameters.Add("@optsts", SqlDbType.Char, 1).Value = p.p_optsts;
            cmd.Parameters.Add("@optpic", SqlDbType.VarChar, 50).Value = p.p_optpic;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void delete_Rec(clsoptprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delopt", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@optcod ", SqlDbType.Int).Value = p.p_optcod;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }

        public List<clsoptprp> Display_Rec(Int32 qstcod)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspopt", con);
            cmd.Parameters.Add("@qstcod", SqlDbType.Int).Value = qstcod;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsoptprp> obj = new List<clsoptprp>();
            while (dr.Read())
            {
                clsoptprp k = new clsoptprp();
                k.p_optcod = Convert.ToInt32(dr[0]);
                k.p_optqstcod = Convert.ToInt32(dr[1]);
                k.p_optdsc = dr[2].ToString();
                k.p_optsts = Convert.ToChar(dr[3]);
                k.p_optpic = dr[4].ToString();
                obj.Add(k);
            }

            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }

        List<clsoptprp> Find_Rec()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndopt", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsoptprp> obj = new List<clsoptprp>();
            while (dr.Read())
            {
                clsoptprp k = new clsoptprp();
                k.p_optcod = Convert.ToInt32(dr[0]);
                k.p_optqstcod = Convert.ToInt32(dr[1]);
                k.p_optdsc = dr[2].ToString();
                k.p_optsts = Convert.ToChar(dr[3]);
                k.p_optpic = dr[4].ToString();
                obj.Add(k);
            }

            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }




    //********************************************4
    public interface intreg
    {
        Int32 p_regcod
        {
            get;
            set;
        }
        String p_regnam
        {
            get;
            set;
        }
        String p_regeml
        {
            get;
            set;
        }
        String p_regpas
        {
            get;
            set;
        }

        Char p_regsts
        {
            get;
            set;
        }
    }
    public class clsregprp : intreg
    {
        String regnam, regpas, regeml;
        Int32 regcod;
        Char regsts;
        public int p_regcod
        {
            get
            {
                return regcod;
            }
            set
            {
                regcod = value;
            }
        }

        public string p_regnam
        {
            get
            {
                return regnam;
            }
            set
            {
                regnam = value;
            }
        }

        public string p_regeml
        {
            get
            {
                return regeml;
            }
            set
            {
                regeml = value;
            }
        }

        public string p_regpas
        {
            get
            {
                return regpas;
            }
            set
            {
                regpas = value;
            }
        }

        public char p_regsts
        {
            get
            {
                return regsts;
            }
            set
            {
                regsts = value;
            }
        }
    }
    public class clsreg : clscon
    {
        public Int32 logincheck(String usreml, String usrpwd, out char usrrol)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("logincheck", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@usreml", SqlDbType.VarChar, 50).Value = usreml;
            cmd.Parameters.Add("@usrpwd", SqlDbType.VarChar, 50).Value = usrpwd;
            cmd.Parameters.Add("@usrcod", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@usrrol", SqlDbType.Char, 1).Direction = ParameterDirection.Output;
            cmd.ExecuteNonQuery();
            Int32 a = Convert.ToInt32(cmd.Parameters["@usrcod"].Value);
            usrrol = Convert.ToChar(cmd.Parameters["@usrrol"].Value);
            cmd.Dispose();
            con.Close();
            return a;
        }
        public void Save_Rec(clsregprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insreg", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@regnam", SqlDbType.VarChar, 50).Value = p.p_regnam;
            cmd.Parameters.Add("@regeml", SqlDbType.VarChar, 50).Value = p.p_regeml;
            cmd.Parameters.Add("@regpwd", SqlDbType.VarChar, 50).Value = p.p_regpas;
            cmd.Parameters.Add("@regsts", SqlDbType.Char, 1).Value = p.p_regsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clsregprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updreg", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@regcod", SqlDbType.Int).Value = p.p_regcod;
            cmd.Parameters.Add("@regnam", SqlDbType.VarChar, 50).Value = p.p_regnam;
            cmd.Parameters.Add("@regeml", SqlDbType.VarChar, 50).Value = p.p_regeml;
            cmd.Parameters.Add("@regpas", SqlDbType.VarChar, 50).Value = p.p_regpas;
            cmd.Parameters.Add("@regsts", SqlDbType.Char, 1).Value = p.p_regsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsregprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delreg", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@regcod", SqlDbType.Int).Value = p.p_regcod;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsregprp> Find_Rec(Int32 regcod)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndreg", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@regcod", SqlDbType.Int).Value = regcod;
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            List<clsregprp> obj = new List<clsregprp>();
            while (dr.Read())
            {
                clsregprp k = new clsregprp();
                k.p_regcod = Convert.ToInt32(dr[0]);
                k.p_regnam = dr[1].ToString();
                k.p_regeml = dr[2].ToString();
                k.p_regpas = dr[3].ToString();
                k.p_regsts = Convert.ToChar(dr[4]);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;

        }
        public List<clsregprp> Disp_Rec(Int32 regcod)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspreg", con);
            cmd.CommandType = CommandType.StoredProcedure;
            // cmd.Parameters.Add("@regcod", SqlDbType.Int).Value = regcod;
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            List<clsregprp> obj = new List<clsregprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsregprp k = new clsregprp();
                k.p_regcod = Convert.ToInt32(dr[0]);
                k.p_regnam = dr[1].ToString();
                k.p_regeml = dr[2].ToString();
                k.p_regpas = dr[3].ToString();
                k.p_regsts = Convert.ToChar(dr[4]);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;

        }

    }





    //*******************************************5
    public interface intscr
    {
        Int32 p_scrcod
        {
            get;
            set;
        }
        Int32 p_scrregcod
        {
            get;
            set;
        }
        Int32 p_scrteccod
        {
            get;
            set;
        }
        String p_scrdat
        {
            get;
            set;
        }
        Int32 p_scrval
        {
            get;
            set;
        }
    }
    public interface intscrrpt
    {
        Int32 p_scrcod
        {
            get;
            set;
        }
        String p_scrregnam
        {
            get;
            set;
        }
        Int32 p_scrteccod
        {
            get;
            set;
        }
        String p_scrtecnam
        {
            get;
            set;
        }
        string p_scrdat
        {
            get;
            set;
        }
        Int32 p_scrval
        {
            get;
            set;
        }
    }
    public class clsscrprp : intscr
    {
        private Int32 scrcod, scrregcod, scrteccod, scrval;
        private String scrdat;
        public int p_scrcod
        {
            get
            {
                return scrcod;
            }
            set
            {
                scrcod = value;
            }
        }

        public int p_scrregcod
        {
            get
            {
                return scrregcod;
            }
            set
            {
                scrregcod = value;
            }
        }

        public int p_scrteccod
        {
            get
            {
                return scrteccod;
            }
            set
            {
                scrteccod = value;
            }
        }

        public String p_scrdat
        {
            get
            {
                return scrdat;
            }
            set
            {
                scrdat = value;
            }
        }

        public int p_scrval
        {
            get
            {
                return scrval;
            }
            set
            {
                scrval = value;
            }
        }
    }
    public class clsscrrptprp : intscrrpt
    {
        private string scrtecnam, scrregnam;
        private Int32 scrcod, scrteccod, scrval;
        private String scrdat;
        public int p_scrcod
        {
            get
            {
                return scrcod;
            }
            set
            {
                scrcod = value;
            }
        }

        public String p_scrregnam
        {
            get
            {
                return scrregnam;
            }
            set
            {
                scrregnam = value;
            }
        }
        public string p_scrtecnam
        {
            get
            {
                return scrtecnam;
            }
            set
            {
                scrtecnam = value;
            }
        }

        public int p_scrteccod
        {
            get
            {
                return scrteccod;
            }
            set
            {
                scrteccod = value;
            }
        }

        public String p_scrdat
        {
            get
            {
                return scrdat;
            }
            set
            {
                scrdat = value;
            }
        }

        public int p_scrval
        {
            get
            {
                return scrval;
            }
            set
            {
                scrval = value;
            }
        }
    }
    public class clsscr : clscon
    {
        public DataSet dspmyscr(Int32 regcod)
        {
            SqlDataAdapter adp = new SqlDataAdapter("dspmyscr", con);
            adp.SelectCommand.CommandType = CommandType.StoredProcedure;
            adp.SelectCommand.Parameters.Add("@regcod", SqlDbType.Int).Value = regcod;
            DataSet ds = new DataSet();
            adp.Fill(ds);
            return ds;
        }
        public Int32 chksts(Int32 regcod, Int32 teccod)
        {
            SqlDataAdapter adp = new SqlDataAdapter("chksts", con);
            adp.SelectCommand.CommandType = CommandType.StoredProcedure;
            adp.SelectCommand.Parameters.Add("@regcod", SqlDbType.Int).Value = regcod;
            adp.SelectCommand.Parameters.Add("@teccod", SqlDbType.Int).Value = teccod;
            DataSet ds = new DataSet();
            adp.Fill(ds);
            Int32 a = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            return a;
        }
        public Int32 Save_Rec(clsscrprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insscr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Add("@scrcod", SqlDbType.Int).Value = p.p_scrcod;
            cmd.Parameters.Add("@scrregcod", SqlDbType.Int).Value = p.p_scrregcod;
            cmd.Parameters.Add("@scrteccod", SqlDbType.Int).Value = p.p_scrteccod;
            cmd.Parameters.Add("@scrdat", SqlDbType.Date).Value = p.p_scrdat;
            cmd.Parameters.Add("@scrval", SqlDbType.Int).Value = p.p_scrval;
            cmd.Parameters.Add("@r", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();
            Int32 a = Convert.ToInt32(cmd.Parameters["@r"].Value);
            cmd.Dispose();
            con.Close();
            return a;
        }
        public void Update_Rec(clsscrprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updscr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@scrcod", SqlDbType.Int).Value = p.p_scrcod;
            cmd.Parameters.Add("@scrregcod", SqlDbType.Int).Value = p.p_scrregcod;
            cmd.Parameters.Add("@scrteccod", SqlDbType.Int).Value = p.p_scrteccod;
            cmd.Parameters.Add("@scrdat", SqlDbType.Date).Value = p.p_scrdat;
            cmd.Parameters.Add("@scrvalcod", SqlDbType.Int).Value = p.p_scrval;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Upd_Rec(clsscrprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("update tbscr set scrval = @scrval where scrcod = @scrcod", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@scrcod", SqlDbType.Int).Value = p.p_scrcod;
            cmd.Parameters.Add("@scrvalcod", SqlDbType.Int).Value = p.p_scrval;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsscrprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delscr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@scrcod", SqlDbType.Int).Value = p.p_scrcod;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();


        }
        public List<clsscrprp> Disp_Rec()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspscr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            // cmd.Parameters.Add("@regcod", SqlDbType.Int).Value = regcod;
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            List<clsscrprp> obj = new List<clsscrprp>();
            while (dr.Read())
            {
                clsscrprp k = new clsscrprp();
                k.p_scrcod = Convert.ToInt32(dr[0]);
                k.p_scrregcod = Convert.ToInt32(dr[1]);
                k.p_scrteccod = Convert.ToInt32(dr[2]);
                k.p_scrdat = Convert.ToString(dr[3]);
                k.p_scrval = Convert.ToInt32(dr[4]);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
        public List<clsscrrptprp> Disp_Rpt()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspscrrpt", con);
            cmd.CommandType = CommandType.StoredProcedure;
            // cmd.Parameters.Add("@regcod", SqlDbType.Int).Value = regcod;
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            List<clsscrrptprp> obj = new List<clsscrrptprp>();
            while (dr.Read())
            {
                clsscrrptprp k = new clsscrrptprp();
                k.p_scrcod = Convert.ToInt32(dr[0]);
                k.p_scrregnam = Convert.ToString(dr[6]);
                k.p_scrteccod = Convert.ToInt32(dr[2]);
                k.p_scrdat = Convert.ToString(dr[3]);
                k.p_scrval = Convert.ToInt32(dr[4]);
                k.p_scrtecnam = Convert.ToString(dr[5]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;

        }
        public List<clsscrprp> Find_Rec(Int32 scrteccod)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndscr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@scrteccod", SqlDbType.Int).Value = scrteccod;
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            List<clsscrprp> obj = new List<clsscrprp>();
            if (dr.HasRows)
            {
                dr.Read();
                clsscrprp k = new clsscrprp();
                k.p_scrcod = Convert.ToInt32(dr[0]);
                k.p_scrregcod = Convert.ToInt32(dr[1]);
                k.p_scrteccod = Convert.ToInt32(dr[2]);
                k.p_scrdat = Convert.ToString(dr[3]);

                k.p_scrval = Convert.ToInt32(dr[4]);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }
}