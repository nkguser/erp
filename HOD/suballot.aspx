﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="suballot.aspx.cs" Inherits="HOD_suballot" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" style="width:auto" runat="server">
    <ContentTemplate>
     <table width="100%" style="text-align: left; font-weight: bold;" >
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
                Course</td>
            <td >
                <asp:DropDownList ID="DDLCourse" runat="server" 
                     Height="20px" style="font-weight: bold" Width="200px">
                </asp:DropDownList>
                <ajax:CascadingDropDown ID="CascadingDropDown1" runat="server" Category="Course" TargetControlID="DDLCourse" LoadingText="Loading Course..." PromptText="Select Course" ServiceMethod="BindCoursedropdown" ServicePath="DropdownWebService.asmx">
                </ajax:CascadingDropDown>
               <asp:RequiredFieldValidator ID="ddlCourse1" runat="server" ErrorMessage="*" 
                    ForeColor="Red" ControlToValidate="DDLCourse" ValidationGroup="abc"></asp:RequiredFieldValidator>
            </td>
            <td >
                 </td>
            <td >
                Subject
                </td>
            <td>
                <asp:DropDownList ID="DDlSubject" runat="server" Height="20px" 
                    style="font-weight: bold" Width="300px" 
                    onselectedindexchanged="DDlSubject_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ddlGroup5" runat="server" 
                    ControlToValidate="DDlSubject" ErrorMessage="*" ForeColor="Red" 
                    TabIndex="1"></asp:RequiredFieldValidator>
            </td>
            
        </tr>
        <tr>
            <td  align="right">
                Department </td>
            <td >
                <asp:DropDownList ID="DDLDept" runat="server"  Height="20px" style="font-weight: bold" Width="200px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ddlDept1" runat="server" 
                    ControlToValidate="DDLDept" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="abc"></asp:RequiredFieldValidator>
                <ajax:CascadingDropDown ID="CascadingDropDown2" runat="server" Category="Department" TargetControlID="DDLDept" ParentControlID="DDLCourse" LoadingText="Loading Department..." PromptText="Select Department" ServiceMethod="BindDepartmentdropdown" ServicePath="DropdownWebService.asmx">
</ajax:CascadingDropDown>
               
            </td>
            <td >
                </td>
            <td >
                Teacher</td>
            <td>
                <asp:DropDownList ID="DDlTeacher" runat="server" AutoPostBack="True" 
                    Height="20px" onselectedindexchanged="DDlTeacher_SelectedIndexChanged" 
                    style="font-weight: bold" Width="300px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ddlGroup4" runat="server" 
                    ControlToValidate="DDlTeacher" ErrorMessage="*" ForeColor="Red" TabIndex="1"></asp:RequiredFieldValidator>
                
            </td>
            
        </tr>
        <tr>
            <td align="right" >
                Class</td>
            <td >
                <asp:DropDownList ID="DDLClass" runat="server" Height="20px" style="font-weight: bold" Width="200px" >
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ddlClass3" runat="server" 
                    ControlToValidate="DDLClass" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="abc"></asp:RequiredFieldValidator>
                <ajax:CascadingDropDown ID="DDLClass_CascadingDropDown" runat="server" 
                    Category="Class" LoadingText="Loading Class..." ParentControlID="DDLDept" 
                    PromptText="Select Class" ServiceMethod="BindClassdropdown" 
                    ServicePath="DropdownWebService.asmx" TargetControlID="DDLClass">
                </ajax:CascadingDropDown>
            </td>
            <td >
                </td>
            <td >
                <b>Group</b></td>
            <td>
                <asp:DropDownList ID="DDLGroup" runat="server" Height="20px" 
                    style="font-weight: bold" Width="300px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="ddlGroup7" runat="server" 
                    ControlToValidate="DDLGroup" ErrorMessage="*" ForeColor="Red" 
                    ValidationGroup="abc"></asp:RequiredFieldValidator>
                <ajax:CascadingDropDown ID="DDLGroup_CascadingDropDown" runat="server" 
                    Category="Group" LoadingText="Loading Group..." ParentControlID="DDLClass" 
                    PromptText="Select Group" ServiceMethod="BindGroupdropdown" 
                    ServicePath="DropdownWebService.asmx" TargetControlID="DDLGroup">
                </ajax:CascadingDropDown>
            </td>
            
        </tr>
         <tr>
             <td  >
                </td>
             
         </tr>
         <tr>
             <td  >
                 </td>
             <td >
                
             </td>
             <td >
                 <asp:Button ID="BTNSubAllot" runat="server" onclick="BTNSubAllot_Click" 
                     style="font-weight: 700" Text="Allot" ValidationGroup="abc" />
             </td>
             <td colspan="2">
             <asp:Label ID="LBLmsg" runat="server" Font-Bold="True" ForeColor="#003300"></asp:Label>
             </td>
             <td>
             </td>
             <td>
             </td>
         </tr>
         <tr>
             <td >
                 </td>
             
         </tr>
        </table>
        <table  style="text-align: inherit;width:100%;font-weight: bold;" >
        <tr>
          <td >
            <div style="overflow:scroll;height:300px;">
                <asp:GridView ID="GridAllTeacher" runat="server" AutoGenerateColumns="False" 
                    EmptyDataText="There are no data records to display." 
                    AllowSorting="True" CellPadding="3" ForeColor="#333333" GridLines="None" 
                    onrowcancelingedit="GridAllTeacher_RowCancelingEdit" 
                    onrowdeleting="GridAllTeacher_RowDeleting" 
                    onrowediting="GridAllTeacher_RowEditing" 
                    onrowupdating="GridAllTeacher_RowUpdating" 
                    DataKeyNames="subjectallotteacherid,subjectallotsubjectid,subjectallotgroupid" 
                    onrowdatabound="GridAllTeacher_RowDataBound" 
                    onselectedindexchanged="GridAllTeacher_SelectedIndexChanged" 
                    Width="100%"  >
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Teacher" >
                            <EditItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Teacher") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Teacher") %>' ></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Course">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLCourse" runat="server" 
                                    >
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("Coursename") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Department">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Depname") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                           
                                <asp:DropDownList ID="DDLDept" runat="server" 
                                    >
                                </asp:DropDownList>
                           
                            </EditItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Session">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLSession" runat="server" 
                                    DataTextField="Session" DataValueField="Session_id">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label5" runat="server" Text='<%# Eval("Session") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Class">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLClass" runat="server" AutoPostBack="True" 
                                    DataTextField="Class_name" DataValueField="Class_id">
                                </asp:DropDownList>
                                <br />
                                <asp:DropDownList ID="DDLGroup" runat="server" 
                                    DataTextField="Group_Title" DataValueField="Group_id">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("class") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Semester">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLSem" runat="server">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label8" runat="server" Text='<%# Eval("SubjectSem") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Subject">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLSub" runat="server" 
                                    DataTextField="Subject_title" DataValueField="subject_id">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label9" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName='<%# "Update" %>' 
                                    Text="Update"></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton5" runat="server" CommandName='<%# "Cancel" %>' 
                                    Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LnkBtnDelete" Text="De-Allot" CommandName="Delete" 
                                    runat="server" />
                                <%--<asp:LinkButton ID="LinkButton2" runat="server" CommandName="edit" 
                                    Text=" Change"></asp:LinkButton>--%>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </div>
            </td>
        </tr>
    </table>
    <table>
    <tr>
    <td align="center">
     <asp:Panel ID="p1" ScrollBars="Auto" Height="42px" width="100%"  runat="server">
               
                <asp:GridView ID="GridTeacher" runat="server" 
                    AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                    DataKeyNames="subjectallotteacherid,subjectallotsubjectid,subjectallotgroupid" 
                    EmptyDataText="There are no data records to display." ForeColor="Red" 
                    GridLines="None" 
                    onrowdeleting="GridTeacher_RowDeleting"  style="font-weight: bold;"
                    Visible="False" Font-Bold="True" >
                    <HeaderStyle CssClass="grid" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Teacher">
                            <EditItemTemplate>
                                <asp:Label ID="Label10" runat="server" Text='<%# Eval("Teacher") %>'></asp:Label>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label11" runat="server" Text='<%# Eval("Teacher") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Class">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLClass4" runat="server" AutoPostBack="True" 
                                    DataTextField="Class_name" DataValueField="Class_id">
                                </asp:DropDownList>
                                <br />
                                <asp:DropDownList ID="DDLGroup8" runat="server" DataTextField="Group_Title" 
                                    DataValueField="Group_id">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label12" runat="server" Text='<%# Eval("class") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Semester">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLSem0" runat="server">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label13" runat="server" Text='<%# Eval("SubjectSem") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Subject">
                            <EditItemTemplate>
                                <asp:DropDownList ID="DDLSub0" runat="server" DataTextField="Subject_title" 
                                    DataValueField="subject_id">
                                </asp:DropDownList>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label14" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <EditItemTemplate>
                                <asp:LinkButton ID="LinkButton6" runat="server" CommandName='<%# "Update" %>' 
                                    Text="Update"></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton7" runat="server" CommandName='<%# "Cancel" %>' 
                                    Text="Cancel"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="LnkBtnDelete0" runat="server" CommandName="Delete" 
                                    Text="De-Allot" />
                                <%--<asp:LinkButton ID="LinkButton2" runat="server" CommandName="edit" 
                                    Text=" Change"></asp:LinkButton>--%>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
               </asp:Panel>
     <asp:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" 
                    runat="server" HorizontalSide="Right" TargetControlID="p1" 
                    VerticalSide="Bottom" >
                </asp:AlwaysVisibleControlExtender>
    </td></tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

