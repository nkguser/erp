﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" MasterPageFile="~/Admin/InnerAdmin.master" AutoEventWireup="true" CodeFile="regstudent.aspx.cs" Inherits="Admin_regstudent" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <table width="100%">
            
            <tr>
                <td align="left">
                    Student Registration</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
    
                    <table class="styledmenu">
                        <tr>
                            <td style="height: 18px;">
    
        <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" Height="16px" oniteminserting="DetailsView1_ItemInserting"             
             Width="100%" onmodechanging="DetailsView1_ModeChanging" 
                        onpageindexchanging="DetailsView1_PageIndexChanging" DefaultMode="Insert" 
                        oniteminserted="DetailsView1_ItemInserted" style="margin-left: 0px">
            <Fields>
                <asp:TemplateField HeaderText="Roll Number *"  SortExpression="studentrollno">
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Eval("studentrollno") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox2" ValidationGroup="abc"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="**" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox2" Type="Integer" ValidationGroup="abc"></asp:CompareValidator>
                       
                        &nbsp;
                        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                            Text="Check Availabilty" />
                    </InsertItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Registration Number" 
                    SortExpression="studentregno">
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Eval("studentregno") %>'></asp:TextBox>
                    </InsertItemTemplate>
                </asp:TemplateField>
           
                <asp:TemplateField HeaderText="Course *">
                    <%--<ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("coursename") %>'></asp:Label>
                    </ItemTemplate>--%>
                    <InsertItemTemplate>
                        
                        <asp:DropDownList ID="ddlCourse" runat="server" Width="200px"></asp:DropDownList> 
<ajax:CascadingDropDown ID="CascadingDropDown1" runat="server" Category="Course" TargetControlID="ddlCourse" LoadingText="Loading Course..." PromptText="Select Course" ServiceMethod="BindCoursedropdown" ServicePath="DropdownWebService.asmx" >
</ajax:CascadingDropDown>
<asp:RequiredFieldValidator ID="ddlCourse1" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="ddlCourse" ValidationGroup="abc"></asp:RequiredFieldValidator>
                    </InsertItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Department *">
                    <InsertItemTemplate>
                        <asp:DropDownList ID="ddlDepartment" runat="server" Width="200px"></asp:DropDownList>
                                  <asp:RequiredFieldValidator ID="ddlDepartment1" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="ddlDepartment" ValidationGroup="abc"></asp:RequiredFieldValidator>
<ajax:CascadingDropDown ID="CascadingDropDown2" runat="server" Category="Department" TargetControlID="ddlDepartment" ParentControlID="ddlCourse" LoadingText="Loading Department..." PromptText="Select Department" ServiceMethod="BindDepartmentdropdown" ServicePath="DropdownWebService.asmx">
</ajax:CascadingDropDown>
                    </InsertItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Class *">
                    <InsertItemTemplate>
                        <asp:DropDownList ID="ddlClass" runat="server" Width="200px"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ddlClass1" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="ddlClass" ValidationGroup="abc"></asp:RequiredFieldValidator>            
<ajax:CascadingDropDown ID="CascadingDropDown3" runat="server" Category="Class" TargetControlID="ddlClass" ParentControlID="ddlDepartment"  LoadingText="Loading Class..." PromptText="Select Class" ServiceMethod="BindClassdropdown" ServicePath="DropdownWebService.asmx">
</ajax:CascadingDropDown>                       
                    </InsertItemTemplate>
                   
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Group *">
                    <InsertItemTemplate>
                        <asp:DropDownList ID="ddlGroup" runat="server" Width="200px"></asp:DropDownList>
                        <asp:RequiredFieldValidator ID="ddlGroup1" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="ddlGroup" ValidationGroup="abc"></asp:RequiredFieldValidator>
<ajax:CascadingDropDown ID="CascadingDropDown4" runat="server" Category="Group" TargetControlID="ddlGroup" ParentControlID="ddlClass"  LoadingText="Loading Group..." PromptText="Select Group" ServiceMethod="BindGroupdropdown" ServicePath="DropdownWebService.asmx">
</ajax:CascadingDropDown>
                    </InsertItemTemplate>
                </asp:TemplateField>
               
                <asp:TemplateField HeaderText="First Name *" SortExpression="studentfname">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Eval("studentfname") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3"  runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox5"  ></asp:RequiredFieldValidator>
              
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Eval("studentfname") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox6" ValidationGroup="abc"></asp:RequiredFieldValidator>
                       
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label7" runat="server" Text='<%# Eval("studentfname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Middle Name" SortExpression="studentmname">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox7" runat="server" Text='<%# Eval("studentmname") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox8" runat="server" Text='<%# Eval("studentmname") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label8" runat="server" Text='<%# Eval("studentmname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Last Name " SortExpression="studentlname">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox9" runat="server" Text='<%# Eval("studentlname") %>'></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox9"></asp:RequiredFieldValidator>--%>
                        
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox10" runat="server" Text='<%# Eval("studentlname") %>'></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="abc" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox10"></asp:RequiredFieldValidator>--%>
                 
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label9" runat="server" Text='<%# Eval("studentlname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mother Name *" SortExpression="studentmothername">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox11" runat="server" 
                            Text='<%# Eval("studentmothername") %>'></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7"  runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox11"></asp:RequiredFieldValidator>
                        
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox12" runat="server" 
                            Text='<%# Eval("studentmothername") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="abc" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox12"></asp:RequiredFieldValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label10" runat="server" Text='<%# Eval("studentmothername") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Father Name *" SortExpression="studentfathername">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox13" runat="server" 
                            Text='<%# Eval("studentfathername") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9"  runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox13"></asp:RequiredFieldValidator>
                        
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox14" runat="server" 
                            Text='<%# Eval("studentfathername") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="abc" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox14"></asp:RequiredFieldValidator>
                      
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label11" runat="server" Text='<%# Eval("studentfathername") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date Of Birth * (DD/MM/YYYY)" SortExpression="studentdob">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox15" runat="server" Text='<%# Eval("studentdob") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11"  runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox15"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator3" runat="server"  ErrorMessage="**" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox15" Type="Date"></asp:CompareValidator>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox16" runat="server" Text='<%# Eval("studentdob") %>'></asp:TextBox>
                        <asp:CalendarExtender Format="dd/MM/yyyy "  ID="TextBox16_CalendarExtender" runat="server" 
                            Enabled="True" SelectedDate="1990-12-12" TargetControlID="TextBox16">
                        </asp:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="abc" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox16"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator4" ValidationGroup="abc" runat="server" ErrorMessage="**" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox16" Type="Date"></asp:CompareValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label12" runat="server" Text='<%# Eval("studentdob") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Category *" SortExpression="studentcategoryid">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList9" runat="server" DataSourceID="Category0" 
                            DataTextField="categoryname" DataValueField="categoryid">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="Category" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:cn %>" 
                            
                            SelectCommand="SELECT [categoryid], [categoryname] FROM [tbcategory] WHERE ([categorydelsts] = @categorydelsts)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="false" Name="categorydelsts" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:DropDownList ID="DropDownList10" runat="server" DataSourceID="Category0" 
                            DataTextField="categoryname" DataValueField="categoryid">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="Category0" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:cn %>" 
                            
                            
                            SelectCommand="SELECT [categoryid], [categoryname] FROM [tbcategory] WHERE ([categorydelsts] = @categorydelsts)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="false" Name="categorydelsts" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label13" runat="server" Text='<%# Eval("studentcategoryid") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Correspondence Address *" 
                    SortExpression="studentcorresadd">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox17" runat="server" 
                            Text='<%# Eval("studentcorresadd") %>' Height="92px" TextMode="MultiLine" 
                            Width="361px"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidatorcorres" runat="server" ErrorMessage="*" ForeColor="Red"  ControlToValidate="TextBox17"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox18" runat="server" 
                            Text='<%# Eval("studentcorresadd") %>' Height="92px" TextMode="MultiLine" 
                            Width="361px"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidatorcorres1" ValidationGroup="abc" runat="server" ErrorMessage="*" ForeColor="Red"  ControlToValidate="TextBox18"></asp:RequiredFieldValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label14" runat="server" Text='<%# Eval("studentcorresadd") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Permanent Address" 
                    SortExpression="studentpermaadd">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox19" runat="server" Text='<%# Eval("studentpermaadd") %>' 
                            Height="92px" TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox20" runat="server" Text='<%# Eval("studentpermaadd") %>' 
                            Height="92px" TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("studentpermaadd") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Phone Number *" SortExpression="studentphone1">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox21" runat="server" Text='<%# Eval("studentphone1") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13"  runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox21"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox22" runat="server" Text='<%# Eval("studentphone1") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="abc" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox22"></asp:RequiredFieldValidator>                        
                    </InsertItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Alternate Phone Number" 
                    SortExpression="studentphone2">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox23" runat="server" Text='<%# Eval("studentphone2") %>'></asp:TextBox>     
                        
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox24" runat="server" Text='<%# Eval("studentphone2") %>'></asp:TextBox>
                     <asp:CompareValidator ID="CompareValidator51" ValidationGroup="abc" runat="server" ErrorMessage="**" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox24" Type="Integer"></asp:CompareValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label17" runat="server" Text='<%# Eval("studentphone2") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Email_ID" SortExpression="studentemail_id">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox25" runat="server" 
                            Text='<%# Eval("studentemail_id") %>'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1"   runat="server" ErrorMessage="***" ControlToValidate="TextBox25" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red"></asp:RegularExpressionValidator>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox26" runat="server" 
                            Text='<%# Eval("studentemail_id") %>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationGroup="abc" runat="server" ErrorMessage="***" ControlToValidate="TextBox26" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ForeColor="Red"></asp:RegularExpressionValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label18" runat="server" Text='<%# Eval("studentemail_id") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Picture" SortExpression="studentphoto">
                    <EditItemTemplate>
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:FileUpload ID="FileUpload2" runat="server" />
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" Height="80px" Width="80px" 
                            ImageUrl='<%# Eval("studentphoto") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="10 Marks *" SortExpression="student10marks">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox27" runat="server" Text='<%# Eval("student10marks") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox27"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator7" runat="server" ErrorMessage="**" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox27" Type="Integer"></asp:CompareValidator>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox28" runat="server" Text='<%# Eval("student10marks") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ValidationGroup="abc" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="TextBox28"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator8" runat="server" ValidationGroup="abc" ErrorMessage="**" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox28" Type="Integer"></asp:CompareValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label19" runat="server" Text='<%# Eval("student10marks") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="12 Marks" SortExpression="student12marks">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox29" runat="server" Text='<%# Eval("student12marks") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox30" runat="server" Text='<%# Eval("student12marks") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label20" runat="server" Text='<%# Eval("student12marks") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Blood Group" SortExpression="studentbloodgroup">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox31" runat="server" 
                            Text='<%# Eval("studentbloodgroup") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox32" runat="server" 
                            Text='<%# Eval("studentbloodgroup") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label21" runat="server" Text='<%# Eval("studentbloodgroup") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Achievement" SortExpression="studentachievement">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox33" runat="server" 
                            Text='<%# Eval("studentachievement") %>' Height="92px" 
                            TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox34" runat="server" 
                            Text='<%# Eval("studentachievement") %>' Height="92px" 
                            TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label22" runat="server" Text='<%# Eval("studentachievement") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Skill" SortExpression="studentskill">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox35" runat="server" Text='<%# Eval("studentskill") %>' 
                            Height="92px" TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox36" runat="server" Text='<%# Eval("studentskill") %>' 
                            Height="92px" TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label23" runat="server" Text='<%# Eval("studentskill") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Training" SortExpression="studenttraining">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox37" runat="server" 
                            Text='<%# Eval("studenttraining") %>' Height="92px" TextMode="MultiLine" 
                            Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox38" runat="server" 
                            Text='<%# Eval("studenttraining") %>' Height="92px" TextMode="MultiLine" 
                            Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label24" runat="server" Text='<%# Eval("studenttraining") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Hobbies" SortExpression="studenthobbies">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox39" runat="server" Text='<%# Eval("studenthobbies") %>' 
                            Height="92px" TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox40" runat="server" Text='<%# Eval("studenthobbies") %>' 
                            Height="92px" TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label25" runat="server" Text='<%# Eval("studenthobbies") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Resume File" SortExpression="studentresumefile">
                    <EditItemTemplate>
                        <asp:FileUpload ID="FileUpload3" runat="server" />
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:FileUpload ID="FileUpload4" runat="server" />
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label26" runat="server" Text='<%# Eval("studentresumefile") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="IS Hostler" SortExpression="studentishostler">
                   
                    <%-- <EditItemTemplate>
                     <asp:RadioButtonList ID="RBLgender" runat="server">
                       <asp:ListItem Value="True">Yes</asp:ListItem>
                       <asp:ListItem Value="False">No</asp:ListItem>
                     </asp:RadioButtonList>
                       <asp:RequiredFieldValidator ID="RequiredFieldValidatordoj" runat="server" 
                      ControlToValidate="RBLgender" Display="Dynamic" ForeColor="Red">*</asp:RequiredFieldValidator>
                      </EditItemTemplate>--%>
                      <%--<ItemTemplate>
                    <asp:Label ID="Lblgender" runat="server" Text='<%# Eval("studentishostler") %>'></asp:Label>
                     </ItemTemplate>
                </asp:TemplateField>--%>
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" 
                            Text='<%# Eval("studentishostler") %>' />
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:CheckBox ID="CheckBox2" runat="server" 
                            Text='<%# Eval("studentishostler") %>' />
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label27" runat="server" Text='<%# Eval("studentishostler") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <EditItemTemplate>
                        <asp:LinkButton ID="LinkButton6" runat="server" CommandName="update" >Update</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="LinkButton7" runat="server" CommandName="cancel" >Cancel</asp:LinkButton>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Insert" ValidationGroup="abc">Insert</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="LinkButton5" runat="server" CommandName="cancel" >Cancel</asp:LinkButton>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="new">New</asp:LinkButton>
            
                        &nbsp;
                    </ItemTemplate>
                </asp:TemplateField>
            </Fields>
            <RowStyle HorizontalAlign="Left"  />
        </asp:DetailsView>
    
                            </td>
                        </tr>
                        </table>
    
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
   

</asp:Content>

