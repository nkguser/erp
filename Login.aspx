﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Default2" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">  
    <table style="width: 100%">
        <tr>
            <td colspan="2">
                <asp:Label ID="LBLmsg" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" CssClass="unwatermarked" Width="193px"></asp:TextBox>
                <asp:TextBoxWatermarkExtender ID="TextBox1_TextBoxWatermarkExtender" 
                    runat="server" Enabled="True" TargetControlID="TextBox1" 
                    WatermarkText="Enter Username" WatermarkCssClass="watermarked">
                </asp:TextBoxWatermarkExtender>
            </td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server" CssClass="unwatermarked" Width="181px" 
                     TextMode="Password"></asp:TextBox>
                <asp:TextBoxWatermarkExtender ID="TextBox2_TextBoxWatermarkExtender" 
                    runat="server" Enabled="True" TargetControlID="TextBox2" 
                    WatermarkText="Enter Password" WatermarkCssClass="watermarked">
                </asp:TextBoxWatermarkExtender>
            </td>
            <td>
                <asp:Button ID="BTNLogin" runat="server" Text="Continue" CssClass="field2" 
                    Height="39px" Width="86px" onclick="BTNLogin_Click" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

</asp:Content>

