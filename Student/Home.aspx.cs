﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;

public partial class Student_Home : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (Page.IsPostBack == false)
        {
            dleventbind();
            GetData();
            Dlist_bind();
            GridShowSummaryBind(Convert.ToInt32(Session["other"]));
            Dlist_bind2();
            con.Open();
            SqlCommand cmd = new SqlCommand("select studentphoto,studentfname,studentmname,studentlname,studentrollno from tbstudent where studentid=" + Convert.ToInt32(Session["stid"]), con);
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                string xyz = dr[0].ToString();
                string fname = dr[1].ToString();
                string mname = dr[2].ToString();
                string lname = dr[3].ToString();
                Int32 roll = Convert.ToInt32(dr[4]);
                Image2.ImageUrl = "~/Studentpic" + "//" + xyz;
                lblname.Text = fname + " " + mname + " " + lname;
                lblroll.Text = Convert.ToString(roll);
            }
            dr.Close();
            cmd.Dispose();
            SqlCommand cmd54 = new SqlCommand("select depname from tbdep where depid=" + Convert.ToInt32(Session["depid"]), con);
            SqlDataReader dr1 = cmd54.ExecuteReader();
            if (dr1.HasRows)
            {
                dr1.Read();
                string dep = dr1[0].ToString();
                lbldep.Text = dep;
            }
            dr1.Close();
            cmd54.Dispose();
        }
    }
    private void Dlist_bind()
    {
        SqlCommand cmd = new SqlCommand("SELECT tbnotice.noticeid,  tbnotice.noticesubject, tbnotice.noticecontent, tbnotice.noticedate FROM tbnotice INNER JOIN tblevel ON tbnotice.noticelevelid = tblevel.levelid AND tblevel.levelid = 1 ", con);
        cmd.Parameters.Add("@u",SqlDbType.Int).Value=Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DataList2.DataSource = ds;
        DataList2.DataBind();
        cmd.Dispose();
    }
    private void Dlist_bind2()
    {
        SqlCommand cmd = new SqlCommand("SELECT tbnotice.noticeid,  tbnotice.noticesubject, tbnotice.noticecontent, tbnotice.noticedate FROM tbnotice INNER JOIN tblevel ON tbnotice.noticelevelid = tblevel.levelid AND tblevel.levelid = 3 ", con);
        cmd.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DataList1.DataSource = ds;
        DataList1.DataBind();
        cmd.Dispose();
    }
    private void rep_bind(Int32 a)
    {
        SqlCommand cmd = new SqlCommand("SELECT   tbnotice.noticesubject, tbnotice.noticecontent, tbnotice.noticedate FROM tbnotice where noticeid=@u", con);
        cmd.Parameters.Add("@u", SqlDbType.Int).Value = a;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        imglightbox.DataSource = ds;
        imglightbox.DataBind();
        cmd.Dispose();
    }     

    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if(e.CommandName=="Show")
        {
            //Button imgbtn = sender as Button;
            ////Find Image button in gridview
            //Button imgbtntxt = (Button)Repeater1.FindControl("imgbtn");
            //Assign imagebutton url to image field in lightbox
            Int32 a = Convert.ToInt32(DataList1.DataKeys[e.Item.ItemIndex]);
            rep_bind(a);
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:ShowImages();", true);
        }
    }
    protected void DataList2_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "Show")
        {
           
            Int32 a = Convert.ToInt32(DataList2.DataKeys[e.Item.ItemIndex]);
            rep_bind(a);
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:ShowImages();", true);
        }
    }
    private void GetData()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        DDLSem.Items.Clear();
        SqlCommand cmd = new SqlCommand("SELECT     tbcourse.coursesemester FROM  tbcourse INNER JOIN tbdep ON tbcourse.courseid = tbdep.depcourseid INNER JOIN tbclass ON tbdep.depid = tbclass.classdepid INNER JOIN                      tbgroup ON tbclass.classid = tbgroup.groupclassid INNER JOIN                      tbstudent ON tbgroup.groupid = tbstudent.studentgroupid WHERE     (tbcourse.coursedelsts = 0) AND (tbdep.depdelsts = 0) AND (tbgroup.groupdelsts = 0) AND (tbclass.classdelsts = 0) AND (tbstudent.studentdelsts = 0) AND                       (tbstudent.studentid = @stid)", con);
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        int count = Int32.Parse(dr["coursesemester"].ToString());
        DDLSem.Items.Insert(0, "-- Semester --");
        //DDLSem.Items[0].Enabled = false;
        for (int i = 1; i <= count; i++)
        {
            DDLSem.Items.Add(i.ToString());
        }
        // DDLSem.Items.Insert(0, "-- Choose Semester --");
        //DDLSem.Items[0].Enabled = false;
        dr.Close();
        cmd.Dispose();
        con.Close();
    }
    private void GridShowSummaryBind(Int32 a)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT distinct tbstudentattnd.studentattndsubjectid,tbsubject.subjectUNICODE as subject, tbsubject.subjecttitle + ' - ' + tbsubjectype.subjecttypename AS subject1 FROM         tbsubject INNER JOIN tbstudentattnd ON tbsubject.subjectid = tbstudentattnd.studentattndsubjectid INNER JOIN  tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid WHERE     (tbstudentattnd.studentattndstudentid = @stid) AND (tbsubject.subjectsem = @sno) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbsubject.subjectdelsts = 0) AND (tbsubjectype.subjectdelsts = 0) ORDER BY subject", con);
        cmd.Parameters.Add("@sno", SqlDbType.Int).Value = a;
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShowSummary.DataSource = ds;
        GridShowSummary.DataBind();
        cmd.Dispose();
        con.Close();
    }
    protected void GridShowSummary_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("SELECT     COUNT(*) AS count FROM         tbstudentattnd WHERE     (studentattndstudentid = @stid) AND (studentattndattend = 'True') AND (studentattndsubjectid = @sid) AND (studentattnddelsts = 0)", con);
            SqlCommand cmd1 = new SqlCommand("SELECT COUNT(*) AS count FROM tbstudentAttnd WHERE (studentattndstudentid = @stid) AND (studentattndsubjectid = @sid) AND (studentattnddelsts = 0)", con);
            cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSummary.DataKeys[e.Row.RowIndex].Values["studentattndsubjectid"]);
            cmd1.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
            cmd1.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSummary.DataKeys[e.Row.RowIndex].Values["studentattndsubjectid"]);
            Label l1 = (Label)(e.Row.FindControl("LblAt"));
            Label l2 = (Label)(e.Row.FindControl("LblTotal"));
            Label l3 = (Label)(e.Row.FindControl("Lblpercent"));
            Image img123 = (Image)(e.Row.FindControl("imggrp"));
            l1.Text = cmd.ExecuteScalar().ToString();
            l2.Text = cmd1.ExecuteScalar().ToString();
            img123.ImageUrl = "Default.aspx?Prec=" + 100 * Convert.ToInt32(l1.Text) / Convert.ToInt32(l2.Text);
            if (Convert.ToInt32(l2.Text) != 0)
                l3.Text = (100 * Convert.ToInt32(l1.Text) / Convert.ToInt32(l2.Text)).ToString() + "%";
            else
                l3.Text = "--";
            cmd.Dispose();
            cmd1.Dispose();
            con.Close();
        }
    }
    protected void DDLSem_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(DDLSem.SelectedIndex==0)
        {
            DDLSem.SelectedIndex = 0;
        }
        else
        {
            GridShowSummaryBind(Convert.ToInt32(DDLSem.SelectedValue));
            GridShowSummary.Visible = true;
        }
    }
    private void dleventbind()
    {
        //click="MM_openBrWindow('Default3.aspx?Id=749','win2','scrollbars=yes,width=33320,height=500,resizable=no,top=150' )"
        SqlDataAdapter adp = new SqlDataAdapter("select * from tbevent where eventcatid=1", con);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DataList3.DataSource = ds;
        DataList3.DataBind();

    }
    protected void DataListE_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            Int32 key = Convert.ToInt32(DataList3.DataKeys[e.Item.ItemIndex]);
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:window.open('Default3.aspx?Id=" + key + "','win2', 'scrollbars=yes,width=620,height=500,resizable=no,top=150' );", true);
        }
    }
}