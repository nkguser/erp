﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class dir_sattd : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            DDLSessionBind();
        }
    }
    private void DDLSessionBind()
    {
        DDLSession.DataTextField = "session";
        DDLSession.DataValueField = "sessionid";
        DDLSession.DataSource = ERP.AllSession();
        DDLSession.DataBind();
        DDLSession.Items.Insert(0, "-- Select session --");
    }
    protected void DDLSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(DDLSession.SelectedIndex==0)
        {
            SessionViewClear();
        }
        else
        {
            SessionViewClear();
            DDLAllClassBind();
        }
    }
    private void SessionViewClear()
    {
        DDLClass.Items.Clear();
        DDLSubject.Items.Clear();
        LBLTotalDelivered.Text = string.Empty;
        datepanel.Visible = false;
        DDLClass.Enabled = false;
        DDLSubject.Enabled = false;
        DDLClass.Items.Insert(0, "--Select Class--");
        DDLSubject.Items.Insert(0, "--Select Subject--");
        DDLDate.Items.Clear();
        DDLDate.Items.Insert(0, "-- Choose Date --");
        approvedpanel.Visible = false;
    }
    private void DDLAllClassBind()
    {
        DDLClass.DataTextField = "Class";
        DDLClass.DataValueField = "groupid";
        DDLClass.DataSource = ERP.AllClassDIR(Convert.ToInt32(Convert.ToInt32(DDLSession.SelectedValue)));
        DDLClass.DataBind();
        DDLClass.Enabled = true;
        DDLClass.Items.Insert(0, "-- Select Class --");
    }
    protected void DDLClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLClass.SelectedIndex == 0)
        {
            classviewclear();
        }
        else
        {
            classviewclear();
            getdepsubjects();
              
        }
    }
    private void classviewclear()
    {
        DDLSubject.Items.Clear();
        LBLTotalDelivered.Text = string.Empty;
        datepanel.Visible = false;
        DDLSubject.Enabled = false;
        DDLSubject.Items.Insert(0, "--Select Subject--");
        DDLDate.Items.Clear();
        DDLDate.Items.Insert(0, "-- Choose Date --");
        approvedpanel.Visible = false;
    }
    private void getdepsubjects()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmdfind = new SqlCommand("SELECT   tbdep.depid FROM   tbdep INNER JOIN tbclass ON tbdep.depid = tbclass.classdepid INNER JOIN tbgroup ON tbclass.classid = tbgroup.groupclassid WHERE  (tbgroup.groupid = @gid)", con);
        cmdfind.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        Int32 did = Convert.ToInt32(cmdfind.ExecuteScalar());
        SqlCommand cmd = new SqlCommand("SELECT   tbsubject.subjecttitle + '(' + tbsubjectype.subjecttypename + ')' AS subject, tbsubject.subjectid FROM   tbsubject INNER JOIN tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid WHERE (tbsubject.subjectdepid = @did)", con);
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
        SqlDataReader dr = cmd.ExecuteReader();
        DDLSubject.DataTextField = "subject";
        DDLSubject.DataValueField = "Subjectid";
        DDLSubject.DataSource = dr;
        DDLSubject.DataBind();
        DDLSubject.Enabled = true;
        DDLSubject.Items.Insert(0, "-- Select Subject --");
        dr.Close();
        cmd.Dispose();
        con.Close();
    }
    protected void DDLSubject_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSubject.SelectedIndex == 0)
        {
            LBLTotalDelivered.Text = string.Empty;
            datepanel.Visible = false;
            approvedpanel.Visible = false;
        }
        else
        {
            LBLTotalDeliveredBind();
            approvedpanel.Visible = false;
            DDLDateBindapproved();
            datepanel.Visible = true;
        }
    }
    private void LBLTotalDeliveredBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT COUNT(DISTINCT tbstudentattnd.studentattnddate) AS count FROM  tbstudentattnd INNER JOIN tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE        (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0)", con);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        LBLTotalDelivered.Text = cmd.ExecuteScalar().ToString();
        cmd.Dispose();
        con.Close();
    }
    private void DDLDateBindapproved()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT DISTINCT tbstudentattnd.studentattnddate FROM tbstudentattnd INNER JOIN tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE     (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattndapprovlvid = 3) AND (tbstudent.studentdelsts = 0)", con);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DDLDate.DataTextField = "studentattndDate";
        DDLDate.DataValueField = "studentattndDate";
        DDLDate.DataSource = ds;
        DDLDate.DataBind();
        if (ds.Tables[0].Rows.Count == 0)
        {
            DDLDate.Items.Insert(0, "-- No Dates Available --");
            DDLDate.Enabled = false;
        }
        else
        {
            DDLDate.Items.Insert(0, "-- Choose Date --");
            DDLDate.Enabled = true;
        }
        cmd.Dispose();
        ds.Dispose();
        con.Close();
    }
    protected void DDLDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLDate.SelectedIndex == 0)
        {
            approvedpanel.Visible = false;
        }
        else
        {
            approvedpanel.Visible = true;
            GridShowSubmitBind();

        } 
    }
    private void GridShowSubmitBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT tbstudent.studentid, tbstudent.studentrollno, tbstudentattnd.studentattndattend FROM tbstudent INNER JOIN tbstudentattnd ON tbstudent.studentid = tbstudentattnd.studentattndstudentid WHERE (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudentattnd.studentattnddate = @date) AND (tbstudent.studentgroupid = @gid) AND (tbstudent.studentdelsts = 0) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattndapprovlvid = 3)", con);
        cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(DDLDate.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShowSubmit.DataSource = ds;
        GridShowSubmit.DataBind();
        GridShowSubmit.Visible = true;
        cmd.Dispose();
        con.Close();
    }   
    protected void GridShowSubmit_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) AS count FROM  tbstudentattnd WHERE        (studentattndsubjectid = @sid) AND (studentattndstudentid = @rid) AND (studentattndattend = 'True') AND (studentattnddelsts = 0)", con);
            // set gridview datakey
            cmd.Parameters.Add("@rid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSubmit.DataKeys[e.Row.RowIndex].Value);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
            Label l = (Label)(e.Row.FindControl("LBLAttend2"));
            Label l1 = (Label)(e.Row.FindControl("LblAt2"));
            Label l2 = (Label)(e.Row.FindControl("Lblpercent2"));
            l1.Text = cmd.ExecuteScalar().ToString();
            l2.Text = (100 * Convert.ToInt32(l1.Text) / Convert.ToInt32(LBLTotalDelivered.Text)).ToString() + "%";
            cmd.Dispose();
            con.Close();
            if (l.Text.Equals("True"))
                l.Text = "Present";
            else
                l.Text = "Absent";
        }
    }
}