﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/InnerStudent.master" AutoEventWireup="true" CodeFile="Download.aspx.cs" Inherits="Student_Download" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <table class="styledmenu">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                <asp:HyperLink ID="HyperLink1" runat="server" ImageUrl="~/images/timetable.png" ToolTip="Download Time Table" 
                     >Time Table</asp:HyperLink>
            </td>
            <td>
                <asp:HyperLink ID="HyperLink2" runat="server" ImageUrl="~/images/syllabus.png" 
                    ToolTip="Download Syllabus">Syllabus</asp:HyperLink>
            </td>
            <td>
                <asp:HyperLink ID="HyperLink3" runat="server" ImageUrl="~/images/download.png" 
                    ToolTip="Download Holiday List">Holiday List</asp:HyperLink>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

