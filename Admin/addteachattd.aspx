﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" MasterPageFile="~/Admin/InnerAdmin.master" AutoEventWireup="true" CodeFile="addteachattd.aspx.cs" Inherits="Admin_addteachattd" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <table width="100%" style="text-align: left; font-weight: bold;">
   <tr>
   <td>
       &nbsp;</td>
   <td>
        <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Size="X-Large" 
            ForeColor="#003399">Teacher&#39;s Attendance</asp:Label>
        </td>
    </tr>
   <tr>
   <td>
       &nbsp;</td>
   <td>
        <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Size="Large" 
            ForeColor="#003300"></asp:Label>
        </td>
    </tr>
   <tr>
   <td>
   Course</td>
   <td>
        <asp:DropDownList ID="DropDownList1" runat="server" 
            Width="200px">
        </asp:DropDownList>
        <ajax:CascadingDropDown ID="CascadingDropDown1" runat="server" Category="Course" TargetControlID="DropDownList1" LoadingText="Loading Course..." PromptText="Select Course" ServiceMethod="BindCoursedropdown" ServicePath="DropdownWebService.asmx"></ajax:CascadingDropDown>
        <asp:RequiredFieldValidator ID="DropDownList11" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="DropDownList1" ValidationGroup="pqr" ></asp:RequiredFieldValidator>
        </td>
    </tr>
            <tr>
                <td>
                    Department :</td>
                    <td>
                    <asp:DropDownList ID="DropDownList2" runat="server" Width="200px" 
                            AutoPostBack="True">
                    </asp:DropDownList>
                    <ajax:CascadingDropDown ID="CascadingDropDown2" runat="server" Category="Course" TargetControlID="DropDownList2" ParentControlID="DropDownList1" LoadingText="Loading Department..." PromptText="Select Department" ServiceMethod="BindDepartmentdropdown" ServicePath="DropdownWebService.asmx"></ajax:CascadingDropDown>
                    <asp:RequiredFieldValidator ID="DropDownList21" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="DropDownList2" ValidationGroup="pqr" ></asp:RequiredFieldValidator>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    Date(DD/MM/YYYY):</td>
                    <td>
                    <asp:TextBox ID="TextBox1" runat="server" Width="200px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="DropDownList22" runat="server" ErrorMessage="*" 
                            ForeColor="Red" ControlToValidate="TextBox1" ValidationGroup="pqr" 
                            Display="Dynamic" ></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" 
                            ControlToValidate="TextBox1" Display="Dynamic" ErrorMessage="**" 
                            Font-Bold="True" ForeColor="Red" Operator="DataTypeCheck" Type="Date" 
                            ValidationGroup="pqr"></asp:CompareValidator>
                    <%--<asp:RangeValidator ID="RangeValidator1" runat="server" 
                        ControlToValidate="TextBox1" ErrorMessage="RangeValidator" ForeColor="#FF3300" 
                        MaximumValue="30/12/2020" MinimumValue="30/12/2011" Type="Date">asdd</asp:RangeValidator>--%>
                    <asp:CalendarExtender Format="dd/MM/yyyy " ID="TextBox1_CalendarExtender" runat="server" 
                        Enabled="True"  TargetControlID="TextBox1">
                    </asp:CalendarExtender>
                   <%-- <asp:MaskedEditValidator ID="MaskedEditValidator1" runat="server" 
                        ControlExtender="TextBox1_MaskedEditExtender" ControlToValidate="TextBox1" 
                        Display="Dynamic" EmptyValueMessage="Invalid Date" ErrorMessage="*" 
                        Font-Bold="True" ForeColor="#FF3300" InvalidValueMessage="Invalid Date" 
                        SetFocusOnError="True">*</asp:MaskedEditValidator>
                    <asp:MaskedEditExtender ID="TextBox1_MaskedEditExtender" runat="server" 
                        CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" 
                        CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" 
                        CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" 
                        Mask="99/99/9999" MaskType="Date" TargetControlID="TextBox1" 
                        ClearTextOnInvalid="True" UserDateFormat="DayMonthYear">
                    </asp:MaskedEditExtender>--%>&nbsp;<asp:LinkButton ID="LinkButton2" runat="server" onclick="LinkButton2_Click" 
                        style="font-weight: 700" ValidationGroup="pqr">Display</asp:LinkButton>
                    </td>
                <td>
                   </td>
            </tr>
            <tr>
                <td colspan="2">
                    <%--<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                        CellPadding="4" DataKeyNames="teacherid" 
                        EmptyDataText="There are no data records to display." ForeColor="#333333" 
                        GridLines="None" ondatabound="GridView1_DataBound">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />                        
                        <Columns>                                                                                                                                                                  
                            <asp:TemplateField HeaderText="Teacher">
                            <ItemTemplate>
                            <table>
                            <tr>
                            <td>
                                <asp:Image ID="Image1" runat="server" ImageUrl='<%#"~/Teacherpic/" + Eval("teacherphoto") %>'  Height="100px" Width="100px"/>
                            </td>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("name") %>'></asp:Label><br />
                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("postdesc") %>'></asp:Label><br />
                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("depname") %>'></asp:Label>
                            
                                <br />
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                                    DataSourceID="SqlDataSource1" DataTextField="teachattdtypedesc" 
                                    DataValueField="teachtattdtypeid">
                                </asp:RadioButtonList>
                            
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:cn %>" 
                                    DeleteCommand="DELETE FROM [tbteachattdtype] WHERE [teachtattdtypeid] = @teachtattdtypeid" 
                                    InsertCommand="INSERT INTO [tbteachattdtype] ([teachattdtypedesc]) VALUES (@teachattdtypedesc)" 
                                    ProviderName="<%$ ConnectionStrings:cn.ProviderName %>" 
                                    SelectCommand="SELECT [teachtattdtypeid], [teachattdtypedesc] FROM [tbteachattdtype]" 
                                    UpdateCommand="UPDATE [tbteachattdtype] SET [teachattdtypedesc] = @teachattdtypedesc WHERE [teachtattdtypeid] = @teachtattdtypeid">
                                    <DeleteParameters>
                                        <asp:Parameter Name="teachtattdtypeid" Type="Int32" />
                                    </DeleteParameters>
                                    <InsertParameters>
                                        <asp:Parameter Name="teachattdtypedesc" Type="String" />
                                    </InsertParameters>
                                    <UpdateParameters>
                                        <asp:Parameter Name="teachattdtypedesc" Type="String" />
                                        <asp:Parameter Name="teachtattdtypeid" Type="Int32" />
                                    </UpdateParameters>
                                </asp:SqlDataSource>
                            
                            </td>
                            </tr>
                            </table>
                            </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#999999" />
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                    </asp:GridView>--%>
                   
                    <asp:Panel ID="pp" runat="server" ScrollBars="Auto" Visible="false" Height="435px">
                      
                    <asp:DataList ID="DataList1" runat="server" BackColor="#669999" 
                        BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" 
                        DataKeyField="teacherid" GridLines="Both" 
                        onitemdatabound="DataList1_ItemDataBound" RepeatColumns="5" 
                        RepeatDirection="Horizontal" CellSpacing="2" ForeColor="Black" 
                            Font-Bold="False" Font-Italic="False" Font-Overline="False" 
                            Font-Strikeout="False" Font-Underline="False">
                        <AlternatingItemStyle BackColor="White" />
                        <FooterStyle BackColor="#CCCCCC" />
                        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                        <ItemStyle BackColor="#FFCCFF" ForeColor="#4A3C8C" Font-Bold="False" 
                            Font-Italic="False" Font-Overline="False" Font-Strikeout="False" 
                            Font-Underline="False" />
                        <SelectedItemStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                    <ItemTemplate>
                            <table>
                            <tr>
                            <td>
                                <asp:Image ID="Image1" runat="server" 
                                    ImageUrl='<%#"~/Teacherpic/" + Eval("teacherphoto") %>'  Height="40px" 
                                    Width="40px"/>
                            </td>
                            <td >
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("name") %>' 
                                    Font-Bold="True" Font-Size="Smaller"></asp:Label><br />
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("postdesc") %>' 
                                    Font-Bold="True" Font-Size="Smaller"></asp:Label><br />
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("depname") %>' 
                                    Font-Bold="True" Font-Size="Smaller"></asp:Label>
                            
                                <br />
                                <asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                                    DataSourceID="SqlDataSource1" DataTextField="teachattdtypedesc" 
                                    DataValueField="teachtattdtypeid" Font-Bold="True" Font-Size="Smaller" 
                                    ForeColor="#003399">
                                </asp:RadioButtonList>
                            
                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                                    ConnectionString="<%$ ConnectionStrings:cn %>" 
                                    DeleteCommand="DELETE FROM [tbteachattdtype] WHERE [teachtattdtypeid] = @teachtattdtypeid" 
                                    InsertCommand="INSERT INTO [tbteachattdtype] ([teachattdtypedesc]) VALUES (@teachattdtypedesc)" 
                                    ProviderName="<%$ ConnectionStrings:cn.ProviderName %>" 
                                    SelectCommand="SELECT [teachtattdtypeid], [teachattdtypedesc] FROM [tbteachattdtype]" 
                                    UpdateCommand="UPDATE [tbteachattdtype] SET [teachattdtypedesc] = @teachattdtypedesc WHERE [teachtattdtypeid] = @teachtattdtypeid">
                                    <DeleteParameters>
                                        <asp:Parameter Name="teachtattdtypeid" Type="Int32" />
                                    </DeleteParameters>
                                    <InsertParameters>
                                        <asp:Parameter Name="teachattdtypedesc" Type="String" />
                                    </InsertParameters>
                                    <UpdateParameters>
                                        <asp:Parameter Name="teachattdtypedesc" Type="String" />
                                        <asp:Parameter Name="teachtattdtypeid" Type="Int32" />
                                    </UpdateParameters>
                                </asp:SqlDataSource>
                            
                            </td>
                            </tr>
                            </table>
                            </ItemTemplate>
                        <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    </asp:DataList>
                    <asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click" 
                        style="font-weight: 700" ValidationGroup="pqr">Save</asp:LinkButton>
                    </asp:Panel>
                    
                    <br />
                    <br />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
</asp:Content>

