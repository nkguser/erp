﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class HOD_suballot : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            GridAllTeacherBind();
            DDlTeacherBind();
            DDLSubjectBind();
            seserp.DropdownWebService obj = new seserp.DropdownWebService();
            obj.sum(Convert.ToInt32(Session["other"]));
        }
    }
    private void GridTeacherBind()
    {
        SqlCommand cmd = new SqlCommand("DispSubTeacher", con);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);

        cmd.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(DDlTeacher.SelectedValue);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridTeacher.DataSource = ds;
        GridTeacher.DataBind();
        // GridAllTeacher.DataBind();
    }
    private void GridAllTeacherBind()
    {
        SqlCommand cmd = new SqlCommand("dispsuballoted", con);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridAllTeacher.DataSource = ds;
        GridAllTeacher.DataBind();
        GridAllTeacher.Visible = true;
    }
    private void DDLSubjectBind()
    {
        con.Open();
        SqlCommand cmd = new SqlCommand("SELECT tbSubject.Subjectid, tbSubject.Subjecttitle + ' (' + tbSubjectype.SubjectTypename + ')' AS subject FROM tbSubject INNER JOIN tbSubjectype ON tbSubject.Subjecttypeid = tbSubjectype.SubjectTypeid WHERE (Subjectdepid = @did) AND (tbsubject.Subjectdelsts = 0) AND (tbsubjectype.Subjectdelsts = 0)", con);
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        SqlDataReader dr = cmd.ExecuteReader();
        DDlSubject.DataTextField = "subject";
        DDlSubject.DataValueField = "Subjectid";
        DDlSubject.DataSource = dr;
        DDlSubject.DataBind();
        DDlSubject.Items.Insert(0, "-- Select Subject --");
    }
    private void DDlTeacherBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand com = new SqlCommand("select teacherid,TeacherFname +' '+TeacherMName +' '+TeacherLname as Name from tbTeacher where teacherDepid=@did and teacherdelsts=0 ", con);
        int i = Convert.ToInt32(Session["depid"]);
        com.Parameters.Add("@did", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        SqlDataReader dr1 = com.ExecuteReader();
        DDlTeacher.DataValueField = "teacherid";
        DDlTeacher.DataTextField = "name";
        DDlTeacher.DataSource = dr1;
        DDlTeacher.DataBind();
        DDlTeacher.Items.Insert(0, "-- Select Teacher --");
        com.Dispose();
        con.Close();

    }
    protected void BTNSubAllot_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (DDlTeacher.SelectedIndex == 0 && DDlSubject.SelectedIndex == 0)
        {
            LBLmsg.Text = "Choose Teacher and Subject";
        }
        else if (DDlTeacher.SelectedIndex == 0)
        {
            LBLmsg.Text = "Choose Teacher ";
        }
        else if (DDlSubject.SelectedIndex == 0)
        {
            LBLmsg.Text = "Choose Subject";
        }
        else
        {
            if (Convert.ToInt32(DDLDept.SelectedValue) != Convert.ToInt32(Session["depid"]))
            {
                LBLmsg.Text = "You are not authorized to Alott to other department";
            }
            else
            {
                SqlCommand comcheck = new SqlCommand("SELECT  count(*) as cnt FROM tbsubjectallot WHERE  (subjectallotsubjectid = @sid) AND (subjectallotgroupid = @gid)", con);
                //comcheck.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(DDlTeacher.SelectedValue);
                comcheck.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDlSubject.SelectedValue);
                comcheck.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLGroup.SelectedValue);
                Int32 check = Convert.ToInt32(comcheck.ExecuteScalar());
                if (check == 0)
                {


                    SqlCommand com = new SqlCommand("Insert into tbsubjectAllot values(@tid,@sid,@gid,0,@aid)", con);
                    com.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(DDlTeacher.SelectedValue);
                    com.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDlSubject.SelectedValue);
                    com.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLGroup.SelectedValue);
                    com.Parameters.Add("@aid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
                    com.ExecuteNonQuery();
                    com.Dispose();
                    con.Close();
                    GridAllTeacherBind();
                    GridTeacherBind();
                    LBLmsg.Text = "Alotted Successfully";

                }
                else
                {
                    LBLmsg.Text = "Already Alotted, Please Choose other options ";
                }
            }

        }
    }
    protected void GridAllTeacher_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridAllTeacher.EditIndex = e.NewEditIndex;
        GridAllTeacherBind();
    }
    protected void GridAllTeacher_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //GridAllTeacherBind();
    }
    protected void GridAllTeacher_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridAllTeacher.EditIndex = -1;
        GridAllTeacherBind();
    }
    protected void GridAllTeacher_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        Int32 tid, sid, gid;
        tid = (Int32)(GridAllTeacher.DataKeys[e.RowIndex].Values["subjectallotteacherid"]);
        sid = (Int32)(GridAllTeacher.DataKeys[e.RowIndex].Values["subjectallotsubjectid"]);
        gid = (Int32)(GridAllTeacher.DataKeys[e.RowIndex].Values["subjectallotgroupid"]);
        SqlCommand cmd = new SqlCommand(" Delete from tbsubjectallot WHERE (subjectallotteacherid = @tid) AND (subjectallotsubjectid = @sid) AND (subjectallotgroupid = @gid)", con);
        cmd.Parameters.Add("@tid", SqlDbType.Int).Value = tid;
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        GridAllTeacherBind();
        if (DDlTeacher.SelectedIndex == 0)
            return;
        else
            GridTeacherBind();
    }
    protected void GridAllTeacher_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        if (e.Row.RowState == DataControlRowState.Edit)
        {
            DropDownList ddlcr = (DropDownList)e.Row.FindControl("DDLCourse");
            DropDownList ddld = (DropDownList)e.Row.FindControl("DDLDept");
            DropDownList ddlses = (DropDownList)e.Row.FindControl("DDLSession");
            DropDownList ddlcl = (DropDownList)e.Row.FindControl("DDLClass");
            DropDownList ddlg = (DropDownList)e.Row.FindControl("DDLGroup");
            DropDownList ddlsem = (DropDownList)e.Row.FindControl("DDLSem");
            DropDownList ddlSub = (DropDownList)e.Row.FindControl("DDLSub");
            ddlcr.DataSource = ERP.AllCourse();
            ddlcr.DataBind();
            ddlses.DataSource = ERP.AllSession();
            ddlses.DataBind();
            ddlcl.Enabled = true;
            ddlses.Items.Insert(0, "-- Select Class --");
        }
        //}
    }
    protected void GridAllTeacher_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void DDlTeacher_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDlTeacher.SelectedIndex == 0)
        {
            ddlGroup4.Enabled = true;
        }
        else
        {

            GridTeacherBind();
            GridTeacher.Visible = true;
            ddlGroup4.Enabled = false;
        }
    }
    //protected void GridAllTeacher_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    GridAllTeacher.PageIndex = e.NewPageIndex;
    //    GridAllTeacherBind();
    //}

    //protected void GridTeacher_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //{
    //    GridTeacher.PageIndex = e.NewPageIndex;
    //    GridTeacherBind();
    //}
    protected void DDlSubject_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDlTeacher.SelectedIndex == 0)
        {
            ddlGroup5.Enabled = true;
        }
        else
        {
            ddlGroup4.Enabled = false;
        }
    }
    protected void GridTeacher_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        Int32 tid, sid, gid;
        tid = (Int32)(GridTeacher.DataKeys[e.RowIndex].Values["subjectallotteacherid"]);
        sid = (Int32)(GridTeacher.DataKeys[e.RowIndex].Values["subjectallotsubjectid"]);
        gid = (Int32)(GridTeacher.DataKeys[e.RowIndex].Values["subjectallotgroupid"]);
        SqlCommand cmd = new SqlCommand(" Delete from tbsubjectallot WHERE (subjectallotteacherid = @tid) AND (subjectallotsubjectid = @sid) AND (subjectallotgroupid = @gid)", con);
        cmd.Parameters.Add("@tid", SqlDbType.Int).Value = tid;
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        GridAllTeacherBind();
        GridTeacherBind();
    }
    
}