﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default3.aspx.cs" Inherits="Default3" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table class="style1">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:DataList ID="DataList1" runat="server" DataKeyField="Eventid" 
                    CellPadding="2" ForeColor="#333333" 
                    ShowFooter="False" RepeatColumns="4" RepeatDirection="Horizontal">
                    <AlternatingItemStyle BackColor="White" />
                    <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <ItemStyle BackColor="#E3EAEB" />
                    <ItemTemplate>
                    <table><tr><td> <asp:Image ID="Image1" runat="server"  ImageUrl='<%#"~/event/"+Eval("Eventlogo") %>' Height="100" Width="100"/></td>
                    <td>
                    <h2>
                        <asp:Label ID="EventNAmeLabel" runat="server" Text='<%# Eval("EventNAme") %>' />
                        </h2>
                        <br />                        
                        <b>From : </b>
                        <asp:Label ID="eventfrmdatLabel" runat="server" 
                            Text='<%# Eval("eventfrmdat") %>' />
                        <br />
                        <b>To : </b>
                        <asp:Label ID="eventtodatLabel" runat="server" 
                            Text='<%# Eval("eventtodat") %>' />
                        <br />
                        <b>Venue : </b>
                        <asp:Label ID="eventvenueLabel" runat="server" 
                            Text='<%# Eval("eventvenue") %>' />                                             
<br /></td></tr>
<tr><td colspan="2"><i> 
                        <asp:Label ID="eventdetailLabel" runat="server" 
                            Text='<%# Eval("eventdetail") %>' />
                            </i> </td></tr></table><br /><br />
                    </ItemTemplate>
                    <SelectedItemStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                </asp:DataList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <div>
    
    </div>
    </form>
</body>
</html>
