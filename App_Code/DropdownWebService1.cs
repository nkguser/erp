﻿using System;
using System.Collections;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Collections.Specialized;
using AjaxControlToolkit;
using System.Configuration;
using System.Data;

/// <summary>
/// Summary description for DropdownWebService1
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService()]
public class DropdownWebService1 : System.Web.Services.WebService
{

    [WebMethod]
    public CascadingDropDownNameValue[] BindCoursedropdown(string knownCategoryValues, string category)
    {
        SqlConnection concourse = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        concourse.Open();
        SqlCommand cmdcourse = new SqlCommand("select * from tbcourse", concourse);
        SqlDataAdapter dacourse = new SqlDataAdapter(cmdcourse);
        cmdcourse.ExecuteNonQuery();
        DataSet dscourse = new DataSet();
        dacourse.Fill(dscourse);
        concourse.Close();
        List<CascadingDropDownNameValue> coursedetails = new List<CascadingDropDownNameValue>();
        foreach (DataRow dtrow in dscourse.Tables[0].Rows)
        {
            string CourseID = dtrow["courseid"].ToString();
            string CourseName = dtrow["coursename"].ToString();
            coursedetails.Add(new CascadingDropDownNameValue(CourseName, CourseID));
        }
        return coursedetails.ToArray();
    }
    [WebMethod]
    public CascadingDropDownNameValue[] BindSessiondropdown(string knownCategoryValues, string category)
    {
        SqlConnection conSession = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        conSession.Open();
        SqlCommand cmdSession = new SqlCommand("select * from tbSession", conSession);
        SqlDataAdapter daSession = new SqlDataAdapter(cmdSession);
        cmdSession.ExecuteNonQuery();
        DataSet dsSession = new DataSet();
        daSession.Fill(dsSession);
        conSession.Close();
        List<CascadingDropDownNameValue> Sessiondetails = new List<CascadingDropDownNameValue>();
        foreach (DataRow dtrow in dsSession.Tables[0].Rows)
        {
            string SessionID = dtrow["Sessionid"].ToString();
            string SessionName = dtrow["sessionstart"].ToString();
            Sessiondetails.Add(new CascadingDropDownNameValue(SessionName, SessionID));
        }
        return Sessiondetails.ToArray();
    }
    [WebMethod]
    public CascadingDropDownNameValue[] BindDepartmentdropdown(string knownCategoryValues, string category)
    {
        int CourseID;
        StringDictionary coursedetails = AjaxControlToolkit.CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);
        CourseID = Convert.ToInt32(coursedetails["Course"]);
        SqlConnection condepartment = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        condepartment.Open();
        SqlCommand cmddepartment = new SqlCommand("select * from tbdep where depcourseid=@courseid", condepartment);
        cmddepartment.Parameters.AddWithValue("@courseid", CourseID);
        cmddepartment.ExecuteNonQuery();
        SqlDataAdapter dadepartment = new SqlDataAdapter(cmddepartment);
        DataSet dsdepartment = new DataSet();
        dadepartment.Fill(dsdepartment);
        condepartment.Close();
        List<CascadingDropDownNameValue> departmentdetails = new List<CascadingDropDownNameValue>();
        foreach (DataRow dtdepartmentrow in dsdepartment.Tables[0].Rows)
        {
            string departmentID = dtdepartmentrow["depid"].ToString();
            string departmentname = dtdepartmentrow["depname"].ToString();
            departmentdetails.Add(new CascadingDropDownNameValue(departmentname, departmentID));
        }
        return departmentdetails.ToArray();
    }
   
    [WebMethod]
    public CascadingDropDownNameValue[] BindClassdropdown(string knownCategoryValues, string category)
    {

        int departmentID;
        StringDictionary departmentdetails = AjaxControlToolkit.CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);
        departmentID = Convert.ToInt32(departmentdetails["department"]);
        SqlConnection conclass = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        conclass.Open();
        SqlCommand cmdclass = new SqlCommand("SELECT CONVERT(varchar(12), tbsession.sessionstart) + ' - ' + tbclass.classname AS classname, tbclass.classid FROM tbclass INNER JOIN tbdep ON tbclass.classdepid = tbdep.depid INNER JOIN tbsession ON tbclass.classsessionid = tbsession.Sessionid WHERE        (tbdep.depid = @depid) ORDER BY classname", conclass);
        cmdclass.Parameters.AddWithValue("@depID", departmentID);
        cmdclass.ExecuteNonQuery();
        SqlDataAdapter daclass = new SqlDataAdapter(cmdclass);
        DataSet dsclass = new DataSet();
        daclass.Fill(dsclass);
        conclass.Close();
        List<CascadingDropDownNameValue> classdetails = new List<CascadingDropDownNameValue>();
        foreach (DataRow dtclassrow in dsclass.Tables[0].Rows)
        {
            string classID = dtclassrow["classid"].ToString();
            string classname = dtclassrow["classname"].ToString();
            classdetails.Add(new CascadingDropDownNameValue(classname, classID));

        }
        return classdetails.ToArray();
    }
    [WebMethod]
    public CascadingDropDownNameValue[] BindGroupdropdown(string knownCategoryValues, string category)
    {
        int ClassID;
        StringDictionary classdetails = AjaxControlToolkit.CascadingDropDown.ParseKnownCategoryValuesString(knownCategoryValues);
        ClassID = Convert.ToInt32(classdetails["Class"]);
        SqlConnection congroup = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        congroup.Open();
        SqlCommand cmdgroup = new SqlCommand("select * from tbgroup where groupclassid=@classid", congroup);
        cmdgroup.Parameters.AddWithValue("@classid", ClassID);
        cmdgroup.ExecuteNonQuery();
        SqlDataAdapter dagroup = new SqlDataAdapter(cmdgroup);
        DataSet dsgroup = new DataSet();
        dagroup.Fill(dsgroup);
        congroup.Close();
        List<CascadingDropDownNameValue> groupdetails = new List<CascadingDropDownNameValue>();
        foreach (DataRow dtgrouprow in dsgroup.Tables[0].Rows)
        {
            string groupID = dtgrouprow["groupid"].ToString();
            string groupname = dtgrouprow["groupname"].ToString();
            groupdetails.Add(new CascadingDropDownNameValue(groupname, groupID));
        }
        return groupdetails.ToArray();
    }
}

