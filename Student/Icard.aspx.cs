﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;

public partial class Student_Icard : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        frmbind();
        bari();
    }
    private void frmbind()
    {
        SqlCommand cmd = new SqlCommand("studenticard", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        FormView1.DataSource = ds;
        FormView1.DataBind();
    }
    private void bari()
    {
        string rollno = ((Label)(FormView1.FindControl("LBLRollNo"))).Text;
        ((System.Web.UI.WebControls.Image)FormView1.FindControl("Image1")).ImageUrl = "~/Student/barcode.aspx?a=" + rollno;

    }
    //private void barimage(string s)
    //{
    //    if (Request.QueryString["a"] != null)
    //    {
    //        Int32 w = s.Length * 40;
    //        Bitmap bmp = new Bitmap(w, w);
    //        Graphics gr = Graphics.FromImage(bmp);
    //        //      InstalledFontCollection insfnt = new InstalledFontCollection();
    //        Font fnt = new Font("IDAutomationHC39M", 18);
    //        PointF pntf = new PointF(1, 1);
    //        SolidBrush brwr = new SolidBrush(Color.Black);
    //        SolidBrush br = new SolidBrush(Color.White);
    //        gr.FillRectangle(br, 0, 0, w, w);
    //        gr.DrawString(s, fnt, brwr, pntf);
    //        Response.ContentType = "image/jpeg";
    //        bmp.Save(Response.OutputStream, ImageFormat.Jpeg);
    //    }
    //}
}