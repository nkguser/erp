﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" MasterPageFile="~/Admin/InnerAdmin.master" AutoEventWireup="true" CodeFile="RegTeacher.aspx.cs" Inherits="Admin_RegTeacher" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
       <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" 
           Height="16px" oniteminserting="DetailsView1_ItemInserting"             
            onmodechanging="DetailsView1_ModeChanging" Width="100%"             
            DataKeyNames="teacherid" 
            style="margin-right: 0px; margin-top: 15px" DefaultMode="Insert">
            
            <Fields>
                <asp:TemplateField HeaderText="Teacher ID * (Numeric Only)">
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox79" runat="server" Text='<%# Eval("teacherclgid") %>'></asp:TextBox>
                    <asp:RequiredFieldValidator ID="teacherclgid" runat="server"  ErrorMessage="*"   ValidationGroup="abc"  ForeColor="Red"  ControlToValidate="TextBox79"></asp:RequiredFieldValidator>
                    &nbsp;<asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                            Text="Check Availabilty" />
                    </InsertItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Course *">
                    <%--<ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("coursename") %>'></asp:Label>
                    </ItemTemplate>--%>
                    <InsertItemTemplate>
                        
                        <asp:DropDownList ID="ddlCourse" runat="server" Width="200px"></asp:DropDownList> 
<ajax:CascadingDropDown ID="CascadingDropDown1" runat="server" Category="Course" TargetControlID="ddlCourse" LoadingText="Loading Course..." PromptText="Select Course" ServiceMethod="BindCoursedropdown" ServicePath="DropdownWebService.asmx">
</ajax:CascadingDropDown>
<asp:RequiredFieldValidator ID="ddlCourse1" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="ddlCourse" ValidationGroup="abc"></asp:RequiredFieldValidator>
                    </InsertItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Department *">
                    <InsertItemTemplate>
                        <asp:DropDownList ID="ddlDepartment" runat="server" Width="200px"></asp:DropDownList>
                                  <asp:RequiredFieldValidator ID="ddlDepartment1" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="ddlDepartment" ValidationGroup="abc"></asp:RequiredFieldValidator>
<ajax:CascadingDropDown ID="CascadingDropDown2" runat="server" Category="Department" TargetControlID="ddlDepartment" ParentControlID="ddlCourse" LoadingText="Loading Department..." PromptText="Select Department" ServiceMethod="BindDepartmentdropdown" ServicePath="DropdownWebService.asmx">
</ajax:CascadingDropDown>
                    </InsertItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Salutation *" SortExpression="teachersalutation">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList1" runat="server">
                            <asp:ListItem>Mr.</asp:ListItem>
                            <asp:ListItem>Mrs.</asp:ListItem>
                            <asp:ListItem>Miss</asp:ListItem>
                            <asp:ListItem>Dr.</asp:ListItem>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:DropDownList ID="DropDownList2" runat="server">
                            <asp:ListItem>Mr.</asp:ListItem>
                            <asp:ListItem>Mrs.</asp:ListItem>
                            <asp:ListItem>Miss</asp:ListItem>
                            <asp:ListItem>Dr.</asp:ListItem>
                        </asp:DropDownList>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("teachersalutation") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="First Name *" SortExpression="teacherfname">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("teacherfname") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  ErrorMessage="*"   ValidationGroup="abc" ForeColor="Red"  ControlToValidate="TextBox1"></asp:RequiredFieldValidator>
                        
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Eval("teacherfname") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"  ErrorMessage="*"   ValidationGroup="abc" ForeColor="Red"  ControlToValidate="TextBox2"></asp:RequiredFieldValidator>
                        
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("teacherfname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Middle Name" SortExpression="teachermname">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox3" runat="server" Text='<%# Eval("teachermname") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Eval("teachermname") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Eval("teachermname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Last Name" SortExpression="teacherlname">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox5" runat="server" Text='<%# Eval("teacherlname") %>'></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"  ErrorMessage="*"   ValidationGroup="abc" ForeColor="Red"  ControlToValidate="TextBox5"></asp:RequiredFieldValidator>
                        
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox6" runat="server" Text='<%# Eval("teacherlname") %>'></asp:TextBox>
                   
                     
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Eval("teacherlname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date Of Birth * (DD/MM/YYYY)" SortExpression="teacherdob">
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox8" runat="server" Text='<%# Eval("teacherdob","{0:d}") %>'></asp:TextBox>
                        <asp:CalendarExtender Format="dd/MM/yyyy "  ID="TextBox8_CalendarExtender" runat="server" 
                            Enabled="True" SelectedDate="1985-12-12" TargetControlID="TextBox8">
                        </asp:CalendarExtender>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server"  ErrorMessage="*"   ValidationGroup="abc" ForeColor="Red"  ControlToValidate="TextBox8"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator2" runat="server"  ErrorMessage="**"   ValidationGroup="abc" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox8" Type="Date"></asp:CompareValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label7" runat="server" Text='<%# Eval("teacherdob","{0:d}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date Of Joining * (DD/MM/YYYY)" SortExpression="teacherdoj">
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox10" runat="server" Text='<%# Eval("teacherdoj","{0:d}") %>'></asp:TextBox>
                        <asp:CalendarExtender Format="dd/MM/yyyy "  ID="TextBox10_CalendarExtender" runat="server" 
                            Enabled="True" SelectedDate="2009-12-12" TargetControlID="TextBox10">
                        </asp:CalendarExtender>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"  ErrorMessage="*"   ValidationGroup="abc" ForeColor="Red"  ControlToValidate="TextBox10"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator4" runat="server"  ErrorMessage="**"   ValidationGroup="abc" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox10" Type="Date"></asp:CompareValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label8" runat="server" Text='<%# Eval("teacherdoj","{0:d}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Father's name *" 
                    SortExpression="teacherfathername">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox11" runat="server" 
                            Text='<%# Eval("teacherfathername") %>'></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server"  ErrorMessage="*"   ValidationGroup="abc" ForeColor="Red"  ControlToValidate="TextBox11"></asp:RequiredFieldValidator>
                       
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox12" runat="server" 
                            Text='<%# Eval("teacherfathername") %>'></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server"  ErrorMessage="*"   ValidationGroup="abc" ForeColor="Red"  ControlToValidate="TextBox12"></asp:RequiredFieldValidator>
                        
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label9" runat="server" Text='<%# Eval("teacherfathername") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Mother's Name *" 
                    SortExpression="teachermothername">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox13" runat="server" 
                            Text='<%# Eval("teachermothername") %>'></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server"  ErrorMessage="*"   ValidationGroup="abc" ForeColor="Red"  ControlToValidate="TextBox13"></asp:RequiredFieldValidator>
                        
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox14" runat="server" 
                            Text='<%# Eval("teachermothername") %>'></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server"  ErrorMessage="*"   ValidationGroup="abc" ForeColor="Red"  ControlToValidate="TextBox14"></asp:RequiredFieldValidator>
                       
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label10" runat="server" Text='<%# Eval("teachermothername") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Correspondence Address *" 
                    SortExpression="teachercorresadd">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox15" runat="server" Height="92px" 
                            Text='<%# Eval("teachercorresadd") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server"  ErrorMessage="*"   ValidationGroup="abc" ForeColor="Red"  ControlToValidate="TextBox15"></asp:RequiredFieldValidator>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox16" runat="server" Height="92px" 
                            Text='<%# Eval("teachercorresadd") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server"  ErrorMessage="*"   ValidationGroup="abc" ForeColor="Red"  ControlToValidate="TextBox16"></asp:RequiredFieldValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label11" runat="server" Text='<%# Eval("teachercorresadd") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Permanent Address" 
                    SortExpression="teacherpermaadd">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox17" runat="server" Height="92px" 
                            Text='<%# Eval("teacherpermaadd") %>' 
                            TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox18" runat="server" Height="92px" 
                            Text='<%# Eval("teacherpermaadd") %>' 
                            TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label12" runat="server" Text='<%# Eval("teacherpermaadd") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Post *" SortExpression="teacherpostid">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList7" runat="server" DataSourceID="Post" 
                            DataTextField="postdesc" DataValueField="postid">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="Post" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:cn %>" 
                            SelectCommand="SELECT [postid], [postdesc] FROM [tbpost] WHERE ([postdelsts] = @postdelsts)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="false" Name="postdelsts" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:DropDownList ID="DropDownList8" runat="server" DataSourceID="Post" 
                            DataTextField="postdesc" DataValueField="postid">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="Post" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:cn %>" 
                            SelectCommand="SELECT [postid], [postdesc] FROM [tbpost] WHERE ([postdelsts] = @postdelsts)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="false" Name="postdelsts" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label13" runat="server" Text='<%# Eval("teacherpostid") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Qualification" 
                    SortExpression="teacherqualification">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox19" runat="server" Height="92px" 
                            Text='<%# Eval("teacherqualification") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox20" runat="server" Height="92px" 
                            Text='<%# Eval("teacherqualification") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label14" runat="server" 
                            Text='<%# Eval("teacherqualification") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Experience" SortExpression="teacherexp">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox21" runat="server" Height="92px" 
                            Text='<%# Eval("teacherexp") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox22" runat="server" Height="92px" 
                            Text='<%# Eval("teacherexp") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("teacherexp") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Phone Number *" SortExpression="teacherphone1">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox23" runat="server" Text='<%# Eval("teacherphone1") %>'></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server"  ErrorMessage="*"   ValidationGroup="abc" ForeColor="Red"  ControlToValidate="TextBox23"></asp:RequiredFieldValidator>                         
                        
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox24" runat="server" Text='<%# Eval("teacherphone1") %>'></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server"  ErrorMessage="*"   ValidationGroup="abc" ForeColor="Red"  ControlToValidate="TextBox24"></asp:RequiredFieldValidator>
                        
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label16" runat="server" Text='<%# Eval("teacherphone1") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Alternate Phone Number" 
                    SortExpression="teacherphone2">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox25" runat="server" Text='<%# Eval("teacherphone2") %>'></asp:TextBox>
                     
                        <asp:CompareValidator ID="CompareValidator7" runat="server"  ErrorMessage="**"   ValidationGroup="abc" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox25" Type="Integer"></asp:CompareValidator>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox26" runat="server" Text='<%# Eval("teacherphone2") %>'></asp:TextBox>
                     
                        <asp:CompareValidator ID="CompareValidator8" runat="server"  ErrorMessage="**"   ValidationGroup="abc" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox26" Type="Integer"></asp:CompareValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label17" runat="server" Text='<%# Eval("teacherphone2") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="E_mail" SortExpression="teacheremailid">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox27" runat="server" Text='<%# Eval("teacheremailid") %>'></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"  ErrorMessage="***"   ValidationGroup="abc" ForeColor="Red" ControlToValidate="TextBox27" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox28" runat="server" Text='<%# Eval("teacheremailid") %>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"  ErrorMessage="***"   ValidationGroup="abc" ForeColor="Red" ControlToValidate="TextBox28" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label18" runat="server" Text='<%# Eval("teacheremailid") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Picture" SortExpression="teacherphoto">
                    <EditItemTemplate>
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:FileUpload ID="FileUpload2" runat="server" />
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" Height="80px" 
                            ImageUrl='<%# Eval("teacherphoto") %>' Width="80px" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Gender" SortExpression="teachergender">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox1" runat="server" 
                            Text='<%# Eval("teachergender") %>' />
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:CheckBox ID="CheckBox2" runat="server" 
                            Text='<%# Eval("teachergender") %>' />
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label20" runat="server" Text='<%# Eval("teachergender") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Research Journals" 
                    SortExpression="teacherresrchjrnls">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox31" runat="server" Height="92px" 
                            Text='<%# Eval("teacherresrchjrnls") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox32" runat="server" Height="92px" 
                            Text='<%# Eval("teacherresrchjrnls") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label21" runat="server" Text='<%# Eval("teacherresrchjrnls") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Research Conferences" 
                    SortExpression="teacherresrchcnfrncs">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox33" runat="server" Height="92px" 
                            Text='<%# Eval("teacherresrchcnfrncs") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox34" runat="server" Height="92px" 
                            Text='<%# Eval("teacherresrchcnfrncs") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label22" runat="server" 
                            Text='<%# Eval("teacherresrchcnfrncs") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Workshop/Seminar" 
                    SortExpression="teacherworkshopseminar">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox35" runat="server" Height="92px" 
                            Text='<%# Eval("teacherworkshopseminar") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox36" runat="server" Height="92px" 
                            Text='<%# Eval("teacherworkshopseminar") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label23" runat="server" 
                            Text='<%# Eval("teacherworkshopseminar") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Other Acts" SortExpression="teacherotheracts">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox37" runat="server" Height="92px" 
                            Text='<%# Eval("teacherotheracts") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox38" runat="server" Height="92px" 
                            Text='<%# Eval("teacherotheracts") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label24" runat="server" Text='<%# Eval("teacherotheracts") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Specialization" 
                    SortExpression="teacherspecialization">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox39" runat="server" Height="92px" 
                            Text='<%# Eval("teacherspecialization") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox40" runat="server" Height="92px" 
                            Text='<%# Eval("teacherspecialization") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label25" runat="server" 
                            Text='<%# Eval("teacherspecialization") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Project/Award" 
                    SortExpression="teacherprojectaward">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox41" runat="server" Height="92px" 
                            Text='<%# Eval("teacherprojectaward") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox42" runat="server" Height="92px" 
                            Text='<%# Eval("teacherprojectaward") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label26" runat="server" 
                            Text='<%# Eval("teacherprojectaward") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Subject Undertaken" 
                    SortExpression="teachersubjectundertaken">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox43" runat="server" Height="92px" 
                            Text='<%# Eval("teachersubjectundertaken") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox44" runat="server" Height="92px" 
                            Text='<%# Eval("teachersubjectundertaken") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label27" runat="server" 
                            Text='<%# Eval("teachersubjectundertaken") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Conferences Conducted" 
                    SortExpression="teachercnfrncconducted">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox45" runat="server" Height="92px" 
                            Text='<%# Eval("teachercnfrncconducted") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox46" runat="server" Height="92px" 
                            Text='<%# Eval("teachercnfrncconducted") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label28" runat="server" 
                            Text='<%# Eval("teachercnfrncconducted") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Conferences Attended" 
                    SortExpression="teachercnfrncattend">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox47" runat="server" Height="92px" 
                            Text='<%# Eval("teachercnfrncattend") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox48" runat="server" Height="92px" 
                            Text='<%# Eval("teachercnfrncattend") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label29" runat="server" 
                            Text='<%# Eval("teachercnfrncattend") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Other Info" SortExpression="teacherotherinfo">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox49" runat="server" Height="92px" 
                            Text='<%# Eval("teacherotherinfo") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox50" runat="server" Height="92px" 
                            Text='<%# Eval("teacherotherinfo") %>' TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label30" runat="server" Text='<%# Eval("teacherotherinfo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Category" SortExpression="teachercategoryid">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList9" runat="server" DataSourceID="Ctegory" 
                            DataTextField="categoryname" DataValueField="categoryid">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="Ctegory" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:cn %>" 
                            SelectCommand="SELECT [categoryid], [categoryname] FROM [tbcategory] WHERE ([categorydelsts] = @categorydelsts)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="false" Name="categorydelsts" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:DropDownList ID="DropDownList10" runat="server" DataSourceID="Ctegory" 
                            DataTextField="categoryname" DataValueField="categoryid">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="Ctegory" runat="server" 
                            ConnectionString="<%$ ConnectionStrings:cn%>" 
                            SelectCommand="SELECT [categoryid], [categoryname] FROM [tbcategory] WHERE ([categorydelsts] = @categorydelsts)">
                            <SelectParameters>
                                <asp:Parameter DefaultValue="false" Name="categorydelsts" Type="Boolean" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label31" runat="server" Text='<%# Eval("teachercategoryid") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Maritial Status" 
                    SortExpression="teachermaritalstatus">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox3" Text='<%# Eval("teachermaritalstatus") %>' runat="server" />
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:CheckBox ID="CheckBox4" Text='<%# Eval("teachermaritalstatus") %>' runat="server" />
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label32" runat="server" 
                            Text='<%# Eval("teachermaritalstatus") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Membership Of Professional Bodies" 
                    SortExpression="teachermembershipofprofessionalbodies">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox51" runat="server" Height="92px" 
                            Text='<%# Eval("teachermembershipofprofessionalbodies") %>' 
                            TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox52" runat="server" Height="92px" 
                            Text='<%# Eval("teachermembershipofprofessionalbodies") %>' 
                            TextMode="MultiLine" Width="361px"></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label33" runat="server" 
                            Text='<%# Eval("teachermembershipofprofessionalbodies") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pan Number" SortExpression="teacherpanno">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox53" runat="server" Text='<%# Eval("teacherpanno") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox54" runat="server" Text='<%# Eval("teacherpanno") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label34" runat="server" Text='<%# Eval("teacherpanno") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PF Number" SortExpression="teacherpfno">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox55" runat="server" Text='<%# Eval("teacherpfno") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox56" runat="server" Text='<%# Eval("teacherpfno") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label35" runat="server" Text='<%# Eval("teacherpfno") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Salary Bank Name" 
                    SortExpression="teachersalaryBankname">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox57" runat="server" 
                            Text='<%# Eval("teachersalaryBankname") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox58" runat="server" 
                            Text='<%# Eval("teachersalaryBankname") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label36" runat="server" 
                            Text='<%# Eval("teachersalaryBankname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Salary Branch Name" 
                    SortExpression="teachersalarybranchname">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox59" runat="server" 
                            Text='<%# Eval("teachersalarybranchname") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox60" runat="server" 
                            Text='<%# Eval("teachersalarybranchname") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label37" runat="server" 
                            Text='<%# Eval("teachersalarybranchname") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Bank Account Number" 
                    SortExpression="teacherbankaccno">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox61" runat="server" 
                            Text='<%# Eval("teacherbankaccno") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox62" runat="server" 
                            Text='<%# Eval("teacherbankaccno") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label38" runat="server" Text='<%# Eval("teacherbankaccno") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ISFC Code" SortExpression="teacherISFCcode">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox63" runat="server" 
                            Text='<%# Eval("teacherISFCcode") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox64" runat="server" 
                            Text='<%# Eval("teacherISFCcode") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label39" runat="server" Text='<%# Eval("teacherISFCcode") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Gross Pay" SortExpression="teachergrosspay">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox65" runat="server" 
                            Text='<%# Eval("teachergrosspay") %>'></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator9" runat="server"  ErrorMessage="**"   ValidationGroup="abc" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox65" Type="Integer"></asp:CompareValidator>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox66" runat="server" 
                            Text='<%# Eval("teachergrosspay") %>'></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator10" runat="server"  ErrorMessage="**"   ValidationGroup="abc" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox66" Type="Integer"></asp:CompareValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label40" runat="server" Text='<%# Eval("teachergrosspay") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pay Scale" SortExpression="teacherpayscale">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox67" runat="server" 
                            Text='<%# Eval("teacherpayscale") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox68" runat="server" 
                            Text='<%# Eval("teacherpayscale") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label41" runat="server" Text='<%# Eval("teacherpayscale") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Teacher Or Non Teaching" 
                    SortExpression="teacherTORNTsts">
                    <EditItemTemplate>
                        <asp:CheckBox ID="CheckBox5" runat="server" 
                            Text='<%# Eval("teacherTORNTsts") %>' />
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:CheckBox ID="CheckBox6" runat="server" 
                            Text='<%# Eval("teacherTORNTsts") %>' />
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label42" runat="server" Text='<%# Eval("teacherTORNTsts") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Appointment Type" 
                    SortExpression="teacherappointtype">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox69" runat="server" 
                            Text='<%# Eval("teacherappointtype") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox70" runat="server" 
                            Text='<%# Eval("teacherappointtype") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label43" runat="server" Text='<%# Eval("teacherappointtype") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="University Approvel Letter Number" 
                    SortExpression="teacherUnivaprroveletterno">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox71" runat="server" 
                            Text='<%# Eval("teacherUnivaprroveletterno") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox72" runat="server" 
                            Text='<%# Eval("teacherUnivaprroveletterno") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label44" runat="server" 
                            Text='<%# Eval("teacherUnivaprroveletterno") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="University Approvel Date" 
                    SortExpression="teacherunivapproveDate">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox73" runat="server" 
                            Text='<%# Eval("teacherunivapproveDate") %>'></asp:TextBox>
                     <asp:CompareValidator ID="CompareValidator11" runat="server"  ErrorMessage="**"   ValidationGroup="abc" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox73" Type="Date"></asp:CompareValidator>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox74" runat="server" 
                            Text='<%# Eval("teacherunivapproveDate","{0:d}") %>'></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator12" runat="server"  ErrorMessage="**"   ValidationGroup="abc" ForeColor="Red" Operator="DataTypeCheck" ControlToValidate="TextBox74" Type="Date"></asp:CompareValidator>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label45" runat="server" 
                            Text='<%# Eval("teacherunivapproveDate","{0:d}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Religion">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox75" runat="server" 
                            Text='<%# Eval("teacherreligion") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:TextBox ID="TextBox76" runat="server" 
                            Text='<%# Eval("teacherreligion") %>'></asp:TextBox>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label46" runat="server" Text='<%# Eval("teacherreligion") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <EditItemTemplate>
                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="update">Update</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="LinkButton5" runat="server" CommandName="cancel" >Cancel</asp:LinkButton>
                    </EditItemTemplate>
                    <InsertItemTemplate>
                        <asp:LinkButton ID="LinkButton6" runat="server" CommandName="insert" ValidationGroup="abc">Insert</asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="LinkButton7" runat="server" CommandName="cancel" >Cancel</asp:LinkButton>
                    </InsertItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="new">New</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Fields>
             
        </asp:DetailsView>
   
</asp:Content>

