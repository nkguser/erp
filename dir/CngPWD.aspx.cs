﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;
public partial class dir_CngPWD : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
    }
    protected void LBChangePwd_Click(object sender, EventArgs e)
    {
        SqlCommand cmd1 = new SqlCommand("select userpwd from tbuser where userlogid=@l ", con);
        string s = User.Identity.Name;
        cmd1.Parameters.Add("@l", SqlDbType.VarChar, 50).Value = User.Identity.Name;
        SqlDataReader dr = cmd1.ExecuteReader();
        dr.Read();
        string a = dr["userpwd"].ToString();
        dr.Close();
        string b = TXTOldPwd.Text;
        if (a == b)
        {
            SqlCommand cmd = new SqlCommand("update tbuser set userpwd=@up where userlogid=@u", con);
            cmd.Parameters.Add("@u", SqlDbType.VarChar, 50).Value = User.Identity.Name;
            cmd.Parameters.Add("@up", SqlDbType.VarChar, 50).Value = TXTNewPwd.Text;
            cmd.ExecuteNonQuery();
            LblMessage.Text = "Password Changed Successfully";
        }
        else if (a != b)
        {
            LblMessage.Text = "Password Not Changed. Please Re-Enter Your Current Password Again";
        }
        else
        {
            LblMessage.Text = "Sorry for Inconvienance...An Internal Errod has Occured";
        }
    }
}