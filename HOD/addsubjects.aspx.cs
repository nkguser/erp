﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class HOD_addsubjects : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            subtype();
            sem();
        }
    }
    private void subtype()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT * FROM tbSubjectype where subjectdelsts=0", con);
        SqlDataReader dr = cmd.ExecuteReader();
        DDLType.DataTextField = "SubjectTypename";
        DDLType.DataValueField = "SubjectTypeid";
        DDLType.DataSource = dr;
        DDLType.DataBind();
        cmd.Dispose();
        dr.Close();
        con.Close();
    }
    private void sem()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        DDLSem.Items.Clear();
        SqlCommand cmd = new SqlCommand("SELECT tbCourse.coursesemester FROM tbCourse INNER JOIN tbDep ON tbCourse.Courseid = tbDep.depCourseid WHERE (tbDep.Depid = @did ) and tbcourse.coursedelsts=0", con);
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        int count = Int32.Parse(dr["coursesemester"].ToString());
        DDLSem.Items.Insert(0, "-- Choose Semester --");
        for (int i = 1; i <= count; i++)
        {
            DDLSem.Items.Add(i.ToString());
        }
        dr.Close();
        cmd.Dispose();
        con.Close();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        kitmerp.clsSubject obj = new kitmerp.clsSubject();
        kitmerp.clsSubjectprp objprp = new kitmerp.clsSubjectprp();
        objprp.p_subjectunicode=TxtUniCode.Text;
        objprp.p_subjectsem=Convert.ToInt32(DDLSem.SelectedValue);
        objprp.p_subjecttitle=TxtSubTitle.Text;
        objprp.p_subjectdepid=Convert.ToInt32(Session["depid"]);
        objprp.p_subjecttypeid = Convert.ToInt32(DDLType.SelectedValue);
        objprp.p_subjectshorttitle = TxtSubshortTitle.Text;
        obj.Save_Rec(objprp);
        con.Close();
        GridView1.DataBind();
        Label1.Text = TxtSubTitle.Text + " Added Successfully";
        TxtSubTitle.Text = string.Empty;
        TxtSubshortTitle.Text = string.Empty;
        TxtUniCode.Text = string.Empty;
        TxtUniCode.Focus();
   
    }
}