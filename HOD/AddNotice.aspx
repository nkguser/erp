﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="AddNotice.aspx.cs" Inherits="HOD_AddNotice" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%" style="text-align: left; font-weight: bold;" >
        <tr>
            <td >
                To:</td>
            <td align=left>
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="LevelNotice" 
                    DataTextField="leveldesc" DataValueField="levelid">
                </asp:DropDownList>
                <asp:SqlDataSource ID="LevelNotice" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:cn %>" 
                    SelectCommand="SELECT [levelid], [leveldesc] FROM [tblevel] WHERE ([leveldelsts] = @leveldelsts)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="false" Name="leveldelsts" Type="Boolean" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td>
                </td>
        </tr>
        
        <tr>
            <td >
                Subject :</td>
            <td >
                <asp:TextBox ID="txtheader" runat="server" Height="42px" TextMode="MultiLine" 
                    Width="100%"></asp:TextBox>
                
            </td>
            <td>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtheader" Display="Dynamic" ErrorMessage="*" 
                    Font-Bold="True" ForeColor="Red" ValidationGroup="abc"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td >
                Content</td>
            <td >
                <cc1:Editor ID="Editor1" runat="server" Height="300px" Width="100%" />
            </td>
            <td >
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="Editor1" Display="Dynamic" ErrorMessage="*" Font-Bold="True" 
                    ForeColor="Red" ValidationGroup="abc"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td >
                <asp:Button ID="Button1" runat="server" Text="Save" Width="76px" 
                    onclick="Button1_Click" ValidationGroup="abc" />
            </td>
            <td align=left>
                <asp:Label ID="lblmsg" runat="server" Font-Bold="True" ForeColor="#003300"></asp:Label>
            </td>
            <td>
               </td>
        </tr>
        <tr>
            <td colspan="3" >
                </td>
        </tr>
        <tr>
            <td >
                </td>
            <td align=center>
            <div  style="overflow:scroll;height:100px;visibility:inherit">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                     DataKeyNames="noticeid" 
                    EmptyDataText="There are no data records to display." CellPadding="4" 
                ForeColor="#333333" GridLines="None" Width="100%" 
                    onrowediting="GridView1_RowEditing" ShowHeaderWhenEmpty="True">
                <AlternatingRowStyle BackColor="White"  />
                    <Columns>
                        <asp:TemplateField HeaderText="Notice Subject">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Noticesubject") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                        <asp:Literal ID="Lt" runat="server" Text='<%# Eval("Noticecontent") %>'></asp:Literal>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Notice Date">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Noticedate","{0:d}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView></div>
            </td>
            <td>
                </td>
        </tr>
        <tr><td></td></tr>
        
    </table>
</asp:Content>

