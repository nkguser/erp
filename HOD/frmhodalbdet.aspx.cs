﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
public partial class HOD_frmhodalbdet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            Int32 a = Convert.ToInt32(Request.QueryString["acod"]);
            nsphotosnak.clsalb obj = new nsphotosnak.clsalb();
            List<nsphotosnak.clsalbprp> k = obj.Find_rec(a);
            Label1.Text = k[0].p_albnam;
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        nsphotosnak.clsalbdet obj = new nsphotosnak.clsalbdet();
        nsphotosnak.clsalbdetprp objprp = new nsphotosnak.clsalbdetprp();
        objprp.p_albdetalbcod = Convert.ToInt32(Request.QueryString["acod"]);
        objprp.p_albdetdsc = TextBox1.Text;
        String s = FileUpload1.FileName;
        s = s.Substring(s.LastIndexOf('.'));
        objprp.p_albdetpic = s;
        Int32 a = obj.Save_Rec(objprp);
        String st = Server.MapPath("../albums") + "//" + Request.QueryString["acod"];
        FileUpload1.PostedFile.SaveAs(st + "//" + a.ToString() + s);
        TextBox1.Text = String.Empty;
        DataList1.DataBind();
    }
    protected void DataList1_EditCommand(object source, DataListCommandEventArgs e)
    {
        Int32 a = Convert.ToInt32(DataList1.DataKeys[e.Item.ItemIndex]);
        String s = e.CommandArgument.ToString();
        nsphotosnak.clsalb obj = new nsphotosnak.clsalb();
        nsphotosnak.clsalbprp objprp = new nsphotosnak.clsalbprp();
        objprp.p_albcod = Convert.ToInt32(Request.QueryString["acod"]);
        objprp.p_albcvrpic = a.ToString() + s;
        obj.updcvrpic(objprp);
        DataList1.DataBind();
    }
    protected void DataList1_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        Int32 a = Convert.ToInt32(DataList1.DataKeys[e.Item.ItemIndex]);
        String s = e.CommandArgument.ToString();
        FileInfo fi = new FileInfo(Server.MapPath("../albums") + "//" +
            Request.QueryString["acod"] + "//" + a.ToString() + s);
        fi.Delete();
        nsphotosnak.clsalbdet obj = new nsphotosnak.clsalbdet();
        nsphotosnak.clsalbdetprp objprp = new nsphotosnak.clsalbdetprp();
        objprp.p_albdetcod = a;
        obj.Delete_Rec(objprp);
        DataList1.DataBind();
    }
}