﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

public partial class Student_Profile : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (Page.IsPostBack == false)
            det_bind();
    }
    protected void DetailsView1_ItemUpdating(object sender, DetailsViewUpdateEventArgs e)
    {
        TextBox perma = ((TextBox)(DetailsView1.Rows[15].FindControl("TextBox19")));
        TextBox p2 = ((TextBox)(DetailsView1.Rows[17].FindControl("TextBox23")));
        TextBox email = ((TextBox)(DetailsView1.Rows[18].FindControl("TextBox25")));
        TextBox achieve = ((TextBox)(DetailsView1.Rows[23].FindControl("TextBox33")));
        TextBox skill = ((TextBox)(DetailsView1.Rows[24].FindControl("TextBox35")));
        TextBox training = ((TextBox)(DetailsView1.Rows[25].FindControl("TextBox37")));
        TextBox hobbies = ((TextBox)(DetailsView1.Rows[26].FindControl("TextBox39")));
        FileUpload resume = ((FileUpload)(DetailsView1.Rows[27].FindControl("FileUpload3")));
        SqlCommand cmd = new SqlCommand("updviewstudent", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        if (p2.Text == null || p2.Text == "")
        {
            cmd.Parameters.Add("@p2", SqlDbType.Int).Value = Convert.DBNull;
        }
        else
        {
            cmd.Parameters.Add("@p2", SqlDbType.BigInt).Value = Convert.ToInt64(p2.Text);
        }
        if (perma.Text == null)
        {
            cmd.Parameters.Add("@perma", SqlDbType.VarChar, 300).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@perma", SqlDbType.VarChar, 300).Value = perma.Text;
        }
        if (email.Text == null)
        {
            cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = email.Text;
        }
        if (achieve.Text == null)
        {
            cmd.Parameters.Add("@achieve", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@achieve", SqlDbType.VarChar, 200).Value = achieve.Text;
        }
        if (skill.Text == null)
        {
            cmd.Parameters.Add("@skill", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@skill", SqlDbType.VarChar, 200).Value = skill.Text;
        }
        if (training.Text == null)
        {
            cmd.Parameters.Add("@training", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@training", SqlDbType.VarChar, 200).Value = training.Text;
        }
        if (hobbies.Text == null)
        {
            cmd.Parameters.Add("@hobbies", SqlDbType.VarChar, 200).Value = Convert.DBNull.ToString();
        }
        else
        {
            cmd.Parameters.Add("@hobbies", SqlDbType.VarChar, 200).Value = hobbies.Text;
        }
        if (resume.FileName == null || resume.FileName == string.Empty)
        {
            SqlCommand cmd1 = new SqlCommand("select studentresumefile from tbstudent where studentid=" + Convert.ToInt32(DetailsView1.DataKey[0].ToString()), con);
            SqlDataReader dr = cmd1.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                string abc = dr[0].ToString();
                if(abc!=null||abc!=string.Empty)
                cmd.Parameters.Add("@resume", SqlDbType.VarChar, 100).Value = abc;
            }
            else
            {
                cmd.Parameters.Add("@resume", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
            }
            dr.Close();
            cmd1.Dispose();

        }
        else
        {
            SqlCommand cmd1 = new SqlCommand("select studentresumefile from tbstudent where studentid=" + Convert.ToInt32(DetailsView1.DataKey[0].ToString()), con);
            SqlDataReader dr = cmd1.ExecuteReader();
            if (dr.HasRows)
            {
                dr.Read();
                string abc = dr[0].ToString();
                if (abc == null || abc == string.Empty)
                {

                }
                else
                {
                    File.Delete(Server.MapPath("~\\StudentResume") + "\\" + abc);
                }
            }
            dr.Close();
            cmd1.Dispose();
            Label rno1 = ((Label)(DetailsView1.Rows[0].FindControl("Label1")));
            DirectoryInfo di1 = new DirectoryInfo(Server.MapPath("~\\Studentresume" + "\\"));
            if (di1.Exists == false)
                di1.Create();
            string fn1 = rno1.Text.Substring(2, 5) + Path.GetExtension(resume.PostedFile.FileName);
            string sp1 = Server.MapPath("~\\Studentresume" + "\\");
            if (sp1.EndsWith("\\") == false)
                sp1 += "\\";
            sp1 += fn1;
            resume.PostedFile.SaveAs(sp1);
            cmd.Parameters.Add("@resume", SqlDbType.VarChar, 100).Value = fn1;
        }
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        DetailsView1.ChangeMode(DetailsViewMode.ReadOnly);
        det_bind();

    }
    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        DetailsView1.ChangeMode(e.NewMode);
        det_bind();
    }
    private void det_bind()
    {
        SqlCommand cmd = new SqlCommand("dispstudent", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@rno", SqlDbType.Int).Value =Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DetailsView1.DataSource = ds;
        DetailsView1.DataBind();
    }
}