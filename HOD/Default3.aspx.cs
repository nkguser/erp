﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Default3 : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
            dleventbind();
    }
    private void dleventbind()
    {
        //click="MM_openBrWindow('Default3.aspx?Id=749','win2','scrollbars=yes,width=620,height=500,resizable=no,top=150' )"
        SqlDataAdapter adp = new SqlDataAdapter("select * from tbevent where eventid="+Convert.ToInt32(Request.QueryString["id"]), con);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DataList1.DataSource = ds;
        DataList1.DataBind();

    }
}