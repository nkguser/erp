﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dir/MasterPage.master" AutoEventWireup="true" CodeFile="attdrep.aspx.cs" Inherits="dir_attdrep" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      <table class="style1">
            <tr>
                <td class="style2" colspan="2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style2">
                    &nbsp;</td>
                <td class="style2">
                    Roll-ID</td>
                <td>
                    <asp:TextBox ID="TextBox41" runat="server" 
                        ontextchanged="TextBox41_TextChanged" ValidationGroup="abc" Width="308px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="style2" colspan="2">
                    &nbsp;</td>
                <td>
                    <asp:CompareValidator ID="CompareValidator52" runat="server" 
                        ControlToValidate="TextBox41" Display="Dynamic" ErrorMessage="*" 
                        Font-Bold="True" ForeColor="Red" Operator="DataTypeCheck" Type="Integer" 
                        ValidationGroup="abc"></asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" 
                        ControlToValidate="TextBox41" Display="Dynamic" ErrorMessage="**" 
                        Font-Bold="True" ForeColor="Red" ValidationGroup
                       ="abc"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="style2" colspan="2">
                    &nbsp;</td>
                <td>
                    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" Height="16px" 
                        Visible="False" Width="100%">
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
                        <EditRowStyle BackColor="#999999" />
                        <EmptyDataTemplate>
                            No Record&nbsp; Found<br />
                        </EmptyDataTemplate>
                        <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
                        <Fields>
                        <asp:TemplateField HeaderText="id" 
                                SortExpression="studentid" Visible="false">                               
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("studentid") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Roll Number" SortExpression="studentrollno">
                                
                                                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("studentrollno") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" SortExpression="studentfname">
                                                               
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("studentfname") + " " + Eval("studentlname") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>                           
                            <asp:TemplateField HeaderText="Picture" SortExpression="studentphoto">
                                
                             
                                <ItemTemplate>
                                    <%--<asp:Image ID="Image1" runat="server" Height="80px" 
                                        ImageUrl='<%# Eval("studentphoto") %>' Width="80px" />--%>
                                    <asp:Image ID="Image1" runat="server" Height="80px" 
                                        ImageUrl='<%# "~/Studentpic/" + Eval("studentphoto") %>' Width="80px" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    </asp:DetailsView>
                </td>
            </tr>
            <tr>
                <td class="style2" colspan="2">
                    &nbsp;</td>
                <td>
                    <asp:Panel ID="Panel1" runat="server" Visible="False">
                        <table class="styledmenu">
                            <tr>
                                <td>
                                    Choose Semester</td>
                                <td>
                                    <asp:DropDownList ID="DDlSem" runat="server" AutoPostBack="True" 
                                        onselectedindexchanged="DDlSem_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                               <td colspan="2">
                <asp:GridView ID="GridShow" runat="server" 
                    AutoGenerateColumns="False" onrowdatabound="GridShow_RowDataBound" 
                    EmptyDataText="No Records Found" CellPadding="4" ForeColor="#333333" 
                    GridLines="None">
                
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Sessional">                            
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("studentSessionalno") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Subject">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("studentsessionalDate","{0:d}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Attendance">
                            <ItemTemplate>
                                <asp:Label ID="LBLAttend" runat="server" Text='<%# Eval("studentsessionalattnd") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Marks Obtained">                            
                            <ItemTemplate>
                                <asp:Label ID="LBLMarks" runat="server" Text='<%# Eval("studentsessionalMarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Max Marks">
                            <ItemTemplate>
                                <asp:Label ID="LblMMarks" runat="server" Text='<%# Eval("studentsessionalmmarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:GridView ID="GridShowSummary" runat="server" AutoGenerateColumns="False" 
                                        CellPadding="4" DataKeyNames="studentattndsubjectid" ForeColor="#333333" 
                                        GridLines="None" onrowdatabound="GridShowSummary_RowDataBound" Width='717px'>
                                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Subject">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblSubject" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Attended">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblAt" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Total Delivered">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblTotal" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%age">
                                                <ItemTemplate>
                                                    <asp:Label ID="Lblpercent" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EditRowStyle BackColor="#999999" />
                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                        <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                        <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="style2" colspan="2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
    
                    &nbsp;</td>
            </tr>
        </table>
    </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

