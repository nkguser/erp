﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Teacher_testopt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            Int32 a = Convert.ToInt32(Request.QueryString["qcod"]);
            ot.clsqst obj = new ot.clsqst();
            List<ot.clsqstprp> k = obj.Find_Rec(a);
            if (k.Count > 0)
                Label1.Text = k[0].p_qstdsc;
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        ot.clsopt obj = new ot.clsopt();
        ot.clsoptprp objprp = new ot.clsoptprp();
        objprp.p_optdsc = TextBox1.Text;
        objprp.p_optqstcod = Convert.ToInt32(Request.QueryString["qcod"]);
        if (CheckBox1.Checked == true)
            objprp.p_optsts = 'T';
        else
            objprp.p_optsts = 'F';
        String s = FileUpload1.FileName;
        if (s != "")
            s = s.Substring(s.LastIndexOf('.'));
        objprp.p_optpic = s;
        Int32 a = obj.Save_Rec(objprp);
        if (s != "")
            FileUpload1.PostedFile.SaveAs(Server.MapPath("~/optpics") + "//" + a.ToString() + s);
        TextBox1.Text = String.Empty;
        CheckBox1.Checked = false;
        GridView1.DataBind();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Image img = (Image)(e.Row.FindControl("img1"));
            Int32 a = Convert.ToInt32(GridView1.DataKeys[e.Row.RowIndex][0]);
            String s = GridView1.DataKeys[e.Row.RowIndex][1].ToString();
            if (s == "")
                img.Visible = false;
            else
                img.ImageUrl = "~/optpics/" + a.ToString() + s;
        }
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        ot.clsopt obj = new ot.clsopt();
        ot.clsoptprp objprp = new ot.clsoptprp();
        objprp.p_optcod = Convert.ToInt32(GridView1.DataKeys[e.RowIndex][0]);
        obj.delete_Rec(objprp);
        GridView1.DataBind();
        e.Cancel = true;
    }
}