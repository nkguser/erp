﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;
using System.Data.SqlClient;
using kitmerp;

public partial class Admin_RegTeacher : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (Page.IsPostBack == false)
        {
            seserp.DropdownWebService obj = new seserp.DropdownWebService();
            obj.sum(Convert.ToInt32(Session["other"]));
        }
        
    }
    protected void DetailsView1_ModeChanging(object sender, DetailsViewModeEventArgs e)
    {
        DetailsView1.ChangeMode(e.NewMode);
        DetailsView1.DataBind();
    }
    protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {
       
            Int64 p1;// grosspay;
            Int32 did, cid, pid; //p2;
            //string salutation, fname, mname, lname, mtname, ftname, corres, perma, qualification, experience, email, photo, resrchjour, resrchcnfrncs, workshop, otheracts, specialization, project, subundertaken, cnfrnconduct, cnfrnattend, otherinfo, msts, memofprofbody, pan, pf, sbankname, sbranchname, baccnumber, isfc, payscale, apptype, uniappletternumber,religion;
            string salutation, fname, mtname, ftname,userlogid, corres;
            DateTime dob, doj;// uniappdate;
            Boolean g1, tornt, msts;
            TextBox tcid = ((TextBox)(DetailsView1.Rows[0].FindControl("Textbox79")));
            did = Convert.ToInt32(((DropDownList)(DetailsView1.Rows[2].FindControl("ddlDepartment"))).SelectedValue);
            salutation = ((DropDownList)(DetailsView1.Rows[3].FindControl("DropDownList2"))).SelectedValue;
            fname = ((TextBox)(DetailsView1.Rows[4].FindControl("TextBox2"))).Text;
            TextBox mname = ((TextBox)(DetailsView1.Rows[5].FindControl("TextBox4")));
            TextBox lname = ((TextBox)(DetailsView1.Rows[6].FindControl("TextBox6")));
            dob = Convert.ToDateTime(((TextBox)(DetailsView1.Rows[7].FindControl("TextBox8"))).Text);
            doj = Convert.ToDateTime(((TextBox)(DetailsView1.Rows[8].FindControl("TextBox10"))).Text);
            ftname = ((TextBox)(DetailsView1.Rows[9].FindControl("TextBox12"))).Text;
            mtname = ((TextBox)(DetailsView1.Rows[10].FindControl("TextBox14"))).Text;
            corres = ((TextBox)(DetailsView1.Rows[11].FindControl("TextBox16"))).Text;
            TextBox perma = ((TextBox)(DetailsView1.Rows[12].FindControl("TextBox18")));
            pid = Convert.ToInt32(((DropDownList)(DetailsView1.Rows[13].FindControl("DropDownList8"))).SelectedValue);
            TextBox qualification = ((TextBox)(DetailsView1.Rows[14].FindControl("TextBox20")));
            TextBox experience = ((TextBox)(DetailsView1.Rows[15].FindControl("TextBox22")));
            p1 = Convert.ToInt64(((TextBox)(DetailsView1.Rows[16].FindControl("TextBox24"))).Text);
            TextBox p2 = ((TextBox)(DetailsView1.Rows[17].FindControl("TextBox26")));
            TextBox email = ((TextBox)(DetailsView1.Rows[18].FindControl("TextBox28")));
            FileUpload photo = ((FileUpload)(DetailsView1.Rows[19].FindControl("FileUpload2")));
            CheckBox g = ((CheckBox)(DetailsView1.Rows[20].FindControl("CheckBox2")));
            if (g.Checked == true)
            {
                g1 = true;
            }
            else
            {
                g1 = false;
            }
            TextBox resrchjour = ((TextBox)(DetailsView1.Rows[21].FindControl("TextBox32")));
            TextBox resrchcnfrncs = ((TextBox)(DetailsView1.Rows[22].FindControl("TextBox34")));
            TextBox workshop = ((TextBox)(DetailsView1.Rows[23].FindControl("TextBox36")));
            TextBox otheracts = ((TextBox)(DetailsView1.Rows[24].FindControl("TextBox38")));
            TextBox specialization = ((TextBox)(DetailsView1.Rows[25].FindControl("TextBox40")));
            TextBox project = ((TextBox)(DetailsView1.Rows[26].FindControl("TextBox42")));
            TextBox subundertaken = ((TextBox)(DetailsView1.Rows[27].FindControl("TextBox44")));
            TextBox cnfrnconduct = ((TextBox)(DetailsView1.Rows[28].FindControl("TextBox46")));
            TextBox cnfrnattend = ((TextBox)(DetailsView1.Rows[29].FindControl("TextBox48")));
            TextBox otherinfo = ((TextBox)(DetailsView1.Rows[30].FindControl("TextBox50")));
            cid = Convert.ToInt32(((DropDownList)(DetailsView1.Rows[31].FindControl("DropDownList10"))).SelectedValue);
            CheckBox mst = ((CheckBox)(DetailsView1.Rows[32].FindControl("CheckBox4")));
            if (mst.Checked == true)
            {
                msts = true;
            }
            else
            {
                msts = false;
            }
            TextBox memofprofbody = ((TextBox)(DetailsView1.Rows[33].FindControl("TextBox52")));
            TextBox pan = ((TextBox)(DetailsView1.Rows[34].FindControl("TextBox54")));
            TextBox pf = ((TextBox)(DetailsView1.Rows[35].FindControl("TextBox56")));
            TextBox sbankname = ((TextBox)(DetailsView1.Rows[36].FindControl("TextBox58")));
            TextBox sbranchname = ((TextBox)(DetailsView1.Rows[37].FindControl("TextBox60")));
            TextBox baccnumber = ((TextBox)(DetailsView1.Rows[38].FindControl("TextBox62")));
            TextBox isfc = ((TextBox)(DetailsView1.Rows[39].FindControl("TextBox64")));
            TextBox grosspay = ((TextBox)(DetailsView1.Rows[40].FindControl("TextBox66")));
            TextBox payscale = ((TextBox)(DetailsView1.Rows[41].FindControl("TextBox68")));
            CheckBox tornt1 = ((CheckBox)(DetailsView1.Rows[42].FindControl("CheckBox6")));
            if (tornt1.Checked == true)
            {
                tornt = true;
            }
            else
            {
                tornt = false;
            }
            TextBox apptype = ((TextBox)(DetailsView1.Rows[43].FindControl("TextBox70")));
            TextBox uniappletternumber = ((TextBox)(DetailsView1.Rows[44].FindControl("TextBox72")));
            TextBox uniappdate = ((TextBox)(DetailsView1.Rows[45].FindControl("TextBox74")));
            TextBox religion = ((TextBox)(DetailsView1.Rows[46].FindControl("TextBox76")));
            DropDownList ddldep = (DropDownList)(DetailsView1.Rows[2].FindControl("ddlDepartment"));
            if (lname.Text.Length.Equals(3))
            {
                userlogid = fname.Substring(0, 2) + lname.Text.Substring(0, 3) +tcid.Text+ dob.Day.ToString() + dob.Month.ToString() + ddldep.SelectedItem.ToString();
            }
            else
            {
                userlogid = fname.Substring(0, 2) + fname.Substring(0, 2) +tcid.Text +dob.Day.ToString() + dob.Month.ToString() + ddldep.SelectedItem.ToString();
            }    
            

            SqlCommand cmd = new SqlCommand("insteacher", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlCommand cmdcheck = new SqlCommand("select count(*) as cnt from tbteacher where teacherclgid=@u", con);
            cmdcheck.Parameters.Add("@u", SqlDbType.VarChar, 50).Value = tcid.Text;
            Int32 check = Convert.ToInt32(cmdcheck.ExecuteScalar());
            if (check == 0)
            {
                cmd.Parameters.Add("teacherclgid", SqlDbType.VarChar, 50).Value = tcid.Text;
                cmd.Parameters.Add("@userlogid", SqlDbType.VarChar, 50).Value = userlogid.ToUpper();
                cmd.Parameters.Add("@salutation", SqlDbType.VarChar, 8).Value = salutation;
                cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
                cmd.Parameters.Add("@fname", SqlDbType.VarChar, 50).Value = fname;
                if (mname.Text == null)
                {
                    cmd.Parameters.Add("@mname", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@mname", SqlDbType.VarChar, 50).Value = mname.Text;
                }
                if (lname.Text == null)
                {
                    cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50).Value = " ";
                }
                else
                {
                    cmd.Parameters.Add("@lname", SqlDbType.VarChar, 50).Value = lname.Text;
                }
                cmd.Parameters.Add("@dob", SqlDbType.DateTime).Value = dob;
                cmd.Parameters.Add("@doj", SqlDbType.DateTime).Value = doj;
                cmd.Parameters.Add("@mtname", SqlDbType.VarChar, 50).Value = mtname;
                cmd.Parameters.Add("@ftname", SqlDbType.VarChar, 50).Value = ftname;
                cmd.Parameters.Add("@corres", SqlDbType.VarChar, 500).Value = corres;
                if (perma.Text == null)
                {
                    cmd.Parameters.Add("@perma", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@perma", SqlDbType.VarChar, 500).Value = perma.Text;
                }
                cmd.Parameters.Add("@pid", SqlDbType.Int).Value = pid;
                if (qualification.Text == null)
                {
                    cmd.Parameters.Add("@qualification", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@qualification", SqlDbType.VarChar, 50).Value = qualification.Text;
                }
                if (experience.Text == null)
                {
                    cmd.Parameters.Add("@experience", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@experience", SqlDbType.VarChar, 50).Value = experience.Text;
                }
                cmd.Parameters.Add("@p1", SqlDbType.BigInt).Value = p1;
                if (p2.Text == null || p2.Text == "")
                {
                    cmd.Parameters.Add("@p2", SqlDbType.BigInt).Value = Convert.DBNull;
                }
                else
                {
                    cmd.Parameters.Add("@p2", SqlDbType.BigInt).Value = Convert.ToInt64(p2.Text);
                }
                if (email.Text == null)
                {
                    cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = email.Text;
                }
                if (photo.FileName == null || photo.FileName == string.Empty || photo.FileName == "")
                {
                    cmd.Parameters.Add("@photo", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
                }
                else
                {
                    DirectoryInfo di = new DirectoryInfo(Server.MapPath("~\\Teacherpic"));
                    if (di.Exists == false)
                        di.Create();
                    string fn = tcid.Text.Substring(2, 5) + Path.GetExtension(photo.PostedFile.FileName);
                    string sp = Server.MapPath("~\\Teacherpic");
                    if (sp.EndsWith("\\") == false)
                        sp += "\\";
                    sp += fn;
                    photo.PostedFile.SaveAs(sp);
                    cmd.Parameters.Add("@photo", SqlDbType.VarChar, 100).Value = fn;
                }
                cmd.Parameters.Add("@g1", SqlDbType.Bit).Value = g1;
                if (resrchjour.Text == null)
                {
                    cmd.Parameters.Add("@resrchjour", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@resrchjour", SqlDbType.VarChar, 500).Value = resrchjour.Text;
                }
                if (resrchcnfrncs.Text == null)
                {
                    cmd.Parameters.Add("@resrchcnfrncs", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@resrchcnfrncs", SqlDbType.VarChar, 500).Value = resrchcnfrncs.Text;
                }
                if (workshop.Text == null)
                {
                    cmd.Parameters.Add("@workshop", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@workshop", SqlDbType.VarChar, 500).Value = workshop.Text;
                }
                if (otheracts.Text == null)
                {
                    cmd.Parameters.Add("@otheracts", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@otheracts", SqlDbType.VarChar, 500).Value = otheracts.Text;
                }
                if (specialization.Text == null)
                {
                    cmd.Parameters.Add("@specialization", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@specialization", SqlDbType.VarChar, 500).Value = specialization.Text;
                }
                if (project.Text == null)
                {
                    cmd.Parameters.Add("@project", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@project", SqlDbType.VarChar, 500).Value = project.Text;
                }
                if (subundertaken.Text == null)
                {
                    cmd.Parameters.Add("@subundertaken", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@subundertaken", SqlDbType.VarChar, 500).Value = subundertaken.Text;
                }
                if (cnfrnconduct.Text == null)
                {
                    cmd.Parameters.Add("@cnfrnconduct", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@cnfrnconduct", SqlDbType.VarChar, 500).Value = cnfrnconduct.Text;
                }
                if (cnfrnattend.Text == null)
                {
                    cmd.Parameters.Add("@cnfrnattend", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@cnfrnattend", SqlDbType.VarChar, 500).Value = cnfrnattend.Text;
                }
                if (otherinfo.Text == null)
                {
                    cmd.Parameters.Add("@otherinfo", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@otherinfo", SqlDbType.VarChar, 500).Value = otherinfo.Text;
                }
                cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
                cmd.Parameters.Add("@msts", SqlDbType.Bit).Value = msts;
                if (memofprofbody.Text == null)
                {
                    cmd.Parameters.Add("@memofprofbody ", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@memofprofbody ", SqlDbType.VarChar, 500).Value = memofprofbody.Text;
                }
                if (pan.Text == null)
                {
                    cmd.Parameters.Add("@pan ", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@pan ", SqlDbType.VarChar, 50).Value = pan.Text;
                }
                if (religion.Text == null)
                {
                    cmd.Parameters.Add("@religion ", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@religion ", SqlDbType.VarChar, 50).Value = religion.Text;
                }
                if (pf.Text == string.Empty)
                {
                    cmd.Parameters.Add("@pf ", SqlDbType.VarChar, 500).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@pf ", SqlDbType.VarChar, 500).Value = pf.Text;
                }
                if (sbankname.Text == null)
                {
                    cmd.Parameters.Add("@sbankname ", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@sbankname ", SqlDbType.VarChar, 100).Value = sbankname.Text;
                }
                if (sbranchname.Text == null)
                {
                    cmd.Parameters.Add("@sbranchname ", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@sbranchname ", SqlDbType.VarChar, 100).Value = sbranchname.Text;
                }
                if (baccnumber.Text == null)
                {
                    cmd.Parameters.Add("@baccnumber", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@baccnumber", SqlDbType.VarChar, 50).Value = baccnumber.Text;
                }
                if (isfc.Text == null)
                {
                    cmd.Parameters.Add("@isfc ", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@isfc ", SqlDbType.VarChar, 50).Value = isfc.Text;
                }

                if (grosspay.Text == null || grosspay.Text == "")
                {
                    cmd.Parameters.Add("@grosspay", SqlDbType.BigInt).Value = Convert.DBNull;
                }
                else
                {
                    cmd.Parameters.Add("@grosspay", SqlDbType.BigInt).Value = Convert.ToInt64(grosspay.Text);
                }

                if (payscale.Text == null)
                {
                    cmd.Parameters.Add("@payscale ", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@payscale ", SqlDbType.VarChar, 50).Value = payscale.Text;
                }
                cmd.Parameters.Add("@tornt", SqlDbType.Bit).Value = true;
                if (apptype.Text == null)
                {
                    cmd.Parameters.Add("@apptype ", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@apptype ", SqlDbType.VarChar, 50).Value = apptype.Text;
                }
                if (uniappletternumber.Text == null)
                {
                    cmd.Parameters.Add("@uniappletternumber ", SqlDbType.VarChar, 50).Value = Convert.DBNull.ToString();
                }
                else
                {
                    cmd.Parameters.Add("@uniappletternumber ", SqlDbType.VarChar, 50).Value = uniappletternumber.Text;
                }
                if (uniappdate.Text == null || uniappdate.Text == "")
                {
                    cmd.Parameters.Add("@uniappdate", SqlDbType.DateTime).Value = Convert.DBNull;
                }
                else
                {
                    cmd.Parameters.Add("@uniappdate", SqlDbType.DateTime).Value = Convert.ToDateTime(uniappdate.Text);
                }
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                DetailsView1.ChangeMode(DetailsViewMode.ReadOnly);
                DetailsView1.ChangeMode(DetailsViewMode.Insert);
                Response.Redirect("~/admin/regteacher.aspx");
            }
        else
        {
            tcid.Text = string.Empty;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Teacher ID Not Available !');", true);

        }
        

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        try
        {
            TextBox tcid = ((TextBox)(DetailsView1.Rows[0].FindControl("TextBox79")));
            if (tcid.Text == string.Empty || tcid.Text == "")
            {
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Enter Teacher ID in the given field !');", true);
            }
            else
            {
                SqlCommand cmdcheck = new SqlCommand("select count(*) as cnt from tbteacher where teacherclgid=@u", con);
                cmdcheck.Parameters.Add("@u", SqlDbType.VarChar,50).Value = tcid.Text;
                Int32 check = Convert.ToInt32(cmdcheck.ExecuteScalar());
                if (check == 0)
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Teacher ID Available !');", true);
                }
                else
                {
                    tcid.Text = string.Empty;
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Teacher ID Not Available !');", true);

                }
            }
        }
        catch (Exception exp)
        {
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Enter numeric number only !');", true);
        }
    }
}