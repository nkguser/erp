﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Common/Common.master" AutoEventWireup="true" CodeFile="frmhodgallerypage.aspx.cs" Inherits="HOD_frmhodgallerypage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<link href="css/lightbox.css" rel="stylesheet" type="text/css" />
    <script src="js/prototype.js" type="text/javascript"></script>
    <script src="js/scriptaculous.js?load=effects,builder" type="text/javascript"></script>
    <script src="js/lightbox.js" type="text/javascript"></script>
    <style type="text/css">
        body
        {
            margin: 50px 0px;
            padding: 0px;
            background-color: #000000;
            color: #ffffff;
        }
        #content
        {
            width: 620px;
            margin: 0px auto;
        }
        #desc
        {
            margin: 10px;
            float: left;
            font-family: Arial, sans-serif;
            font-size: 12px;
        }
    </style>

    <link type="text/css" rel="stylesheet" href="css/tn3.css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <!-- include tn3 plugin -->
    <script type="text/javascript" src="js/jquery.tn3lite.min.js"></script>

    <script type="text/javascript">
        var j = jQuery.noConflict();
        j(document).ready(function () {
            //Thumbnailer.config.shaderOpacity = 1;
            var tn1 = j('.mygallery').tn3({
                skinDir: "skins",
                imageClick: "fullscreen",
                autoplay: true,
                image: {
                    maxZoom: 0.5,
                    crop: true,
                    clickEvent: "dblclick",
                    transitions: [{
                        type: "blinds"
                    }, {
                        type: "grid"
                    }, {
                        type: "grid",
                        duration: 460,
                        easing: "easeInQuad",
                        gridX: 1,
                        gridY: 8,
                        // flat, diagonal, circle, random
                        sort: "random",
                        sortReverse: false,
                        diagonalStart: "bl",
                        // fade, scale
                        method: "scale",
                        partDuration: 360,
                        partEasing: "easeOutSine",
                        partDirection: "left"
                    }]
                }
            });
        });
</script>

    <h2>
        Gallery
    </h2>

    <div id="dvlb" runat="server">
        <asp:DataList RepeatColumns="5" RepeatDirection="Horizontal" ID="Dllightbox" 
            runat="server" onselectedindexchanged="Dllightbox_SelectedIndexChanged">
        
            <ItemTemplate>
                <a href='../albums/<%#Eval("p_albdetalbcod") %>/<%#Eval("p_albdetcod") %><%#Eval("p_albdetpic") %>' id="anc" rel="lightbox[roadtrip1]">
                    <img src='../albums/<%#Eval("p_albdetalbcod") %>/<%#Eval("p_albdetcod") %><%#Eval("p_albdetpic") %>' id="pic" height="110px" width="110px"
                        style="padding: 5px; border: 1px solid #CCCCCC; margin: 2px" />
                </a>
            </ItemTemplate>
        </asp:DataList>
        <script type="text/javascript" language="javascript">
            j(document).ready(function () {
                j('#anc').trigger("click");
                //               var target = $.find('a[rel^=lightbox]') || $.find('area[rel^=lightbox]');
                //               alert(target);
                //               if (target) {
                //                   //event.stop();
                //                   $.Lightbox().start(target);
                //               }
            }
       );
        
        </script>
    </div>


    <div id="dvslide" runat="server" class="mygallery">
        <div class="tn3 album">
            <asp:DataList ID="Dlslide" runat="server">
                <ItemTemplate>
                    <%--<h4>
                        Fixed Dimensions</h4>
                    <div class="tn3 description">
                        Images with fixed dimensions</div>
                    <div class="tn3 thumb">
                        images/35x35/1.jpg</div>--%>
                    <ol>
                        <h4>
                            <%#Eval("p_albdetdsc")%></h4>

                        <div class="tn3 description">
                            My Gallery
                      <%--  <a href="UploadedImages/<%#Eval("ImageName") %>">--%>
                            <img src='../albums/<%#Eval("p_albdetalbcod") %>/<%#Eval("p_albdetcod") %><%#Eval("p_albdetpic") %>' />
                        <%--</a>--%>
                        </div>
                        
                    </ol>
                    
                </ItemTemplate>
                
            </asp:DataList>
        </div>
    </div>
</asp:Content>

