﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            HttpBrowserCapabilities browse = Request.Browser;
            //if (browse.Browser != "Chrome")
            //{
            //    //ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation","javascript:alert('You're not using the Chrome browser. Please use Chrome for best compatibility.');",true);
            //    LBLmsg.Text = "You're not using the Chrome browser. Please use Chrome for best compatibility.";
            //}
            //else
            //{
                if (User.Identity.IsAuthenticated)
                    Server.Transfer("logging.aspx");
            //}
        }
    }
    private Int32 logincheck(String usrid, string pwd, out Int32 userrole,out Int32 depid,out string depname,out  Int32 userid,out Int32 other,out Int32 stid,out Int32 start,out Int32 phase, out Int32 cursem)
    {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
            if (con.State == ConnectionState.Closed)
            con.Open();
            SqlCommand cmd = new SqlCommand("loginCheck", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@userlogid", SqlDbType.VarChar,50).Value = usrid;
            cmd.Parameters.Add("@pwd", SqlDbType.VarChar, 50).Value = pwd;
            cmd.Parameters.Add("@userroleid", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@depid", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@depname", SqlDbType.VarChar,50).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@userid", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@stid", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@other", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@start", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@phase", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@cursem", SqlDbType.Int).Direction = ParameterDirection.Output; 
            cmd.Parameters.Add("@ret", SqlDbType.Bit).Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();
            Int32 a = Convert.ToInt32(cmd.Parameters["@ret"].Value);
            userrole = 0;
            depid = 0;
            depname = string.Empty;
            userid = 0;
            stid = 0;
            other = 0;
            start = 0;
            phase = 0;
            cursem = 0;
            if (a == 1)
            {
                userrole = Convert.ToInt32(cmd.Parameters["@userroleid"].Value);
                depid = Convert.ToInt32(cmd.Parameters["@depid"].Value);
                depname = Convert.ToString(cmd.Parameters["@depname"].Value);
                userid = Convert.ToInt32(cmd.Parameters["@userid"].Value);
                other = Convert.ToInt32(cmd.Parameters["@other"].Value); 
                stid = Convert.ToInt32(cmd.Parameters["@stid"].Value);
                start = Convert.ToInt32(cmd.Parameters["@start"].Value);
                phase = Convert.ToInt32(cmd.Parameters["@phase"].Value);
                cursem = Convert.ToInt32(cmd.Parameters["@cursem"].Value);        
            }
            cmd.Dispose();
            con.Close();
            return a;
    }

    protected void BTNLogin_Click(object sender, EventArgs e)
    {
        HttpBrowserCapabilities browse = Request.Browser;
        //if (browse.Browser != "Chrome")
        //{
        //    //ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('You're not using the Chrome browser. Please use Chrome for best compatibility.');", true);
        //    LBLmsg.Text = "You're not using the Chrome browser. Please use Chrome for best compatibility.";
        //}
        //else
        //{
            Int32 userrole, a, depid, userid,other, stid,start,cursem,phase;
            string depname;
            a = logincheck(TextBox1.Text,TextBox2.Text,out userrole,out depid,out depname,out userid,out other,out stid,out start,out phase,out cursem);
            if (a == -1)
            {
                LBLmsg.Text = "Invalid User";
            }
            else if (a == -2)
            {
                LBLmsg.Text = "Wrong Password";
            }
            else
            {
                Session["depid"] = depid;
                Session["depname"] = depname;
                Session["userid"] = userid;
                Session["stid"] = stid;
                Session["other"] = other;
                Session["start"] = start;
                Session["phase"] = phase;
                Session["cursem"] = cursem;
                FormsAuthenticationTicket logtkt = new FormsAuthenticationTicket(1, TextBox1.Text, DateTime.Now, DateTime.Now.AddMinutes(20), false, userrole.ToString(), FormsAuthentication.FormsCookiePath);
                string logtktst = FormsAuthentication.Encrypt(logtkt);
                HttpCookie logck = new HttpCookie(FormsAuthentication.FormsCookieName, logtktst);
                Response.Cookies.Add(logck);
                //Session["logid"] = TextBox1.Text;
                //Session["userrole"] = userrole;            
                Server.Transfer("logging.aspx");
            }
        //}
    }
}