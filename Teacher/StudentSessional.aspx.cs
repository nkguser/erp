﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;


public partial class Teacher_StudentSessional : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {       
        if (Page.IsPostBack == false)
        {
            DDLSessionBind();
        }
    }
    private void DDLSessionBind()
    {        
        DDLSession.DataTextField = "session";
        DDLSession.DataValueField = "sessionid";
        DDLSession.DataSource = ERP.AllSession();
        DDLSession.DataBind();
        DDLSession.Items.Insert(0, "-- Select session --");
    }
    protected void DDLSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(DDLSession.SelectedIndex==0)
        {
            sessionclear();
        }
        else
        {
            sessionclear();
            classbind();
        }
    }
    private void sessionclear()
    {
        DDLClass.Items.Clear();
        DDLClass.Items.Insert(0, "-- Choose Class --");
        DDLClass.Enabled = false;
        DDLSubject.Items.Clear();
        DDLSubject.Items.Insert(0, "-- Choose Subject --");
        DDLSubject.Enabled = false;
        RBLOpt.Enabled = false;
        RBLOpt.ClearSelection();
        fillshowpanel.Visible = false;
        DDLSessional.Items.Clear();
        LBLDate.Text = string.Empty;
        LBLMaxMarks.Text = string.Empty;
        LBLmsg.Text = string.Empty;
        TxtDate.Visible = false;
        TXTMaxMarks.Visible = false;
        fillpanel.Visible = false;
        viewpanel.Visible = false;
        submitpanel.Visible = false;
    }
    private void classbind()
    {
        DDLClass.DataTextField = "Class";
        DDLClass.DataValueField = "subjectallotgroupid";
        DDLClass.DataSource = ERP.TeacherClass(Convert.ToInt32(Session["stid"]), Convert.ToInt32(DDLSession.SelectedValue));
        DDLClass.DataBind();
        DDLClass.Enabled = true;
        DDLClass.Items.Insert(0, "-- Select Class --");
    }
    protected void DDLClass_SelectedIndexChanged(object sender, EventArgs e)
    {
       if(DDLClass.SelectedIndex==0)
       {
           classclear();
       }
       else
       {
           classclear();
           subjectbind();
       }
    }
    private void classclear()
    {
        DDLSubject.Items.Clear();
        DDLSubject.Items.Insert(0, "-- Choose Subject --");
        DDLSubject.Enabled = false;
        RBLOpt.Enabled = false;
        RBLOpt.ClearSelection();
        fillshowpanel.Visible = false;
        DDLSessional.Items.Clear();
        LBLDate.Text = string.Empty;
        LBLMaxMarks.Text = string.Empty;
        LBLmsg.Text = string.Empty;
        TxtDate.Visible = false;
        TXTMaxMarks.Visible = false;
        fillpanel.Visible = false;
        viewpanel.Visible = false;
        submitpanel.Visible = false;
    }
    private void subjectbind()
    {
        DDLSubject.DataTextField = "subject";
        DDLSubject.DataValueField = "subjectallotsubjectid";
        DDLSubject.DataSource = ERP.TeacherSubject(Convert.ToInt32(Session["stid"]), Convert.ToInt32(DDLClass.SelectedValue));
        DDLSubject.DataBind();
        DDLSubject.Enabled = true;
        DDLSubject.Items.Insert(0, "-- Select Subject --");
    }
    protected void DDLSubject_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSubject.SelectedIndex == 0)
        {
            subjectclear();
        }
        else
        {
            subjectclear();
            RBLOpt.Enabled = true;
        }
    }
    private void subjectclear()
    {
        RBLOpt.Enabled = false;
        RBLOpt.ClearSelection();
        fillshowpanel.Visible = false;
        DDLSessional.Items.Clear();
        LBLDate.Text = string.Empty;
        LBLMaxMarks.Text = string.Empty;
        LBLmsg.Text = string.Empty;
        TxtDate.Visible = false;
        TXTMaxMarks.Visible = false;
        fillpanel.Visible = false;
        viewpanel.Visible = false;
        submitpanel.Visible = false;
    }
    protected void RBLOpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RBLOpt.SelectedIndex == 0)
        {
            viewpanel.Visible = false;
            submitpanel.Visible = false;
            fillshowpanel.Visible = true;
            TxtDate.Text = string.Empty;
            TXTMaxMarks.Text = string.Empty;
            LBLDate.Text = string.Empty;
            LBLMaxMarks.Text = string.Empty;
            TxtDate.Visible = true;
            TXTMaxMarks.Visible = true;
            LBLmsg.Text = string.Empty;
            DDLSessional.Items.Clear();
            Int32 i = SessionalNo() + 1;
            if (i <= 3)
            {
                DDLSessional.Items.Add(i.ToString());
            }
            if (DDLSessional.Text == string.Empty)
            {
                LBLmsg.Text = "All Sessional Marks have been filled by you!!!";
            }
            else
            {
                GridFillBind();
                if (GridFill.Rows.Count == 0)
                {
                    LBLmsg.Text = "There are no records for submission of marks..";
                }
                else
                {
                    fillpanel.Visible = true;
                    LBLmsg.Text = string.Empty;
                }
            }
        }
        else
        {
            fillpanel.Visible = false;
            viewpanel.Visible = false;
            submitpanel.Visible = false;
            fillshowpanel.Visible = true;
            TxtDate.Text = string.Empty;
            TXTMaxMarks.Text = string.Empty;
            LBLDate.Text = string.Empty;
            LBLMaxMarks.Text = string.Empty;
            TxtDate.Visible = false;
            TXTMaxMarks.Visible = false;
            LBLmsg.Text = string.Empty;
            DDLSessional.Items.Clear();
            DDLSessional.Items.Insert(0,"-- Choose Semester --");
            for (Int32 i = 1; i <= 3; i++)
            {
                DDLSessional.Items.Add(i.ToString());
            }                    
        }
    }
    protected void DDLSessional_SelectedIndexChanged(object sender, EventArgs e)
    {
        Int32 sid;
        if(RBLOpt.SelectedIndex==0)
        {
            return;
        }
        else
        {
            if(DDLSessional.SelectedIndex==0)
            {
                LBLDate.Text = string.Empty;
                LBLMaxMarks.Text = string.Empty;
                LBLmsg.Text = string.Empty;
                viewpanel.Visible = false;
                submitpanel.Visible = false;
            }
            else
            {
                Int32 sesno = Convert.ToInt32(DDLSessional.SelectedValue);
                Int32 sbid = Convert.ToInt32(DDLSubject.SelectedValue);
                Int32 gid = Convert.ToInt32(DDLClass.SelectedValue);
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlCommand cmd = new SqlCommand("SELECT  top 1  studentid FROM tbstudent WHERE (studentgroupid =@gid )", con);
                cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    sid = Convert.ToInt32(dr["studentid"]);
                    SqlCommand cmdcheck = new SqlCommand("SELECT COUNT(*) AS cnt FROM tbstudentsessional WHERE (studentsessionalsubjectid = @sbid) AND (studentsessionalstudentid = @sid) AND (studentsessionalno = @sesno)", con);
                    cmdcheck.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
                    cmdcheck.Parameters.Add("@sbid", SqlDbType.Int).Value = sbid;
                    cmdcheck.Parameters.Add("@sesno", SqlDbType.Int).Value = sesno;
                    dr.Close();
                    Int32 check = Convert.ToInt32(cmdcheck.ExecuteScalar());
                    if (check == 0)
                    {
                        LBLmsg.Text = sesno + " Sessional Marks not yet filled ";
                        LBLDate.Text = string.Empty;
                        LBLMaxMarks.Text = string.Empty;
                        viewpanel.Visible = false;
                        submitpanel.Visible = false;
                    }
                    else
                    {
                        SqlCommand submitcheck = new SqlCommand("SELECT COUNT(*) AS cnt FROM tbstudentsessional WHERE (studentsessionalsubjectid = @sbid) AND (studentsessionalstudentid = @sid) AND (studentsessionalno = @sesno) AND (studentsessionalreqlvlid = 1) AND  (studentsessionaldelsts = 0)", con);
                        submitcheck.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
                        submitcheck.Parameters.Add("@sbid", SqlDbType.Int).Value = sbid;
                        submitcheck.Parameters.Add("@sesno", SqlDbType.Int).Value = sesno;
                        Int32 subcheck = Convert.ToInt32(submitcheck.ExecuteScalar());
                        if (subcheck == 0)
                        {
                            viewpanel.Visible = false;
                            submitpanel.Visible = true;
                            LBLmsg.Text = sesno + " Sessional Marks already been Submitted";
                            GridShowSubmitBind();
                        }
                        else
                        {
                            viewpanel.Visible = true;
                            submitpanel.Visible = false;
                            LBLmsg.Text = string.Empty;
                            GridShowBind();
                        }
                    }
                }
                else
                {
                    LBLmsg.Text = "There are no records exists for Class " + DDLClass.SelectedItem;
                }
                cmd.Dispose();
        }
        }
    }
    private void GridFillBind()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT studentid,studentrollno FROM tbStudent where studentgroupid=@gid", con);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridFill.DataSource = ds;
        GridFill.DataBind();
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        Application.Lock();
        Int32 c = GridFill.Rows.Count;
        for (Int32 i = 0; i < c; i++)
        {
            GridFill.SelectRow(i);
            Int32 l = Convert.ToInt32(GridFill.SelectedDataKey.Value);
            Boolean b = Convert.ToBoolean(((RadioButtonList)(GridFill.SelectedRow.FindControl("RBLAttendance"))).SelectedValue);
            Decimal d = Convert.ToDecimal(((TextBox)(GridFill.SelectedRow.FindControl("TxtMarks"))).Text);
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("INSERT INTO tbstudentsessional (studentsessionalstudentid, studentsessionalsubjectid, studentsessionalno, studentsessionaldate, studentsessionalattnd, studentsessionalmarks, studentsessionalmmarks, studentsessionalreqlvlid) VALUES (@rid,@sid,@sesn_no,@date,@at,@m,@mm, 1)", con);
            cmd.Parameters.Add("@rid", SqlDbType.Int).Value = l;
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = DDLSubject.SelectedValue;
            DateTime sdate = Convert.ToDateTime(TxtDate.Text);
            cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(sdate.ToShortDateString());
            cmd.Parameters.Add("@sesn_no", SqlDbType.Int).Value = Convert.ToInt32(DDLSessional.SelectedValue);
            cmd.Parameters.Add("@at", SqlDbType.Bit).Value = b;
            cmd.Parameters.Add("@m", SqlDbType.Decimal).Value = d;
            cmd.Parameters.Add("@mm", SqlDbType.Int).Value = Convert.ToInt32(TXTMaxMarks.Text);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        if (Convert.ToInt32(DDLSessional.SelectedValue) == 3)
        {
            Response.Redirect("~/Teacher/StudentSessional.aspx");
            LBLmsg.Text = " 3rd Sessional Marks has been Submitted Successfully!!! ";
        }
        else
        {
            LBLmsg.Text = Convert.ToInt32(DDLSessional.SelectedValue) + " Sessional Marks has been Submitted Successfully!!! ";
            fillpanel.Visible = false;
            fillshowpanel.Visible = false;
            DDLSubject.SelectedIndex = 0;
            RBLOpt.Enabled = false;

        }
    }
    private Int32 SessionalNo()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SessionalNot", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@s", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.ExecuteScalar();
        Int32 k = Convert.ToInt32(cmd.Parameters["@s"].Value);
        cmd.Dispose();
        con.Close();
        return k;
    }
    private Int32 SessionalNoHOD()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SessionalNoHodsubmit", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@s", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.ExecuteScalar();
        Int32 k = Convert.ToInt32(cmd.Parameters["@s"].Value);
        cmd.Dispose();
        con.Close();
        return k;
    }
    private void GridShowBind()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT     tbstudentsessional.studentsessionaldate, tbstudentsessional.studentsessionalmmarks, tbstudent.studentid FROM         tbstudentsessional INNER JOIN                 tbstudent ON tbstudentsessional.studentsessionalstudentid = tbstudent.studentid INNER JOIN                      tbgroup ON tbstudent.studentgroupid = tbgroup.groupid WHERE     (tbstudentsessional.studentsessionalno = @sno) AND (tbstudentsessional.studentsessionalsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND                       (tbstudentsessional.studentsessionaldelsts = 0) AND (tbgroup.groupdelsts = 0) AND (tbstudent.studentdelsts = 0) AND (tbstudentsessional.studentsessionalreqlvlid = 1)", con);
        cmd.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DDLSessional.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            LBLDate.Text = Convert.ToDateTime(dr["studentsessionaldate"]).ToShortDateString();
            LBLMaxMarks.Text = dr["studentsessionalmmarks"].ToString();
        }
        cmd.Dispose();
        dr.Close();
        con.Close();        
        GridShow.DataSource = ERP.DispSessional(Convert.ToInt32(DDLSessional.SelectedValue), Convert.ToInt32(DDLSubject.SelectedValue), Convert.ToInt32(DDLClass.SelectedValue));
        GridShow.DataBind();
    }
    protected void GridShow_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridShow.EditIndex = e.NewEditIndex;
        GridShowBind();
    }
    protected void GridShow_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        Int32 rno = Convert.ToInt32(GridShow.DataKeys[e.RowIndex].Value);
        Boolean at = Convert.ToBoolean(((RadioButtonList)(GridShow.Rows[e.RowIndex].FindControl("RBLAttendance"))).SelectedValue);
        Decimal mks = Convert.ToDecimal(((TextBox)(GridShow.Rows[e.RowIndex].FindControl("TxtMarks"))).Text);
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("UPDATE tbstudentSessional SET studentSessionalattnd =@a, studentsessionalMarks =@m where studentsessionalstudentid=@rid and studentsessionalsubjectid=@sid and studentsessionaldate=@d and studentsessionalno=@sno and studentsessionaldelsts=0 and studentsessionalreqlvlid=1", con);
        cmd.Parameters.Add("@a", SqlDbType.Bit).Value = at;
        cmd.Parameters.Add("@m", SqlDbType.Decimal).Value = mks;
        cmd.Parameters.Add("@rid", SqlDbType.Int).Value = rno;
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@d", SqlDbType.Date).Value = Convert.ToDateTime(LBLDate.Text);
        cmd.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DDLSessional.SelectedValue);
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        GridShow.EditIndex = -1;
        GridShowBind();
    }
    protected void GridShow_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridShow.EditIndex = -1;
        GridShowBind();
    }
    protected void GridShow_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType==DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Edit)
        //{
        //    GridViewRow r=e.Row;
        //    RadioButtonList rd = (RadioButtonList)(e.Row.FindControl("RBLAttendance"));
        //    rd.Items.FindByValue(e.Row.["Sessional_Attendance"]).Selected = true;

        //}
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            //Label l = (Label)(GridShow.Rows[e.Row.RowIndex].FindControl("LBLAttend"));
            Label l = (Label)(e.Row.FindControl("LBLAttend"));
            if (l.Text.Equals("True"))
                l.Text = "Present";
            else
                l.Text = "Absent";
        }
    }
    protected void LnkBtnSubmitHod_Click(object sender, EventArgs e)
    {
            if (con.State == ConnectionState.Closed)
            con.Open();
            SqlCommand cmd = new SqlCommand("UPDATE  tbstudentsessional SET studentsessionalreqlvlid =2 FROM tbstudentsessional INNER JOIN tbstudent ON tbstudentsessional.studentsessionalstudentid = tbstudent.studentid WHERE   (tbstudent.studentgroupid = @gid) and   (studentsessionaldate = @date)  AND (studentsessionalno = @sno) AND (studentsessionalsubjectid = @sid) AND (studentsessionaldelsts = 0) and studentsessionalreqlvlid=1", con);
            cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(LBLDate.Text);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
            cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
            cmd.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DDLSessional.SelectedValue);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
            DDLSessional.SelectedIndex = 0;
            LBLDate.Text = string.Empty;
            LBLMaxMarks.Text = string.Empty;
            viewpanel.Visible = false;
            LBLmsg.Text = "Marks has been Submitted Successfully";
    }
    private void GridShowSubmitBind()
    {
       
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT  tbstudentsessional.studentsessionaldate, tbstudentsessional.studentsessionalmmarks, tbstudent.studentid FROM tbstudentsessional INNER JOIN    tbstudent ON tbstudentsessional.studentsessionalstudentid = tbstudent.studentid INNER JOIN tbgroup ON tbstudent.studentgroupid = tbgroup.groupid WHERE (tbstudentsessional.studentsessionalno = @sno) AND (tbstudentsessional.studentsessionalsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND    (tbstudentsessional.studentsessionaldelsts = 0) AND (tbgroup.groupdelsts = 0) AND (tbstudent.studentdelsts = 0) AND (tbstudentsessional.studentsessionalreqlvlid = 2)", con);
        cmd.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DDLSessional.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            LBLDate.Text = Convert.ToDateTime(dr["studentsessionaldate"]).ToShortDateString();
            LBLMaxMarks.Text = dr["studentsessionalmmarks"].ToString();
        }
        cmd.Dispose();
        dr.Close();
        con.Close();
        GridshowSubmit.DataSource = ERP.DispSessionalsubmit(Convert.ToInt32(DDLSessional.SelectedValue), Convert.ToInt32(DDLSubject.SelectedValue), Convert.ToInt32(DDLClass.SelectedValue));
        GridshowSubmit.DataBind();
    }
}