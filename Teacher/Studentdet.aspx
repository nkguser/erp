﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="Studentdet.aspx.cs" Inherits="Teacher_Studentdet" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table class="style2" style="width: 311px">                                 
                                   
                                    <tr>
                                        <td class="style18" style="font-weight: bold; text-align: left;">
                                            Session</td>
                                        <td class="style9">
                                            <asp:DropDownList ID="DDLDeleteClassSession" runat="server" AutoPostBack="True" 
                                                onselectedindexchanged="DDLDeleteClassSession_SelectedIndexChanged" 
                                                style="margin-left: 0px" Height="22px" Width="212px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style18" style="font-weight: bold; text-align: left;">
                                            Class</td>
                                        <td class="style9">
                                            <asp:DropDownList ID="DDLDeleteClassName" runat="server" 
                                                Enabled="False" 
                                                onselectedindexchanged="DDLDeleteClassName_SelectedIndexChanged" 
                                                Height="22px" Width="212px">
                                                <asp:ListItem>-- Select Class --</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style18" style="font-weight: bold">
                                            &nbsp;</td>
                                        <td class="style10">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style18">
                                            &nbsp;</td>
                                        <td class="style10">
                                            <asp:Button ID="Button1" runat="server" Text="View" onclick="Button1_Click" 
                                                Width="118px" style="font-weight: 700" />
                                        </td>
                                    </tr>
                                </table>
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%" 
        Font-Names="Verdana" Font-Size="8pt" InteractiveDeviceInfos="(Collection)" 
        WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
        <LocalReport ReportPath="Teacher\teacher_stdet.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="SqlDataSource1" Name="Teacher_stdet" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:cn %>" SelectCommand="HOD_Rpt_Sdet" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="DDLDeleteClassSession" Name="sid" 
                PropertyName="SelectedValue" Type="Int32" />
            <asp:ControlParameter ControlID="DDLDeleteClassName" Name="gid" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>

