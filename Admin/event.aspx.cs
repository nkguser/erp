﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Net;

public partial class Admin_event : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        grd_bind();

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        

            if (Button1.Text == "Save")
            {
                string el = Guid.NewGuid().ToString();
                //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
                
                if (FileUpload1.FileName == "")
                {

                    el = "logo.jpg";                    
                }
                else
                {
                    el = el + FileUpload1.FileName;
                    FileUpload1.PostedFile.SaveAs(Server.MapPath("~/event/") + el);
                }
                SqlCommand cmd = new SqlCommand("INSERT INTO tbevent (EventNAme, eventdetail, eventfrmdat, eventtodat, eventvenue, Eventcatid, Eventlogo,eventclgid) VALUES (@ename,@ed,@ef,@et,@ev,@ec,@el,eclgid)", con);
                cmd.Parameters.Add("@ename", SqlDbType.VarChar, 50).Value = TextBox1.Text;//
                cmd.Parameters.Add("@ed", SqlDbType.NText).Value = Editor2.Content;
                cmd.Parameters.Add("@ef", SqlDbType.DateTime).Value = Convert.ToDateTime(TextBox3.Text);//
                cmd.Parameters.Add("@et", SqlDbType.DateTime).Value = Convert.ToDateTime(TextBox5.Text);//
                cmd.Parameters.Add("@ev", SqlDbType.VarChar, 100).Value = TextBox2.Text;//
                cmd.Parameters.Add("@ec", SqlDbType.Int).Value = Convert.ToInt32(DropDownList1.SelectedValue);
                cmd.Parameters.Add("@el", SqlDbType.VarChar, 100).Value = el;
                cmd.Parameters.Add("@eclgid", SqlDbType.Int).Value = Convert.ToInt32(Session["other"]);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
                grd_bind();
            }
        
        else
        {
            
            SqlCommand cmd = new SqlCommand("UPDATE    tbevent SET              EventNAme = @name, eventdetail = @ed, eventfrmdat = @ef, eventtodat = @et, eventvenue = @ev, Eventcatid = @ec, Eventlogo = @el WHERE     (Eventid = @eid)", con);
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = TextBox1.Text;
            cmd.Parameters.Add("@ed", SqlDbType.NText).Value = Editor2.Content;
            Int32 x = Convert.ToInt32(ViewState["cod"]);
            cmd.Parameters.Add("@eid", SqlDbType.Int).Value = Convert.ToInt32(ViewState["cod"]);
            cmd.Parameters.Add("@ef", SqlDbType.DateTime).Value = Convert.ToDateTime(TextBox3.Text);
            cmd.Parameters.Add("@et", SqlDbType.DateTime).Value = Convert.ToDateTime(TextBox5.Text);
            cmd.Parameters.Add("@ec", SqlDbType.Int).Value = Convert.ToInt32(DropDownList1.SelectedValue);
            cmd.Parameters.Add("@ev", SqlDbType.VarChar, 100).Value = TextBox2.Text;//
            if (FileUpload1.FileName == null || FileUpload1.FileName == string.Empty)
            {
                SqlCommand cmd1 = new SqlCommand("select eventlogo from tbevent where eventid=" + Convert.ToInt32(ViewState["cod"]), con);
                SqlDataReader dr = cmd1.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    string abc = dr[0].ToString();
                    if (abc != null || abc != string.Empty)
                    {
                        cmd.Parameters.Add("@el", SqlDbType.VarChar, 100).Value = abc;
                    }
                    else
                    {
                        cmd.Parameters.Add("@el", SqlDbType.VarChar, 100).Value = Convert.DBNull.ToString();
                    }
                }
                dr.Close();
                cmd1.Dispose();
            }
            else
            {
                
                
                DirectoryInfo di = new DirectoryInfo(Server.MapPath("~/event/"));
                if (di.Exists == false)
                    di.Create();
                string el = Guid.NewGuid().ToString();

                el = el + FileUpload1.FileName;
                FileUpload1.PostedFile.SaveAs(Server.MapPath("~/event/") + el);
                cmd.Parameters.Add("@el", SqlDbType.VarChar, 100).Value = el;
                SqlCommand cmd1 = new SqlCommand("select eventlogo from tbevent where eventid=" + Convert.ToInt32(ViewState["cod"]), con);
                SqlDataReader dr = cmd1.ExecuteReader();

                if (dr.HasRows)
                {
                    dr.Read();
                    string abc = dr[0].ToString();
                    if(abc!="logo.jpg")
                    File.Delete(Server.MapPath("~/event/" + abc));
                }
                dr.Close();
                cmd1.Dispose();
            }
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();            
            Button1.Text = "Save";
           
        }
            Response.Redirect("~/ADMIN/event.aspx");          
    }
    private void grd_bind()
    {
        SqlCommand cmd2 = new SqlCommand("SELECT     tbevent.Eventid, tbevent.EventNAme, tbevent.eventdetail, tbevent.eventfrmdat, tbevent.eventtodat, tbevent.eventvenue, tbevent.Eventlogo,     tbeventcat.eventcatid,     tbeventcat.Eventcatdetail FROM         tbevent INNER JOIN                      tbeventcat ON tbevent.Eventcatid = tbeventcat.Eventcatid where tbevent.eventclgid="+ Convert.ToInt32(Session["other"])+ "  order by tbevent.eventfrmdat", con);        
        SqlDataAdapter adp = new SqlDataAdapter(cmd2);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridView1.DataSource = ds;
        GridView1.DataBind();
        cmd2.Dispose();
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        Int32 a = Convert.ToInt32(GridView1.DataKeys[e.NewEditIndex][0]);
        ViewState["cod"] = a;
        SqlCommand cmd3 = new SqlCommand("SELECT tbevent.Eventid, tbevent.EventNAme, tbevent.eventdetail, tbevent.eventfrmdat, tbevent.eventtodat, tbevent.eventvenue, tbevent.Eventlogo,  tbeventcat.eventcatid,       tbeventcat.Eventcatdetail FROM         tbevent INNER JOIN                      tbeventcat ON tbevent.Eventcatid = tbeventcat.Eventcatid where tbevent.eventid=@u order by tbevent.eventfrmdat ", con);
        cmd3.Parameters.Add("@u", SqlDbType.Int).Value = a;
        SqlDataReader dr = cmd3.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            TextBox1.Text= dr["eventnaME"].ToString();
            TextBox2.Text = dr["EVENTVENUE"].ToString();
            TextBox3.Text = dr["eventfrmdat"].ToString();
            TextBox5.Text = dr["eventtodat"].ToString();            

            Editor2.Content = dr["eVENTdETAIL"].ToString();
            DropDownList1.SelectedIndex = -1;
            DropDownList1.Items.FindByValue(dr["Eventcatid"].ToString()).Selected = true;
        }
        dr.Close();
        cmd3.Dispose();
        con.Close();

        Button1.Text = "update";
        e.Cancel = true;       
    }

}
