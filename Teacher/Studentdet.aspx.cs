﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class Teacher_Studentdet : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (Page.IsPostBack == false)
        {
            DDLDeleteClassSessionBind();
        }
    }
    private void DDLDeleteClassSessionBind()
    {
        DataSet ds = AllSession();
        DDLDeleteClassSession.DataValueField = "Sessionid";
        DDLDeleteClassSession.DataTextField = "session";
        DDLDeleteClassSession.DataSource = ds;
        DDLDeleteClassSession.DataBind();
        DDLDeleteClassSession.Items.Insert(0, "--Select Session --");
    }
    protected void DDLDeleteClassSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDLDeleteClassNameBind();
    }
    protected void DDLDeleteClassName_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    private void DDLDeleteClassNameBind()
    {
        DDLDeleteClassName.DataTextField = "classname";
        DDLDeleteClassName.DataValueField = "classid";
        DDLDeleteClassName.DataSource = AllClass(Convert.ToInt32(Session["depid"]), Convert.ToInt32(DDLDeleteClassSession.SelectedValue));
        DDLDeleteClassName.DataBind();
        DDLDeleteClassName.Enabled = true;
        DDLDeleteClassName.Items.Insert(0, "-- Select Class --");
    }
    private DataSet AllClass(Int32 did, Int32 sid)
    {
        SqlCommand cmd = new SqlCommand("dispclass");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    private DataSet AllSession()
    {
        SqlDataAdapter adp = new SqlDataAdapter("dispsession", con);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        ReportViewer1.LocalReport.Refresh();
    }
}