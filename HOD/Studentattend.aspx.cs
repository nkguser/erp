﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class HOD_Studentattend : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    protected void RBLOpt_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (RBLOpt.SelectedIndex == 0)
        {
            SessionClear();
            SessionViewClear();
            DDLSessionfBind();
            PanelFill.Visible = true;
            PanelView.Visible = false;
        }
        else
        {
            SessionClear();
            SessionViewClear();
            DDLSessionBind();
            PanelFill.Visible = false;
            PanelView.Visible = true;
        }
    }
    private void getdepsubjects()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("dispsubjecthod", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        cmd.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataReader dr = cmd.ExecuteReader();
        DDLSubject.DataTextField = "subject";
        DDLSubject.DataValueField = "Subjectid";
        DDLSubject.DataSource = dr;
        DDLSubject.DataBind();
        DDLSubject.Items.Insert(0, "-- Select Subject --");
        DDLSubject.Items[0].Attributes.Add("Disabled", "Disabled");
        dr.Close();
        cmd.Dispose();
        con.Close();
    }
    
    private void DDLSessionBind()
    {
        DDLSession.DataTextField = "session";
        DDLSession.DataValueField = "sessionid";
        DDLSession.DataSource = ERP.AllSession();
        DDLSession.DataBind();
        DDLSession.Items.Insert(0, "-- Select session --");
    }
    private void DDLSessionfBind()
    {
        DDlSessionf.DataTextField = "session";
        DDlSessionf.DataValueField = "sessionid";
        DDlSessionf.DataSource = ERP.AllSession();
        DDlSessionf.DataBind();
        DDlSessionf.Items.Insert(0, "-- Select session --");
    }
    protected void DDLSession_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (DDLSession.SelectedIndex == 0)
        {
            SessionViewClear();

        }
        else
        {
            SessionViewClear();
            DDLAllClassBind();
            DDLSubject.Enabled = false;
        }

    }

    private void SessionViewClear()
    {
        DDLClass.Items.Clear();
        DDLSubject.Items.Clear();
        LBLTotalDelivered.Text = string.Empty;
        datepanel.Visible = false;
        DDLClass.Enabled = false;
        DDLSubject.Enabled = false;
        DDLClass.Items.Insert(0, "--Select Class--");
        DDLSubject.Items.Insert(0, "--Select Subject--");
        Panel1.Visible = false;
        RadioButtonList1.ClearSelection();
        DDLDate.Items.Clear();
        DDLDate.Items.Insert(0, "-- Choose Date --");
        approvedpanel.Visible = false;
    }

    private void DDLAllClassBind()
    {
        DDLClass.DataTextField = "Class";
        DDLClass.DataValueField = "subjectallotgroupid";
        DDLClass.DataSource = ERP.AllClassHOD(Convert.ToInt32(Session["depid"]), Convert.ToInt32(DDLSession.SelectedValue), Convert.ToInt32(Session["stid"]));
        DDLClass.DataBind();
        DDLClass.Enabled = true;
        //DDLSubject.Enabled = true;
        DDLClass.Items.Insert(0, "-- Select Class --");
    }
    private void DDLTeacherClassBind()
    {
        DDLclassf.DataTextField = "Class";
        DDLclassf.DataValueField = "subjectallotGroupid";
        DDLclassf.DataSource = ERP.TeacherClass(Convert.ToInt32(Session["stid"]), Convert.ToInt32(DDlSessionf.SelectedValue));
        DDLclassf.DataBind();
        DDLclassf.Enabled = true;
        //DDLSubject.Enabled = true;
        DDLclassf.Items.Insert(0, "-- Select Class --");
        DDLclassf.Items[0].Attributes.Add("disabled", "disabled");
        DDLclassf.Items[0].Selected = true;
    }
    protected void DDLClassf_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLclassf.SelectedIndex == 0)
        {
            ClassClear();
        }
        else
        {
            ClassClear();
            fillsubject();
        }

    }

    private void fillsubject()
    {
        DDLSubjectf.DataTextField = "subject";
        DDLSubjectf.DataValueField = "subjectallotSubjectid";
        DDLSubjectf.DataSource = ERP.TeacherSubject(Convert.ToInt32(Session["stid"]), Convert.ToInt32(DDLclassf.SelectedValue));
        DDLSubjectf.DataBind();
        DDLSubjectf.Enabled = true;
        DDLSubjectf.Items.Insert(0, "-- Select Subject --");
        DDLSubjectf.Items[0].Attributes.Add("disabled", "disabled");
        DDLSubjectf.Items[0].Selected = true;
    }

    private void ClassClear()
    {
        Fillattendance.Visible = false;
        DDLSubjectf.Items.Clear();
        DDLSubjectf.Items.Insert(0, "--Select Subject--");
        LBLTotalDeliveredf.Text = string.Empty;
    }
   
    private void DDLDateBindnotapproved()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT DISTINCT tbstudentattnd.studentattnddate FROM tbstudentattnd INNER JOIN tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE     (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattndapprovlvid = 2) AND (tbstudent.studentdelsts = 0)", con);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DDLDate.DataTextField = "studentattndDate";
        DDLDate.DataValueField = "studentattndDate";
        DDLDate.DataSource = ds;
        DDLDate.DataBind();
        DDLDate.Items.Insert(0, "-- Choose Date --");
        DDLDate.Items[0].Attributes.Add("disabled", "disabled");
        DDLDate.Items[0].Selected = true;
    }
    private void DDLDateBindapproved()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT DISTINCT tbstudentattnd.studentattnddate FROM tbstudentattnd INNER JOIN tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE     (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattndapprovlvid = 3) AND (tbstudent.studentdelsts = 0)", con);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DDLDate.DataTextField = "studentattndDate";
        DDLDate.DataValueField = "studentattndDate";
        DDLDate.DataSource = ds;
        DDLDate.DataBind();
        cmd.Dispose();
        DDLDate.Items.Insert(0, "-- Choose Date --");
        DDLDate.Items[0].Attributes.Add("disabled", "disabled");
        DDLDate.Items[0].Selected = true;
    }
    private void DDLDatesubmithodBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT DISTINCT tbstudentattnd.studentattnddate FROM tbstudentattnd INNER JOIN tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE     (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattndapprovlvid = 2) AND (tbstudent.studentdelsts = 0)", con);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DDLdatesubmithod.DataTextField = "studentattndDate";
        DDLdatesubmithod.DataValueField = "studentattndDate";
        DDLdatesubmithod.DataSource = ds;
        DDLdatesubmithod.DataBind();
        cmd.Dispose();
        DDLdatesubmithod.Items.Insert(0, "-- Choose Date --");
        DDLdatesubmithod.Items[0].Attributes.Add("disabled", "disabled");
        DDLdatesubmithod.Items[0].Selected = true;
    }
    private void GridFillBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT studentid,studentRollno FROM tbStudent where studentgroupid=@gid", con);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLclassf.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridFill.DataSource = ds;
        GridFill.DataBind();
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {

        DateTime sdate = Convert.ToDateTime(TxtDate.Text);
        Int32 c = GridFill.Rows.Count;
        if (c != 0)
        {
            GridFill.SelectRow(0);
            Int32 l1 = Convert.ToInt32(GridFill.SelectedDataKey.Value);
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmddate = new SqlCommand("SELECT COUNT(*) AS cnt FROM tbstudentattnd WHERE (studentattndstudentid = @sid) AND (studentattndsubjectid = @ssid) AND (studentattnddate = @sadate)", con);
            cmddate.Parameters.Add("@sid", SqlDbType.Int).Value = l1;
            cmddate.Parameters.Add("@ssid", SqlDbType.Int).Value = DDLSubjectf.SelectedValue;
            cmddate.Parameters.Add("@sadate", SqlDbType.Date).Value = Convert.ToDateTime(sdate.ToShortDateString());
            Int32 check = Convert.ToInt32(cmddate.ExecuteScalar());
            if (check == 0)
            {
                for (Int32 i = 0; i < c; i++)
                {
                    GridFill.SelectRow(i);
                    Int32 l = Convert.ToInt32(GridFill.SelectedDataKey.Value);
                    Boolean b = Convert.ToBoolean(((RadioButtonList)(GridFill.SelectedRow.FindControl("RBLAttendance"))).SelectedValue);
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    Application.Lock();
                    SqlCommand cmd = new SqlCommand("INSERT INTO tbstudentattnd (studentattndstudentid, studentattndsubjectid, studentattnddate, studentattndattend, studentattndapprovlvid, studentattnddelsts) VALUES (@rid,@sid,@date,@at,2,0)", con);
                    cmd.Parameters.Add("@rid", SqlDbType.Int).Value = l;
                    cmd.Parameters.Add("@sid", SqlDbType.Int).Value = DDLSubjectf.SelectedValue;
                    cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(sdate.ToShortDateString());
                    cmd.Parameters.Add("@at", SqlDbType.Bit).Value = b;
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    con.Close();
                    Application.UnLock();

                }
                LBLTotalDeliveredfBind();
                GridFillBind();
                LBLmsgf.Text = "Records saved successfully for date : " + sdate.ToShortDateString();
                TxtDate.Text = string.Empty;
            }
            else
                LBLmsgf.Text = "Records already submitted for date : " + sdate.ToShortDateString();
        }
        else
            LBLmsgf.Text = " There are no records for submission........... ";
    }
    private void GridShowBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT tbstudent.studentid, tbstudent.studentrollno, tbstudentattnd.studentattndattend FROM  tbstudent INNER JOIN tbstudentattnd ON tbstudent.studentid = tbstudentattnd.studentattndstudentid WHERE     (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudentattnd.studentattnddate = @date) AND (tbstudent.studentgroupid = @gid) AND (tbstudent.studentdelsts = 0) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattndapprovlvid = 2)", con);
        cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(DDLDate.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShow.DataSource = ds;
        GridShow.DataBind();
        cmd.Dispose();
        con.Close();
    }
    private void GridShowSubmitBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT tbstudent.studentid, tbstudent.studentrollno, tbstudentattnd.studentattndattend FROM tbstudent INNER JOIN tbstudentattnd ON tbstudent.studentid = tbstudentattnd.studentattndstudentid WHERE (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudentattnd.studentattnddate = @date) AND (tbstudent.studentgroupid = @gid) AND (tbstudent.studentdelsts = 0) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattndapprovlvid = 3)", con);
        cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(DDLDate.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShowSubmit.DataSource = ds;
        GridShowSubmit.DataBind();
        cmd.Dispose();
        con.Close();

    }
    protected void GridShow_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridShow.EditIndex = e.NewEditIndex;
        GridShowBind();
    }
    protected void GridShow_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        Int32 rno = Convert.ToInt32(GridShow.DataKeys[e.RowIndex].Value);
        Boolean at = Convert.ToBoolean(((RadioButtonList)(GridShow.Rows[e.RowIndex].FindControl("RBLAttendance"))).SelectedValue);
        if (con.State == ConnectionState.Closed)
            con.Open();
        // set datakeyfield to student id in grid view
        SqlCommand cmd = new SqlCommand("UPDATE   tbstudentattnd SET  studentattndattend = @a FROM   tbstudentattnd INNER JOIN              tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE        (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudentattnd.studentattnddate = @d) AND (tbstudentattnd.studentattnddelsts = 0) AND   (tbstudent.studentdelsts = 0) AND (tbstudentattnd.studentattndstudentid = @rid) AND (tbstudentattnd.studentattndapprovlvid = 2)", con);
        cmd.Parameters.Add("@a", SqlDbType.Bit).Value = at;
        cmd.Parameters.Add("@rid", SqlDbType.Int).Value = rno;
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@d", SqlDbType.Date).Value = Convert.ToDateTime(DDLDate.SelectedValue);
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        GridShow.EditIndex = -1;
        GridShowBind();
    }
    protected void GridShow_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridShow.EditIndex = -1;
        GridShowBind();
    }
    protected void GridShow_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType==DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Edit)
        //{
        //    GridViewRow r=e.Row;
        //    RadioButtonList rd = (RadioButtonList)(e.Row.FindControl("RBLAttendance"));
        //    rd.Items.FindByValue(e.Row.["Sessional_Attendance"]).Selected = true;

        //}
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) AS count FROM  tbstudentattnd WHERE  (studentattndsubjectid = @sid) AND (studentattndstudentid = @rid) AND (studentattndattend = 'True') AND (studentattnddelsts = 0)", con);
            // set gridview datakey
            cmd.Parameters.Add("@rid", SqlDbType.Int).Value = Convert.ToInt32(GridShow.DataKeys[e.Row.RowIndex].Value);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
            Label l = (Label)(e.Row.FindControl("LBLAttend"));
            Label l1 = (Label)(e.Row.FindControl("LblAt"));
            Label l2 = (Label)(e.Row.FindControl("Lblpercent"));
            l1.Text = cmd.ExecuteScalar().ToString();
            if (Convert.ToInt32(LBLTotalDelivered.Text) != 0 || LBLTotalDelivered.Text != "")
                l2.Text = (100 * Convert.ToInt32(l1.Text) / Convert.ToInt32(LBLTotalDelivered.Text)).ToString() + "%";
            else
                l2.Text = "--";
            cmd.Dispose();
            con.Close();
            if (l.Text.Equals("True"))
                l.Text = "Present";
            else
                l.Text = "Absent";
        }
    }
    protected void DDLDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLDate.SelectedIndex == 0)
        {
            Panel1.Visible = false;
            approvedpanel.Visible = false;
        }
        else
        {
            if (RadioButtonList1.SelectedIndex == 0)
            {
                GridShowSubmit.Visible = false;
                Panel1.Visible = true;
                GridShowBind();
                approvedpanel.Visible = false;
            }
            //else if (RadioButtonList1.SelectedIndex != 0 || RadioButtonList1.SelectedIndex != 1)
            //{
            //    Panel1.Visible = false;
            //    approvedpanel.Visible = false;
            //}
            else
            {
                GridShowSubmit.Visible = true;
                Panel1.Visible = false;
                approvedpanel.Visible = true;
                GridShowSubmitBind();
                //GridShowSubmit.Visible = true;
            }
        }


    }
    private void LBLTotalDeliveredfBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT COUNT(DISTINCT tbstudentattnd.studentattnddate) AS count FROM  tbstudentattnd INNER JOIN tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE        (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0) ", con);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubjectf.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLclassf.SelectedValue);
        LBLTotalDeliveredf.Text = cmd.ExecuteScalar().ToString();
        cmd.Dispose();
        con.Close();
    }
    private void LBLTotalDeliveredBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT COUNT(DISTINCT tbstudentattnd.studentattnddate) AS count FROM  tbstudentattnd INNER JOIN tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE        (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0)", con);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        LBLTotalDelivered.Text = cmd.ExecuteScalar().ToString();
        cmd.Dispose();
        con.Close();
    }
    protected void GridFill_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("SELECT  COUNT(*) AS count FROM         tbstudentattnd WHERE     (studentattndattend = 'True') AND (studentattndsubjectid = @sid) AND (studentattndstudentid = @rid)  AND (studentattnddelsts = 0)", con);
            //set datakaey fiel student id in grid
            cmd.Parameters.Add("@rid", SqlDbType.Int).Value = Convert.ToInt32(GridFill.DataKeys[e.Row.RowIndex].Value);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubjectf.SelectedValue);
            Label l1 = (Label)(e.Row.FindControl("LblAt"));
            Label l2 = (Label)(e.Row.FindControl("Lblpercent"));
            l1.Text = cmd.ExecuteScalar().ToString();
            //try
            //{
            //    l2.Text = (100 * Convert.ToInt32(l1.Text) / Convert.ToInt32(LBLTotalDelivered.Text)).ToString() + "%";
            //}
            //catch (Exception ex)
            //{

            //    l2.Text = "--";
            //}
            if (LBLTotalDeliveredf.Text == string.Empty || Convert.ToInt32(LBLTotalDeliveredf.Text) != 0 || LBLTotalDeliveredf.Text == null)
                l2.Text = (100 * Convert.ToInt32(l1.Text) / Convert.ToInt32(LBLTotalDeliveredf.Text)).ToString() + "%";
            else
                l2.Text = "--";
            cmd.Dispose();
            con.Close();
        }
    }
    protected void GridShowSubmit_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) AS count FROM  tbstudentattnd WHERE        (studentattndsubjectid = @sid) AND (studentattndstudentid = @rid) AND (studentattndattend = 'True') AND (studentattnddelsts = 0)", con);
            // set gridview datakey
            cmd.Parameters.Add("@rid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSubmit.DataKeys[e.Row.RowIndex].Value);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
            Label l = (Label)(e.Row.FindControl("LBLAttend2"));
            Label l1 = (Label)(e.Row.FindControl("LblAt2"));
            Label l2 = (Label)(e.Row.FindControl("Lblpercent2"));
            l1.Text = cmd.ExecuteScalar().ToString();
            l2.Text = (100 * Convert.ToInt32(l1.Text) / Convert.ToInt32(LBLTotalDelivered.Text)).ToString() + "%";
            cmd.Dispose();
            con.Close();
            if (l.Text.Equals("True"))
                l.Text = "Present";
            else
                l.Text = "Absent";
        }
    }
    //protected void LnkBtnShowSubmit_Click(object sender, EventArgs e)
    //{
    //    //hide();
    //    GridShowSubmitBind();
    //    DDLDatesubmithodBind();
    //    PanelFill.Visible = true;
    //    GridShowSubmit.Visible = true;
    //}
    protected void LnkBtmSubmitHod_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("UPDATE tbstudentattnd SET   studentattndapprovlvid = 3 FROM    tbstudentattnd INNER JOIN  tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE     (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattnddate <= @date) AND  (tbstudentattnd.studentattndsubjectid = @sid) and (studentattndapprovlvid = 2)", con);
        cmd.Parameters.Add("@date", SqlDbType.Date).Value = Convert.ToDateTime(DDLdatesubmithod.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        Panel1.Visible = false;
        GridShowSubmit.Visible = true;
        Panel1.Visible = false;
        DDLDateBindapproved();
        RadioButtonList1.SelectedIndex = 1;

    }

    //protected void DDLSem_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (con.State == ConnectionState.Closed)
    //        con.Open();
    //    SqlCommand cmd = new SqlCommand("SELECT tbSubject.Subjectid, tbSubject.Subjecttitle + ' (' + tbSubjectype.SubjectTypename + ')' AS subject FROM tbSubject INNER JOIN tbSubjectype ON tbSubject.Subjecttypeid = tbSubjectype.SubjectTypeid WHERE (Subjectsem = @sem) AND (Subjectdepid = @did) AND (tbsubject.Subjectdelsts = 0) AND (tbsubjectype.Subjectdelsts = 0)", con);
    //    cmd.Parameters.Add("@did", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
    //    cmd.Parameters.Add("@sem", SqlDbType.Int).Value = Convert.ToInt32(DDLSem.SelectedValue);
    //    SqlDataReader dr = cmd.ExecuteReader();
    //    DDLSubject.DataTextField = "subject";
    //    DDLSubject.DataValueField = "Subjectid";
    //    DDLSubject.DataSource = dr;
    //    DDLSubject.DataBind();
    //    DDLSubject.Items.Insert(0, "-- Select Subject --");
    //    DDLSubject.Items[0].Attributes.Add("Disabled", "Disabled");
    //    dr.Close();
    //    cmd.Dispose();
    //    con.Close();
    //}
    protected void DDLGrp_SelectedIndexChanged(object sender, EventArgs e)
    {
        //GridFillBind();        
    }
    //protected void DDLCoursef_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    DDLDeptf.DataTextField = "Depname";
    //    DDLDeptf.DataValueField = "Depid";
    //    DDLDeptf.DataSource = ERP.AllDepartment(Convert.ToInt32(DDLCoursef.SelectedValue));
    //    DDLDeptf.DataBind();
    //    DDLDeptf.Items.Insert(0, "-- Select Department --");
    //    DDLDeptf.Items[0].Attributes.Add("disabled", "disabled");
    //    DDLDeptf.Items[0].Selected = true;
    //}
    protected void DDLSubjectf_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSubjectf.SelectedIndex == 0)
        {
            SubjectClear();

        }
        else
        {
            SubjectClear();
            LBLTotalDeliveredfBind();
            Fillattendance.Visible = true;
            GridFillBind();
        }
    }

    private void SubjectClear()
    {
        Fillattendance.Visible = false;
        LBLTotalDeliveredf.Text = string.Empty;
    }
    protected void DDLSubject_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSubject.SelectedIndex == 0)
        {
            SubjectViewclear();
        }
        else
        {
            SubjectViewclear();
            LBLTotalDeliveredBind();
            if (Convert.ToInt32(LBLTotalDelivered.Text) == 0)
            {
                datepanel.Visible = false;
            }
            else
                datepanel.Visible = true;
        }
    }

    private void SubjectViewclear()
    {
        LBLTotalDelivered.Text = string.Empty;
        datepanel.Visible = false;
        Panel1.Visible = false;
        RadioButtonList1.ClearSelection();
        DDLDate.SelectedIndex = 0;
        approvedpanel.Visible = false;
        DDLDate.Items.Clear();
        DDLDate.Items.Insert(0, "-- Choose Date --");
    }
    //private void DDLDatesubmithodBind()
    //{
    //    if (con.State == ConnectionState.Closed)
    //        con.Open();
    //    SqlCommand cmd = new SqlCommand("SELECT DISTINCT tbstudentattnd.studentattnddate FROM tbstudentattnd INNER JOIN tbstudent ON tbstudentattnd.studentattndstudentid = tbstudent.studentid WHERE     (tbstudentattnd.studentattndsubjectid = @sid) AND (tbstudent.studentgroupid = @gid) AND (tbstudentattnd.studentattnddelsts = 0) AND (tbstudentattnd.studentattndapprovlvid = 2) AND (tbstudent.studentdelsts = 0)", con);
    //    cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLGrp.SelectedValue);
    //    cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSubject.SelectedValue);
    //    SqlDataAdapter adp = new SqlDataAdapter(cmd);
    //    DataSet ds = new DataSet();
    //    adp.Fill(ds);
    //    DDLdatesubmithod.DataTextField = "studentattndDate";
    //    DDLdatesubmithod.DataValueField = "studentattndDate";
    //    DDLdatesubmithod.DataSource = ds;
    //    DDLdatesubmithod.DataBind();
    //    DDLdatesubmithod.Items.Insert(0, "-- Choose Date --");
    //    DDLdatesubmithod.Items[0].Attributes.Add("disabled", "disabled");
    //    DDLdatesubmithod.Items[0].Selected = true;
    //}
    protected void DDlSessionf_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (DDlSessionf.SelectedIndex == 0)
        {
            SessionClear();

        }
        else
        {
            SessionClear();
            DDLclassf.Enabled = true;
            DDLTeacherClassBind();
        }

    }

    private void SessionClear()
    {
        DDLclassf.Items.Clear();
        DDLSubjectf.Items.Clear();
        LBLTotalDeliveredf.Text = string.Empty;
        Fillattendance.Visible = false;
        DDLclassf.Enabled = false;
        DDLSubjectf.Enabled = false;
        DDLclassf.Items.Insert(0, "--Select Class--");
        DDLSubjectf.Items.Insert(0, "--Select Subject--");
        DDLSubjectf.Items[0].Attributes.Add("disabled", "disabled");
    }
    protected void DDLClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLClass.SelectedIndex == 0)
        {
            ClassViewClear();
        }
        else
        {
            ClassViewClear();
            getdepsubjects();
            DDLSubject.Enabled = true;
        }
    }

    private void ClassViewClear()
    {
        DDLSubject.Items.Clear();
        DDLSubject.Enabled = false;
        DDLSubject.Items.Insert(0, "-- Select Subject --");
        LBLTotalDelivered.Text = string.Empty;
        datepanel.Visible = false;
        Panel1.Visible = false;
        RadioButtonList1.ClearSelection();
        approvedpanel.Visible = false;
        DDLDate.SelectedIndex = 0;
        DDLDate.Items.Clear();
        DDLDate.Items.Insert(0, "-- Choose Date --");
    }
    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RadioButtonList1.SelectedIndex == 0)
        {
            DDLDateBindnotapproved();
            DDLDatesubmithodBind();
            approvedpanel.Visible = false;

        }
        else
        {
            DDLDateBindapproved();
            Panel1.Visible = false;
        }
    }
}