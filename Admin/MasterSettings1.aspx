﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/InnerAdmin.master" AutoEventWireup="true" CodeFile="MasterSettings1.aspx.cs" Inherits="Admin_Default" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
    <ProgressTemplate>
        <img src="../images/ajax-loader1.gif" alt="Loading . . ." />
    </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
         <table class="styledmenu">
        <tr>
            <td>
                &nbsp;</td>
                <td><br />
               <asp:Menu ID="Menu1" runat="server" BackColor="#FFFBD6" 
                    DynamicHorizontalOffset="2" Font-Names="Times New Roman" Font-Size="Medium" 
                    ForeColor="#990000" onmenuitemclick="Menu1_MenuItemClick" 
                    StaticSubMenuIndent="10px" Font-Bold="True">
                    <DynamicHoverStyle BackColor="#990000" ForeColor="White" />
                    <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                    <DynamicMenuStyle BackColor="#FFFBD6" />
                    <DynamicSelectedStyle BackColor="#FFCC66" />
                    <Items>
                        <asp:MenuItem Text="User" Value="User">
                            <asp:MenuItem Text="Remove User" Value="1"></asp:MenuItem>
                            <asp:MenuItem Text="Reset Password" Value="2"></asp:MenuItem>
                        </asp:MenuItem>
                        <asp:MenuItem Text="Session" Value="Session">
                            <asp:MenuItem Text="New Session" Value="3"></asp:MenuItem>
                            <asp:MenuItem Text="Remove Session" Value="4"></asp:MenuItem>
                        </asp:MenuItem>
                        <asp:MenuItem Text="Course">
                            <asp:MenuItem Text="New Course" Value="5"></asp:MenuItem>
                            <asp:MenuItem Text="Remove Course" Value="6"></asp:MenuItem>
                        </asp:MenuItem>
                        <asp:MenuItem Text="Department   " Value="Department">
                            <asp:MenuItem Text="New Department" Value="7"></asp:MenuItem>
                            <asp:MenuItem Text="Remove Department" Value="8"></asp:MenuItem>
                        </asp:MenuItem>
                        <asp:MenuItem Text="Class" Value="Class">
                            <asp:MenuItem Text="New Class" Value="9"></asp:MenuItem>
                            <asp:MenuItem Text="Remove Class" Value="10"></asp:MenuItem>
                        </asp:MenuItem>
                        <asp:MenuItem Text="Group">
                            <asp:MenuItem Text="New Group" Value="11"></asp:MenuItem>
                            <asp:MenuItem Text="Remove Group" Value="12"></asp:MenuItem>
                        </asp:MenuItem>
                    </Items>
                    <StaticHoverStyle BackColor="#990000" ForeColor="White" />
                    <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
                    <StaticSelectedStyle BackColor="#FFCC66" />
                </asp:Menu></td>
            
            <td class="style4">
                <asp:Panel ID="PanlResetPass" runat="server" Visible="False">
                    <table class="style25">
                        <tr>
                            <td class="style6" colspan="2">
                                <strong>Reset Password</strong></td>
                        </tr>
                        <tr>
                            <td class="style30" nowrap="nowrap">
                                User ID</td>
                            <td>
                                <asp:TextBox ID="TxtResetPassUserID" runat="server" AutoPostBack="True" 
                                    ontextchanged="TxtResetPassUserID_TextChanged" CssClass="unwatermarked"></asp:TextBox>
                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                    ControlToValidate="TxtResetPassUserID" Display="Dynamic" 
                                    ErrorMessage="RequiredFieldValidator" ForeColor="Red">*</asp:RequiredFieldValidator>
                                &nbsp;&nbsp;
                                <asp:Label ID="LblChkUser2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="style31">
                            </td>
                            <td class="style5">
                                <asp:Button ID="BtnResetPass" runat="server" Text="Reset" 
                                    onclick="BtnResetPass_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="PanlDelUsr" runat="server" Visible="False">
                    <table class="style2">
                        <tr>
                            <td class="style6" colspan="2" nowrap="nowrap">
                                <strong>Remove User</strong></td>
                        </tr>
                        <tr>
                            <td class="style17" nowrap="nowrap">
                                User ID</td>
                            <td class="style18">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="TxtDeleteUserId" runat="server" AutoPostBack="True" 
                                    ontextchanged="TxtDeleteUserId_TextChanged"></asp:TextBox>
                                &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                    ControlToValidate="TxtDeleteUserId" Display="Dynamic" 
                                    ErrorMessage="RequiredFieldValidator" ForeColor="Red">*</asp:RequiredFieldValidator>
&nbsp;
                                <asp:Label ID="LblChkUser3" runat="server"></asp:Label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="style29" nowrap="nowrap">
                            </td>
                            <td class="style5">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="BtnDeleteUser" runat="server" Text="Remove" 
                                    onclick="BtnDeleteUser_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="PanlNewSesn" runat="server" Visible="False">
                    <table class="style2">
                        <tr>
                            <td class="style6" colspan="3">
                                <strong>Register Session</strong></td>
                        </tr>
                        <tr>
                            <td class="style32" nowrap="nowrap">
                                &nbsp;</td>
                            <td class="style9">
                                &nbsp;</td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style32">
                                Session Start Year</td>
                            <td class="style9">
                                <asp:TextBox ID="TxtNewSessionStart" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                                    ControlToValidate="TxtNewSessionStart" Display="Dynamic" 
                                    ErrorMessage="RequiredFieldValidator" ForeColor="Red">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator4" runat="server" 
                                    ControlToValidate="TxtNewSessionStart" Display="Dynamic" 
                                    ErrorMessage="CompareValidator" ForeColor="Red" Operator="DataTypeCheck" 
                                    Type="Integer">*</asp:CompareValidator>
                            </td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style32">
                                Session End Year</td>
                            <td class="style9">
                                <asp:TextBox ID="TxtNewSessionEnd" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
                                    ControlToValidate="TxtNewSessionEnd" Display="Dynamic" 
                                    ErrorMessage="RequiredFieldValidator" ForeColor="Red">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator5" runat="server" 
                                    ControlToValidate="TxtNewSessionEnd" Display="Dynamic" 
                                    ErrorMessage="CompareValidator" ForeColor="Red" Operator="DataTypeCheck" 
                                    Type="Integer">*</asp:CompareValidator>
                            </td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style33">
                            </td>
                            <td class="style10">
                                <asp:Button ID="BtnRegSession" runat="server" Text="Register" 
                                    onclick="BtnRegSession_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                 <asp:Panel ID="PanlDelSesn" runat="server" Visible="False">
                    <table class="style2">
                        <tr>
                            <td class="style6" colspan="3" nowrap="nowrap">
                                <strong>Remove Session</strong></td>
                        </tr>
                        <tr>
                            <td class="style36" nowrap="nowrap">
                                Session </td>
                            <td>
                                <asp:DropDownList ID="DDLDeleteSession" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style37" nowrap="nowrap">
                            </td>
                            <td class="style5">
                                <asp:Button ID="BtnDeleteSession" runat="server" Text="Remove" 
                                    onclick="BtnDeleteSession_Click" />
                            </td>
                            <td class="style5">
                                &nbsp;</td>
                        </tr>
                    </table>
                </asp:Panel>
                 <asp:Panel ID="PanlNewCrs" runat="server" Visible="False">
                    <table class="style2">
                        <tr>
                            <td class="style6" colspan="3">
                                <strong>Register New Course</strong></td>
                        </tr>
                        <tr>
                            <td class="style38">
                                Course Name</td>
                            <td class="style9">
                                <asp:TextBox ID="TxtNewCourseName" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                                    ControlToValidate="TxtNewCourseName" Display="Dynamic" 
                                    ErrorMessage="RequiredFieldValidator" ForeColor="Red">*</asp:RequiredFieldValidator>
                            </td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style38">
                                No. Of Semester</td>
                            <td class="style9">
                                <asp:TextBox ID="TxtNOS" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                                    ControlToValidate="TxtNOS" Display="Dynamic" 
                                    ErrorMessage="RequiredFieldValidator" ForeColor="Red">*</asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator7" runat="server" 
                                    ControlToValidate="TxtNOS" Display="Dynamic" ErrorMessage="CompareValidator" 
                                    ForeColor="Red" Operator="DataTypeCheck" Type="Integer">*</asp:CompareValidator>
                            </td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style39">
                            </td>
                            <td class="style10">
                                <asp:Button ID="BtnRegCourse" runat="server" Text="Register" 
                                    onclick="BtnRegCourse_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                 <asp:Panel ID="PanlDelCrs" runat="server" Visible="False">
                    <table class="style2">
                        <tr>
                            <td class="style6" colspan="3" nowrap="nowrap">
                                <strong>Remove Course</strong></td>
                        </tr>
                        <tr>
                            <td class="style40" nowrap="nowrap">
                                Choose Course</td>
                            <td>
                                <asp:DropDownList ID="DDLDeleteCourse" runat="server">
                                </asp:DropDownList>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style41" nowrap="nowrap">
                            </td>
                            <td class="style5">
                                <asp:Button ID="BtnDeleteCourse" runat="server" Text="Remove" 
                                    onclick="BtnDeleteCourse_Click" />
                            </td>
                            <td class="style5">
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                 <asp:Panel ID="PanlNewDept" runat="server" Visible="False">
                    <table class="style2">
                        <tr>
                            <td class="style6" colspan="2" nowrap="nowrap">
                                <strong>Register New Department</strong></td>
                        </tr>
                        <tr>
                            <td class="style55" nowrap="nowrap">
                                Course</td>
                            <td class="style13">
                                <asp:DropDownList ID="DDLNewDeptCourseName" runat="server" Width="200px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style55" nowrap="nowrap">
                                Department</td>
                            <td class="style13" nowrap="nowrap">
                                <asp:TextBox ID="TxtNewDeptName" runat="server" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" 
                                    ControlToValidate="TxtNewDeptName" Display="Dynamic" 
                                    ErrorMessage="RequiredFieldValidator" ForeColor="Red">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style56" nowrap="nowrap">
                            </td>
                            <td class="style10">
                                <asp:Button ID="BtnRegDept" runat="server" Text="Register" 
                                    onclick="BtnRegDept_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                 <asp:Panel ID="PanlDelDept" runat="server" Visible="False">
                    <table class="style2">
                        <tr>
                            <td class="style6" colspan="3">
                                <strong>Remove Department</strong></td>
                        </tr>
                        <tr>
                            <td class="style36">
                                Choose Course</td>
                            <td class="style9">
                                <asp:DropDownList ID="DDLDeleteDeptCourse" runat="server" AutoPostBack="True" 
                                    onselectedindexchanged="DDLDeleteDeptCourse_SelectedIndexChanged" 
                                    Width="200px">
                                </asp:DropDownList>
                            </td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style36">
                                Department</td>
                            <td class="style9">
                                <asp:DropDownList ID="DDLDeleteDeptName" runat="server" Enabled="False" 
                                    Width="200px">
                                    <asp:ListItem>-- Select Department --</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style37">
                            </td>
                            <td class="style10">
                                <asp:Button ID="BtnDeleteDept" runat="server" Text="Remove" 
                                    onclick="BtnDeleteDept_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>              
                 <asp:Panel ID="PanlNewClass" runat="server" Visible="False">
                    <table class="style2">
                        <tr>
                            <td class="style16" colspan="3" nowrap="nowrap">
                                <strong>Register New Class</strong></td>
                        </tr>
                        <tr>
                            <td class="style47" nowrap="nowrap">
                                Course</td>
                            <td class="style13">
                                <asp:DropDownList ID="DDLNewClassCourse" runat="server" AutoPostBack="True" 
                                    onselectedindexchanged="DDLNewClassCourse_SelectedIndexChanged" 
                                    Width="200px">
                                </asp:DropDownList>
                            </td>
                            <td class="style14">
                                &nbsp;</td>
                            <td class="style15">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style47" nowrap="nowrap">
                                Department</td>
                            <td class="style13">
                                <asp:DropDownList ID="DDLNewClassDept" runat="server" Enabled="False" 
                                    Width="200px">
                                    <asp:ListItem>-- Select Department --</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="style14">
                                &nbsp;</td>
                            <td class="style15">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style47" nowrap="nowrap">
                                Session</td>
                            <td class="style13">
                                <asp:DropDownList ID="DDLNewClassSession" runat="server" Width="200px">
                                </asp:DropDownList>
                            </td>
                            <td class="style14">
                                &nbsp;</td>
                            <td class="style15">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style47" nowrap="nowrap">
                                Class</td>
                            <td class="style13" nowrap="nowrap">
                                <asp:TextBox ID="TxtNewClassName" runat="server" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" 
                                    ControlToValidate="TxtNewClassName" Display="Dynamic" 
                                    ErrorMessage="RequiredFieldValidator" ForeColor="Red">*</asp:RequiredFieldValidator>
                            </td>
                            <td class="style14">
                                &nbsp;</td>
                            <td class="style15">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style48" nowrap="nowrap">
                            </td>
                            <td class="style10">
                                <asp:Button ID="BtnRegClass" runat="server" Text="Register" 
                                    onclick="BtnRegClass_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                 <asp:Panel ID="PanlDelClass" runat="server" Visible="False">
                    <table class="style2">
                        <tr>
                            <td class="style6" colspan="3">
                                <strong>Remove Class&nbsp;</strong></td>
                        </tr>
                        <tr>
                            <td class="style38">
                                Choose Course</td>
                            <td class="style9">
                                <asp:DropDownList ID="DDLDeleteClassCourse" runat="server" AutoPostBack="True" 
                                    onselectedindexchanged="DDLDeleteClassCourse_SelectedIndexChanged" 
                                    Width="200px">
                                </asp:DropDownList>
                            </td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style38">
                                Department</td>
                            <td class="style9">
                                <asp:DropDownList ID="DDLDeleteClassDept" runat="server" 
                                    Enabled="False" Width="200px" >
                                    <asp:ListItem>-- Select Department --</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style38">
                                Session</td>
                            <td class="style9">
                                <asp:DropDownList ID="DDLDeleteClassSession" runat="server" AutoPostBack="True" 
                                    onselectedindexchanged="DDLDeleteClassSession_SelectedIndexChanged" 
                                    Width="200px">
                                </asp:DropDownList>
                            </td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style38">
                                Class</td>
                            <td class="style9">
                                <asp:DropDownList ID="DDLDeleteClassName" runat="server" Enabled="False" 
                                    Width="200px">
                                    <asp:ListItem>-- Select Class --</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="style11">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td class="style39">
                            </td>
                            <td class="style10">
                                <asp:Button ID="BtnDeleteClass" runat="server" Text="Remove" 
                                    onclick="BtnDeleteClass_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="PanlNewGroup" runat="server" Visible="False">
                    <table class="style2">
                        <tr>
                            <td class="style16" colspan="2" nowrap="nowrap">
                                <strong>Register New Group</strong></td>
                        </tr>
                        <tr>
                            <td class="style51" nowrap="nowrap">
                                Course</td>
                            <td class="style13">
                                <asp:DropDownList ID="DDLNewGroupCourse" runat="server" AutoPostBack="True" 
                                    onselectedindexchanged="DDLNewGroupCourse_SelectedIndexChanged" 
                                    Width="200px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style51" nowrap="nowrap">
                                Department</td>
                            <td class="style13">
                                <asp:DropDownList ID="DDLNewGroupDept" runat="server" Enabled="False" 
                                    Width="200px">
                                    <asp:ListItem>-- Select Department --</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style51" nowrap="nowrap">
                                Session</td>
                            <td class="style13">
                                <asp:DropDownList ID="DDLNewGroupSession" runat="server" AutoPostBack="True" 
                                    onselectedindexchanged="DDLNewGroupSession_SelectedIndexChanged" 
                                    Width="200px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style51" nowrap="nowrap">
                                Class</td>
                            <td class="style13">
                                <asp:DropDownList ID="DDLNewGroupClass" runat="server" Enabled="False" 
                                    Width="200px">
                                    <asp:ListItem>-- Select Class --</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style51" nowrap="nowrap">
                                Group</td>
                            <td class="style13" nowrap="nowrap">
                                <asp:TextBox ID="TxtNewGroup" runat="server" Width="200px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" 
                                    ControlToValidate="TxtNewGroup" Display="Dynamic" 
                                    ErrorMessage="RequiredFieldValidator" ForeColor="Red">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="style21" nowrap="nowrap">
                            </td>
                            <td class="style10">
                                <asp:Button ID="BtnRegGroup" runat="server" Text="Register" 
                                    onclick="BtnRegGroup_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
         <asp:Panel ID="PanlDelGroup" runat="server" Visible="False">
                    <table class="style2">
                        <tr>
                            <td class="style6" colspan="2" nowrap="nowrap">
                                <strong>Remove Group</strong></td>
                        </tr>
                        <tr>
                            <td class="style54" nowrap="nowrap">
                                Choose Course</td>
                            <td class="style9">
                                <asp:DropDownList ID="DDLDeleteGroupCourse" runat="server" AutoPostBack="True" 
                                    onselectedindexchanged="DDLDeleteGroupCourse_SelectedIndexChanged" 
                                    Width="200px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style54" nowrap="nowrap">
                                Department</td>
                            <td class="style9">
                                <asp:DropDownList ID="DDLDeleteGroupDept" runat="server" Width="200px">
                                    <asp:ListItem>-- Select Department --</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style54" nowrap="nowrap">
                                Session</td>
                            <td class="style9">
                                <asp:DropDownList ID="DDLDeleteGroupSession" runat="server" AutoPostBack="True" 
                                    onselectedindexchanged="DDLDeleteGroupSession_SelectedIndexChanged" 
                                    Width="200px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style54" nowrap="nowrap">
                                Class</td>
                            <td class="style9">
                                <asp:DropDownList ID="DDLDeleteGroupClass" runat="server" Enabled="False" 
                                    AutoPostBack="True" 
                                    onselectedindexchanged="DDLDeleteGroupClass_SelectedIndexChanged" 
                                    Width="200px">
                                    <asp:ListItem>-- Select Class --</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style54" nowrap="nowrap">
                                Group</td>
                            <td class="style9">
                                <asp:DropDownList ID="DDLDeleteGroupName" runat="server" Enabled="False" 
                                    Width="200px">
                                    <asp:ListItem>-- Select Group --</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="style53" nowrap="nowrap">
                            </td>
                            <td class="style10">
                                <asp:Button ID="BtnDeleteGroup" runat="server" Text="Remove" 
                                    onclick="BtnDeleteGroup_Click" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>                
            </td> 
        </tr>
        <tr>
            <td class="style4">
                </td>
           <td class="style4">
            
            </td> 
            <td class="style4">
                </td>
        </tr>
        <tr>
            <td class="style3">
                </td>
            <td class="style3">
                </td>
            <td class="style3">
                </td>
        </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
    
 
</asp:Content>

