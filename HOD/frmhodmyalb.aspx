﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="frmhodmyalb.aspx.cs" Inherits="HOD_frmhodmyalb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <center><h2>
        My Albums</h2></center>
    <p>
        <asp:DataList ID="DataList1" runat="server" DataSourceID="ObjectDataSource1" 
            Width="969px" RepeatColumns="4" RepeatDirection="Horizontal" 
            DataKeyField="p_albcvrpic" onitemdatabound="DataList1_ItemDataBound" 
            onupdatecommand="DataList1_UpdateCommand" 
            oncancelcommand="DataList1_CancelCommand" 
            oneditcommand="DataList1_EditCommand" 
            ondeletecommand="DataList1_DeleteCommand" onitemcommand="DataList1_ItemCommand">
            <ItemTemplate>
    
                <table style="width: 100%">
                    <tr>
                        <td align="center">
               <asp:Image ID="img1" runat="server"  Height="95px" Width="95px"/>                    
                         </td>
                    </tr>
                    <tr>
                        <td align="center">
      <a href='frmalbdet.aspx?acod=<%#Eval("p_albcod") %>'><%#Eval("p_albnam") %></a>                    
                           </td>
                    </tr>
                    <tr>
                        <td align="center">
     <asp:LinkButton ID="lk1" runat="server" CommandArgument='<%#Eval("p_albcod") %>'
      CommandName="Edit" Text="Edit" />   
      <asp:LinkButton ID="lk2" runat="server" CommandArgument='<%#Eval("p_albcod") %>'                
      CommandName="Delete" Text="Delete" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
    <asp:LinkButton ID="lk3" runat="server" CommandArgument='<%#Eval("p_albcod") %>'
    CommandName="cancel" Text="Preview Album"></asp:LinkButton>                    
    <asp:LinkButton ID="lk4" runat="server" CommandArgument='<%#Eval("p_albcod") %>'
    CommandName="Update" Text="Upload Images" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
 <asp:LinkButton ID="lk5" runat="server" CommandArgument='<%#Eval("p_albcod") %>'                        
 CommandName="save" Text="Share this album" />
                         </td>
                    </tr>
                </table>
    
            </ItemTemplate>
        </asp:DataList>
    </p>
    <p>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
            SelectMethod="Display_rec" TypeName="nsphotosnak.clsalb" 
            OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:SessionParameter Name="regcod" SessionField="depid" Type="Int32" />
            </SelectParameters>
        </asp:ObjectDataSource>
    </p>
</asp:Content>

