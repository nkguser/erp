﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO.Compression; 

public partial class Teacher_testrpt : System.Web.UI.Page
{
    SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            DDLDateBind();
            DDLTestBind();
        }  
    }
    private void DDLDateBind()
    {
        if (cn.State == ConnectionState.Closed)
            cn.Open();
        SqlCommand cmd = new SqlCommand("SELECT DISTINCT tbscr.scrdat FROM    tbscr INNER JOIN tbtec ON tbscr.scrteccod = tbtec.teccod WHERE (tbtec.tecteacherID = @tid)", cn);
        cmd.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DDLDate.DataTextField = "scrdat";
        DDLDate.DataValueField = "scrdat";
        DDLDate.DataSource = ds;
        DDLDate.DataBind();
        cmd.Dispose();
        DDLDate.Items.Insert(0, "-- Choose Date --");
    }
    private void DDLTestBind()
    {
        if (cn.State == ConnectionState.Closed)
            cn.Open();
        SqlCommand cmd = new SqlCommand("SELECT [teccod], [tecnam] FROM [tbtec] WHERE ([tecteacherID] = @tid)", cn);
        cmd.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DDLTest.DataTextField = "tecnam";
        DDLTest.DataValueField = "teccod";
        DDLTest.DataSource = ds;
        DDLTest.DataBind();
        cmd.Dispose();
        DDLTest.Items.Insert(0, "-- Choose Test --");
    }
    
    private void grdbind()
    {
        SqlCommand cmd = new SqlCommand("SELECT        tbstudent.studentrollno, tbstudent.studentfname + ' ' + tbstudent.studentmname + ' ' + tbstudent.studentlname  AS Name, tbscr.scrcod, tbscr.scrregcod, tbscr.scrteccod, tbscr.scrdat, tbscr.scrval FROM            tbscr INNER JOIN tbstudent ON tbscr.scrregcod = tbstudent.studentid INNER JOIN tbtec ON tbscr.scrteccod = tbtec.teccod WHERE        (tbtec.tecteacherID = @stid) AND (tbscr.scrteccod = @tid) and (tbscr.scrdat = @did)", cn);
        cmd.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(DDLTest.SelectedValue);
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        if (DDLDate.SelectedIndex == 0)
        {
            cmd.Parameters.Add("@did", SqlDbType.Date).Value = Convert.DBNull;
        }
        else
        cmd.Parameters.Add("@did", SqlDbType.Date).Value = Convert.ToDateTime(DDLDate.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridView1.DataSource = ds;
        GridView1.DataBind();
        p1.Visible = true;
    }
    private void grdbind1()
    {
        SqlCommand cmd = new SqlCommand("SELECT        tbstudent.studentrollno, tbstudent.studentfname + ' ' + tbstudent.studentmname + ' ' + tbstudent.studentlname  AS Name, tbscr.scrcod, tbscr.scrregcod, tbscr.scrteccod, tbscr.scrdat, tbscr.scrval FROM            tbscr INNER JOIN tbstudent ON tbscr.scrregcod = tbstudent.studentid INNER JOIN tbtec ON tbscr.scrteccod = tbtec.teccod WHERE        (tbtec.tecteacherID = @stid) AND (tbscr.scrteccod = @tid) ", cn);
        cmd.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(DDLTest.SelectedValue);
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridView1.DataSource = ds;
        GridView1.DataBind();
        p1.Visible = true;
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLTest.SelectedIndex == 0)
        {
            p1.Visible = false;
            DDLDateBind();
            DDLDate.Enabled = false;
        }
        else
        {
            p1.Visible = true;
            DDLDateBind();
            DDLDate.Enabled = true;
            grdbind1();
        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (cn.State == ConnectionState.Closed)
            cn.Open();
        Int32 scod;
        scod = (Int32)(GridView1.DataKeys[e.RowIndex].Values["scrcod"]);
        SqlCommand cmd = new SqlCommand(" Delete from tbscr WHERE scrcod=@scod", cn);
        cmd.Parameters.Add("@scod", SqlDbType.Int).Value = scod;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        cn.Close();
        grdbind();
    }
    protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLDate.SelectedIndex == 0)
        {
            p1.Visible = true;
            grdbind1();
        }
        else
        {
            p1.Visible = true;
            grdbind();
        }
    }
}