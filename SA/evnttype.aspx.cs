﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public partial class SA_evnttype : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("INSERT INTO tbeventcat (Eventcatdetail) VALUES (@ed)", con);
        cmd.Parameters.Add("@ed", SqlDbType.VarChar, 50).Value = TextBox1.Text;//      
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        GridView1.DataBind();
        TextBox1.Text = string.Empty;
    }  
}