﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class HOD_mksreq : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlCommand cmdcheck1 = new SqlCommand("select studentid from tbstudent where studentrollno=@u", con);
        cmdcheck1.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(TextBox1.Text);
        if (con.State == ConnectionState.Closed)
            con.Open();
        Int32 sid = Convert.ToInt32(cmdcheck1.ExecuteScalar());

        SqlCommand cmdcheck2 = new SqlCommand("SELECT     count(*) FROM         tbstudentsessional  WHERE     (studentsessionalreqlvlid >= 2) AND (studentsessionalstudentid = @sid) AND (studentsessionalsubjectid = @sub) AND (studentsessionalno = @sno)", con);
        cmdcheck2.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
        cmdcheck2.Parameters.Add("@sub", SqlDbType.Int).Value = Convert.ToInt32(DropDownList1.SelectedValue);
        cmdcheck2.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DropDownList2.SelectedValue);

        if (con.State == ConnectionState.Closed)
            con.Open();
        Int32 check2 = Convert.ToInt32(cmdcheck2.ExecuteScalar());
        if (check2 == 0)
        {
            l1.Visible = true;
            l1.Text = "Please check details, Either Student Rollno is invalid or marks haven't been submitted...";
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Please check details, Either Student Rollno is invalid or marks haven't been submitted...');", true);
        }
        else
        {
            l1.Visible = false;
            SqlCommand cmdcheck3 = new SqlCommand("SELECT  * FROM         tbstudentsessional  WHERE     (studentsessionalreqlvlid >= 2) AND (studentsessionalstudentid = @sid) AND (studentsessionalsubjectid = @sub) AND (studentsessionalno = @sno)", con);
            cmdcheck3.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            cmdcheck3.Parameters.Add("@sub", SqlDbType.Int).Value = Convert.ToInt32(DropDownList1.SelectedValue);
            cmdcheck3.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DropDownList2.SelectedValue);

            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlDataReader dr = cmdcheck3.ExecuteReader();
            dr.Read();
            DateTime sdate = Convert.ToDateTime(dr["studentsessionaldate"]);
            Boolean oat = Convert.ToBoolean(dr["studentsessionalattnd"]);
            Boolean nat = Convert.ToBoolean(RadioButtonList1.SelectedValue);
            Int32 nmks = Convert.ToInt32(TextBox3.Text);
            Int32 omks = Convert.ToInt32(dr["studentsessionalmarks"]);
            string desc = TextBox2.Text;
            dr.Close();

            //insert soch k
            /*
             * INSERT INTO tbmarksreq
                  (marksreqstid, marksreqsubid, marksreqsesno, marksreqsesdate, marksreqoldattd, marksreqnewattd, marksreqnewmarks, marksreqoldmaRKS, marksreqdesc, 
                  marksreqreqlvlid, marksreqreqsts)
VALUES     (@sid,@sub,@sno,@sdate,@oat,@nat,@nmks,@omks,@desc, 2, 0)
             */

            SqlCommand cmd = new SqlCommand("INSERT INTO tbmarksreq (marksreqstid, marksreqsubid, marksreqsesno, marksreqsesdate, marksreqoldattd, marksreqnewattd, marksreqnewmarks, marksreqoldmaRKS, marksreqdesc,marksreqreqlvlid,marksreqtid) VALUES     (@sid,@sub,@sno,@sdate,@oat,@nat,@nmks,@omks,@desc, 3,@tid)", con);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
            cmd.Parameters.Add("@sub", SqlDbType.Int).Value = Convert.ToInt32(DropDownList1.SelectedValue);
            cmd.Parameters.Add("@sno", SqlDbType.Int).Value = Convert.ToInt32(DropDownList2.SelectedValue);
            cmd.Parameters.Add("@sdate", SqlDbType.Date).Value = sdate;
            cmd.Parameters.Add("@oat", SqlDbType.Bit).Value = oat;
            cmd.Parameters.Add("@nat", SqlDbType.Bit).Value = nat;
            cmd.Parameters.Add("@nmks", SqlDbType.Int).Value = nmks;
            cmd.Parameters.Add("@omks", SqlDbType.Int).Value = omks;
            cmd.Parameters.Add("@desc", SqlDbType.VarChar, 50).Value = desc;
            cmd.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
            if (con.State == ConnectionState.Closed)
                con.Open();
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
            Response.Redirect("~/hod/sentmksreq.aspx");
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        //try
        //{

        if (TextBox1.Text == string.Empty || TextBox1.Text == "")
        {
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Enter Roll Number in the given field');", true);
        }
        else
        {
            SqlCommand cmdcheck = new SqlCommand("select count(*) as cnt from tbstudent where studentrollno=@u", con);
            cmdcheck.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(TextBox1.Text);
            if (con.State == ConnectionState.Closed)
                con.Open();
            Int32 check = Convert.ToInt32(cmdcheck.ExecuteScalar());
            if (check == 0)
            {
                Panel1.Visible = false;
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Please Check Roll Number ...');", true);
            }
            else
            {

                SqlCommand cmd = new SqlCommand("SELECT tbstudent.studentid, tbsubjectallot.subjectallotsubjectid, tbsubject.subjecttitle + ' - ' + tbsubjectype.subjecttypename AS subject FROM tbsubject INNER JOIN tbsubjectallot ON tbsubject.subjectid = tbsubjectallot.subjectallotsubjectid INNER JOIN tbstudent ON tbsubjectallot.subjectallotgroupid = tbstudent.studentgroupid INNER JOIN tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid WHERE (tbstudent.studentrollno = @roll) AND (tbsubjectallot.subjectallotteacherid = @tid)", con);
                cmd.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
                cmd.Parameters.Add("@roll", SqlDbType.Int).Value = Convert.ToInt32(TextBox1.Text);
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {

                    DropDownList1.DataValueField = "subjectallotsubjectid";
                    DropDownList1.DataTextField = "subject";
                    DropDownList1.DataSource = dr;
                    DropDownList1.DataBind();
                    DropDownList1.Items.Insert(0, "-- Select Subject -- ");
                    cmd.Dispose();
                    dr.Close();
                    con.Close();
                    Panel1.Visible = true;
                }
                else
                {
                    Panel1.Visible = false;
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('No Subjects Alloted to this student ...');", true);
                }


                //Response.Write("<script language='javascript'>alert('Roll Number Available');</script>");
                //

                /*
                 * selection of subject
                 * SELECT tbstudent.studentid, tbsubjectallot.subjectallotsubjectid, tbsubject.subjecttitle + ' - ' + tbsubjectype.subjecttypename AS subject
    FROM            tbsubject INNER JOIN
                             tbsubjectallot ON tbsubject.subjectid = tbsubjectallot.subjectallotsubjectid INNER JOIN
                             tbstudent ON tbsubjectallot.subjectallotgroupid = tbstudent.studentgroupid INNER JOIN
                             tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid
    WHERE        (tbstudent.studentrollno = @roll) AND (tbsubjectallot.subjectallotteacherid = @tid)
                 */

                // else
                {
                    //label me msg - check roll no.

                }
            }
        }
        //}
        //catch (Exception exp)
        //{
        //    Response.Write("<script language='javascript'>alert('Enter numeric number only!');</script>");
        //}

        //int roll = Convert.ToInt32(TextBox1.Text);
        //sql   query  here


    }
}