﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class HOD_frmhodshralb : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Image img = (Image)(e.Item.FindControl("img1"));
            LinkButton lk = (LinkButton)(e.Item.FindControl("lk5"));
            Int32 a = Convert.ToInt32(lk.CommandArgument);
            nsphotosnak.clsalb obj = new nsphotosnak.clsalb();
            List<nsphotosnak.clsalbprp> k = obj.Find_rec(a);
            if (k[0].p_albsts == 'P')
                lk.Visible = true;
            else
                lk.Visible = false;
            if (k[0].p_albcvrpic == "nopic.jpg")
                img.ImageUrl = "~/nopic.jpg";
            else
                img.ImageUrl = "~/albums/" + a.ToString() + "//" + k[0].p_albcvrpic;
        }
    }

    protected void DataList1_CancelCommand(object source, DataListCommandEventArgs e)
    {
        Int32 a = Convert.ToInt32(e.CommandArgument);
        nsphotosnak.clsalb obj = new nsphotosnak.clsalb();
        List<nsphotosnak.clsalbprp> k = obj.Find_rec(a);
        Response.Redirect("~/Common/frmhodgallerypage.aspx?acod=" + a.ToString() + "&id=" + k[0].p_albtmpcod);
    }
    
}