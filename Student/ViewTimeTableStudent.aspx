﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/InnerStudent.master" AutoEventWireup="true" CodeFile="ViewTimeTableStudent.aspx.cs" Inherits="Student_ViewTimeTableStudent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <table class="styledmenu">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                            <asp:Panel ID="PanlDelClass" runat="server">
                                <table class="style2" style="width: 311px">
                                    <tr>
                                        <td class="style6" colspan="3">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style18">
                                            &nbsp;</td>
                                        <td class="style9">
                                            &nbsp;</td>
                                        <td class="style11">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td class="style7">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style18">
                                            &nbsp;</td>
                                        <td class="style9">
                                            &nbsp;</td>
                                        <td class="style11">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td class="style7">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style18">
                                            Session</td>
                                        <td class="style9">
                                            <asp:DropDownList ID="DDLDeleteClassSession" runat="server" AutoPostBack="True" 
                                                onselectedindexchanged="DDLDeleteClassSession_SelectedIndexChanged" 
                                                style="margin-left: 0px" Height="22px" Width="212px">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="style11">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td class="style7">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style18">
                                            Class</td>
                                        <td class="style9">
                                            <asp:DropDownList ID="DDLDeleteClassName" runat="server" 
                                                Enabled="False" 
                                                Height="22px" Width="212px">
                                                <asp:ListItem>-- Select Class --</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="style11">
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td>
                                            &nbsp;</td>
                                        <td class="style7">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style18">
                                            &nbsp;</td>
                                        <td class="style10">
                                            &nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="style18">
                                            &nbsp;</td>
                                        <td class="style10">
                                            <asp:Button ID="Button1" runat="server" Text="Submit" onclick="Button1_Click" 
                                                Width="118px" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                                CellPadding="4" ForeColor="#333333" 
                                GridLines="None" Width="242px" 
                                >
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                    <asp:TemplateField HeaderText="File">
                                        <ItemTemplate>
                                            <asp:LinkButton
                                             ID="Lk" runat="server" Text='<%# Eval("timetablefile")%>' ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Class">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%#Eval("classname") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Session"></asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            &nbsp;
                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("sessionstart") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#999999" />
                                <EmptyDataTemplate>
                                    No Record Found
                                </EmptyDataTemplate>
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                            </asp:GridView>
                        </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

