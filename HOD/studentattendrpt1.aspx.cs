﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using kitmerp;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class HOD_studentattendrpt1 : System.Web.UI.Page
{
    Int32 id1;
    Label l1, l2, l3;
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            DDLSessionBind();
        }
    }
    private void GetData()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        DDLSem.Items.Clear();
        SqlCommand cmd = new SqlCommand("SELECT   tbcourse.coursesemester, tbcourse.courseid FROM  tbcourse INNER JOIN tbdep ON tbcourse.courseid = tbdep.depcourseid INNER JOIN tbteacher ON tbdep.depid = tbteacher.teacherdepid WHERE (tbcourse.coursedelsts = 0) AND (tbdep.depdelsts = 0) AND (tbteacher.teacherid = @stid)", con);
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        int count = Int32.Parse(dr["coursesemester"].ToString());
        DDLSem.Items.Insert(0, "-- Choose Semester --");
        for (int i = 1; i <= count; i++)
        {
            DDLSem.Items.Add(i.ToString());
        }
        dr.Close();
        cmd.Dispose();
        con.Close();
    }
    private void DDLSessionBind()
    {
        DDlSession.DataTextField = "session";
        DDlSession.DataValueField = "sessionid";
        DDlSession.DataSource = ERP.AllSession();
        DDlSession.DataBind();
        DDlSession.Items.Insert(0, "-- Select session --");
    }
    private void DDLAllClassBind()
    {
        DDLclass.DataTextField = "Class";
        DDLclass.DataValueField = "subjectallotgroupid";
        DDLclass.DataSource = ERP.AllClassHOD(Convert.ToInt32(Session["depid"]), Convert.ToInt32(DDlSession.SelectedValue), Convert.ToInt32(Session["stid"]));
        DDLclass.DataBind();
        DDLclass.Enabled = true;
        //DDLSubject.Enabled = true;
        DDLclass.Items.Insert(0, "-- Select Class --");
    }
    private void GridShowSummaryBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT tbstudent.studentid,tbstudent.studentrollno FROM   tbstudent INNER JOIN tbgroup ON tbstudent.studentgroupid = tbgroup.groupid WHERE   (tbstudent.studentdelsts = 0) AND (tbgroup.groupid = @gid)", con);
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLclass.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShowSummary.DataSource = ds;
        GridShowSummary.DataBind();
        cmd.Dispose();
        con.Close();
        // populate gridview
        //DT = GetData.GetQuestionNameDataList(qid);
        //listResponses.DataSource = DT;
        //listResponses.DataBind(); 
    }
    protected void GridShowSummary_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //GridShowSummary.Rows[0].Cells[1].Visible = false;
        // GridShowSummary.Rows[0].Cells[2].Visible = false;
        //GridShowSummary.Rows[0].Cells[0].ColumnSpan = 3;
        //if (e.Row.DataItem != null)
            //{
            //   string Key = DataBinder.Eval(e.Row.DataItem, "product_id").ToString();
                //DataList dtaProductProperties = ((DataList)e.Row.Cells[0].FindControl("dtaProductBooleanProperties"));
                //getProductProperties.SelectParameters[0].DefaultValue = Key;
                //getProductProperties.DataBind();
            //}
        if (e.Row.RowType == DataControlRowType.Header)
        {     
            DataList dl = (DataList)e.Row.Cells[1].FindControl("subject");

        }
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            DataList dl2 = (DataList)e.Row.Cells[1].FindControl("subjectno"); 
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) AS counta FROM tbstudentattnd WHERE     (studentattndstudentid = @stid) AND (studentattndattend = 'True') AND (studentattndsubjectid = @sid) AND (studentattnddelsts = 0)", con);
            SqlCommand cmd1 = new SqlCommand("SELECT COUNT(*) AS countt FROM tbstudentAttnd WHERE (studentattndstudentid = @stid) AND (studentattndsubjectid = @sid) AND (studentattnddelsts = 0)", con);
            //SqlCommand cmd = new SqlCommand("SELECT COUNT(*) AS counta FROM tbstudentattnd WHERE     (studentattndstudentid = @stid) AND (studentattndattend = 'True') AND (studentattndsubjectid = @sid) AND (studentattnddelsts = 0);SELECT COUNT(*) AS countt FROM tbstudentAttnd WHERE (studentattndstudentid = @stid) AND (studentattndsubjectid = @sid) AND (studentattnddelsts = 0)", con);
            //SqlDataAdapter adp = new SqlDataAdapter(cmd);
            cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSummary.DataKeys[e.Row.RowIndex].Values["studentid"]);
            cmd.Parameters.Add("@sid", SqlDbType.Int).Value = id1;
            cmd1.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(GridShowSummary.DataKeys[e.Row.RowIndex].Values["studentid"]);
            cmd1.Parameters.Add("@sid", SqlDbType.Int).Value = id1;
            l1.Text= cmd.ExecuteScalar().ToString();
            l2.Text = cmd1.ExecuteScalar().ToString();
            if (Convert.ToInt32(l2.Text) != 0)
                l3.Text = (100 * Convert.ToInt32(l1.Text) / Convert.ToInt32(l2.Text)).ToString() + "%";
            else
                l3.Text = "--";
            cmd.Dispose();
            cmd1.Dispose();
            con.Close();
            //Binddl(dl2, (int)GridShowSummary.DataKeys[e.Item.ItemIndex]);
            //DataTable dt = new DataTable("Items");
            // adp.Fill(dt);
            //DataSet ds=new DataSet();
            //adp.Fill(ds);
            //dl2.DataSource =ds.Tables[0];
            // l1.DataBind();
            //dl2.DataSource=ds.Tables[1];
            //l2.DataBind();
            //l1.DataBind();
            //l1.Text = a;
            //l2.Text = b;
            //foreach (GridViewRow row in GridShowSummary.Rows)
            //{
            //    if ((DataList)row.FindControl("dl2") is DataList)
            //    {
            //        ((DataList)row.FindControl("dl2")).DataSource = dt;
            //        ((DataList)row.FindControl("dl2")).DataBind();
            //    }

            //}
        }
    }
    protected void DDlSessionf_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDLAllClassBind();
       
    }
    
    protected void DDLClassf_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDLSem.Enabled = true;
        GetData();
        
    }
    protected void DDLSem_SelectedIndexChanged(object sender, EventArgs e)
    {
        panelfill.Visible = true;
        GridShowSummaryBind();
    }
    protected void subject_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        
        if (e.Item.ItemType == ListItemType.Item)
        {
          Label id= ((Label)e.Item.FindControl("LBLID"));
          id1=Convert.ToInt32(id.Text);
        }
    }
    protected void subjectno_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        //GridView gridResponses = (GridView)e.Item.FindControl("gridResponses");
        //BindGrid(gridResponses, (int)listResponses.DataKeys[e.Item.ItemIndex], DT.Rows[e.Item.ItemIndex][2].ToString());
        if (e.Item.ItemType == ListItemType.Item)
        {
            l1 = (Label)(e.Item.FindControl("LBLA"));
            l2 = (Label)(e.Item.FindControl("LBLT"));
            l3 = (Label)(e.Item.FindControl("LBLP"));
        }
    }
     private void BindGrid(DataList DataList, int questionId)
    {
        // get the answerID and title for the current question.
        //DataTable answersDataTable = new DataTable();
        //answersDataTable = GetData.GetAnswerResponses(questionId);
        //DataTable tempResponses = new DataTable();

        // checkbox question type - loop through each answer and obtain the number of responses.       
       // for (int answer = 0; answer < answersDataTable.Rows.Count; answer++)
       // {
       //     // populate tempaory datatable and replace DT with the response count.
       //     string answerID = answersDataTable.Rows[answer][0].ToString();
       //     tempResponses = GetData.getIndividualQuestionResponses(questionId, answerID);
       //     answersDataTable.Rows[answer][2] = tempResponses.Rows[0][0];
       // }
       // if (GridView.Rows.Count > 6)
       // {
       //     for (int x = 6; x < GridView.Rows.Count; x++)
       //     {
       //         GridView.Rows[x].Visible = false;
       //     }

       //     // I want to populate the label here!!!!!!
       //}

   }
     
}