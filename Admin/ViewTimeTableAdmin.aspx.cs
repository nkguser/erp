﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class Admin_ViewTimeTableAdmin : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (Page.IsPostBack == false)
        {

            DDLDeleteClassSessionBind();
        }
    }

    private void DDLDeleteClassNameBind()
    {
        DDLDeleteClassName.DataTextField = "classname";
        DDLDeleteClassName.DataValueField = "classid";
        DDLDeleteClassName.DataSource = ERP.AllClassAdmin(Convert.ToInt32(DDLDeleteClassSession.SelectedValue));
        DDLDeleteClassName.DataBind();
        DDLDeleteClassName.Enabled = true;
        DDLDeleteClassName.Items.Insert(0, "-- Select Class --");
    }
    private DataSet AllClass(Int32 did, Int32 sid)
    {
        SqlCommand cmd = new SqlCommand("dispclass");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    private DataSet AllDepartment(int cid)
    {
        SqlCommand cmd = new SqlCommand("dispdept");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    private DataSet AllCourse()
    {
        SqlDataAdapter adp = new SqlDataAdapter("dispcourse", con);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    private DataSet AllSession()
    {
        SqlDataAdapter adp = new SqlDataAdapter("dispsession", con);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    private void DDLDeleteClassSessionBind()
    {
        DataSet ds = AllSession();
        DDLDeleteClassSession.DataValueField = "Sessionid";
        DDLDeleteClassSession.DataTextField = "session";
        DDLDeleteClassSession.DataSource = ds;
        DDLDeleteClassSession.DataBind();
        DDLDeleteClassSession.Items.Insert(0, "--Select Session --");
    }
    private void grid_bind()
    {
        SqlCommand cmd = new SqlCommand("SELECT tbtimetable.timetablefile, tbclass.classname, tbsession.sessionstart FROM tbtimetable INNER JOIN tbclass ON tbtimetable.timetableclassid = tbclass.classid INNER JOIN tbsession ON tbclass.classsessionid = tbsession.Sessionid INNER JOIN tbdep ON tbclass.classdepid = tbdep.depid and timetableclassid=@t ", con);
        cmd.Parameters.Add("@t", SqlDbType.Int).Value = Convert.ToInt32(DDLDeleteClassName.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridView1.DataSource = ds;
        GridView1.DataBind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlCommand cmd1 = new SqlCommand("select count(*) as cnt from tbtimetable where timetableclassid=@t", con);
        cmd1.Parameters.Add("@t", SqlDbType.Int).Value = Convert.ToInt32(DDLDeleteClassName.SelectedValue);
        Int32 a = Convert.ToInt32(cmd1.ExecuteScalar());
        if (a == 0)
        {

        }
        else
        {
            grid_bind();
        }
        grid_bind();
    }
    protected void DDLDeleteClassSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDLDeleteClassNameBind();
    }
    
}