﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SA/InnerSA.master" AutoEventWireup="true" CodeFile="Feedbackfromaspx.aspx.cs" Inherits="SA_Feedbackfromaspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
        <td align="center">
        <asp:Label ID="lll" runat="server" Font-Bold="True" Font-Size="X-Large" 
                ForeColor="#003399">Suggestions By ERP User</asp:Label>
        </td>
        </tr>
        <tr>
        <td align="center">
            &nbsp;</td>
        </tr>
        <tr>
            <td >
            <asp:Panel ID="ppp" runat="server" Height="350px" ScrollBars="Auto">
    <asp:GridView ID="GridView1" runat="server" 
        AutoGenerateColumns="False" CellPadding="4" DataSourceID="feedback" 
        ForeColor="#333333" GridLines="None" Width="865px">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="feedbackheader" HeaderText="Subject" 
                SortExpression="feedbackheader" />
            <asp:BoundField DataField="feedbackcontent" HeaderText="Description" 
                SortExpression="feedbackcontent" HtmlEncode="False" 
                HtmlEncodeFormatString="False" />
            <asp:BoundField DataField="feedbackdate" HeaderText="Date Time" 
                SortExpression="feedbackdate" DataFormatString="{0:d}" />
            <asp:BoundField DataField="feedbackfromid" HeaderText="From" 
                SortExpression="feedbackfromid" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>   </asp:Panel>
    <asp:SqlDataSource ID="feedback" runat="server" 
        ConnectionString="<%$ ConnectionStrings:cn %>" 
        SelectCommand="SELECT [feedbackheader], [feedbackcontent], [feedbackdate], [feedbackfromid] FROM [tbfeedback] WHERE ([feedbackdelsts] = @feedbackdelsts)">
        <SelectParameters>
            <asp:Parameter DefaultValue="false" Name="feedbackdelsts" Type="Boolean" />
        </SelectParameters>
    </asp:SqlDataSource>
            </td>
        </tr>
        
    </table>
</asp:Content>

