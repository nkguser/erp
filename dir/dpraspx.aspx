﻿




<%@ Page Title="" Language="C#" MasterPageFile="~/dir/MasterPage.master" AutoEventWireup="true" CodeFile="dpraspx.aspx.cs" Inherits="dir_dpraspx" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
            <table width="100%" style="text-align: left; font-weight: bold;">
             <tr>
              <td style="width: 226px">
                  Session</td>
              <td>
                  <asp:DropDownList ID="DDLSession" runat="server" AutoPostBack="True" 
                      onselectedindexchanged="DDLSession_SelectedIndexChanged" Width="300px">
                  </asp:DropDownList>
              </td>
              <td>
                  &nbsp;</td>
          </tr>
          <tr>
            <td style="width: 226px">
                Class</td>
            <td>
                <asp:DropDownList ID="DDLClass" runat="server" AutoPostBack="True" 
                    Enabled="False" onselectedindexchanged="DDLClass_SelectedIndexChanged" 
                    Width="300px">
                    <asp:ListItem>-- Select Class --</asp:ListItem>
                </asp:DropDownList>
                &nbsp;<asp:LinkButton ID="lbdate" runat="server" Font-Bold="True" 
                    Font-Size="Smaller" ForeColor="#000066" onclick="lbdate_Click" Visible="False">Change Date</asp:LinkButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" rowspan="2">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        
     
        <tr>
            <td style="height: 19px">
                </td>
        </tr>
        </TABLE>
        <asp:Panel ID="p1" runat="server" Visible="false">
        <table width="100%" style="text-align: left; font-weight: bold;">
                <tr>
                    <td style="width: 226px" >
                        Date(Optional)(DD/MM/YYYY)</td>
                    <td>
                        <asp:TextBox ID="TextBox1" runat="server" 
                            Width="300px"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="TextBox1" Display="Dynamic" ErrorMessage="*" 
                            Font-Bold="True" ForeColor="Red" ValidationGroup="abc"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" 
                            ControlToValidate="TextBox1" Display="Dynamic" ErrorMessage="**" 
                            Font-Bold="True" ForeColor="Red" Operator="DataTypeCheck" Type="Date" 
                            ValidationGroup="abc"></asp:CompareValidator>
                        <asp:CalendarExtender Format="dd/MM/yyyy "  ID="TextBox1_CalendarExtender" runat="server" 
                            Enabled="True" TargetControlID="TextBox1">
                        </asp:CalendarExtender>
                        &nbsp;<asp:LinkButton ID="lbenter" runat="server" Font-Bold="True" 
                            Font-Size="Smaller" ForeColor="#000066" onclick="lbenter_Click" 
                            ValidationGroup="abc">Show Data</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr></TABLE></asp:Panel>
                <table width="100%" style="text-align: left; font-weight: bold;">
                <tr>
                    <td>
                        </td>
                    <td>
                        </td>
                    <td>
                        </td>
                </tr>
              <tr>
                          <td colspan='3'>
                              <asp:GridView ID="GridShowSubmit" runat="server" AutoGenerateColumns="False" 
                                  CellPadding="4" ForeColor="#333333" GridLines="None" Visible="False" 
                                  EmptyDataText="No Records Present For Display" Width="100%" 
                                  DataKeyNames="subjectid" onrowdatabound="GridShowSubmit_RowDataBound" 
                                  ShowHeaderWhenEmpty="True">
                                  <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                  <Columns>
                                      <asp:TemplateField HeaderText="Subject">
                                          <ItemTemplate>
                                              <asp:Label ID="Lblsubject" runat="server" Text='<%# Eval("subject") %>'></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Teacher ">
                                       <ItemTemplate>
                                              <asp:Label ID="LblT" runat="server" ></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      
                                      <asp:TemplateField HeaderText="Delievered">
                                          <ItemTemplate>
                                              <asp:Label ID="LBLD" runat="server" ></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Students">
                                       <ItemTemplate>
                                              <asp:Label ID="LBLS" runat="server" ></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Present">
                                      <ItemTemplate>
                                              <asp:Label ID="LBLAP" runat="server"></asp:Label>
                                      </ItemTemplate>

                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="%age">
                                          <ItemTemplate>
                                              <asp:Label ID="LBLP" runat="server"></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                  </Columns>
                                  <EditRowStyle BackColor="#999999" />
                                  <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                  <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                  <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                  <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                  <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                  <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                  <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                  <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                  <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                              </asp:GridView>
                          </td>
                          
                      </tr>
                <tr>
                    <td style="width: 226px">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                </table>
            
              
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

