﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/InnerStudent.master" AutoEventWireup="true" CodeFile="Sessional.aspx.cs" Inherits="Student_Sessional" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
       <table class="style2">
        <tr>
            <td>
                Semester</td>
            <td>
                <asp:DropDownList ID="DDLSem" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLSem_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="GridShow" runat="server" 
                    AutoGenerateColumns="False" onrowdatabound="GridShow_RowDataBound" 
                    EmptyDataText="No Records Found" CellPadding="4" ForeColor="#333333" 
                    GridLines="None">
                
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Sessional">                            
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("studentSessionalno") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Subject">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Subject") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("studentsessionalDate","{0:d}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Attendance">
                            <ItemTemplate>
                                <asp:Label ID="LBLAttend" runat="server" Text='<%# Eval("studentsessionalattnd") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Marks Obtained">                            
                            <ItemTemplate>
                                <asp:Label ID="LBLMarks" runat="server" Text='<%# Eval("studentsessionalMarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Max Marks">
                            <ItemTemplate>
                                <asp:Label ID="LblMMarks" runat="server" Text='<%# Eval("studentsessionalmmarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
            <td>              
            </td>
        </tr>
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

