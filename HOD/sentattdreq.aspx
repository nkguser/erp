﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="sentattdreq.aspx.cs" Inherits="HOD_sentattdreq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    <div  style="overflow:scroll;height:500px;width:100%;visibility:inherit">
<asp:DataList ID="DataList1" runat="server" DataKeyField="attdreqid" 
            CellPadding="4" ForeColor="#333333" 
            ondeletecommand="DataList1_DeleteCommand" Width="100%"
            onitemdatabound="DataList1_ItemDataBound">
            <AlternatingItemStyle BackColor="White" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <ItemStyle BackColor="#9BB4E6" Font-Bold="False" Font-Italic="False" 
                Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                Wrap="True" />
            <ItemTemplate>
            <table>
            <tr>
            <td width="150px">
           <asp:Image ID="Image1" runat="server"  ImageUrl='<%#"~/Studentpic/"+Eval("studentphoto") %>' Height="100" Width="100"/>
            </td>
                

            <td width="150px">
            <b>
            <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>'/></b><br />
                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Studentrollno") %>'/><br />
                <asp:Label ID="Label3" runat="server" Text='<%# Eval("class") %>'/><br />
                <br />                
                </td>
                <td>
                    <asp:Label ID="Label5" runat="server" Text=/>

                <b><asp:Label ID="Label4" runat="server" 
                    Text='<%# Eval("Subject") %>' /></b>
                    <br />
                     <b>Attendance Date:</b>
                <asp:Label ID="attdreqdateLabel" runat="server" 
                    Text='<%# Eval("attdreqdate","{0:d}") %>' /> &nbsp;<b> New Attendance  :</b>
                <asp:Label ID="attdreqattdLabel" runat="server" 
                    Text='<%# Eval("attdreqattd") %>' />
                <br />
                    <b>Request Date :</b>
                <asp:Label ID="attdreqattddateLabel" runat="server" 
                    Text='<%# Eval("attdreqattddate","{0:d}") %>' /><br />                 
             
                
                <b>Response Date:</b>
                <asp:Label ID="attdreqapprovedateLabel" runat="server" 
                    Text='<%# Eval("attdreqapprovedate","{0:d}") %>' />
                <br />
                
                <b>Request Status:</b>
                <asp:Label ID="attdreqreqstsLabel" runat="server" 
                    Text='<%# Eval("attdreqreqsts") %>' />
                </td>
                </tr>
                <tr>
                <td colspan="3">
                <b>Description :</b>
                <asp:Label ID="attdreqdescLabel" runat="server" 
                    Text='<%# Eval("attdreqdesc") %>' /><br />
                    <p align="right">
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Delete" CommandArgument='<%# Eval("attdreqid") %>'>Delete</asp:LinkButton>
                    </p>
            </td>
            </tr>
            </table>   
            </ItemTemplate>
            <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        </asp:DataList></div>
        </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

