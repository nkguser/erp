﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dir/MasterPage.master" AutoEventWireup="true" CodeFile="appattdreq.aspx.cs" Inherits="dir_appattdreq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate><center>
    <asp:Label ID="l1" runat="server" Text="Attendence Request" Font-Bold="True" 
            Font-Size="XX-Large" ForeColor="#000066"></asp:Label></center>
        <br>
        
        <asp:Panel ID="p1" runat="server" Height="390px" ScrollBars="Auto">
            <asp:DataList ID="DataList1" runat="server" CellPadding="4" 
                DataKeyField="attdreqid" ForeColor="#333333" 
                ondeletecommand="DataList1_DeleteCommand" oneditcommand="DataList1_EditCommand" 
                onitemdatabound="DataList1_ItemDataBound" Width="100%">
                <AlternatingItemStyle BackColor="White" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <ItemStyle BackColor="#9BB4E6" Font-Bold="False" Font-Italic="False" 
                    Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                    Wrap="True" />
                <ItemTemplate>
                    <table>
                        <tr>
                            <td>
                            </td>
                            <td colspan="2">
                                <p align="right">
                                    <asp:LinkButton ID="LinkButton1" runat="server" 
                                        CommandArgument='<%# Eval("attdreqid") %>' CommandName="Delete">Reject</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" runat="server" 
                                        CommandArgument='<%# Eval("attdreqid") %>' CommandName="Edit">Approve</asp:LinkButton>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td width="150px">
                                <asp:Image ID="Image1" runat="server" Height="100" 
                                    ImageUrl='<%#"~/Studentpic/"+Eval("studentphoto") %>' Width="100" />
                            </td>
                            <td width="150px">
                                <b>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Name") %>' />
                                </b>
                                <br />
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Studentrollno") %>' />
                                <br />
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("class") %>' />
                                <br />
                                <br />
                            </td>
                            <td>
                                <asp:Label ID="Label5" runat="server" />
                                <b>
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Subject") %>' />
                                </b>
                                <br />
                                <b>Attendance Date:</b>
                                <asp:Label ID="attdreqdateLabel" runat="server" 
                                    Text='<%# Eval("attdreqdate","{0:d}") %>' />
                                &nbsp;<b> New Attendance :</b>
                                <asp:Label ID="attdreqattdLabel" runat="server" 
                                    Text='<%# Eval("attdreqattd") %>' />
                                <br />
                                <b>Request Date :</b>
                                <asp:Label ID="attdreqattddateLabel" runat="server" 
                                    Text='<%# Eval("attdreqattddate","{0:d}") %>' />
                                <br />
                                <b>Response Date:</b>
                                <asp:Label ID="attdreqapprovedateLabel" runat="server" 
                                    Text='<%# Eval("attdreqapprovedate","{0:d}") %>' />
                                <br />
                                <b>Request Status:</b>
                                <asp:Label ID="attdreqreqstsLabel" runat="server" 
                                    Text='<%# Eval("attdreqreqsts") %>' />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b>Description :</b>
                                <asp:Label ID="attdreqdescLabel" runat="server" 
                                    Text='<%# Eval("attdreqdesc") %>' />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td align="right" colspan="2">
                                <b>By : <i><u>
                                <asp:Label ID="Label6" runat="server" Text='<%# Eval("tName") %>' />
                                </u></i></b>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
                <SelectedItemStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            </asp:DataList>
        </asp:Panel>
        </br>
        </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

