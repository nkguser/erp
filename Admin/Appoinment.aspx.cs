﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_Appoinment : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

        if (con.State == ConnectionState.Closed)
            con.Open();
        //if (Page.IsPostBack == false)
        appgridbind();
    }
    private void appgridbind()
    {
        SqlDataAdapter adp = new SqlDataAdapter("select * from tbapp where appclgid=" + Convert.ToInt32(Session["other"]), con);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShowSummary.DataSource = ds;
        GridShowSummary.DataBind();
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        SqlCommand cmd1 = new SqlCommand("select count(*) as cnt from tbapp where appid=@u", con);
        cmd1.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(ViewState["cod"]);
        Int32 a = Convert.ToInt32(cmd1.ExecuteScalar());
        if (a == 0)
        {
            if (btnAdd.Text == "Add Appointment")
            {
                SqlCommand cmd = new SqlCommand("insert into tbapp(apptitle,appdesc,appwith,appwithname,appdate,appaddedon,appduration,appclgid,applvlid) values(@t,@d,@w,@wn,@ad,@aaon,@adur,@aclgid,@lvl)", con);
                if (txtDate.Text == string.Empty || txtname.Text == string.Empty || txtNHB.Text == string.Empty || txtTitle.Text == string.Empty || txtText.Text == string.Empty)
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Give all Inputs !');", true);
                }
                else
                {
                    cmd.Parameters.Add("@t", SqlDbType.VarChar, 100).Value = txtTitle.Text;
                    cmd.Parameters.Add("@d", SqlDbType.VarChar, 500).Value = txtText.Text;
                    cmd.Parameters.Add("@w", SqlDbType.VarChar, 50).Value = DropDownList1.SelectedItem.Text;
                    cmd.Parameters.Add("@wn", SqlDbType.VarChar, 100).Value = txtname.Text;
                    cmd.Parameters.Add("@ad", SqlDbType.DateTime).Value = Convert.ToDateTime(txtDate.Text);
                    cmd.Parameters.Add("@aaon", SqlDbType.Date).Value = DateTime.Now.ToShortDateString();
                    cmd.Parameters.Add("@adur", SqlDbType.Float).Value = txtNHB.Text;
                    if (User.IsInRole("2"))
                    {
                        cmd.Parameters.Add("@alvl", SqlDbType.Int).Value = 2;
                    }
                    else if (User.IsInRole("1"))
                    {
                        cmd.Parameters.Add("@alvl", SqlDbType.Int).Value = 1;
                    }
                    cmd.Parameters.Add("@aclgid", SqlDbType.Int).Value = Convert.ToInt32(Session["other"]);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    con.Close();
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Appoinment added Successfully !');", true);
                    txtText.Text = string.Empty;
                    txtNHB.Text = string.Empty;
                    txtTitle.Text = string.Empty;
                    txtname.Text = string.Empty;
                    txtDate.Text = string.Empty;
                    appgridbind();
                }
            }
        }
        else
        {
            SqlCommand cmd = new SqlCommand("update tbapp set apptitle=@t,appdesc=@d,appwith=@w,appwithname=@wn,appdate=@ad,appaddedon=@aaon,appduration=@adur where appid=@a", con);
            cmd.Parameters.Add("@a", SqlDbType.Int).Value = Convert.ToInt32(ViewState["cod"]);
            cmd.Parameters.Add("@t", SqlDbType.VarChar, 100).Value = txtTitle.Text;
            cmd.Parameters.Add("@d", SqlDbType.VarChar, 500).Value = txtText.Text;
            cmd.Parameters.Add("@w", SqlDbType.VarChar, 50).Value = DropDownList1.SelectedItem.Text;
            cmd.Parameters.Add("@wn", SqlDbType.VarChar, 100).Value = txtname.Text;
            if (txtDate.Text == string.Empty || txtDate.Text == "")
            {
                cmd.Parameters.Add("@ad", SqlDbType.DateTime).Value = Convert.ToDateTime(lbldt.Text);
            }
            else
            {
                cmd.Parameters.Add("@ad", SqlDbType.DateTime).Value = Convert.ToDateTime(txtDate.Text);
            }
            cmd.Parameters.Add("@aaon", SqlDbType.Date).Value = DateTime.Now.ToShortDateString();
            cmd.Parameters.Add("@adur", SqlDbType.Float).Value = txtNHB.Text;
            if (txtname.Text == string.Empty || txtNHB.Text == string.Empty || txtTitle.Text == string.Empty || txtText.Text == string.Empty)
            {
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Give all Inputs !');", true);
            }
            else
            {
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Appoinment Updated Successfully !');", true);
                txtText.Text = string.Empty;
                txtNHB.Text = string.Empty;
                txtTitle.Text = string.Empty;
                txtname.Text = string.Empty;
                txtDate.Text = string.Empty;
                lbldt.Text = string.Empty;
                btnAdd.Text = "Add Appointment";
                appgridbind();
            }
        }
    }
    protected void GridShowSummary_RowEditing(object sender, GridViewEditEventArgs e)
    {
        Int32 a = Convert.ToInt32(GridShowSummary.DataKeys[e.NewEditIndex][0]);
        ViewState["cod"] = a;
        SqlCommand cmd3 = new SqlCommand("select * from tbapp where appid=@u  ", con);
        cmd3.Parameters.Add("@u", SqlDbType.Int).Value = a;
        SqlDataReader dr = cmd3.ExecuteReader();
        if (dr.HasRows)
        {
            dr.Read();
            txtTitle.Text = dr["apptitle"].ToString();
            txtText.Text = dr["appdesc"].ToString();
            txtname.Text = dr["appwithname"].ToString();
            DropDownList1.SelectedIndex = -1;
            DropDownList1.Items.FindByText(dr["appwith"].ToString()).Selected = true;
            //txtDate.Text = Convert.ToDateTime(dr["appdate"]).ToShortDateString();
            lbldt.Text = Convert.ToDateTime(dr["appdate"]).ToString();
            txtNHB.Text = dr["appduration"].ToString();
        }
        dr.Close();
        cmd3.Dispose();
        btnAdd.Text = "Update Appointment ";
        e.Cancel = true;
    }

    //-----------------Coding may be  used in future-----------
    //SqlDataAdapter da = new SqlDataAdapter("sp_SearchAppointments", con);
    //da.SelectCommand.CommandType = CommandType.StoredProcedure;
    //da.SelectCommand.Parameters.AddWithValue("@username", username);
    //da.SelectCommand.Parameters.AddWithValue("@title", "%" +  title + "%");
    //da.SelectCommand.Parameters.AddWithValue("@text", "%"  +  text + "%");
    //DataSet ds = new DataSet();
    //da.Fill(ds, "appointments");
    //return ds.Tables[0];
}