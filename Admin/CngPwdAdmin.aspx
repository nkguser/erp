﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/InnerAdmin.master" AutoEventWireup="true" CodeFile="CngPwdAdmin.aspx.cs" Inherits="Admin_CngPwdAdmin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table class="styledmenu">
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 148px">
                </td>
            <td style="height: 148px">
                    &nbsp;</td>
            <td style="height: 148px">
                    <table class="style56">
                        <tr>
                            <td class="style57">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="4">
                                &nbsp;</td>
                            <td class="style58">
                                Current password :</td>
                            <td class="style59">
                                <asp:TextBox ID="TXTOldPwd" runat="server" Width="181px" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TXTOldPwd" Display="Dynamic" 
                                    ErrorMessage="*" ForeColor="Red" ValidationGroup="abc">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr >
                            <td class="style60" >
                                New Password :</td>
                            <td class="style61">
                                <asp:TextBox ID="TXTNewPwd" runat="server" Width="181px" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                    ControlToValidate="TXTNewPwd" Display="Dynamic" 
                                    ErrorMessage="**" ForeColor="Red" ValidationGroup="abc">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr >
                            <td class="style64"  style="height: 28px">
                                Confirm Password :</td>
                            <td class="style65"  style="height: 28px">
                                <asp:TextBox ID="TXTConfirmPwd" runat="server" Width="181px" 
                                    TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                                    ControlToValidate="TXTConfirmPwd" Display="Dynamic" 
                                    ErrorMessage="**" ForeColor="Red" ValidationGroup="abc">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr >
                            <td class="style64"  style="height: 18px">
                                <asp:LinkButton ID="LBChangePwd" runat="server" ForeColor="#56719A" 
                                    onclick="LBChangePwd_Click" ValidationGroup="abc">Change Password</asp:LinkButton>
                            </td>
                            <td class="style65"  style="height: 18px">
                                <asp:Label ID="LblMessage" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr >
                            <td align="center" class="style57" >
                                &nbsp;</td>
                            <td >
                                <asp:CompareValidator ID="CompareValidator1" runat="server" 
                                    ControlToCompare="TXTNewPwd" ControlToValidate="TXTConfirmPwd" 
                                    ErrorMessage="CompareValidator" ForeColor="Red">Password is Mismatched</asp:CompareValidator>
                            </td>
                        </tr>
                    </table>
                </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

