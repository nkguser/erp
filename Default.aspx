﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 171px;
            height: 60px;
        }
        .style2
        {
            width: 77%;
        }
        .style5
        {
            width: 11px;
        }
        .style7
        {
            width: 58px;
        }
        .style8
        {
            width: 241px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <img alt="" class="style1" src="about-logo.jpg" /><br />
    </div>
    <div >
        <div>
      <table class="style2">
            <tr>
                <td>
                    <hr style="color:Black;width:100%
                    "/>
                </td>
                <td rowspan="3">
                    <asp:Image ID="Image1" runat="server" Width="200px" Height=200px ImageUrl="~/AshishCSE.jpg"
                     />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel1" runat="server" Width="402px">
                        <table class="style2">
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp;&nbsp; Who</td>
                                <td class="style5">
                                    //
                                </td>
                                <td class="style8">
                                    Ashish Vashisht</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp;&nbsp; What</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    Asst. Professor</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp; Where</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    KITM, Kurukshetra-Hr.</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp; When</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    2007 - Present</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Why</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    Project Guide</td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <hr  style="color:Black;width:400px
                    "/>
                </td>
            </tr>
        </table></div>
        <br />
        <div>
      <table class="style2">
            <tr>
                <td>
                    <hr style="color:Black;width:100%
                    "/>
                </td>
                <td rowspan="3">
                    <asp:Image ID="Image2" runat="server" Width="200px" Height=200px ImageUrl="~/rajan cse.JPG"
                     />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel2" runat="server" Width="402px">
                        <table class="style2">
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp;&nbsp; Who</td>
                                <td class="style5">
                                    //
                                </td>
                                <td class="style8">
                                    Rajam Jain</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp;&nbsp; What</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    Asst. Professor</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp; Where</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    KITM, Kurukshetra-Hr.</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp; When</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    2008 - Present</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Why</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    Project Guide</td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <hr  style="color:Black;width:400px
                    "/>
                </td>
            </tr>
        </table></div>
        <br />
        <div>
      <table class="style2">
            <tr>
                <td>
                    <hr style="color:Black;width:100%
                    "/>
                </td>
                <td rowspan="3">
                    <asp:Image ID="Image3" runat="server" Width="200px" Height=200px ImageUrl="~/8161.jpg"
                     />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel3" runat="server" Width="402px">
                        <table class="style2">
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp;&nbsp; Who</td>
                                <td class="style5">
                                    //
                                </td>
                                <td class="style8">
                                    Nitin Kumar</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp;&nbsp; What</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    Web Developer, Front End Designer</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp; Where</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    nitin.garg.8161@gmail.com</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp; When</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    2010 - Present</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Why</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    Passion</td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <hr  style="color:Black;width:400px
                    "/>
                </td>
            </tr>
        </table></div>
        <br />
        <div>
      <table class="style2">
            <tr>
                <td>
                    <hr style="color:Black;width:100%
                    "/>
                </td>
                <td rowspan="3">
                    <asp:Image ID="Image4" runat="server" ImageUrl="~/8171.JPG" Width="200px" Height=200px
                     />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel4" runat="server" Width="402px">
                        <table class="style2">
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp;&nbsp; Who</td>
                                <td class="style5">
                                    //
                                </td>
                                <td class="style8">
                                    Manu Singhal</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp;&nbsp; What</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    Web Developer, Database Designer</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp; Where</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    manusinghal19@gmail.com</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp; When</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    2010 - Present</td>
                            </tr>
                            <tr>
                                <td class="style7">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Why</td>
                                <td class="style5">
                                    //</td>
                                <td class="style8">
                                    Passion</td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <hr  style="color:Black;width:400px
                    "/>
                </td>
            </tr>
        </table></div></div>
    </form>
</body>
</html>
