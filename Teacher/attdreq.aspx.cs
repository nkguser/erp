﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class Teacher_attdreq : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (TextBox2.Text == null || TextBox2.Text == "" || TextBox2.Text == string.Empty)
        {
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Reason Field can't left Blank ... !');", true);
        }
        else
        {
            SqlCommand cmdcheck = new SqlCommand("select studentid from tbstudent where studentrollno=@u", con);
            cmdcheck.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(TextBox1.Text);
            if (con.State == ConnectionState.Closed)
                con.Open();
            Int32 sid = Convert.ToInt32(cmdcheck.ExecuteScalar());
            SqlCommand cmd = new SqlCommand("INSERT INTO tbattdreq (attdreqstid, attdreqsubid, attdreqattddate, attdreqattd, attdreqreqlvlid, attdreqdate, attdreqdesc, attdreqtid) VALUES (@stid,@subid,@attddate,@attd,2,@reqdate,@desc,@tid)", con);
            cmd.Parameters.Add("@stid", SqlDbType.Int).Value = sid;
            cmd.Parameters.Add("@subid", SqlDbType.Int).Value = Convert.ToInt32(DropDownList1.SelectedValue);
            cmd.Parameters.Add("@attddate", SqlDbType.DateTime).Value = Convert.ToDateTime(DropDownList2.SelectedItem.ToString());
            cmd.Parameters.Add("@attd", SqlDbType.Bit).Value = Convert.ToBoolean(RadioButtonList1.SelectedValue);
            cmd.Parameters.Add("@reqdate", SqlDbType.DateTime).Value = System.DateTime.Now;
            //cmd.Parameters.Add("@appdate", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@desc", SqlDbType.VarChar, 50).Value = TextBox2.Text;
            cmd.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
            Response.Redirect("~/teacher/sentattdreq.aspx");
        }

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        //try
        //{

        if (TextBox1.Text == string.Empty || TextBox1.Text == "")
        {
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Enter Roll Number in the given field !');", true);
        }
        else
        {
            SqlCommand cmdcheck = new SqlCommand("select count(*) as cnt from tbstudent where studentrollno=@u", con);
            cmdcheck.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(TextBox1.Text);
            if (con.State == ConnectionState.Closed)
                con.Open();
            Int32 check = Convert.ToInt32(cmdcheck.ExecuteScalar());
            if (check == 0)
            {
                Panel1.Visible = false;
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Please Check Roll Number ... !');", true);
            }
            else
            {
                SqlCommand cmd = new SqlCommand("SELECT tbstudent.studentid, tbsubjectallot.subjectallotsubjectid, tbsubject.subjecttitle + ' - ' + tbsubjectype.subjecttypename AS subject FROM tbsubject INNER JOIN tbsubjectallot ON tbsubject.subjectid = tbsubjectallot.subjectallotsubjectid INNER JOIN tbstudent ON tbsubjectallot.subjectallotgroupid = tbstudent.studentgroupid INNER JOIN tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid WHERE (tbstudent.studentrollno = @roll) AND (tbsubjectallot.subjectallotteacherid = @tid)", con);
                cmd.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
                cmd.Parameters.Add("@roll", SqlDbType.Int).Value = Convert.ToInt32(TextBox1.Text);
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {

                    DropDownList1.DataValueField = "subjectallotsubjectid";
                    DropDownList1.DataTextField = "subject";
                    DropDownList1.DataSource = dr;
                    DropDownList1.DataBind();
                    DropDownList1.Items.Insert(0, "-- Select Subject --");
                    DropDownList1.Items[0].Attributes.Add("disabled", "disabled");
                    Panel1.Visible = true;
                }
                else
                {
                    Panel1.Visible = false;
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('No Subjects Alloted to this student ... !');", true);
                }
                cmd.Dispose();
                dr.Close();
                con.Close();
               

                //Response.Write("<script language='javascript'>alert('Roll Number Available');</script>");
                //

                /*
                 * selection of subject
                 * SELECT tbstudent.studentid, tbsubjectallot.subjectallotsubjectid, tbsubject.subjecttitle + ' - ' + tbsubjectype.subjecttypename AS subject
    FROM            tbsubject INNER JOIN
                             tbsubjectallot ON tbsubject.subjectid = tbsubjectallot.subjectallotsubjectid INNER JOIN
                             tbstudent ON tbsubjectallot.subjectallotgroupid = tbstudent.studentgroupid INNER JOIN
                             tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid
    WHERE        (tbstudent.studentrollno = @roll) AND (tbsubjectallot.subjectallotteacherid = @tid)
                 */

                // else
                {
                    //label me msg - check roll no.

                }
            }
        }
        //}
        //catch (Exception exp)
        //{
        //    Response.Write("<script language='javascript'>alert('Enter numeric number only!');</script>");
        //}

        //int roll = Convert.ToInt32(TextBox1.Text);
        //sql   query  here       


    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        SqlCommand cmdcheck = new SqlCommand("select studentid from tbstudent where studentrollno=@u", con);
        cmdcheck.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(TextBox1.Text);
        if (con.State == ConnectionState.Closed)
            con.Open();
        Int32 sid = Convert.ToInt32(cmdcheck.ExecuteScalar());
        SqlCommand cmd = new SqlCommand("SELECT  studentattnddate,studentattndattend FROM   tbstudentattnd WHERE        (studentattndsubjectid = @sub) AND (studentattndapprovlvid >= 2) AND (studentattndstudentid = @sid)", con);
        cmd.Parameters.Add("@sub", SqlDbType.Int).Value = Convert.ToInt32(DropDownList1.SelectedValue);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlDataReader dr = cmd.ExecuteReader();
        if (dr.HasRows)
        {
            DropDownList2.DataValueField = "studentattnddate";
            DropDownList2.DataTextField = "studentattnddate";
            DropDownList2.DataSource = dr;
            DropDownList2.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('No Dates Found...Either you have not filled attendance for this student or it has not been submitted to HOD/DIRECTOR !');", true);
        }
        cmd.Dispose();
        dr.Close();
        con.Close();

        /*
         * query for dates
         * 
         * SELECT        studentattnddate,studentattndattend
FROM            tbstudentattnd
WHERE        (studentattndsubjectid = @sub) AND (studentattndapprovlvid >= 2) AND (studentattndstudentid = @sid)
         * 
         * show attd in label
         */

    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        if (TextBox1.Text == string.Empty || TextBox1.Text == "")
        {
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Enter Roll Number in the given field !');", true);
        }
        else
        {
            SqlCommand cmdcheck = new SqlCommand("select count(*) as cnt from tbstudent where studentrollno=@u", con);
            cmdcheck.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(TextBox1.Text);
            if (con.State == ConnectionState.Closed)
                con.Open();
            Int32 check = Convert.ToInt32(cmdcheck.ExecuteScalar());
            if (check == 0)
            {
                ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('Please Check Roll Number ... !');", true);
            }
            else
            {
                SqlCommand cmd = new SqlCommand("SELECT tbstudent.studentid, tbsubjectallot.subjectallotsubjectid, tbsubject.subjecttitle + ' - ' + tbsubjectype.subjecttypename AS subject FROM tbsubject INNER JOIN tbsubjectallot ON tbsubject.subjectid = tbsubjectallot.subjectallotsubjectid INNER JOIN tbstudent ON tbsubjectallot.subjectallotgroupid = tbstudent.studentgroupid INNER JOIN tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid WHERE (tbstudent.studentrollno = @roll) AND (tbsubjectallot.subjectallotteacherid = @tid)", con);
                cmd.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
                cmd.Parameters.Add("@roll", SqlDbType.Int).Value = Convert.ToInt32(TextBox1.Text);
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlDataReader dr = cmd.ExecuteReader();

                if (dr.HasRows)
                {

                    DropDownList1.DataValueField = "subjectallotsubjectid";
                    DropDownList1.DataTextField = "subject";
                    DropDownList1.DataSource = dr;
                    DropDownList1.DataBind();
                    DropDownList1.Items.Insert(0, "-- Select Subject --");
                    DropDownList1.Items[0].Attributes.Add("disabled", "disabled");
                    Panel1.Visible = true;
                }
                else
                {
                    Panel1.Visible = false;
                    ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:alert('No Subjects Alloted to this student ...!');", true);
                }
                cmd.Dispose();
                dr.Close();
                con.Close();


                //Response.Write("<script language='javascript'>alert('Roll Number Available');</script>");
                //

                /*
                 * selection of subject
                 * SELECT tbstudent.studentid, tbsubjectallot.subjectallotsubjectid, tbsubject.subjecttitle + ' - ' + tbsubjectype.subjecttypename AS subject
    FROM            tbsubject INNER JOIN
                             tbsubjectallot ON tbsubject.subjectid = tbsubjectallot.subjectallotsubjectid INNER JOIN
                             tbstudent ON tbsubjectallot.subjectallotgroupid = tbstudent.studentgroupid INNER JOIN
                             tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid
    WHERE        (tbstudent.studentrollno = @roll) AND (tbsubjectallot.subjectallotteacherid = @tid)
                 */

                // else
                {
                    //label me msg - check roll no.

                }
            }
        }
        //}
        //catch (Exception exp)
        //{
        //    Response.Write("<script language='javascript'>alert('Enter numeric number only!');</script>");
        //}

       // int roll = Convert.ToInt32(TextBox1.Text);
        //sql   query  here



    }
}