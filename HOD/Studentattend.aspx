﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="Studentattend.aspx.cs" Inherits="HOD_Studentattend" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      <table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td>
                Choose Option</td>
            <td align="left">
                <asp:RadioButtonList ID="RBLOpt" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="RBLOpt_SelectedIndexChanged" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">Fill</asp:ListItem>
                    <asp:ListItem Value="1">View</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
        <td colspan="3">
            <asp:Panel ID="PanelFill" runat="server" Visible="False">
            <table width="100%">
         <%--    <tr>
            <td>
                Course</td>
            <td>
                <asp:DropDownList ID="DDLCoursef" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLCoursef_SelectedIndexChanged" >
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Department</td>
            <td>
                <asp:DropDownList ID="DDLDeptf" runat="server">
                    <asp:ListItem>-- Select Department --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>--%>
        <tr>
            <td>
                Session</td>
            <td>
                <asp:DropDownList ID="DDlSessionf" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDlSessionf_SelectedIndexChanged" Width="200px">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Class</td>
            <td>
                <asp:DropDownList ID="DDLclassf" runat="server" AutoPostBack="True" 
                    Enabled="False" onselectedindexchanged="DDLClassf_SelectedIndexChanged" 
                    Width="200px">
                    <asp:ListItem>-- Select Class --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                Subject</td>
            <td>
                <asp:DropDownList ID="DDLSubjectf" runat="server" Enabled="False" 
                    onselectedindexchanged="DDLSubjectf_SelectedIndexChanged" 
                    AutoPostBack="True" Width="200px">
                    <asp:ListItem>-- Choose Subject --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
                <tr>
                    <td>
                        Date(DD/MM/YY)</td>
                    <td>
                        <asp:TextBox ID="TxtDate" runat="server" Width="200px"></asp:TextBox>
                        <asp:CalendarExtender Format="dd/MM/yyyy "  ID="TxtDate_CalendarExtender" runat="server" 
                            Enabled="True" TargetControlID="TxtDate" Animated="False" 
                            DaysModeTitleFormat="d-MMMM-yyyy" PopupPosition="Right" 
                            TodaysDateFormat="d-MMMM-yyyy">
                        </asp:CalendarExtender>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                            ControlToValidate="TxtDate" ErrorMessage="RequiredFieldValidator" 
                            Font-Bold="True" ForeColor="Red" ValidationGroup="abc" Display="Dynamic">*</asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator4" runat="server" 
                            ControlToValidate="TxtDate" ErrorMessage="CompareValidator" Font-Bold="True" 
                            ForeColor="Red" Operator="DataTypeCheck" Type="Date" ValidationGroup="abc" 
                            Display="Dynamic">*</asp:CompareValidator>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        Total No. Of&nbsp; Periods Delivered</td>
                    <td>
                        <asp:Label ID="LBLTotalDeliveredf" 
                            runat="server"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr></table>
                <asp:Panel ID="Fillattendance" runat="server" Visible="False">
                <table width="100%" style="text-align: left; font-weight: bold;">
                <tr>
                    <td align="center" colspan="2">
           <div  style="overflow:scroll;height:300px;visibility:inherit">
                        <asp:GridView ID="GridFill" runat="server" AutoGenerateColumns="False" 
                            CellPadding="4" DataKeyNames="studentid" ForeColor="#333333" GridLines="None" 
                            onrowdatabound="GridFill_RowDataBound" Width=800px>
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            <Columns>
                                <asp:TemplateField HeaderText="Roll No">
                                    <ItemTemplate>
                                        <asp:Label ID="LblRollNo" runat="server" Text='<%# Eval("studentRollno") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Attendance">
                                    <ItemTemplate>
                                        <asp:RadioButtonList ID="RBLAttendance" runat="server" 
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" Value="True">Present</asp:ListItem>
                                            <asp:ListItem Value="false">Absent</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Attended">
                                    <ItemTemplate>
                                        <asp:Label ID="LblAt" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="%age">
                                    <ItemTemplate>
                                        <asp:Label ID="Lblpercent" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                        </asp:GridView></div
                    
                    
                    </td>
               </tr>
                <tr>
                    <td>
                        <asp:LinkButton ID="LinkButton2" runat="server" onclick="LinkButton2_Click" 
                            ValidationGroup="abc">Save</asp:LinkButton>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="LBLmsgf" runat="server" Font-Bold="True" ForeColor="#006600"></asp:Label>
                    </td>
                </tr>
      </table>
      </asp:Panel>
      
            </asp:Panel>
            </td>
        </tr>
        <tr>
        <td colspan="3">
            <asp:Panel ID="PanelView" runat="server" Visible="False">
            <table width="100%">
             <tr>
              <td>
                  Session</td>
              <td>
                  <asp:DropDownList ID="DDLSession" runat="server" AutoPostBack="True" 
                      onselectedindexchanged="DDLSession_SelectedIndexChanged" Width="200px">
                  </asp:DropDownList>
              </td>
              <td>
                  &nbsp;</td>
          </tr>
          <tr>
            <td>
                Class</td>
            <td>
                <asp:DropDownList ID="DDLClass" runat="server" AutoPostBack="True" 
                    Enabled="False" Width="200px" 
                    onselectedindexchanged="DDLClass_SelectedIndexChanged">
                    <asp:ListItem>-- Select Class --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <%--<tr>
            <td>
                Semester</td>
            <td>
                <asp:DropDownList ID="DDLSem" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="DDLSem_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>--%>
        
     <%--     <tr>
              <td>
                  Group</td>
              <td>
                  <asp:DropDownList ID="DDLGrp" runat="server" 
                     style="height: 22px" onselectedindexchanged="DDLGrp_SelectedIndexChanged">
                      <asp:ListItem>-- Select Group --</asp:ListItem>
                  </asp:DropDownList>
              </td>
              <td>
                  &nbsp;</td>
          </tr>--%>
        <tr>
            <td>
                Subject</td>
            <td>
                <asp:DropDownList ID="DDLSubject" runat="server" 
                    AutoPostBack="True" 
                    onselectedindexchanged="DDLSubject_SelectedIndexChanged" Width="200px" Enabled="False" 
                    >
                    <asp:ListItem>-- Choose Subject --</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
                <tr>
                    <td>
                        Total No. Of&nbsp; Periods Delivered</td>
                    <td style="margin-left: 40px">
                        <asp:Label ID="LBLTotalDelivered" runat="server"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                </table>
                 <asp:Panel ID="datepanel" runat="server" Visible="False" >
                <table width="100%" style="text-align: left; font-weight: bold;">
        <tr>
            <td>
                </td>
            <td align="left">
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" 
                    onselectedindexchanged="RadioButtonList1_SelectedIndexChanged">
                    <asp:ListItem Value="0">Not Approved</asp:ListItem>
                    <asp:ListItem Value="1">Approved</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
        
       
                <tr>
                
                    <td style="width: 423px;" align="left">
                        Date </td>
                    <td align="left">
                        
                        <asp:DropDownList ID="DDLDate" runat="server" AutoPostBack="True" 
                            DataTextFormatString="{0:D}" 
                            onselectedindexchanged="DDLDate_SelectedIndexChanged" Width="200px">
                            <asp:ListItem>-- Choose Date --</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                   
                </tr> 
                </table>
                </asp:Panel>
                <asp:Panel ID="Panel1" runat="server" Visible="False">
                <table width="100%" style="text-align: left; font-weight: bold;">
                <tr>
                    <td>
                       </td>
                    
                </tr>
                                      <tr>
                                          <td align="center">
            <div  style="overflow:scroll;height:300px;visibility:inherit">
                                              <asp:GridView ID="GridShow" runat="server" AutoGenerateColumns="False" 
                                                  CellPadding="4" DataKeyNames="studentid" ForeColor="#333333" GridLines="None" 
                                                  onrowcancelingedit="GridShow_RowCancelingEdit" 
                                                  onrowdatabound="GridShow_RowDataBound" onrowediting="GridShow_RowEditing" 
                                                  onrowupdating="GridShow_RowUpdating" Width="800px">
                                                  <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                                  <Columns>
                                                      <asp:TemplateField HeaderText="Roll No">
                                                          <ItemTemplate>
                                                              <asp:Label ID="LblRollNo" runat="server" Text='<%# Eval("studentRollno") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="Attendance">
                                                          <EditItemTemplate>
                                                              <asp:RadioButtonList ID="RBLAttendance" runat="server" 
                                                                  RepeatDirection="Horizontal">
                                                                  <asp:ListItem Selected="True" Value="True">Present</asp:ListItem>
                                                                  <asp:ListItem Value="false">Absent</asp:ListItem>
                                                              </asp:RadioButtonList>
                                                          </EditItemTemplate>
                                                          <ItemTemplate>
                                                              <asp:Label ID="LBLAttend" runat="server" 
                                                                  Text='<%# Eval("studentattndattend") %>'></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="Total Attended">
                                                          <ItemTemplate>
                                                              <asp:Label ID="LblAt" runat="server"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="%age">
                                                          <ItemTemplate>
                                                              <asp:Label ID="Lblpercent" runat="server"></asp:Label>
                                                          </ItemTemplate>
                                                      </asp:TemplateField>
                                                      <asp:TemplateField>
                                                          <EditItemTemplate>
                                                              <asp:LinkButton ID="LBUpdate" runat="server" CommandName="Update" Text="Update"></asp:LinkButton>
                                                              &nbsp;&nbsp;&nbsp;
                                                              <asp:LinkButton ID="LBCancel" runat="server" CommandName="Cancel" Text="Cancel"></asp:LinkButton>
                                                          </EditItemTemplate>
                                                          <ItemTemplate>
                                                              <asp:LinkButton ID="LBEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                                          </ItemTemplate>
                                                      </asp:TemplateField>
                                                  </Columns>
                                                  <EditRowStyle BackColor="#999999" />
                                                  <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                  <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                                  <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                                  <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                                  <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                  <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                                  <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                                  <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                                  <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                                              </asp:GridView>
                                              </div>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              Approve<strong> upto&nbsp; :</strong> &nbsp;<asp:DropDownList ID="DDLdatesubmithod" 
                                                 DataTextFormatString="{0:D}"  runat="server">
                                              </asp:DropDownList>

                                              <asp:LinkButton ID="LnkBtmSubmitHod" runat="server" 
                                                  onclick="LnkBtmSubmitHod_Click">Approve</asp:LinkButton>
                                          </td>
                                      </tr>
                                  </table>
                              </asp:Panel>
                              <br />
                              <br />
                              <asp:Panel ID="approvedpanel" runat="server" Visible="False">
                         <table width="100%" style="text-align: left; font-weight: bold;">
                         <tr>
                          <td align="center">
                                <div  style="overflow:scroll;height:300px;visibility:inherit">
                              <asp:GridView ID="GridShowSubmit" runat="server" AutoGenerateColumns="False" 
                                  CellPadding="4" DataKeyNames="studentid" ForeColor="#333333" GridLines="None" 
                                  onrowdatabound="GridShowSubmit_RowDataBound" Visible="False" Width="800px">
                                  <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                  <Columns>
                                      <asp:TemplateField HeaderText="Roll No">
                                          <ItemTemplate>
                                              <asp:Label ID="LblRollNo2" runat="server" Text='<%# Eval("studentRollno") %>'></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Attendance">
                                          <ItemTemplate>
                                              <asp:Label ID="LBLAttend2" runat="server" 
                                                  Text='<%# Eval("studentattndattend") %>'></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Total Attended">
                                          <ItemTemplate>
                                              <asp:Label ID="LblAt2" runat="server"></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                      <asp:TemplateField HeaderText="%age">
                                          <ItemTemplate>
                                              <asp:Label ID="Lblpercent2" runat="server"></asp:Label>
                                          </ItemTemplate>
                                      </asp:TemplateField>
                                  </Columns>
                                  <EditRowStyle BackColor="#999999" />
                                  <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                  <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                  <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                  <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                  <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                  <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                  <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                  <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                  <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                              </asp:GridView>
                              </div>
                              
                          </td>
                      </tr>
                <tr>
                    <td>
                        </td>
                    
                </tr>
                </table>
                </asp:Panel>
            </asp:Panel>
        </td>
        </tr>
</table>        
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

