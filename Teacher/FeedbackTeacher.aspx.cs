﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;

public partial class Teacher_FeedbackTeacher : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SqlCommand cmd = new SqlCommand("insert into tbfeedback(feedbackheader,feedbackcontent,feedbackdate,feedbacklevelid,feeedbacktolevelid,feedbackfromid,feedbackdelsts) values(@fh,@fc,@fd,Null,Null,@ffid,0) ", con);
        cmd.Parameters.Add("@fh", SqlDbType.VarChar, 100).Value = TextBox1.Text;
        cmd.Parameters.Add("@fc", SqlDbType.NText).Value = Editor1.Content;
        cmd.Parameters.Add("@fd", SqlDbType.DateTime).Value = System.DateTime.Now;
        cmd.Parameters.Add("ffid", SqlDbType.VarChar, 50).Value = User.Identity.Name;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        lblfeedback.Text = "Your Messsage " + TextBox1.Text + "has been Successfully Sent";
        TextBox1.Text = String.Empty;
        Editor1.Content = String.Empty;
    }
}