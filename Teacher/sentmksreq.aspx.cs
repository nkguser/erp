﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class Teacher_sentmksreq : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {        
        datalbind();
        /*
         * SELECT     tbattdreq.attdreqid, tbattdreq.attdreqstid, tbattdreq.attdreqattddate, tbattdreq.attdreqattd, tbattdreq.attdreqdate, tbattdreq.attdreqapprovedate, tbattdreq.attdreqdesc, 
                      tbattdreq.attdreqreqsts, tbstudent.studentrollno, tbclass.classname + ' - ' + tbgroup.groupname AS class, 
                      tbstudent.studentfname + ' ' + tbstudent.studentlname AS name, tbsubject.subjecttitle + ' - ' + tbsubjectype.subjecttypename AS subject
FROM         tbgroup INNER JOIN
                      tbattdreq INNER JOIN
                      tbstudent ON tbattdreq.attdreqstid = tbstudent.studentid ON tbgroup.groupid = tbstudent.studentgroupid INNER JOIN
                      tbclass ON tbgroup.groupclassid = tbclass.classid INNER JOIN
                      tbsubjectype INNER JOIN
                      tbsubject ON tbsubjectype.subjecttypeid = tbsubject.subjecttypeid ON tbattdreq.attdreqsubid = tbsubject.subjectid
WHERE     (tbattdreq.attdreqreqlvlid >= 2)
ORDER BY tbattdreq.attdreqdate
         * 
         */
        //query to fetch sname, spic, sroll, subject, date , desc, attd, order by reqdate and sts=null
        //where stid, lvlid=2 or 3,  for hod - 2 and 3,   for dir - 3
        // datatlist
    }
    private void datalbind()
    {

        /*
         * SELECT     tbmarksreq.marksreqid, tbmarksreq.marksreqstid, tbmarksreq.marksreqsubid, tbmarksreq.marksreqsesno, tbmarksreq.marksreqsesdate, tbmarksreq.marksreqoldattd,
                       tbmarksreq.marksreqnewattd, tbmarksreq.marksreqoldmaRKS, tbmarksreq.marksreqnewmarks, tbmarksreq.marksreqdesc, tbmarksreq.marksreqreqsts, 
                      tbmarksreq.marksreqapprovedate, tbmarksreq.marksreqtid, tbstudent.studentfname + ' ' + tbstudent.studentlname AS name, 
                      tbclass.classname + ' - ' + tbgroup.groupname AS class, tbstudent.studentrollno, tbsubject.subjecttitle + ' - ' + tbsubjectype.subjecttypename AS subject
FROM         tbsubjectype INNER JOIN
                      tbsubject ON tbsubjectype.subjecttypeid = tbsubject.subjecttypeid INNER JOIN
                      tbmarksreq ON tbsubject.subjectid = tbmarksreq.marksreqsubid INNER JOIN
                      tbgroup INNER JOIN
                      tbstudent ON tbgroup.groupid = tbstudent.studentgroupid INNER JOIN
                      tbclass ON tbgroup.groupclassid = tbclass.classid ON tbmarksreq.marksreqstid = tbstudent.studentid
WHERE     (tbmarksreq.marksreqstid = @tid) AND (tbmarksreq.marksreqreqlvlid >= 2)
         * 
         */
        SqlCommand cmd = new SqlCommand("SELECT     tbmarksreq.marksreqid, tbmarksreq.marksreqstid, tbmarksreq.marksreqsubid, tbmarksreq.marksreqsesno, tbmarksreq.marksreqsesdate, tbmarksreq.marksreqoldattd, tbmarksreq.marksreqnewattd, tbmarksreq.marksreqoldmaRKS, tbmarksreq.marksreqnewmarks, tbmarksreq.marksreqdesc, tbmarksreq.marksreqreqsts, tbmarksreq.marksreqapprovedate, tbmarksreq.marksreqtid, tbstudent.studentfname + ' ' + tbstudent.studentlname AS name, tbclass.classname + ' - ' + tbgroup.groupname AS class, tbstudent.studentrollno,tbstudent.studentphoto, tbsubject.subjecttitle + ' - ' + tbsubjectype.subjecttypename AS subject  FROM         tbsubjectype INNER JOIN                      tbsubject ON tbsubjectype.subjecttypeid = tbsubject.subjecttypeid INNER JOIN                      tbmarksreq ON tbsubject.subjectid = tbmarksreq.marksreqsubid INNER JOIN                      tbgroup INNER JOIN                      tbstudent ON tbgroup.groupid = tbstudent.studentgroupid INNER JOIN                      tbclass ON tbgroup.groupclassid = tbclass.classid ON tbmarksreq.marksreqstid = tbstudent.studentid WHERE     (tbmarksreq.marksreqtid = @tid) AND (tbmarksreq.marksreqreqlvlid >= 2)", con);
        cmd.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        DataList1.DataSource = ds;
        DataList1.DataBind();
        if (ds.Tables[0].Rows.Count == 0)
        {
            Label4.Text = "No Requests have been made till now ...";
            Label4.Visible = true;
        }

    }

    protected void DataList1_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("DELETE FROM tbmarksreq WHERE (marksreqid = @rid)", con);
        cmd.Parameters.Add("@rid", SqlDbType.Int).Value = Convert.ToInt32(e.CommandArgument);
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        datalbind();
    }
    protected void DataList1_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        Label l1 = (Label)(e.Item.FindControl("marksreqnewattdLabel"));
        if (l1.Text == "False")
            l1.Text = "Absent";
        else
            l1.Text = "Present";
        Label l4 = (Label)(e.Item.FindControl("marksreqoldattdLabel"));
        if (l4.Text == "False")
            l4.Text = "Absent";
        else
            l4.Text = "Present";
        Label l2 = (Label)(e.Item.FindControl("marksreqreqstsLabel"));
        if (l2.Text == "False")
            l2.Text = "Rejected";
        else if (l2.Text == "True")
            l2.Text = "Approved";
        else
            l2.Text = "Waiting ...";
        Label l3 = (Label)(e.Item.FindControl("marksreqapprovedateLabel"));
        if (l3.Text == "")
            l3.Text = "---";
    }
}