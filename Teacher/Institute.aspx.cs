﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Teacher_Institute : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            hidepanel();
            Panel1.Visible = true;
        }
    }
    private void hidepanel()
    {
        Panel1.Visible = false;
        Panel2.Visible = false;
        Panel3.Visible = false;
        Panel4.Visible = false;
        Panel5.Visible = false;
        Panel6.Visible = false;
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        hidepanel();
        Panel1.Visible = true;
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        hidepanel();
        Panel2.Visible = true;
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        hidepanel();
        Panel3.Visible = true;
    }
    protected void LinkButton6_Click(object sender, EventArgs e)
    {
        hidepanel();
        Panel4.Visible = true;
    }
    protected void LinkButton7_Click(object sender, EventArgs e)
    {
        hidepanel();
        Panel3.Visible = true;
    }
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        hidepanel();
        Panel5.Visible = true;
    }
    protected void LinkButton8_Click(object sender, EventArgs e)
    {
        hidepanel();
        Panel6.Visible = true;
    }
   
}