﻿<%@ Page Title="" Language="C#" MasterPageFile="~/HOD/InnerHOD.master" AutoEventWireup="true" CodeFile="Studentsessionalrpt.aspx.cs" Inherits="HOD_Studentsessionalrpt" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="text-align: left; font-weight: bold;" width="100%">
                <tr>
                    <td>
                        Session</td>
                    <td>
                        <asp:DropDownList ID="DDLSession" runat="server" AutoPostBack="True" 
                            onselectedindexchanged="DDLSession_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        Semester</td>
                    <td>
                        <asp:DropDownList ID="DDLSem" runat="server" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        Class</td>
                    <td>
                        <asp:DropDownList ID="DDLClass" runat="server" AutoPostBack="True" 
                            Enabled="False" onselectedindexchanged="DDLClass_SelectedIndexChanged">
                            <asp:ListItem>-- Select Class --</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        Group</td>
                    <td>
                        <asp:DropDownList ID="DDLGrp" runat="server" style="height: 22px">
                            <asp:ListItem>-- Select Group --</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%">
                        </rsweb:ReportViewer>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>

