﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using kitmerp;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Student_Sessional : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
            GetData();
    }
    private void GetData()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        DDLSem.Items.Clear();
        SqlCommand cmd = new SqlCommand("SELECT     tbcourse.coursesemester FROM         tbcourse INNER JOIN                      tbdep ON tbcourse.courseid = tbdep.depcourseid INNER JOIN                      tbclass ON tbdep.depid = tbclass.classdepid INNER JOIN                      tbgroup ON tbclass.classid = tbgroup.groupclassid INNER JOIN                      tbstudent ON tbgroup.groupid = tbstudent.studentgroupid WHERE     (tbcourse.coursedelsts = 0) AND (tbdep.depdelsts = 0) AND (tbgroup.groupdelsts = 0) AND (tbclass.classdelsts = 0) AND (tbstudent.studentdelsts = 0) AND                       (tbstudent.studentid = @stid)", con);
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        int count = Int32.Parse(dr["coursesemester"].ToString());
        DDLSem.Items.Insert(0, "-- Choose Semester --");
        for (int i = 1; i <= count; i++)
        {
            DDLSem.Items.Add(i.ToString());
        }
        dr.Close();
        cmd.Dispose();
        con.Close();
    }
    private void GridShowBind()
    {
        SqlCommand cmd = new SqlCommand("DispSessionalStdnt", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDLSem.SelectedValue);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridShow.DataSource = ds;
        GridShow.DataBind();
        cmd.Dispose();
    }
    protected void GridShow_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && (e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate))
        {
            Label l = (Label)(e.Row.FindControl("LBLAttend"));
            if (l.Text.Equals("True"))
                l.Text = "Present";
            else
                l.Text = "Absent";
        }
    }
    protected void DDLSem_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLSem.SelectedIndex == 0)
        {
            DDLSem.SelectedIndex = 0;
            GridShow.Visible = false;
        }
        else
        {
            GridShowBind();
            if (GridShow.Rows.Count == 0)
            {
                return;
            }
            else
                GridShow.Visible = true;
        }
    }
}