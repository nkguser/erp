﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Student/InnerStudent.master" AutoEventWireup="true" CodeFile="Icard.aspx.cs" Inherits="Student_Icard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:FormView ID="FormView1" runat="server" Width="80%" 
        style="text-align: left">
        <HeaderTemplate ><center>
            <span style="color: #FF0000; font-size: x-large"><strong>KURUKSHETRA INSTITUTE 
            OF TECHNOLOGY &amp; MANAGEMENT</strong></span>
            <br /><span style="color: #333399">Kurukshetra -Pehowa Road, Bhor Saidan<br /> 
            KURUKSHETRA - 136 119, Haryana<br /> Ph.:01741-283841-42, Fax.: -1741-283843<br /> 
            E.mail: </span><a href="mailto:kitm@sify.com" style="color: #333399">
            kitm@sify.com</a><span style="color: #333399">,&nbsp;&nbsp;&nbsp; Web: www.kitm.in</span>
            </center>
            <hr />
        </HeaderTemplate>


        <ItemTemplate>
            <table class="styledmenu">
                <tr>
                    <td>
                        <strong>Name:</strong></td>
                    <td>
                        <asp:Label ID="LBLName" runat="server" Font-Bold="True" Text='<%#Eval("name") %>'></asp:Label>
                    </td>
                    <td rowspan="4">
                        <asp:Image ID="Image2" runat="server" Height=100px Width=100px  ImageUrl='<%#"~/Studentpic/"+Eval("Studentphoto")%>'/>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        Class:</td>
                    <td>
                        <asp:Label ID="LBLClass" runat="server" Font-Bold="True"><%#Eval("class") %></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        Roll No:</td>
                    <td>
                        <asp:Label ID="LBLRollNo" runat="server" Font-Bold="True" Text='<%#Eval("studentrollno") %>'></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                               
                <tr>
                    <td style="font-weight: 700">
                        ADDRESS:</td>
                    <td style="font-weight: 700">
                        <asp:Literal ID="Litaddress" runat="server" Text='<%#Eval("address") %>'></asp:Literal>                        
                    </td>
                    <td rowspan="4">
                        <asp:Image ID="Image1" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        DATE OF BIRTH:</td>
                    <td>
                        <asp:Label ID="LBLDob" runat="server" Font-Bold="True"><%#Eval("studentdob","{0:d}") %></asp:Label>
                    </td>
                </tr>
                
                <tr>
                    <td style="font-weight: 700">
                        VALID UPTO:</td>
                    <td style="font-weight: 700">
                        <asp:Label ID="LBLValid" runat="server" Font-Bold="True">July - <%#Eval("sessionend") %></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        PHONE.:</td>
                    <td>
                        <asp:Label ID="LBLphone" runat="server" Font-Bold="True"><%#Eval("studentphone1") %></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        INSTRUCTIONS:</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        1. Print of online card is applicable only in the case of non-issuement/lost of 
                        original indentity card.</td>
                </tr>
                <tr>
                    <td colspan="3">
                        2.This card must be produced on demand.</td>
                </tr>
                <tr>
                    <td colspan="3">
                        3.This card is not transerrable and if misused holder will be responsible.</td>
                </tr>
                <tr>
                    <td colspan="3">
                        4. this card is valid after being attested by Institute Office only.</td>
                </tr>
                <tr>
                    <td colspan="3">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="font-weight: 700">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </ItemTemplate>


    </asp:FormView>
</asp:Content>

