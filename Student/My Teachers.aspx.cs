﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class Student_My_Teachers : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        if (Page.IsPostBack == false)
            dlbind();
      
    }
    private void dlbind()
    {

        SqlCommand cmd1=new SqlCommand("select studentgroupid from tbstudent where studentid="+Convert.ToInt32(Session["stid"]),con);
        SqlDataReader dr=cmd1.ExecuteReader();
        if(dr.HasRows)
        {
            dr.Read();
            int gid=Convert.ToInt32(dr[0]);
            dr.Close();
            SqlCommand cmd = new SqlCommand("myteachers", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@u", SqlDbType.Int).Value = gid;
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            adp.Fill(ds);
            DataList2.DataSource = ds;
            DataList2.DataBind();
        }
    }
    private void rep_bind(Int32 a)
    {
        SqlCommand cmd = new SqlCommand("steachersts", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@u", SqlDbType.Int).Value = a;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        imglightbox.DataSource = ds;
        imglightbox.DataBind();
        cmd.Dispose();
    }
    protected void DataList2_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "Show")
        {
            Int32 a = Convert.ToInt32(DataList2.DataKeys[e.Item.ItemIndex]);
            rep_bind(a);
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "ShowValidation", "javascript:ShowImages();", true);
        }
    }
}