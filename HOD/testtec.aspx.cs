﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

public partial class HOD_testtec : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (con.State == ConnectionState.Closed)
        {
            con.Open();
        }
        SqlCommand cmd = new SqlCommand("instec", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@tecnam", SqlDbType.VarChar, 50).Value = TextBox1.Text;
        cmd.Parameters.Add("@tecteacherid", SqlDbType.Int).Value = Session["stid"];
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        TextBox1.Text = string.Empty;
        GridView2.DataBind();
    } 
}