﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.SessionState" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        // Code that runs on application startup

    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs
        //Response.Redirect("Error.aspx");
    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started
       
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {
        if (HttpContext.Current.User != null)
        {
            FormsIdentity fi = (FormsIdentity)(User.Identity);
            FormsAuthenticationTicket k = fi.Ticket;
            string ud = k.UserData;
            string[] ar = ud.Split('|');
            HttpContext.Current.User=new System.Security.Principal.GenericPrincipal(fi,ar);
            
        }
    }

    //protected void Application_BeginRequest(object sender, EventArgs e)
    //{
    //      String path= HttpContext.Current.Request.Path;
    //      String[]  pathElements = path.Split(".",c);
    //      String extenseName = pathElements[pathElements.Length - 1];
    //      if(!extenseName.Equals("aspx", StringComparison.OrdinalIgnoreCase)) 
    //      {
    //        if(!extenseName.Equals("jpg", StringComparison.OrdinalIgnoreCase) || !IsUrl() ) 
    //        {
    //            HttpContext.Current.Response.Redirect("~/NoPermissionPage.aspx");
    //        }
    //    }
    //}
    //protected Boolean IsUrl()
    //{
    //    String  httpReferer  = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_REFERER"];
    //    String serverName  = System.Web.HttpContext.Current.Request.ServerVariables["SERVER_NAME"];
    //    if ((httpReferer != Nothing) && (httpReferer.IndexOf(serverName) = 7)) 
    //        return true;
    //    else
    //        return false;
    //}
</script>
