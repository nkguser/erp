﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class HOD_reqallot : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            seserp.DropdownWebService obj = new seserp.DropdownWebService();
            obj.sum(Convert.ToInt32(Session["other"]));
        }
    }
   
    private void gridbind()
    {
        SqlCommand cmd = new SqlCommand(" SELECT  reqallot.reqallotid,      tbsession.sessionstart, tbclass.classname + ' (' + tbgroup.groupname + ')' AS class, tbsubject.subjecttitle + ' (' + tbsubjectype.subjecttypename + ')' AS subject,  tbdep.depname FROM            tbsession INNER JOIN tbclass ON tbsession.Sessionid = tbclass.classsessionid INNER JOIN tbgroup ON tbclass.classid = tbgroup.groupclassid INNER JOIN reqallot ON tbgroup.groupid = reqallot.reqallotgroupid INNER JOIN tbdep ON reqallot.reqallotto = tbdep.depid INNER JOIN tbsubject ON reqallot.reqallotsubjectid = tbsubject.subjectid INNER JOIN tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid WHERE        (reqallot.reqallotby = @u) AND (reqallot.reqallotsts = 0)", con);
        cmd.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridAllTeacher.DataSource = ds;
        GridAllTeacher.DataBind();
    }
    private void grid2bind()
    {
        SqlCommand cmd = new SqlCommand("   SELECT        reqallot.reqallotid, tbsession.sessionstart, tbclass.classname + ' (' + tbgroup.groupname + ')' AS class, tbsubject.subjecttitle + ' (' + tbsubjectype.subjecttypename + ')' AS subject,  tbteacher.teacherfname + ' ' + tbteacher.teachermname + ' ' + tbteacher.teacherlname + ' (' + tbdep.depname + ')' AS name FROM            tbsession INNER JOIN tbclass ON tbsession.Sessionid = tbclass.classsessionid INNER JOIN tbgroup ON tbclass.classid = tbgroup.groupclassid INNER JOIN reqallot ON tbgroup.groupid = reqallot.reqallotgroupid INNER JOIN tbsubject ON reqallot.reqallotsubjectid = tbsubject.subjectid INNER JOIN tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid INNER JOIN tbteacher ON reqallot.reqallottid = tbteacher.teacherid INNER JOIN tbdep ON tbteacher.teacherdepid = tbdep.depid WHERE        (reqallot.reqallotby = @u) AND (reqallot.reqallotsts = 1)", con);
        cmd.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        Gridapproved.DataSource = ds;
        Gridapproved.DataBind();
    }
    private void grid3bind()
    {
        SqlCommand cmd = new SqlCommand("SELECT        reqallot.reqallotid, tbsession.sessionstart, tbclass.classname + ' (' + tbgroup.groupname + ')' AS class,  tbsubject.subjecttitle + ' (' + tbsubjectype.subjecttypename + ')' AS subject, tbdep.depname, tbsubject.subjectid, tbgroup.groupid,  tbteacher.teacherfname + ' ' + tbteacher.teachermname + ' ' + tbteacher.teacherlname AS name FROM            tbsession INNER JOIN tbclass ON tbsession.Sessionid = tbclass.classsessionid INNER JOIN tbgroup ON tbclass.classid = tbgroup.groupclassid INNER JOIN reqallot ON tbgroup.groupid = reqallot.reqallotgroupid INNER JOIN tbdep ON reqallot.reqallotto = tbdep.depid INNER JOIN tbsubject ON reqallot.reqallotsubjectid = tbsubject.subjectid INNER JOIN tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid INNER JOIN tbteacher ON reqallot.reqallottid = tbteacher.teacherid AND tbdep.depid = tbteacher.teacherdepid WHERE        (reqallot.reqallotto = @u) AND (reqallot.reqallotsts = 1)", con);
        cmd.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        Gridallot.DataSource = ds;
        Gridallot.DataBind();
    }
    private void grid1bind()
    {
        SqlCommand cmd = new SqlCommand(" SELECT  reqallot.reqallotid, tbsession.sessionstart, tbclass.classname + ' (' + tbgroup.groupname + ')' AS class, tbsubject.subjecttitle + ' (' + tbsubjectype.subjecttypename + ')' AS subject, tbdep.depname, tbsubject.subjectid, tbgroup.groupid FROM            tbsession INNER JOIN tbclass ON tbsession.Sessionid = tbclass.classsessionid INNER JOIN tbgroup ON tbclass.classid = tbgroup.groupclassid INNER JOIN reqallot ON tbgroup.groupid = reqallot.reqallotgroupid INNER JOIN tbdep ON reqallot.reqallotto = tbdep.depid INNER JOIN tbsubject ON reqallot.reqallotsubjectid = tbsubject.subjectid INNER JOIN tbsubjectype ON tbsubject.subjecttypeid = tbsubjectype.subjecttypeid  WHERE        (reqallot.reqallotto = @u) AND (reqallot.reqallotsts = 0)", con);
        cmd.Parameters.Add("@u", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        GridTeacher.DataSource = ds;
        GridTeacher.DataBind();
    }
    private void classbind()
    {
        
        if(con.State==ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT CONVERT(varchar(12), tbsession.sessionstart) + ' - ' + CONVERT(varchar(12), tbsession.sessionend) + ' ' + tbclass.classname AS classname, tbclass.classid FROM tbclass INNER JOIN tbdep ON tbclass.classdepid = tbdep.depid INNER JOIN tbsession ON tbclass.classsessionid = tbsession.Sessionid WHERE        (tbdep.depid = @depid) ORDER BY classname", con);
        cmd.Parameters.Add("@depid", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        SqlDataReader dr = cmd.ExecuteReader();
        DDLClass.DataTextField = "classname";
        DDLClass.DataValueField = "classid";
        DDLClass.DataSource = dr;
        DDLClass.DataBind();
        DDLClass.Items.Insert(0, "Select Class");
        DDLGroup.Items.Insert(0, "Select Group");
        dr.Close();
        
    }
    private void groupbind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("select * from tbgroup where groupclassid=@classid", con);
        cmd.Parameters.Add("@classid", SqlDbType.Int).Value = Convert.ToInt32(DDLClass.SelectedValue);
        SqlDataReader dr = cmd.ExecuteReader();
        DDLGroup.DataTextField = "groupname";
        DDLGroup.DataValueField = "groupid";
        DDLGroup.DataSource = dr;
        DDLGroup.DataBind();
        dr.Close();
        DDLGroup.Items.Insert(0, "Select Group");
        
    }
    private void DDLSubjectBind()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("SELECT tbSubject.Subjectid, tbSubject.Subjecttitle + ' (' + tbSubjectype.SubjectTypename + ')' AS subject FROM tbSubject INNER JOIN tbSubjectype ON tbSubject.Subjecttypeid = tbSubjectype.SubjectTypeid WHERE (Subjectdepid = @did) AND (tbsubject.Subjectdelsts = 0) AND (tbsubjectype.Subjectdelsts = 0)", con);
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
        SqlDataReader dr = cmd.ExecuteReader();
        DDlSubject.DataTextField = "subject";
        DDlSubject.DataValueField = "Subjectid";
        DDlSubject.DataSource = dr;
        DDlSubject.DataBind();
        dr.Close();
        DDlSubject.Items.Insert(0, "-- Select Subject --");
        
    }
   
    protected void BTNSubAllot_Click(object sender, EventArgs e)
    {
        if(DDlSubject.SelectedIndex==0 && DDLGroup.Text=="Select Group")
        {
            lblmsg.Text = "Choose Subject and Group";
        }
        else if (DDlSubject.SelectedIndex == 0 )
        {
            lblmsg.Text = "Choose Subject ";
        }
        else if (DDLGroup.Text == "Select Group")
        {
            lblmsg.Text = "Choose Group";
        }
        else if (Convert.ToInt32(DDLDeptd.SelectedValue) != Convert.ToInt32(Session["depid"]))
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand comcheck = new SqlCommand("SELECT  count(*) as cnt FROM tbsubjectallot WHERE    (subjectallotsubjectid = @sid) AND (subjectallotgroupid = @gid)", con);
            comcheck.Parameters.Add("@sid", SqlDbType.Int).Value = Convert.ToInt32(DDlSubject.SelectedValue);
            comcheck.Parameters.Add("@gid", SqlDbType.Int).Value = Convert.ToInt32(DDLGroup.SelectedValue);
            Int32 check = Convert.ToInt32(comcheck.ExecuteScalar());
            if (check == 0)
            {
                SqlCommand cmd = new SqlCommand("insert into reqallot(reqallotby,reqallotgroupid,reqallotsubjectid,reqallotto,reqallotsts) values(@raby,@ragid,@rasid,@rato,0) ", con);
                cmd.Parameters.Add("@raby", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
                cmd.Parameters.Add("@ragid", SqlDbType.Int).Value = Convert.ToInt32(DDLGroup.SelectedValue);
                cmd.Parameters.Add("@rasid", SqlDbType.Int).Value = Convert.ToInt32(DDlSubject.SelectedValue);
                cmd.Parameters.Add("@rato", SqlDbType.Int).Value = Convert.ToInt32(DDLDeptd.SelectedValue);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Close();
                lblmsg.Text = "Allotment Request has been made Successfully";
                gridbind();
            }
            else
            {
                lblmsg.Text = "Already Alloted";
            }

        }
        else
        {
            lblmsg.Text = "You Cannot Alott Subject here.Go to desired section for the same. ";
        }
    }
    protected void DDlSubject_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }

    protected void DDLClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLClass.SelectedIndex == 0)
        {
            DDLGroup.Enabled = false;
            DDLGroup.Items.Clear();
            DDLGroup.Items.Insert(0, "Select Group");
        }
        else
        {
            DDLGroup.Enabled = true;
            groupbind();
        }
    }
    protected void GridAllTeacher_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        Int32 tid;
        tid = (Int32)(GridAllTeacher.DataKeys[e.RowIndex].Values["reqallotid"]);
        SqlCommand cmd = new SqlCommand(" Delete from reqallot WHERE (reqallotid = @tid)", con);
        cmd.Parameters.Add("@tid", SqlDbType.Int).Value = tid;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
        gridbind();
       
    }
    //protected void GridTeacher_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    GridTeacher.SetEditRow(Convert.ToInt32(GridTeacher.SelectedDataKey.Value));
    //}
    protected void GridTeacher_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridTeacher.EditIndex = e.NewEditIndex;
        grid1bind();
    }
    protected void GridTeacher_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridTeacher.EditIndex = -1;
        grid1bind();
    }
    protected void GridTeacher_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
       
        Int32 sid,gid,rid;
       // tid = Convert.ToInt32(((DropDownList)(GridTeacher.Rows[e.RowIndex].FindControl("DDLTeacher"))).DataValueField);
        DropDownList ddlteacher = (DropDownList)GridTeacher.Rows[e.RowIndex].FindControl("DDLTeacher");
        rid = (Int32)(GridTeacher.DataKeys[e.RowIndex].Values["reqallotid"]);
        sid = (Int32)(GridTeacher.DataKeys[e.RowIndex].Values["subjectid"]);
        gid = (Int32)(GridTeacher.DataKeys[e.RowIndex].Values["groupid"]);
        if (con.State == ConnectionState.Closed)
            con.Open();
        Application.Lock();
        SqlCommand com = new SqlCommand("Insert into tbsubjectAllot values(@tid,@sid,@gid,0,@aid)", con);
        com.Parameters.Add("@tid", SqlDbType.Int).Value = Convert.ToInt32(ddlteacher.SelectedValue);
        com.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
        com.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
        com.Parameters.Add("@aid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        com.ExecuteNonQuery();
        com.Dispose();
        SqlCommand comsts = new SqlCommand("update  reqallot set reqallotsts=1,reqallottid=@ttid where reqallotid=@rid", con);
        comsts.Parameters.Add("@rid", SqlDbType.Int).Value = rid;
        comsts.Parameters.Add("@ttid", SqlDbType.Int).Value = Convert.ToInt32(ddlteacher.SelectedValue);
        comsts.ExecuteNonQuery();
        comsts.Dispose();
        con.Close();
        Application.UnLock();
        GridTeacher.EditIndex = -1;
        grid1bind();
    }
    protected void GridTeacher_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowState == DataControlRowState.Edit)
        {
            DropDownList ddlteacher = (DropDownList)e.Row.FindControl("DDLTeacher");

            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand com = new SqlCommand("select teacherid,TeacherFname +' '+TeacherMName +' '+TeacherLname as Name from tbTeacher where teacherDepid=@did and teacherdelsts=0 ", con);
            int i = Convert.ToInt32(Session["depid"]);
            com.Parameters.Add("@did", SqlDbType.Int).Value = Convert.ToInt32(Session["depid"]);
            SqlDataReader dr1 = com.ExecuteReader();
            ddlteacher.DataValueField = "teacherid";
            ddlteacher.DataTextField = "name";
            ddlteacher.DataSource = dr1;
            ddlteacher.DataBind();
            ddlteacher.Enabled = true;
            ddlteacher.Items.Insert(0, "-- Select Teacher --");
            com.Dispose();
            con.Close();
            dr1.Close();
            
            
        }
    }
    protected void RadioButtonList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RadioButtonList2.SelectedIndex == 0)
        {
            p1.Visible = true;
            p2.Visible = false;
            p3.Visible = false;
            p4.Visible = false;
            classbind();
            DDLSubjectBind();
            gridbind();
            lblmsg.Text = string.Empty;
            DDLGroup.Enabled = false;
            DDLGroup.Items.Clear();
            DDLGroup.Items.Insert(0, "Select Group");
        }
        else if (RadioButtonList2.SelectedIndex == 2)
        {
            grid1bind();
            p1.Visible = false;
            p2.Visible = true;
            p3.Visible = false;
            p4.Visible = false;
            lblmsg.Text = string.Empty;
        }
        else if (RadioButtonList2.SelectedIndex == 1)
        {
            grid2bind();
            p1.Visible = false;
            p2.Visible = false;
            p3.Visible = true;
            p4.Visible = false;
            lblmsg.Text = string.Empty;
        }
        else
        {
            grid3bind();
            p1.Visible = false;
            p2.Visible = false;
            p3.Visible = false;
            p4.Visible = true;
            lblmsg.Text = string.Empty;
        }
    }
    
}