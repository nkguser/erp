﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class Teacher_studentdetailsrpt : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            DDLCourseBind();
            DDLSessionBind();
        }
    }
    private void DDLCourseBind()
    {
        DDLCourse.DataTextField = "Coursename";
        DDLCourse.DataValueField = "Courseid";
        DDLCourse.DataSource = ERP.AllCourse();
        DDLCourse.DataBind();
        DDLDept.Enabled = true;
        DDLCourse.Items.Insert(0, "-- Select Course --");
    }
    private void DDLSessionBind()
    {
        DDLSession.DataTextField = "session";
        DDLSession.DataValueField = "sessionid";
        DDLSession.DataSource = ERP.AllSession();
        DDLSession.DataBind();
        DDLSession.Items.Insert(0, "-- Select session --");
    }
    protected void DDLCourse_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDLDept.DataTextField = "Depname";
        DDLDept.DataValueField = "Depid";
        DDLDept.DataSource = ERP.AllDepartment(Convert.ToInt32(DDLCourse.SelectedValue));
        DDLDept.DataBind();
        DDLDept.Items.Insert(0, "-- Select Department --");
    }
    protected void DDLSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDLClass.DataTextField = "Class";
        DDLClass.DataValueField = "subjectallotgroupid";
        DDLClass.DataSource = ERP.TeacherClass(Convert.ToInt32(Session["stid"]), Convert.ToInt32(DDLSession.SelectedValue));
        DDLClass.DataBind();
        DDLClass.Enabled = true;        
        DDLClass.Items.Insert(0, "-- Select Class --");
    }    
}