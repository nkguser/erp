﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace nsphotosnak
{
    //connection ***********************************************************************
    public abstract class clscon
    {
        protected SqlConnection con = new SqlConnection();
        public clscon()
        {
            con.ConnectionString = ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        }
    }
    //**********************************************************************************
    public interface inttemp
    {
        Int32 p_tmpcod
        {
            get;
            set;
        }

        String p_tmpnam
        {
            get;
            set;
        }

        String p_tmpdsppag
        {
            get;
            set;
        }
    }
    public class clstmpprp : inttemp
    {
        private Int32 tmpcod;
        private String tmpnam, tmpdsppag;
        public int p_tmpcod
        {
            get
            {
                return tmpcod;
            }
            set
            {
                tmpcod = value;
            }
        }

        public String p_tmpnam
        {
            get
            {
                return tmpnam;
            }
            set
            {
                tmpnam = value;
            }
        }

        public String p_tmpdsppag
        {
            get
            {
                return tmpdsppag;
            }
            set
            {
                tmpdsppag = value;
            }
        }
    }
    public class clstmp:clscon
    {
        public void Save_Rec(clstmpprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("instmp", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Add("@tmpcod", SqlDbType.Int).Value = p.p_tmpcod;
            cmd.Parameters.Add("@tmpnam", SqlDbType.VarChar, 100).Value = p.p_tmpnam;
            cmd.Parameters.Add("@tmpdsppag", SqlDbType.VarChar, 100).Value = p.p_tmpdsppag;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_Rec(clstmpprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updtmp", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@tmpcod", SqlDbType.Int).Value = p.p_tmpcod;
            cmd.Parameters.Add("@tmpnam", SqlDbType.VarChar, 100).Value = p.p_tmpnam;
            cmd.Parameters.Add("@tmpdsppag", SqlDbType.VarChar, 100).Value = p.p_tmpdsppag;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clstmpprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("deltmp", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@tmpcod", SqlDbType.Int).Value = p.p_tmpcod;
           // cmd.Parameters.Add("@tmpnam", SqlDbType.VarChar, 100).Value = p.p_tmpnam;
           // cmd.Parameters.Add("@tmpdsppag", SqlDbType.VarChar, 100).Value = p.p_tmpdsppag;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clstmpprp> Disp_rec()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dsptmp", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Add("@tmpcod", SqlDbType.Int).Value = p.p_tmpcod;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clstmpprp> obj = new List<clstmpprp>();
            while (dr.Read())
            {
                clstmpprp k = new clstmpprp();
                k.p_tmpcod = Convert.ToInt32(dr[0]);
                k.p_tmpnam = dr[1].ToString();
                k.p_tmpdsppag = dr[2].ToString();
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
        public List<clstmpprp> Find_rec(Int32 tmpcod)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndtmp", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@tmpcod", SqlDbType.Int).Value = tmpcod;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clstmpprp> obj = new List<clstmpprp>();
            if(dr.HasRows)
            {
                dr.Read();
                clstmpprp k = new clstmpprp();
                k.p_tmpcod = Convert.ToInt32(dr[0]);
                k.p_tmpnam = dr[1].ToString();
                k.p_tmpdsppag = dr[2].ToString();
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }


    //**********************************************************************************
    public interface intalb
    {
        Int32 p_albcod
        {
            get;
            set;
        }
        Int32 p_albregcod
        {
            get;
            set;
        }
        String p_albnam
        {
            get;
            set;
        }
        String p_albcvrpic
        {
            get;
            set;
        }
        Int32 p_albtmpcod
        {
            get;
            set;
        }
        Char p_albsts
        {
            get;
            set;
        }
    }
    public class clsalbprp : intalb
    {
       private Int32 albcod,albregcod,albtmpcod;
        private String albnam ,albcvrpic;
        private Char albsts;
        
        public int p_albcod
        {
            get
            {
                return albcod;
            }
            set
            {
                albcod = value;
            }
        }

        public int p_albregcod
        {
            get
            {
                return albregcod;
            }
            set
            {
                albregcod = value;
            }
        }

        public string p_albnam
        {
            get
            {
                return albnam;
            }
            set
            {
                albnam = value;
            }
        }

        public string p_albcvrpic
        {
            get
            {
                return albcvrpic;
            }
            set
            {
                albcvrpic = value;
            }
        }

        public int p_albtmpcod
        {
            get
            {
               return albtmpcod;
            }
            set
            {
                albtmpcod = value;
            }
        }

        public char p_albsts
        {
            get
            {
                return albsts;
            }
            set
            {
                albsts = value;
            }
        }
    }
    public class clsalb : clscon
    {
        public void updcvrpic(clsalbprp p)
        {
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlCommand cmd = new SqlCommand("updcvrpic", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@albcod", SqlDbType.Int).Value = p.p_albcod;
            cmd.Parameters.Add("@albcvrpic", SqlDbType.VarChar, 50).Value = p.p_albcvrpic;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public Int32 Save_Rec(clsalbprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insalb", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@albregcod", SqlDbType.Int).Value = p.p_albregcod;
            cmd.Parameters.Add("@albnam", SqlDbType.VarChar, 100).Value = p.p_albnam;
            cmd.Parameters.Add("@albcvrpic", SqlDbType.VarChar, 100).Value = p.p_albcvrpic;
            cmd.Parameters.Add("@albtmpcod", SqlDbType.Int).Value = p.p_albtmpcod ;
            cmd.Parameters.Add("@albsts", SqlDbType.Char, 1).Value = p.p_albsts;
            cmd.Parameters.Add("@r", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();
            Int32 a = Convert.ToInt32(cmd.Parameters["@r"].Value);
            cmd.Dispose();
            con.Close();
            return a;
        }
        public void Update_Rec(clsalbprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updalb", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@albcod", SqlDbType.Int).Value = p.p_albcod;
            cmd.Parameters.Add("@albregcod", SqlDbType.Int).Value = p.p_albregcod;
            cmd.Parameters.Add("@albnam", SqlDbType.VarChar, 100).Value = p.p_albnam;
         //   cmd.Parameters.Add("@albcvrpic", SqlDbType.VarChar, 100).Value = p.p_albcvrpic;
            cmd.Parameters.Add("@albtmpcod", SqlDbType.Int).Value = p.p_albtmpcod;
            cmd.Parameters.Add("@albsts", SqlDbType.Char, 1).Value = p.p_albsts;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsalbprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delalb", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@albcod", SqlDbType.Int).Value = p.p_albcod;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsalbprp> Display_rec(Int32 regcod)
        {
          
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspalb", con);
            cmd.Parameters.Add("@regcod", SqlDbType.Int).Value = regcod;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr=cmd.ExecuteReader();
            List<clsalbprp> obj=new List<clsalbprp>();
            while(dr.Read())
            {
                clsalbprp k=new clsalbprp();
                k.p_albcod= Convert.ToInt32( dr[0]);
                k.p_albregcod=Convert.ToInt32(dr[1]);
                k.p_albnam=dr[2].ToString();
                k.p_albcvrpic=dr[3].ToString();
                k.p_albtmpcod=Convert.ToInt32(dr[4]);
                k.p_albsts=Convert.ToChar(dr[5]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
        public List<clsalbprp> Dspshralb(Int32 regcod)
        {

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspshralb", con);
            cmd.Parameters.Add("@regcod", SqlDbType.Int).Value = regcod;
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsalbprp> obj = new List<clsalbprp>();
            while (dr.Read())
            {
                clsalbprp k = new clsalbprp();
                k.p_albcod = Convert.ToInt32(dr[0]);
                k.p_albregcod = Convert.ToInt32(dr[1]);
                k.p_albnam = dr[2].ToString();
                k.p_albcvrpic = dr[3].ToString();
                k.p_albtmpcod = Convert.ToInt32(dr[4]);
                k.p_albsts = Convert.ToChar(dr[5]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
        public List<clsalbprp> Find_rec( Int32 albocd)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("fndalb", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@albcod", SqlDbType.Int).Value = albocd;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsalbprp> obj = new List<clsalbprp>();
            if(dr.HasRows)
            {
                dr.Read();
                clsalbprp k = new clsalbprp();
                k.p_albcod = Convert.ToInt32(dr[0]);
                k.p_albregcod = Convert.ToInt32(dr[1]);
                k.p_albnam = dr[2].ToString();
                k.p_albcvrpic = dr[3].ToString();
                k.p_albtmpcod = Convert.ToInt32(dr[4]);
                k.p_albsts = Convert.ToChar(dr[5]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
        }



    //****************************************************
    public interface intalbdet
    {
        Int32 p_albdetcod
        {
            get;
            set;
        }
        Int32 p_albdetalbcod
        {
            get;
            set;
        }
        String p_albdetpic
        {
            get;
            set;
        }
        String p_albdetdsc
        {
            get;
            set;
        }
    }
   public  class clsalbdetprp : intalbdet
    {
        Int32 albdetcod, albdetalbcod;
        String albdetpic, albdetdsc;
        public int p_albdetcod
        {
            get
            {
                return albdetcod;
            }
            set
            {
                albdetcod = value;   
            }
        }

        public int p_albdetalbcod
        {
            get
            {
                return albdetalbcod;
            }
            set
            {
                albdetalbcod = value;
            }
        }

        public string p_albdetpic
        {
            get
            {
                return albdetpic;
            }
            set
            {
                albdetpic = value;
            }
        }

        public string p_albdetdsc
        {
            get
            {
                return albdetdsc;
            }
            set
            {
                albdetdsc = value;
            }
        }
    }
    public class clsalbdet:clscon
    {
        public Int32 Save_Rec(clsalbdetprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insalbdet", con);
            cmd.CommandType = CommandType.StoredProcedure;
           // cmd.Parameters.Add("@albdetcod", SqlDbType.Int).Value = p.p_albdetcod;
            cmd.Parameters.Add("@albdetalbcod", SqlDbType.Int).Value = p.p_albdetalbcod;
            cmd.Parameters.Add("@albdetpic", SqlDbType.VarChar, 100).Value = p.p_albdetpic;
            cmd.Parameters.Add("@albdetdsc", SqlDbType.VarChar, 500).Value = p.p_albdetdsc;
            cmd.Parameters.Add("@r", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();
            Int32 a = Convert.ToInt32(cmd.Parameters["@r"].Value);
            cmd.Dispose();
            con.Close();
            return a;
        }
        public void Update_Rec(clsalbdetprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updalbdet", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@albdetcod", SqlDbType.Int).Value = p.p_albdetcod;
            cmd.Parameters.Add("@albdetalbcod", SqlDbType.Int).Value = p.p_albdetalbcod;
            cmd.Parameters.Add("@albdetpic", SqlDbType.VarChar, 100).Value = p.p_albdetpic;
            cmd.Parameters.Add("@albdetdsc", SqlDbType.VarChar, 500).Value = p.p_albdetdsc;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_Rec(clsalbdetprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("delalbdet", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@albdetcod", SqlDbType.Int).Value = p.p_albdetcod;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsalbdetprp> Display_Rec(Int32 albcod)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd=new SqlCommand("dspalbdet",con);
            cmd.CommandType=CommandType.StoredProcedure;
            cmd.Parameters.Add("@albcod", SqlDbType.Int).Value = albcod;
            SqlDataReader dr=cmd.ExecuteReader();
            List<clsalbdetprp> obj=new List<clsalbdetprp>();
            while(dr.Read())
            {
                clsalbdetprp k=new clsalbdetprp();
                k.p_albdetcod= Convert.ToInt32( dr[0]);
                k.p_albdetalbcod= Convert.ToInt32( dr[1]);
                k.p_albdetpic=dr[2].ToString();
                k.p_albdetdsc=dr[3].ToString();
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;

        }
        public List<clsalbdetprp> Find_Rec(Int32 a)
                 {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd=new SqlCommand("fndalbdet",con);
            cmd.CommandType=CommandType.StoredProcedure;
            cmd.Parameters.Add("@albsetcod",SqlDbType.Int).Value=a;
            SqlDataReader dr=cmd.ExecuteReader();
            List<clsalbdetprp> obj=new List<clsalbdetprp>();
            if(dr.HasRows)
            {
                dr.Read();
                clsalbdetprp k=new clsalbdetprp();
                k.p_albdetcod= Convert.ToInt32( dr[0]);
                k.p_albdetalbcod= Convert.ToInt32( dr[1]);
                k.p_albdetpic=dr[2].ToString();
                k.p_albdetdsc=dr[3].ToString();
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;

        
               

        }
    }



    //****************************************************
    public interface intshr
    {
        Int32 p_shrcod
        {
            get;
            set;
        }
        Int32 p_shralbcod
        {
            get;
            set;
        }
        Int32 p_shrregcod
        {
            get;
            set;
        }
    }
    public class clsshrprp : intshr
    {
        Int32 shrcod, shralbcod, shrregcod;
        public int p_shrcod
        {
            get
            {
                return shrcod;
            }
            set
            {
                shrcod = value;
            }
        }

        public int p_shralbcod
        {
            get
            {
               return shralbcod;
            }
            set
            {
                shralbcod = value;
            }
        }

        public int p_shrregcod
        {
            get
            {
                return shrregcod;
            }
            set
            {
                shrregcod = value;
            }
        }
    }
    public class clsshr : clscon
    {
        public void Save_rec(clsshrprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insshr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Add("@shrcod", SqlDbType.Int).Value = p.p_shrcod;
            cmd.Parameters.Add("@shralbcod", SqlDbType.Int).Value = p.p_shralbcod;
            cmd.Parameters.Add("@shrregcod", SqlDbType.Int).Value = p.p_shrregcod;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Update_rec(clsshrprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("updshr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@shrcod", SqlDbType.Int).Value = p.p_shrcod;
            cmd.Parameters.Add("@shralbcod", SqlDbType.Int).Value = p.p_shralbcod;
            cmd.Parameters.Add("@shrregcod", SqlDbType.Int).Value = p.p_shrregcod;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public void Delete_rec(clsshrprp p)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("insshr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@shrcod", SqlDbType.Int).Value = p.p_shrcod;
            cmd.ExecuteNonQuery();
            cmd.Dispose();
            con.Close();
        }
        public List<clsshrprp> Display_rec()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspshr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd.Parameters.Add("@shrcod", SqlDbType.Int).Value = p.p_shrcod;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsshrprp> obj = new List<clsshrprp>();
            while (dr.Read())
            {
                clsshrprp k = new clsshrprp();
                k.p_shrcod = Convert.ToInt32(dr[0]);
                k.p_shralbcod = Convert.ToInt32(dr[1]);
                k.p_shrregcod = Convert.ToInt32(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
        public List<clsshrprp> Find_rec(Int32 a)
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlCommand cmd = new SqlCommand("dspshr", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@shrcod", SqlDbType.Int).Value = a;
            SqlDataReader dr = cmd.ExecuteReader();
            List<clsshrprp> obj = new List<clsshrprp>();
           if(dr.HasRows)
            {
                dr.Read();
                clsshrprp k = new clsshrprp();
                k.p_shrcod = Convert.ToInt32(dr[0]);
                k.p_shralbcod = Convert.ToInt32(dr[1]);
                k.p_shrregcod = Convert.ToInt32(dr[2]);
                obj.Add(k);
            }
            cmd.Dispose();
            con.Close();
            dr.Close();
            return obj;
        }
    }


    }


