﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="testqst.aspx.cs" Inherits="Teacher_testqst" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">      
    <table width="100%" style="text-align: left; font-weight: bold;" >
        <tr>
        <td></td>
            <td colspan="3" style="height: 71px">
                <h1>
                    Question Bank</h1>
            </td>
        </tr>
        <tr>
        <td></td>
            <td >
             <b>   Test</b></td>
            <td >
                <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" 
                    DataSourceID="ObjectDataSource1" DataTextField="p_tecnam" 
                    DataValueField="p_teccod" CssClass="field2">
                </asp:DropDownList>
            </td>
            <td>
                </td>
        </tr>
        <tr><td></td>
            <td >
              <b>  Question </b></td>
            <td  colspan="2">
                <cc1:Editor ID="Editor1" runat="server" Height="350px" Width="750px" />
            </td>
            
        </tr>
        <tr><td></td>
            <td >
              <b>  Picture</b></td>
            <td >
                <asp:FileUpload ID="FileUpload1" runat="server" />
            </td>
            <td>
                </td>
        </tr>
        <tr><td></td>
            <td >
                </td>
            <td >
                <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Submit" />

                </td>
            <td>
               </td>
        </tr>
        <tr><td></td>
            <td colspan="3" align="right">
   
            <asp:Panel ID="p1" ScrollBars="Auto" Height="300px" width="100%"  runat="server">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                    DataSourceID="ObjectDataSource2" Width="100%" 
                    DataKeyNames="p_qstcod,p_qstpic" onrowdatabound="GridView1_RowDataBound" 
                    onrowdeleting="GridView1_RowDeleting" onrowediting="GridView1_RowEditing" 
                    onrowupdating="GridView1_RowUpdating" CellPadding="4" ForeColor="#333333" 
                    GridLines="Vertical">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                     <asp:TemplateField HeaderText="Ques.No.">
                        <ItemTemplate>
                        <%#Container.DataItemIndex +1 %>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Picture">
                        <ItemTemplate>
                        <asp:Image ID="img1" runat="server"  Height="50px" Width="50px"/>
                        </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="p_qstdsc" HeaderText="Question" 
                            SortExpression="p_qstdsc" HtmlEncode="False" 
                            HtmlEncodeFormatString="False" />
                        <asp:CommandField ShowEditButton="True" />
                        <asp:CommandField ShowDeleteButton="True" />
                        <asp:TemplateField>
                        <ItemTemplate>
                        <asp:LinkButton ID="lk1" runat="server" Text="Add Options" CommandName="update" CausesValidation =false  />
                        </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                   <EditRowStyle BackColor="#2461BF" />
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#F5F7FB" />
        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
        <SortedDescendingCellStyle BackColor="#E9EBEF" />
        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
               
               </asp:Panel> 
                

                <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" 
                    SelectMethod="Display_Rec" TypeName="ot.clstec">
                    <SelectParameters>
                        <asp:SessionParameter Name="tid" SessionField="stid" Type="Int32" />
                    </SelectParameters>
                    </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" 
                    SelectMethod="Display_Rec" TypeName="ot.clsqst">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList1" Name="teccod" 
                            PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
                
            </td>
        </tr>
    </table>   
</asp:Content>
