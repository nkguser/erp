﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class logging : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!User.Identity.IsAuthenticated)
            FormsAuthentication.RedirectToLoginPage();
        //Server.Transfer("login.aspx");
        else
        {
            if (User.IsInRole("5"))
            {
                //Server.Transfer("~/Director/Home.aspx");
                Server.Transfer("~/dir/Default.aspx");
            }
            else if (User.IsInRole("4"))
            {
                Server.Transfer("~/Student/Home.aspx");
            }
            else if (User.IsInRole("2"))
            {
                Server.Transfer("~/HOD/Home.aspx");
            }
            else if (User.IsInRole("1"))
            {
                Server.Transfer("~/Admin/Home.aspx");
            }
            else if (User.IsInRole("3"))
            {
                Server.Transfer("~/Teacher/Home.aspx");
            }
            else if (User.IsInRole("7"))
            {
                Server.Transfer("~/SA/Home.aspx");
            }
            else if (User.IsInRole("6"))
            {
                Server.Transfer("~/TNP/Home.aspx");
            }
            else if (User.IsInRole("8"))
            {
                Server.Transfer("~/DAF/Home.aspx");
            }
        }
    }
}