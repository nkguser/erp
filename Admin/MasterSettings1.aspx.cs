﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Admin_Default : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private Int32 CheckLogID(String logid)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("Checkuser", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@uid", SqlDbType.VarChar,50).Value = logid;
        cmd.Parameters.Add("@ret", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.ExecuteNonQuery();
        Int32 a = Convert.ToInt32(cmd.Parameters["@ret"].Value);
        cmd.Dispose();
        con.Close();
        return a;
    }
    private void ResetPWD(String logid)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("Resetpwd", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@uid", SqlDbType.VarChar,50).Value = logid;        
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
    }
    private void DeleteUser(String logid)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("DelUser", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@uid", SqlDbType.VarChar,50).Value = logid;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
    }
    public Int32 ChangePWD(string logid, String OldPwd, string NewPwd)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("ChangePwd", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@uid", SqlDbType.VarChar,50).Value = logid;
        cmd.Parameters.Add("@oldpwd", SqlDbType.VarChar, 50).Value = OldPwd;
        cmd.Parameters.Add("@Newpwd", SqlDbType.VarChar, 50).Value = NewPwd;
        cmd.Parameters.Add("@ret", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.ExecuteNonQuery();
        Int32 k = Convert.ToInt32(cmd.Parameters["@ret"].Value);
        cmd.Dispose();
        con.Close();
        return k;
    }
    private DataSet AllSession()
    {
        SqlDataAdapter adp = new SqlDataAdapter("dispsession", con);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    private void RegSession(Int32 sstart,Int32 send)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("regsession", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@Sesnstart", SqlDbType.Int).Value = sstart;
        cmd.Parameters.Add("@Sesnend", SqlDbType.Int).Value = send;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
    }
    private void DelSession(int sid)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("delsession", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@Sid", SqlDbType.Int).Value = sid;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
    }
    private DataSet AllCourse()
    {
        SqlDataAdapter adp = new SqlDataAdapter("dispcourse", con);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    private void RegCourse(string cname, Int32 nos)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("regcourse", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@cname", SqlDbType.VarChar, 50).Value = cname;
        cmd.Parameters.Add("@nos", SqlDbType.Int).Value = nos;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
    }
    private void DelCourse(int cid)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("delcourse", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
    }
    private DataSet AllDepartment(int cid)
    {
        SqlCommand cmd = new SqlCommand("dispdept");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    private void RegDept(Int32 cid,String dname)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("regdept", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@dname", SqlDbType.VarChar, 50).Value = dname;
        cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
    }
    private void DelDept(Int32 did)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("Deldept", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
    }
    private DataSet AllClass(Int32 did, Int32 sid)
    {
        SqlCommand cmd = new SqlCommand("dispclass");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    private void RegClass(Int32 did,string cname,Int32 sid)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("regclass", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@did", SqlDbType.Int).Value = did;
        cmd.Parameters.Add("@cname", SqlDbType.VarChar, 50).Value = cname; ;
        cmd.Parameters.Add("@sid", SqlDbType.Int).Value = sid;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
    }
    private void DelClass(Int32 cid)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("Delclass", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
    }
    private void RegGroup(String gname,Int32 cid)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("reggroup", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@gname", SqlDbType.VarChar, 50).Value = gname;
        cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
    }
    private DataSet AllGroup(Int32 cid)
    {
        SqlCommand cmd = new SqlCommand("dispgroup");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        cmd.Parameters.Add("@cid", SqlDbType.Int).Value = cid;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        adp.Fill(ds);
        return ds;
    }
    private void DelGroup(Int32 gid)
    {
        if (con.State == ConnectionState.Closed)
            con.Open();
        SqlCommand cmd = new SqlCommand("DelGroup", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@gid", SqlDbType.Int).Value = gid;
        cmd.ExecuteNonQuery();
        cmd.Dispose();
        con.Close();
    }
    private void HidePanels()
    {
        PanlDelClass.Visible = false;
        PanlDelCrs.Visible = false;
        PanlDelDept.Visible = false;
        PanlDelGroup.Visible = false;
        PanlDelSesn.Visible = false;
        PanlNewClass.Visible = false;
        PanlNewCrs.Visible = false;
        PanlNewDept.Visible = false;
        PanlNewGroup.Visible = false;
        PanlNewSesn.Visible = false;        
        PanlResetPass.Visible = false;
        PanlDelUsr.Visible = false;
    }

    protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
    {
        switch (e.Item.Value)
        {            
            case "1":
                HidePanels();
                LblChkUser2.Text = "";
                PanlDelUsr.Visible = true;
                return;
            case "2":
                HidePanels();
                LblChkUser3.Text = "";
                PanlResetPass.Visible = true;
                return;
            case "3":
                HidePanels();
                PanlNewSesn.Visible = true;
                return;
            case "4":
                HidePanels();
                DDLDeleteSessionBind();
                PanlDelSesn.Visible = true;
                return;
            case "5":
                HidePanels();
                PanlNewCrs.Visible = true;
                return;
            case "6":
                HidePanels();
                DDLDeleteCourseBind();
                PanlDelCrs.Visible = true;
                return;
            case "7":
                HidePanels();
                DDLNewDeptCourseNameBind();
                PanlNewDept.Visible = true;
                return;
            case "8":
                HidePanels();
                DDLDeleteDeptCourseBind();
                PanlDelDept.Visible = true;
                return;
            case "9":
                HidePanels();
                DDLNewClassCourseBind();
                DDLNewClassSessionBind();
                PanlNewClass.Visible = true;
                return;
            case "10":
                HidePanels();
                DDLDeleteClassCourseBind();
                DDLDeleteClassSessionBind();
                PanlDelClass.Visible = true;
                return;
            case "11":
                HidePanels();
                DDLNewGroupCourseBind();
                DDLNewGroupSessionBind();
                PanlNewGroup.Visible = true;
                return;
            case "12":
                HidePanels();
                DDLDeleteGroupCourseBind();
                DDLDeleteGroupSessionBind();
                PanlDelGroup.Visible = true;
                return;
        }
    }

    private void DDLNewGroupSessionBind()
    {        
        DataSet ds = AllSession();
        DDLNewGroupSession.DataValueField = "Sessionid";
        DDLNewGroupSession.DataTextField = "session";
        DDLNewGroupSession.DataSource = ds;
        DDLNewGroupSession.DataBind();
        DDLNewGroupSession.Items.Insert(0, "--Select Session --");
    }
    private void DDLDeleteGroupSessionBind()
    {        
        DataSet ds = AllSession();
        DDLDeleteGroupSession.DataValueField = "Sessionid";
        DDLDeleteGroupSession.DataTextField = "session";
        DDLDeleteGroupSession.DataSource = ds;
        DDLDeleteGroupSession.DataBind();
        DDLDeleteGroupSession.Items.Insert(0, "--Select Session --");
    }    
    private void DDLDeleteSessionBind()
    {        
        DataSet ds = AllSession();
        DDLDeleteSession.DataValueField = "Sessionid";
        DDLDeleteSession.DataTextField = "session";
        DDLDeleteSession.DataSource = ds;
        DDLDeleteSession.DataBind();
        DDLDeleteSession.Items.Insert(0, "--Select Session --");
    }
    private void DDLDeleteCourseBind()
    {        
        DataSet ds = AllCourse();
        DDLDeleteCourse.DataValueField = "courseid";
        DDLDeleteCourse.DataTextField = "coursename";
        DDLDeleteCourse.DataSource = ds;
        DDLDeleteCourse.DataBind();
        DDLDeleteCourse.Items.Insert(0, "--Select Course --");
    }     
    private void DDLNewDeptCourseNameBind()
    {        
        DataSet ds =AllCourse();
        DDLNewDeptCourseName.DataValueField = "courseid";
        DDLNewDeptCourseName.DataTextField = "coursename";
        DDLNewDeptCourseName.DataSource = ds;
        DDLNewDeptCourseName.DataBind();
        DDLNewDeptCourseName.Items.Insert(0, "--Select Course --");
    }
    private void DDLDeleteDeptCourseBind()
    {       
        DataSet ds = AllCourse();
        DDLDeleteDeptCourse.DataValueField = "courseid";
        DDLDeleteDeptCourse.DataTextField = "coursename";
        DDLDeleteDeptCourse.DataSource = ds;
        DDLDeleteDeptCourse.DataBind();
        DDLDeleteDeptCourse.Items.Insert(0, "--Select Course --");
    }
    private void DDLNewClassCourseBind()
    {        
        DataSet ds = AllCourse();
        DDLNewClassCourse.DataValueField = "courseid";
        DDLNewClassCourse.DataTextField = "coursename";
        DDLNewClassCourse.DataSource = ds;
        DDLNewClassCourse.DataBind();
        DDLNewClassCourse.Items.Insert(0, "--Select Course --");
    }
    private void DDLDeleteClassCourseBind()
    {        
        DataSet ds = AllCourse();
        DDLDeleteClassCourse.DataValueField = "courseid";
        DDLDeleteClassCourse.DataTextField = "coursename";
        DDLDeleteClassCourse.DataSource = ds;
        DDLDeleteClassCourse.DataBind();
        DDLDeleteClassCourse.Items.Insert(0, "-- Select Course --");
    }
    private void DDLNewGroupCourseBind()
    {        
        DataSet ds = AllCourse();
        DDLNewGroupCourse.DataValueField = "courseid";
        DDLNewGroupCourse.DataTextField = "coursename";
        DDLNewGroupCourse.DataSource = ds;
        DDLNewGroupCourse.DataBind();
        DDLNewGroupCourse.Items.Insert(0, "-- Select Course --");
    }
    private void DDLDeleteGroupCourseBind()
    {       
        DataSet ds = AllCourse();
        DDLDeleteGroupCourse.DataValueField = "courseid";
        DDLDeleteGroupCourse.DataTextField = "coursename";
        DDLDeleteGroupCourse.DataSource = ds;
        DDLDeleteGroupCourse.DataBind();
        DDLDeleteGroupCourse.Items.Insert(0, "-- Select Course --");
    }
    
    protected void DDLDeleteDeptCourse_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDLDeleteDeptNameBind();
    }
    private void DDLDeleteDeptNameBind()
    {        
        DDLDeleteDeptName.DataTextField = "depname";
        DDLDeleteDeptName.DataValueField = "depid";
        DDLDeleteDeptName.DataSource = AllDepartment(Convert.ToInt32(DDLDeleteDeptCourse.SelectedValue));
        DDLDeleteDeptName.DataBind();
        DDLDeleteDeptName.Enabled = true;
        DDLDeleteDeptName.Items.Insert(0, "--Select Department --");
    }
    protected void DDLNewClassCourse_SelectedIndexChanged(object sender, EventArgs e)
    {        
        DDLNewClassDept.DataTextField = "depname";
        DDLNewClassDept.DataValueField = "depid";
        DDLNewClassDept.DataSource = AllDepartment(Convert.ToInt32(DDLNewClassCourse.SelectedValue));
        DDLNewClassDept.DataBind();
        DDLNewClassDept.Enabled = true;
        DDLNewClassDept.Items.Insert(0, "--Select Department --");

    }
    protected void DDLDeleteClassCourse_SelectedIndexChanged(object sender, EventArgs e)
    {        
        DDLDeleteClassDept.DataTextField = "depname";
        DDLDeleteClassDept.DataValueField = "depid";
        DDLDeleteClassDept.DataSource = AllDepartment(Convert.ToInt32(DDLDeleteClassCourse.SelectedValue));
        DDLDeleteClassDept.DataBind();
        DDLDeleteClassDept.Enabled = true;
        DDLDeleteClassDept.Items.Insert(0, "-- Select Department --");
    }
    protected void DDLNewGroupCourse_SelectedIndexChanged(object sender, EventArgs e)
    {        
        DDLNewGroupDept.DataTextField = "depname";
        DDLNewGroupDept.DataValueField = "depid";
        DDLNewGroupDept.DataSource = AllDepartment(Convert.ToInt32(DDLNewGroupCourse.SelectedValue));
        DDLNewGroupDept.DataBind();
        DDLNewGroupDept.Enabled = true;
        DDLNewGroupDept.Items.Insert(0, "-- Select Department --");

    }
    protected void DDLDeleteGroupCourse_SelectedIndexChanged(object sender, EventArgs e)
    {        
        DDLDeleteGroupDept.DataTextField = "depname";
        DDLDeleteGroupDept.DataValueField = "depid";
        DDLDeleteGroupDept.DataSource = AllDepartment(Convert.ToInt32(DDLDeleteGroupCourse.SelectedValue));
        DDLDeleteGroupDept.DataBind();
        DDLDeleteGroupDept.Enabled = true;
        DDLDeleteGroupDept.Items.Insert(0, "-- Select Department --");
    }
    //protected void DDLDeleteClassDept_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    clsClass clas = new clsClass();
    //    DDLDeleteClassName.DataTextField = "class_name";
    //    DDLDeleteClassName.DataValueField = "class_id";
    //    DDLDeleteClassName.DataSource = clas.AllClass(Convert.ToInt32(DDLDeleteClassDept.SelectedValue));
    //    DDLDeleteClassName.DataBind();
    //    DDLDeleteClassName.Enabled = true;
    //    DDLDeleteClassName.Items.Insert(0, "-- Select Class --");

    //}
    //protected void DDLNewGroupDept_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    clsClass clas = new clsClass();
    //    DDLNewGroupClass.DataTextField = "class_name";
    //    DDLNewGroupClass.DataValueField = "class_id";
    //    DDLNewGroupClass.DataSource = clas.AllClass(Convert.ToInt32(DDLNewGroupDept.SelectedValue));
    //    DDLNewGroupClass.DataBind();
    //    DDLNewGroupClass.Enabled = true;
    //    DDLNewGroupClass.Items.Insert(0, "-- Select Class --");

    //}
    //protected void DDLDeleteGroupDept_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    clsClass clas = new clsClass();
    //    DDLDeleteGroupClass.DataTextField = "class_name";
    //    DDLDeleteGroupClass.DataValueField = "class_id";
    //    DDLDeleteGroupClass.DataSource = clas.AllClass(Convert.ToInt32(DDLDeleteGroupDept.SelectedValue));
    //    DDLDeleteGroupClass.DataBind();
    //    DDLDeleteGroupClass.Enabled = true;
    //    DDLDeleteGroupClass.Items.Insert(0, "-- Select Class --");
    //}
  
    protected void TxtResetPassUserID_TextChanged(object sender, EventArgs e)
    {        
        Int32 a = CheckLogID(TxtResetPassUserID.Text);
        if (a != 0)
        {
            LblChkUser2.Text = "User ID Exists...";
            LblChkUser2.ForeColor = System.Drawing.Color.Green;
        }
        else
        {
            LblChkUser2.Text = "User ID " + TxtResetPassUserID.Text + " Not Available!!!";
            LblChkUser2.ForeColor = System.Drawing.Color.Red;
            TxtResetPassUserID.Text = "";
        }
    }
    protected void BtnResetPass_Click(object sender, EventArgs e)
    {        
        Int32 a = CheckLogID(TxtResetPassUserID.Text);
        if (a != 0)
        {
            LblChkUser2.Text = "User ID Exists...";
            LblChkUser2.ForeColor = System.Drawing.Color.Green;
            ResetPWD(TxtResetPassUserID.Text);
            LblChkUser2.Text = "Password Changed for " + TxtResetPassUserID.Text + "...";
            TxtResetPassUserID.Text = "";
        }
        else
        {

            LblChkUser2.Text = "User ID " + TxtResetPassUserID.Text + " Not Available!!!";
            LblChkUser2.ForeColor = System.Drawing.Color.Red;
            TxtResetPassUserID.Text = "";
        }
    }
    protected void TxtDeleteUserId_TextChanged(object sender, EventArgs e)
    {        
        Int32 a = CheckLogID(TxtDeleteUserId.Text);
        if (a != 0)
        {
            LblChkUser3.Text = "User ID Exists...";
            LblChkUser3.ForeColor = System.Drawing.Color.Green;
        }
        else
        {

            LblChkUser3.Text = "User ID " + TxtDeleteUserId.Text + " Not Available!!!";
            LblChkUser3.ForeColor = System.Drawing.Color.Red;
            TxtDeleteUserId.Text = "";
        }

    }
    protected void BtnDeleteUser_Click(object sender, EventArgs e)
    {        
        Int32 a = CheckLogID(TxtDeleteUserId.Text);
        if (a != 0)
        {
            LblChkUser3.Text = "User ID Exists...";
            LblChkUser3.ForeColor = System.Drawing.Color.Green;
            DeleteUser(TxtDeleteUserId.Text);
            LblChkUser3.Text = "User ID " + TxtDeleteUserId.Text + " Deleted...";
            TxtDeleteUserId.Text = "";
        }
        else
        {
            LblChkUser3.Text = "User ID Not Available!!!";
            LblChkUser3.ForeColor = System.Drawing.Color.Red;
        }
    }
    protected void BtnRegSession_Click(object sender, EventArgs e)
    {   
        RegSession( Convert.ToInt32(TxtNewSessionStart.Text), Convert.ToInt32(TxtNewSessionEnd.Text));
        TxtNewSessionEnd.Text = "";
        TxtNewSessionStart.Text = "";
    }
    protected void BtnDeleteSession_Click(object sender, EventArgs e)
    {        
        DelSession(Convert.ToInt32(DDLDeleteSession.SelectedValue));
        DDLDeleteSessionBind();
    }
    protected void BtnRegCourse_Click(object sender, EventArgs e)
    {        
        RegCourse(TxtNewCourseName.Text, Convert.ToInt32(TxtNOS.Text));
        TxtNewCourseName.Text = "";
        TxtNOS.Text = "";
    }
    protected void BtnDeleteCourse_Click(object sender, EventArgs e)
    {        
        DelCourse(Convert.ToInt32(DDLDeleteCourse.SelectedValue));
        DDLDeleteCourseBind();
    }
    protected void BtnRegDept_Click(object sender, EventArgs e)
    {      
        RegDept(Convert.ToInt32(DDLNewDeptCourseName.SelectedValue),TxtNewDeptName.Text);
        TxtNewDeptName.Text = "";
    }
    protected void BtnDeleteDept_Click(object sender, EventArgs e)
    {        
        DelDept(Convert.ToInt32(DDLDeleteDeptName.SelectedValue));
        DDLDeleteDeptNameBind();
    }
    private void DDLDeleteClassSessionBind()
    {        
        DataSet ds = AllSession();
        DDLDeleteClassSession.DataValueField = "Sessionid";
        DDLDeleteClassSession.DataTextField = "session";
        DDLDeleteClassSession.DataSource = ds;
        DDLDeleteClassSession.DataBind();
        DDLDeleteClassSession.Items.Insert(0, "--Select Session --");
    }
    private void DDLNewClassSessionBind()
    {        
        DataSet ds = AllSession();
        DDLNewClassSession.DataValueField = "Sessionid";
        DDLNewClassSession.DataTextField = "session";
        DDLNewClassSession.DataSource = ds;
        DDLNewClassSession.DataBind();
        DDLNewClassSession.Items.Insert(0, "--Select Session --");
    }
    protected void BtnRegClass_Click(object sender, EventArgs e)
    {
        RegClass(Convert.ToInt32(DDLNewClassDept.SelectedValue),TxtNewClassName.Text,Convert.ToInt32(DDLNewClassSession.SelectedValue));
        TxtNewClassName.Text = "";
    }
    protected void BtnDeleteClass_Click(object sender, EventArgs e)
    {        
        DelClass(Convert.ToInt32(DDLDeleteClassName.SelectedValue));
        DDLDeleteClassNameBind();
    }
    protected void BtnDeleteGroup_Click(object sender, EventArgs e)
    {        
        DelGroup(Convert.ToInt32(DDLDeleteGroupName.SelectedValue));
        DDLDeleteGroupNameBind();
    }
    protected void BtnRegGroup_Click(object sender, EventArgs e)
    {
        RegGroup(TxtNewGroup.Text,Convert.ToInt32(DDLNewGroupClass.SelectedValue));
        TxtNewGroup.Text = "";
    }

    protected void DDLDeleteClassSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDLDeleteClassNameBind();
    }

    private void DDLDeleteClassNameBind()
    {        
        DDLDeleteClassName.DataTextField = "classname";
        DDLDeleteClassName.DataValueField = "classid";
        DDLDeleteClassName.DataSource = AllClass(Convert.ToInt32(DDLDeleteClassDept.SelectedValue), Convert.ToInt32(DDLDeleteClassSession.SelectedValue));
        DDLDeleteClassName.DataBind();
        DDLDeleteClassName.Enabled = true;
        DDLDeleteClassName.Items.Insert(0, "-- Select Class --");
    }
    protected void DDLNewGroupSession_SelectedIndexChanged(object sender, EventArgs e)
    {        
        DDLNewGroupClass.DataTextField = "classname";
        DDLNewGroupClass.DataValueField = "classid";
        DDLNewGroupClass.DataSource = AllClass(Convert.ToInt32(DDLNewGroupDept.SelectedValue), Convert.ToInt32(DDLNewGroupSession.SelectedValue));
        DDLNewGroupClass.DataBind();
        DDLNewGroupClass.Enabled = true;
        DDLNewGroupClass.Items.Insert(0, "-- Select Class --");
    }

    protected void DDLDeleteGroupSession_SelectedIndexChanged(object sender, EventArgs e)
    {        
        DDLDeleteGroupClass.DataTextField = "classname";
        DDLDeleteGroupClass.DataValueField = "classid";
        DDLDeleteGroupClass.DataSource = AllClass(Convert.ToInt32(DDLDeleteGroupDept.SelectedValue), Convert.ToInt32(DDLDeleteGroupSession.SelectedValue));
        DDLDeleteGroupClass.DataBind();
        DDLDeleteGroupClass.Enabled = true;
        DDLDeleteGroupClass.Items.Insert(0, "-- Select Class --");
    }
    protected void DDLDeleteGroupClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        DDLDeleteGroupNameBind();

    }

    private void DDLDeleteGroupNameBind()
    {        
        DDLDeleteGroupName.DataTextField = "groupname";
        DDLDeleteGroupName.DataValueField = "Groupid";
        DDLDeleteGroupName.DataSource = AllGroup(Convert.ToInt32(DDLDeleteGroupClass.SelectedValue));
        DDLDeleteGroupName.DataBind();
        DDLDeleteGroupName.Enabled = true;
        DDLDeleteGroupName.Items.Insert(0, "-- Select Group --");
    }    
}