﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SA/InnerSA.master" AutoEventWireup="true" CodeFile="evnttype.aspx.cs" Inherits="SA_evnttype" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table width="100%" style="text-align: left; font-weight: bold;">
    <tr>
        <td style="width: 227px" >
            &nbsp;</td>
        <td >
            Event Type</td>
        <td>
            <asp:TextBox ID="TextBox1" runat="server" Width="263px"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                ErrorMessage="*" Font-Bold="True" ForeColor="Red" 
                ControlToValidate="TextBox1" ValidationGroup="abc"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td style="width: 227px" >
            &nbsp;</td>
        <td >
            </td>
        <td>
            <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                Text="Register" ValidationGroup="abc" />
            </td>
    </tr>
    <tr>
        
        <td colspan="3" align="center">
            <br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                CellPadding="4" DataKeyNames="Eventcatid" DataSourceID="SqlDataSource1" 
                EmptyDataText="There are no data records to display." ForeColor="#333333" 
                GridLines="None" Width="450px">
                <AlternatingRowStyle BackColor="White" />
                <Columns>                   
                    <asp:BoundField DataField="Eventcatdetail" HeaderText="Event Type" 
                        SortExpression="Eventcatdetail" />
                </Columns>
                <EditRowStyle BackColor="#2461BF" />
                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                <SortedDescendingHeaderStyle BackColor="#4870BE" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:cn %>" 
                DeleteCommand="DELETE FROM [tbeventcat] WHERE [Eventcatid] = @Eventcatid" 
                InsertCommand="INSERT INTO [tbeventcat] ([Eventcatdetail]) VALUES (@Eventcatdetail)" 
                ProviderName="<%$ ConnectionStrings:cn.ProviderName %>" 
                SelectCommand="SELECT [Eventcatid], [Eventcatdetail] FROM [tbeventcat]" 
                UpdateCommand="UPDATE [tbeventcat] SET [Eventcatdetail] = @Eventcatdetail WHERE [Eventcatid] = @Eventcatid">
                <DeleteParameters>
                    <asp:Parameter Name="Eventcatid" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Eventcatdetail" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Eventcatdetail" Type="String" />
                    <asp:Parameter Name="Eventcatid" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <br />
        </td>
    </tr>
</table>
</asp:Content>

