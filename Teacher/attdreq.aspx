﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Teacher/InnerTeacher.master" AutoEventWireup="true" CodeFile="attdreq.aspx.cs" Inherits="Teacher_attdreq" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
 <table width="100%" style="text-align: left; font-weight: bold;">
            <tr>
                <td style="width: 173px">
                    <strong>Roll no: </strong>
                </td>
                <td style="margin-left: 40px">
                    <asp:TextBox ID="TextBox1" runat="server" ontextchanged="TextBox1_TextChanged" 
                        Width="300px"></asp:TextBox><asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToValidate="TextBox1" Operator="DataTypeCheck" Display="Dynamic" ErrorMessage="*" 
                        Font-Bold="True" ForeColor="Red" Type="Integer">*</asp:CompareValidator>
                    &nbsp;<asp:Label ID="Button2" runat="server" 
                        Text="Press Enter to Continue . . ." />
                </td>
            </tr>
            </table>
        <asp:Panel ID="Panel1" runat="server" Visible="False">
       
          <table width="100%" style="text-align: left; font-weight: bold;">
            <tr>
                <td>
                    <strong>Subject</strong></td>
                <td style="margin-left: 40px">
                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="DropDownList1_SelectedIndexChanged" Width="300px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="font-weight: 700">
                    Date</td>
                <td style="margin-left: 40px">
                    <asp:DropDownList ID="DropDownList2" runat="server" 
                        DataTextFormatString="{0:d}" Width="300px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="font-weight: 700">
                    Reason</td>
                <td style="margin-left: 40px">
                    <asp:TextBox ID="TextBox2" runat="server" Height="100px" TextMode="MultiLine" 
                        Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="font-weight: 700">
                    Attendance</td>
                <td align="left" style="margin-left: 40px">
                    &nbsp;<asp:RadioButtonList ID="RadioButtonList1" runat="server" 
                        RepeatDirection="Horizontal" style="font-weight: 700">
                        <asp:ListItem Selected="True" Value="True">Present</asp:ListItem>
                        <asp:ListItem Value="False">Absent</asp:ListItem>
                    </asp:RadioButtonList>
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
                <td style="margin-left: 40px">
                    <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                        style="font-weight: 700" Text="Submit Request" />
                </td>
            </tr>
        </table>
    
        </asp:Panel>
       
</asp:Content>

