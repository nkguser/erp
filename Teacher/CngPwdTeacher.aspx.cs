﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using kitmerp;

public partial class Teacher_CngPwdTeacher : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {

        if (con.State == ConnectionState.Closed)
            con.Open();
    }
    protected void LBChangePwd_Click(object sender, EventArgs e)
    {
        ERP user = new ERP();
        string Oldpwd, NewPwd;
        NewPwd = TXTNewPwd.Text;
        Oldpwd = TXTOldPwd.Text;
        SqlCommand cmd = new SqlCommand("SELECT  teacheruserid FROM tbteacher WHERE (teacherid = @stid)", con);
        cmd.Parameters.Add("@stid", SqlDbType.Int).Value = Convert.ToInt32(Session["stid"]);
        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();
        Int32 a = Convert.ToInt32(dr["teacheruserid"]);
        dr.Close();
        Int32 k = user.ChangePWD(a, Oldpwd, NewPwd);
        if (k == 1)
        {
            LblMessage.Text = "Password Changed Successfully";
        }
        else if (k == 2)
        {
            LblMessage.Text = "Password Not Changed. Please Re-Enter Your Current Password Again";
        }
        else
            LblMessage.Text = "Sorry for Inconvienance...An Internal Errod has Occured";


    }
}